# LEEME #

**Clipmaestro** no es un producto, sino un servicio que ha sido diseñado de manera escalable para satisfacer las necesidades de los negocios, tanto emergentes como establecidos.

Es un sistema ERP que permite administrar y utilizar de forma sencilla y eficiente la información de su empresa en tiempo real mediante una conexión segura en internet.

Clipmaestro es una herramienta fundamental para incrementar la eficiencia y productividad de cualquier compañía.

* Versión 0.1
* [Aprenda Mas](http://clipmaestro.com/)

### How do I get set up? ###

* Clipmaestro esta sobre un servidor linux ubuntu 14.04 LTS y corre en un servidor apache2.

* Configuración:
  configuracion básica apache:
  
```
#!xml

<virtualhost *:80>
        ServerName demo.clipmaestro.com
        ServerAlias www.demo.clipmaestro.com
        serverAdmin ivanfer5_25@hotmail.com

        ServerSignature On

        ErrorLog /var/log/apache2/clipmaestro_demo-error.log
        LogLevel warn
        CustomLog /var/log/apache2/clipmaestro_demo.com-access.log combined

        DocumentRoot /var/www/clientes/clipmaestro_demo/clipmaestro
        <Directory /var/www/clientes/clipmaestro_demo/clipmaestro>
                Order allow,deny
                Allow from all
        </Directory>
        WSGIDaemonProcess clipmaestro_demo.djangoserver processes=2 processes=2 threads=15 display-name=%{GROUP}
        WSGIProcessGroup clipmaestro_demo.djangoserver
        WSGIScriptAlias / /var/www/clientes/clipmaestro_demo/clipmaestro/apache.conf/web.wsgi

        <Directory /var/www/clientes/clipmaestro_demo/clipmaestro/apache.conf>
                <Files web.wsgi.py>
                        Order deny,allow
                        Require all granted
                </Files>
        </Directory>
        <Directory /var/www/clientes/clipmaestro_demo/clipmaestro/clipmaestro/media>
                Order deny,allow
                Allow from all
        </Directory>

        Alias /static/ "/var/www/clientes/clipmaestro_demo/clipmaestro/clipmaestro/static/"
        <Directory "/usr/local/lib/python2.7/dist-packages/django/contrib/admin/static/admin">
             Order allow,deny
             Options Indexes
             Allow from all
             IndexOptions FancyIndexing
        </Directory>
</virtualhost>
WSGIPythonPath /var/www/clientes/clipmaestro_demo/clipmaestro

```

## Dependencias ##
pil-1.1.7, pillow2.6, django1.5.5, django_widget_tweaks-1.3, excel_response, html5lib-0.999, pisa-3.0.33, psycopg2, pytz, reportlab, requests, six, sx, urllib3, xlrd, xlwt, barpy-0.1.2


## Base de datos ## 
Postgresql 9.3

## Recursos ##
[pil](http://www.pythonware.com/products/pil/)

[pillow-2.6](https://pypi.python.org/pypi/Pillow/2.6.1)

[django_widget_tweaks-1.3](https://pypi.python.org/pypi/django-widget-
tweaks)

[excel_response](https://pypi.python.org/pypi/django-excel-response)



### Contacto: ###

* ivanfer5_25@hotmail.com
* ifarez@nrmaster.com
* info@clipmaestro.com