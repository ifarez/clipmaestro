#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt, csrf_protect
import json
from contabilidad.funciones.compras_func import *
from django.forms.formsets import formset_factory
from django.contrib.auth.decorators import login_required
from contabilidad.formularios.RetencionesCuentaForm import *
from contabilidad.formularios.ComprasForm import *

from django.db import transaction
from django.db import connection
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context

from django.core.paginator import *
from django import template
from administracion.models import *
from librerias.funciones.permisos import *
from librerias.funciones.paginacion import *


def get_cliente_direccion(request):
    respuesta = []
    q = request.GET.get("cliente_id", "")
    try:
        direcciones = Cliente_Direccion.objects.filter(cliente_id=q)
        for obj in direcciones:
            respuesta.append({"id": obj.id, "value": obj.descripcion + obj.direccion1})
    except:
        respuesta.append({"id": "", "value": "---------"})

    resultado = json.dumps(respuesta)

    return HttpResponse(resultado, mimetype='application/json')