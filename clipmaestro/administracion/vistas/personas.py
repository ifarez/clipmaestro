#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.template import RequestContext
from librerias.funciones.permisos import *
from django.shortcuts import render_to_response
from django.http import *
from datetime import datetime
import datetime
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import *
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from administracion.models import *
from administracion.formularios.personaForm import *
from django.core.paginator import *
import time
import json
from django.db.models import Q
from django.core.files import File
from django.contrib.auth import login, authenticate, logout
from librerias.funciones.validacion_rucs import *
from clipmaestro.settings import *
from django.db import IntegrityError, transaction
import operator
from librerias.funciones.paginacion import *

USUARIOS = 'usuarios'   # Carpeta donde se guarda la imagen de los usuarios

def save_file(file, path=''):
    ''' Función que guarda la ruta de archivos, utilizada para guardar las imágenes
    '''
    dir = str('%s/%s' % (MEDIA_ROOT, USUARIOS))
    if not os.path.isdir(dir):
        os.mkdir(dir)
        os.chmod(dir, 01777)
    filename = file._get_name()
    fd = open('%s/%s/%s' % (MEDIA_ROOT, USUARIOS, str(path) + str(filename)), 'wb')
    for chunk in file.chunks():
        fd.write(chunk)
    fd.close()

def busqueda_personas(buscador, request):
    """
    Función que ejecuta el query de búsqueda
    :param buscador:
    :return:
    """
    predicates = []
    predicates.append(('cedula__icontains', buscador.get_cedula().replace(" ", "")))
    predicates.append(('nombres_completos__icontains', buscador.get_nombre()))
    predicates.append(('username__icontains', buscador.get_usuario()))

    # reate the list of Q objects and run the queries as above..
    q_list = [Q(x) for x in predicates]
    entries = Persona.objects.filter(reduce(operator.and_, q_list)).exclude(id=request.user.id).exclude(estado=0).order_by("nombres_completos")
    return entries

@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso, es_admin=True)
def lista_persona(request):
    '''
    Vista que lista todos los registros de personas
    (usuarios del sistema)
    :param request:
    :return:
    '''
    buscador = FormBuscadorPersona(request.GET)
    # Función que en base a la búsqueda llena una lista
    personas = busqueda_personas(buscador, request)
    paginacion = Paginator(personas, get_num_filas_template())
    numero_pagina = request.GET.get("page")

    try:
        personas = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        personas = paginacion.page(1)
    except EmptyPage:
        personas = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = personas .number

    return render_to_response("persona/personas.html", {"objetos": personas, "total_paginas": total_paginas,
                                "numero": numero, "buscador": buscador,
                                "mostrar_paginacion": muestra_paginacion_template(paginacion.count)},
                                context_instance=RequestContext(request))

@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso, es_admin=True)
def detalle_persona(request, id):
    '''
    Vista que obtiene y muestra la información de un usuario seleccionado
    :param request:
    :param id:
    :return:
    '''
    tipo = 3
    try:
        persona = Persona.objects.get(id=id)
    except Persona.DoesNotExist:
        raise Http404

    return render_to_response('persona/detalle_persona.html',
                              {"objetos": persona, "tipo": tipo,
                               "id": id}, context_instance=RequestContext(request))


####################################### Funciones de la Vista ##########################################################
def EditarPersona(formulario, nuevo, now, request):
    """
    Function EditarPersona(formulario, persona, now , request)
    @ Return contador validador
    Details: Función para editar persona
    """
    contador_editar = 0
    formulario.is_valid()
    nuevo.nombres_completos = formulario.getNombres()
    nuevo.direccion = formulario.getDireccion()
    nuevo.telefono = formulario.getTelefono()
    nuevo.fecha_nacimiento = formulario.getFecha_Nac()
    nuevo.sexo = formulario.getSexo()
    nuevo.estado_civil = formulario.getEstado_Civil()
    nuevo.facebook = formulario.getFacebook()
    nuevo.twitter = formulario.getTwitter()
    nuevo.cedula = formulario.getCedula()
    nuevo.apellidos_completos = formulario.getApellidos()
    nuevo.first_name = formulario.getPrimerNombre()
    nuevo.segundo_nombre = formulario.getSegundoNombre()
    nuevo.last_name = formulario.getPrimerAp()
    nuevo.segundo_apellido = formulario.getSegundoAp()
    nuevo.email = formulario.getCorreo()
    nuevo.sexo = formulario.getSexo()

    try:
        nuevo.fotografia = "usuarios/"+request.FILES['foto'].name
        save_file(request.FILES['foto'])
    except:
        pass

    ################################################ VALIDACIONES ######################################################
    bandera = Persona.objects.exclude(estado=0).filter(Q(cedula=nuevo.cedula)).filter(~Q(user_ptr_id=nuevo.id)).exists()

    if bandera:
        errors = formulario._errors.setdefault("cedula", ErrorList())
        errors.append(u"El Nº de Identificación ya ha sido ingresado.")

    else:

        if not isCedula(nuevo.cedula):
            contador_editar += 1
            errors = formulario._errors.setdefault("cedula", ErrorList())
            errors.append(u"")
            messages.error(request, u"El N° de Identificación es incorrecto.")

    if User.objects.filter(Q(username=nuevo.username), ~Q(id=nuevo.id)).exists():
        contador_editar += 1
        errors = formulario._errors.setdefault("usuario", ErrorList())
        errors.append(u"")
        messages.error(request, u"Error: El usuario ya ha sido ingresado.")

    if Persona.objects.filter(Q(email=nuevo.email), Q(estado=1) | Q(estado=2), ~Q(id=nuevo.id)).exists():
        contador_editar += 1
        errors = formulario._errors.setdefault("email", ErrorList())
        errors.append(u"")
        messages.error(request, u"El Email ya ha sido ingresado.")

    if contador_editar == 0:
        nuevo.fecha_actualizacion = now
        nuevo.usuario_actualizacion = request.user.username
        nuevo.save()

    return contador_editar


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso, es_admin=True)
def agregar_persona(request):
    '''
    Vista que agrega un nuevo usuario al sistema
    :param request:
    :return:
    '''
    now = datetime.datetime.now()
    formulario = FormPersona()
    tipo = 1
    grupos = Grupo.objects.filter(status=1)
    try:
        if request.method == "POST":
            formulario = FormPersona(request.POST, request.FILES)
            persona = Persona()
            if formulario.is_valid():
                empresa = Empresa.objects.all()[0]
                persona.cedula = formulario.getCedula()
                persona.direccion = formulario.getDireccion()
                fecha_nacimiento = formulario.getFecha_Nac()
                persona.fecha_nacimiento = fecha_nacimiento
                persona.nombres_completos = formulario.getNombres()+" "+formulario.getApellidos()
                persona.apellidos_completos = formulario.getApellidos()
                persona.first_name = formulario.getPrimerNombre()
                persona.segundo_nombre = formulario.getSegundoNombre()
                persona.last_name = formulario.getPrimerAp()
                persona.segundo_apellido = formulario.getSegundoAp()
                persona.email = formulario.getCorreo()
                persona.sexo = formulario.getSexo()
                persona.direccion = formulario.getDireccion()
                persona.estado_civil = formulario.getEstado_Civil()
                persona.facebook = formulario.getFacebook()
                persona.twitter = formulario.getTwitter()
                persona.telefono = formulario.getTelefono()
                persona.username = formulario.getUsuario()
                persona.empresa = empresa

                try:
                    persona.fotografia = "usuarios/"+request.FILES['foto'].name
                    save_file(request.FILES['foto'])
                except:
                    pass

                persona.is_superuser = False
                persona.set_password(formulario.getPassword())
                persona.usuario_creacion = request.user.username
                persona.fecha_creacion = now
                persona.save()

                for obj in grupos:
                    id_grupo = request.POST.get("grupos-" + str(obj.id), None)
                    try:
                        if id_grupo is not None:
                            grupo = Grupo.objects.get(id=id_grupo)
                            persona.grupo.add(grupo)
                    except:
                        pass
                messages.success(request, u"El Usuario " + str(persona.username) +
                                          u"  se  registró exitosamente.")
                return HttpResponseRedirect(reverse("lista_persona"))

            else:
                lista_errores = "Por favor verifique los siguientes campos: "
                for i in formulario.errors:
                    lista_errores = lista_errores +(unicode(i)) + ", "
                if formulario.errors:
                    messages.error(request, unicode(lista_errores[0:-2]))
                for i in formulario:
                    if i.errors:
                        i.field.widget.attrs['class'] = 'selectpicker campo_requerido'
    except:
        messages.error(request, u"Existe un error al agregar el usuario,  comuniquese con el administrador")
        transaction.rollback()
    return render_to_response("persona/agregar_persona.html", {"formulario": formulario,
                                                               "tipo": tipo,
                                                               'grupos': grupos,
                                                               "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def cambiar_contrasenia(request):
    '''
    Vista que permite realizar el cambio de contraseña al usuario que elija está opción
    pero deberá confirmar la contraseña anterior para que proceda a realizar está funcionalidad del
    sistema
    :param request:
    :return:
    '''
    formulario = FormEditUser()
    try:
        persona = Persona.objects.get(id=request.user.persona.id)
        cont = 0
        if request.method == "POST":
            formulario = FormEditUser(request.POST)
            if formulario.is_valid():
                clave_nueva = request.POST['clave_actual']
                flag = authenticate(username=request.user.username, password=clave_nueva)
                if flag is None:
                    cont += 1
                    errors = formulario._errors.setdefault("clave_actual", ErrorList())
                    errors.append(u"")
                    messages.error(request, u"Error la contraseña actual es incorrecta")

                if formulario.getPassword1() != formulario.getPassword2():
                    cont += 1
                    errors = formulario._errors.setdefault("password1", ErrorList())
                    errors.append(u"")
                    errors = formulario._errors.setdefault("password2", ErrorList())
                    errors.append(u"")
                    messages.error(request, u"Error las contraseñas no coinciden")
                else:
                    if len(formulario.getPassword1()) < 5 or len(formulario.getPassword2()) < 5:
                        cont += 1
                        errors = formulario._errors.setdefault("password1", ErrorList())
                        errors.append(u"")
                        errors = formulario._errors.setdefault("password2", ErrorList())
                        errors.append(u"")
                        messages.error(request, u"Error debe ingresar una contraseña con un mínimo de 5 "
                                                u"caracteres o números de longitud")

                if cont == 0:
                    persona.set_password(formulario.getPassword1())
                    persona.username = request.user.username
                    persona.fecha_actualizacion = datetime.datetime.now()
                    persona.save()
                    messages.success(request, u'Se realizó el cambio de contraseña exitosamente al usuario : "' +
                                     str(request.user.username) + '"')
                    return HttpResponseRedirect(reverse("administracion"))

            else:

                messages.error(request, u'Por favor debe llene los datos del formulario correctamente.')
                for i in formulario:
                    if i.errors:
                        i.field.widget.attrs['class'] = 'selectpicker campo_requerido'
    except:
        messages.error(request, u"Error al cambiar la contraseña comuniquese con el administrador")
        transaction.rollback()
        return HttpResponseRedirect(reverse("administracion"))

    return render_to_response("persona/cambiar_contrasenia.html", {"formulario": formulario,
                                                               "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso, es_admin=True)
def modificar_persona(request, id):
    '''
    Vista que permite modificar la información de los usuarios del sistema
    :param request:
    :param id:
    :return:
    '''
    today = datetime.datetime.now()
    tipo = 2
    grupos = Grupo.objects.filter(status=1)

    try:
        persona = Persona.objects.get(id=id)
        formulario = FormPersonaEditar(initial={"cedula": persona.cedula, "nombres": persona.nombres_completos,
                                                "direccion": persona.direccion, "sexo": persona.sexo,
                                                "estado_civil": persona.estado_civil, "facebook": persona.facebook,
                                                "twitter": persona.twitter, "telefono": persona.telefono,
                                                "apellidos": persona.apellidos_completos,
                                                "fecha_nac": persona.fecha_nacimiento, "foto": persona.fotografia,
                                                "email": persona.email, "rol": "" })
        if request.method == "POST":
            formulario = FormPersonaEditar(request.POST, request.FILES)
            if formulario.is_valid():
                contador = EditarPersona(formulario, persona, today, request)
                if contador == 0:
                    for obj in grupos:
                        persona.grupo.remove(obj)
                        if request.POST.get("grupos-" + str(obj.id), "") != "":
                            persona.grupo.add(obj)
                    messages.success(request, u"El usuario "+str(persona.username)+
                                              u" ha sido editado exitosamente.")
                    return HttpResponseRedirect(reverse("lista_persona"))
                else:
                    transaction.rollback()
            else:
                lista_errores = u"Por favor verifique los siguientes campos: "
                for i in formulario.errors:
                    lista_errores = lista_errores +(unicode(i)) + ", "
                    if formulario.errors:
                        messages.error(request, unicode(lista_errores[0:-2]))
                    for i in formulario:
                        if i.errors:
                            i.field.widget.attrs['class'] = 'selectpicker campo_requerido'

        return render_to_response("persona/agregar_persona.html",
                                  {"formulario": formulario,
                                   "obj": persona,
                                   "id": id,
                                   "grupos": grupos,
                                   "tipo": tipo,
                                   "request": request}, context_instance=RequestContext(request))
    except Persona.DoesNotExist:
        messages.error(request, u"El usuario no se encuentra registrado")
        return HttpResponseRedirect(reverse("lista_persona"))

@csrf_exempt
@login_required(login_url='/')
def perfil(request, id):
    '''
    Vista que llama a cargar la información del Usuario
    y realiza la funcionalidad de ver el perfil
    :param request:
    :param id:
    :return:
    '''
    today = datetime.datetime.now()
    tipo = 4        # Perfil

    try:
        persona = Persona.objects.get(id=id)
        formulario = FormPersonaEditar(initial={"cedula": persona.cedula, "nombres": persona.nombres_completos,
                                                "direccion": persona.direccion, "sexo": persona.sexo,
                                                "estado_civil": persona.estado_civil, "facebook": persona.facebook,
                                                "twitter": persona.twitter, "telefono": persona.telefono,
                                                "apellidos": persona.apellidos_completos,
                                                "fecha_nac": persona.fecha_nacimiento, "foto": persona.fotografia,
                                                "email": persona.email})
        if request.method == "POST":
            formulario = FormPersonaEditar(request.POST, request.FILES)
            if formulario.is_valid():
                contador = EditarPersona(formulario, persona, today, request)
                if contador == 0:
                    messages.success(request, u"El perfil del  usuario "+str(persona.username)+
                                              u" ha sido editado exitosamente.")
                    return HttpResponseRedirect(reverse("administracion"))
                else:
                    transaction.rollback()
            else:
                lista_errores = u"Por favor verifique los siguientes campos: "
                for i in formulario.errors:
                    lista_errores = lista_errores +(unicode(i)) + ", "
                    if formulario.errors:
                        messages.error(request, unicode(lista_errores[0:-2]))
                    for i in formulario:
                        if i.errors:
                            i.field.widget.attrs['class'] = 'selectpicker campo_requerido'
        return render_to_response("persona/detalle_persona.html",
                                  {"formulario": formulario,
                                   "objetos": persona,
                                   "id": id,
                                   "tipo": tipo,
                                   "request": request}, context_instance=RequestContext(request))

    except Persona.DoesNotExist:
        messages.error(request, u"Existe un problema al realizar la acción de ver el perfil "
                                u"por favor comuníquese con el administrador")
        return HttpResponseRedirect(reverse("lista_persona"))

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso, es_admin=True)
def cambiar_estado_usuarios(request, id):
    '''
    Vista que me permite realizar el cambio de estado de los usuarios
    Ej:
        estado = 0 (Eliminado)
        estado = 1 (Activo)
        estado = 2 (Inactivo)
    :param request:
    :param id:
    :return:
    '''
    try:
        persona_select = Persona.objects.get(id=id)
        now_ac = datetime.datetime.now()

        if persona_select.estado == 1:
            persona_select.estado = 2
            persona_select.is_active = False
            persona_select.usuario_actualizacion = request.user.username
            persona_select.fecha_actualizacion = now_ac
            messages.success(request, u'Se inactivó al usuario : "'+str(persona_select.username)+u'" exitosamente')

        elif persona_select.estado == 2:
            persona_select.estado = 1
            persona_select.is_active = True
            persona_select.usuario_actualizacion = request.user.username
            persona_select.fecha_actualizacion = now_ac
            messages.success(request, u'Se activó al usuario : "'+str(persona_select.username)+u'" exitosamente')
        persona_select.save()

    except (Persona.DoesNotExist, ValueError):
        messages.error(request, u"Error al cambiar de estado.")
    return HttpResponseRedirect(reverse("lista_persona"))

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso, es_admin=True)
def eliminar_persona(request, id):
    '''
    Cambia de Estado al usuario, es decir:
    estado = 0, is_active= False
    :param request:
    :param id:
    :return:
    '''
    try:
        persona = Persona.objects.get(id=id)
        now_ac = datetime.datetime.now()

        if persona.estado != 0:
            persona.estado = 0
            persona.is_active = False
            persona.usuario_actualizacion = request.user.username
            persona.fecha_actualizacion = now_ac
        persona.save()
        messages.success(request, u"El usuario"+str(persona.username)+u" ha sido eliminado exitosamente")
        return HttpResponseRedirect(reverse("lista_persona"))
    except:
          messages.error(request, u"Error al eliminar, El Usuario no se encuentra registrado.")

    return HttpResponseRedirect(reverse("lista_persona"))