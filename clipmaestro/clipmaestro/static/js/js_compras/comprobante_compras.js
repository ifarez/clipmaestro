//Variables globales
var border, outline;
var FECHA_ASIENTO = "#id_fecha_asiento";
var FECHA_EMI = "#id_fecha";
var FECHA_RET = "#id_fecha_emision_retencion";
var SERIE_RET = "#id_serie_ret";
var AUT_RET = "#id_autorizacion_retencion";
var T_REEMBOLSO = "#total_reemb_b";
var TR_BASE0 = "#trbase0";
var TR_BASE12 = "#trbase12";
var S_FORMA_PAGO = "#id_forma_pago";
var S_PAIS = "#id_pais";
var NUM_DOC = "#id_serie";
var NUM_DOC_MODIF = "#id_serie_modif";
var NUM_RET = "#id_numero_retencion";
var S_SUST_TRIB = "#id_sust_tribut";
var S_TIPO_DOC = "#id_tipo_de_doc";
var T_DETALLE_COMP = "#tabla_formset";
var T_DETALLE_REEM = "#tabla_formset_reembolso";
var S_COD_RET_FTE = "select[data-name='combo_cod_ret_fte']";
var S_COD_RET_IVA = "select[data-name='combo_cod_ret_iva']";
var S_COMBO_CUENTA = "select[data-name='combo_cuenta']";


$(function()
{
    var fecha_ret = $("#id_fecha_emision_retencion");
    var formulario = $("#formulario");
    $(".selectpicker").selectpicker("refresh");

    $("#grabar_pagar").click(function(){
        var action = formulario.attr("action");
        formulario.attr("action", action+"?pagar=1");
        $("#grabar").click();
    });

    $("#grabar_agregar").click(function(){
        var action = formulario.attr("action");
        formulario.attr("action", action+"?grabar_agregar=1");
        $("#grabar").click();
    });

    $("#grabar_imprimir").click(function(){
        var action = formulario.attr("action");
        formulario.attr("action", action+"?grabar_imprimir=1");
        $("#grabar").click();
    });

    $( "#id_vence" ).val($( "#id_vence").attr("value"));

    fecha_ret.val(fecha_ret.attr("value"));

    //  Agregar los titulos y la fecha
    $(T_DETALLE_REEM).find("select").each(function(){ AgregarTitulo(this) });

    // Funciónes para que se presente un tooltip encima del combo
    $(T_DETALLE_COMP).find(S_COD_RET_FTE).find("option").each(function()
    {
        $(this).attr("title", $(this).attr("data-codigo"));
        $($(this).parent().get(0)).selectpicker("refresh");
    });
    $(T_DETALLE_COMP).find("select[data-name='combo_iva']").find("option").each(function()
    {
        $(this).attr("title",$(this).attr("data-porcentaje")+"%");
        $($(this).parent().get(0)).selectpicker("refresh");
    });
    $(T_DETALLE_COMP).find(S_COD_RET_IVA).find("option").each(function()
    {
        $(this).attr("title",$(this).attr("data-codigo"));
        $($(this).parent()).selectpicker("refresh");
    });
    $(T_DETALLE_REEM).find("select[data-name='combo_tipo_id']").find("option").each(function()
    {
        $(this).attr("title",$(this).text().split("-")[0]);
    });

    $(T_DETALLE_COMP).find("select").each(function(){ AgregarTitulo(this) });

});


$(document).ready(function(e)
{
    recalcular_ids(T_DETALLE_REEM);
    recalcular_ids(T_DETALLE_COMP);

    //Agrega las mascaras correspondientes
    $('.cod_cta').mask('9.9.99.99.999');
    $(NUM_DOC).mask('999-999-999999999');
    $(NUM_DOC_MODIF).mask('999-999-999999999');
    $(NUM_RET).mask('999999999');
    $(T_DETALLE_REEM).find(".serie").each(function(){
         $(this).mask('999-999-999999999')
    });

    // Carga la funcion de bloquear a la retención del iva dependiendo si es B/S ("30, 70, 100")
    cambio_ret_iva($('select[data-name="bien_serv"]'));

    // Se añade la función "cambio_ret_iva()" cuando cambia de B a S o visceversa
    $('select[data-name="bien_serv"]').change(function(){cambio_ret_iva(this);});

    // Calcula los totales al inicio de la transacción
    CalcularTotales();

    // deshabilita plazo si la transaccion era contado al inicio
    if($("#id_plazo").attr("value")==undefined)
    {
        if($(S_FORMA_PAGO).val()==1)
            $("#id_plazo").attr("disabled",true);
    }

    // Carga la autorización del documento dependiendo de la serie ingresada
    $(SERIE_RET).change(function(){
        CargarAutorizacion();
    });

    // Para que se presente div pais que ingresó si la compra es del exterior
    PagoExterior($("#id_pago"));

    // Para que presente el div doc_modi al inicio si tiene NC/ND
    CambioDocumento($(S_TIPO_DOC));

    $("#id_fecha").change(function(){validar_fecha_em(this)});

    $( "#id_vence" ).change(function(){
        validar_fecha_em(this);
    });

    $(".valor").focusout(function(){CalcularTotales()});
    $(".iva").focusout(function(){CalcularTotales()});
});

