var resultado_total_haber=0.0;
var resultado_total_debe=0.0;
resultado_haber=0;
resultado_debe=0;
var ID_TABLA_CONTABILIDAD = "#tabla_formset";

function recalculardebehaber(){
    var total_debe = 0.0;
    var total_haber = 0.0;
    $("#tabla_formset tbody tr").each(function()
    {
       $(this).find(".debe").each(function(){
            if ($(this).val() != ""){
                 var debe = parseFloat($(this).val());
                 total_debe = total_debe + debe;
            }
       });

       $(this).find(".haber").each(function(){
           if ($(this).val()!=""){
               var haber = parseFloat($(this).val());
               total_haber = total_haber + haber;
           }
        });
    });

    $("#total_debe").html(parseFloat(total_debe).toFixed(2));
    $("#total_haber").html(parseFloat(total_haber).toFixed(2));

    resultado_total_debe=parseFloat(total_debe).toFixed(2);
    resultado_total_haber=parseFloat(total_haber).toFixed(2);

}

// Corregir Validar
function validar_ceros(){
    $("#tabla_formset tbody tr").each(function()
    {
        var debe = $(this).find(".debe");
        var haber = $(this).find(".haber");

        if ($(debe).val() == ""  ){
            $(debe).val(0.0);
        }

        if ($(haber).val() == ""){
            $(haber).val(0.0);
        }

    });
}
function validarEliminar(num2){
    var num = parseInt(num2);
    if (num == 1){
        $("#alert-yn").attr("data-id", $(this).attr("id"));
        $("#alert-yn").attr("data-target",1);
        $("#tit-alert").html("Alerta");
        $("#body-alert").html('<p>No se puede eliminar la fila.</p>');
        $("#alert-yn").modal();
    }
}
function agregar_punto(e){
    var numero = $(e).val();
    var contador = 0.0;

    if(numero!=""){
        if (!isNaN(numero))
            $(e).val(parseFloat(numero).toFixed(2));
        else
            $(e).val((0).toFixed(2));
    }
    else
        $(e).val((0).toFixed(2));

}
function alerts_modal(submit, event){
    var input_fecha = $("#id_fecha");
    var concepto = $("#id_concepto");
    var flag_debe_haber = false;
    var flag_detalle = false;
    var flag_valores = false;
    var flag_cuenta = false;

    if ($(input_fecha).val()=="" || $(concepto).val()==""){
         $(concepto).addClass("campo_requerido");
         $(concepto).focus();
    }
    else{
         $(concepto).removeClass("campo_requerido");
    }


    if ((resultado_total_debe != resultado_total_haber)){
       flag_debe_haber = true;
    }

    $("#tabla_formset tbody tr").each(function(index)
    {
        var codigo = $(this).find(".codigo_cta");
        var debe_detalle = $(this).find(".debe");
        var haber_detalle = $(this).find(".haber");

        if ($(debe_detalle).val()=="" && $(haber_detalle).val()==""){
            flag_detalle = true;
        }

        if ($(codigo).val()==""){
            flag_cuenta = true;
        }

        if ($(debe_detalle).val() == ""&& $(haber_detalle).val() == ""){
            flag_valores = true;
        }

    });

    if(flag_debe_haber == true || flag_cuenta == true || flag_detalle == true){
        $("#alert-yn").attr("data-id", $(this).attr("id"));
        $("#alert-yn").attr("data-target",1);
        $("#tit-alert").html("Alerta ");

        $("#body-alert").html('<p><i class="glyphicon glyphicon-remove tick"></i><strong class="tick-text">Por favor corrija lo siguiente:</strong></p>');

        if(flag_cuenta){
            $("#body-alert").append('<p><i class="glyphicon glyphicon-remove tick"></i><strong class="tick-text">Es requerido que ingrese la cuenta.</strong></p>');
        }

        if(flag_debe_haber){
            $("#body-alert").append('<p><i class="glyphicon glyphicon-remove tick"></i><strong class="tick-text">Los valores del Debe y el Haber, ya que los mismos deben ser iguales.</strong></p>');
        }

        if(flag_detalle){
            $("#body-alert").append('<p><i class="glyphicon glyphicon-remove tick"></i><strong class="tick-text">Los valores del Detalle del Asiento Contable.</strong></p>');

        }

        if(flag_valores){
            $("#body-alert").append('<p><i class="glyphicon glyphicon-remove tick"></i><strong class="tick-text">Es requerido que ingrese valores en el debe o en el haber.</strong></p>');
        }

        $("#alert-yn").modal();
        event.preventDefault();
    }

}

/**************************
 * Encera la columna del debe o del haber dependiendo de cual
 * de las dos haya elegido el usuario
 * @param obj
 */
function desactivar_debe_haber(obj){
    var tr = $(obj).parent().parent();
    var debe = $(tr).find(".debe");
    var haber = $(tr).find(".haber");
    if($(obj).hasClass("debe")){
        if(parseFloat($(obj).val()) > 0){
            haber.val("0.00");
        }
        else{
            haber.val("0.00");
            debe.val("0.00")
        }
    }
    else{
        if(parseFloat($(obj).val()) > 0){
            debe.val("0.00");
        }
        else{
            haber.val("0.00");
            debe.val("0.00")
        }
    }
}

function addConceptos(){

    var concepto_general = $("#id_concepto");
    var concepto_filas = $(".concepto");
    concepto_filas.each(function(){
        if ($(this).attr("data-modificado") == 0 || $(this).attr("data-modificado") == undefined)
        {
            $(this).val($(concepto_general).val());
        }
    });
    //$(concepto_filas).val(($(concepto_general).val()));
}

$(function(){
    var formulario = $("#formulario");

    $("#grabar_agregar").click(function(){
        var action = formulario.attr("action");
        formulario.attr("action", action+"?grabar_agregar=1");
        $("#grabar").click();
    });
    $("#grabar_imprimir").click(function(){
        var action = formulario.attr("action");
        formulario.attr("action", action+"?grabar_imprimir=1");
        $("#grabar").click();
    });
});

/***
 * Función que con la fecha actualiza
 * la secuencia de último asiento contable
 */
function get_fecha_ultimo_secuencia(url){
    var ultimo_secuencia = $("#id_utlimo_asiento");
    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'fecha': $("#id_fecha").val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data){

            if(data[0].status == 0){
                $(ultimo_secuencia).val(data[0].secuencia);
            }

            if(data[0].status == 1){
                $(ultimo_secuencia).val(data[0].secuencia);
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }

    });

}

$(document).ready(function(){
    $('.selectpicker').selectpicker();
    $(".eliminar").click(function(e){
        e.preventDefault();
        if($("#tabla_formset tbody").children().length>2){
            $(this).parent().parent().remove();
            recalcular_ids(ID_TABLA_CONTABILIDAD);
            recalculardebehaber();
        }
        else{
            validarEliminar(1);
        }
    });

     $("#id_concepto").change(function(){
        addConceptos();
     });

    /*********************************************
    * Inhabilitar el "enter" en los inputs para no enviar
    * el formulario con un enter
    */
    $('form :input').keypress(function(e){
        if ( e.which == 13 ) e.preventDefault();
    });

    /*************************************
    * Funcion para recalcular los ids
    **************************************/
    function recalcular_ids(tableid){
        var nombre_form = ""
        $(tableid+" tbody tr").each(function(index)
        {
            var cont = index;
            $(this).find(":input:not(:button, .input-block-level)").each(function()
            {
                var name_campo = $(this).attr("name").split("-");
                nombre_form = name_campo[0];

                var nombre_campo = name_campo[0]+"-" + cont + "-"+name_campo[2];
                $(this).attr("name", nombre_campo);
                $(this).attr("id", "id_"+nombre_campo);
            });
        });
        $('#id_'+nombre_form+'-TOTAL_FORMS').val($(tableid+" tbody").children().length);
    }

    $(".guardar").click(function(e){
        alerts_modal(this,e);
    });

    /******************************** Habilitar - Deshabilitar HABER ******************************/
    $(".debe").change(function(){
        desactivar_debe_haber(this);

        if ($(this).val() == ""){
           // $("#total_debe").html(0.0);
            resultado_total_debe = 0.0;
        }
        else{

           $("#total_debe").html($(this).val());
           resultado_total_debe =  $(this).val();
        }

        agregar_punto(this);
        recalculardebehaber();
    });

     /******************************** Habilitar - Deshabilitar DEBE *******************************/
    $(".haber").change(function(){
        desactivar_debe_haber(this);

        if ($(this).val() == ""){
            //$("#total_haber").html(0.0);
            resultado_total_haber = 0.0;
        }
        else{
            $("#total_haber").html($(this).val());
            resultado_total_haber = $(this).val();
        }

        recalculardebehaber();
        agregar_punto(this);
    });
    /***********************************************************************************************/

    $("#add").click(function(e){
        e.preventDefault();
        var clonar_tr = $(ID_TABLA_CONTABILIDAD+" tbody tr:first").clone();
        clonar_tr.find(".bootstrap-select").remove();
        clonar_tr.find(".concepto").val($("#id_concepto").val()).change(function(){$(this).attr("data-modificado", 1);});

        $(clonar_tr).find("select[data-name='combo_centro_costo']").prop('selectedIndex', 0);
        clonar_tr.find(".eliminar").click(function(e){
            e.preventDefault();
            if($(ID_TABLA_CONTABILIDAD+" tbody").children().length>2){
                $(this).parent().parent().remove();
                recalcular_ids(ID_TABLA_CONTABILIDAD);
                recalculardebehaber();
            }
            else{
                validarEliminar(1);
            }
        });


        clonar_tr.find(".debe").each(function( index ) {
            $(this).val("");
            $(this).parent().find(".error").remove();
        });
        clonar_tr.find(".haber").each(function( index ) {
            $(this).val("");
            $(this).parent().find(".error").remove();
        });
        /********************************  Númerico ************************************************/
        $(clonar_tr.find(".numerico")).keydown(function(event) {
            if ( $.inArray(event.keyCode,[46,8,9,27,110,13,190]) !== -1 ||
                (event.keyCode == 65 && event.ctrlKey === true) ||
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     return;
            }
            else {
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });
        /*******************************************************************************************/

        /***************** Habilitaciónn y Bloqueo de DEBE - HABER ****************************/
        $(clonar_tr.find(".debe")).change(function(){
            desactivar_debe_haber(this);

            recalculardebehaber();
            agregar_punto(this);
        });

        $(clonar_tr.find(".haber")).change(function(){
            desactivar_debe_haber(this);

           recalculardebehaber();
           agregar_punto(this);
        });

        // Refresca los combos selectpicker
        $(clonar_tr).find(".selectpicker").selectpicker("refresh");
        $(ID_TABLA_CONTABILIDAD+" tbody").append(clonar_tr);
        recalcular_ids(ID_TABLA_CONTABILIDAD);
    });

});



