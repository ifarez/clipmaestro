/**
 * Created by Roberto on 08/09/14.
 */
var formulario = "#formulario";

$(document).ready(function(){
   $(".selectpicker").selectpicker();
   $(".opcion-buscar").bind('click', function(event) {
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").addClass("opcion-buscar-hide");
        $( ".container-buscador").removeClass("container-buscador-hide");
    });
   $(".buscador-cerrar").click(function(){
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").removeClass("opcion-buscar-hide");
    });
   $('#limpiar').click(function(){
       limpiar_form(formulario);
   });
   $("#eliminar").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            var tr = $("#lista input[type='radio']:checked").parent().parent();
            var nombre = tr.find(".nombre").text();

            var url = $("#lista input[type='radio']:checked").attr("data-eliminar");
            $("#tit-alert").html("Eliminar Centro ded Costo");
            $("#body-alert").html('<p>¿ Desea eliminar al centro de costo: "<strong>'+nombre+'</strong>"?</p>');
            $("#alert-yn").attr("data-tipo", 0);
            $("#alert-yn").attr("data-href", url);
            $("#alert-yn").modal();
        }

        else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para eliminar un centro de costo.</p>');
            $("#alert-ok").modal();
        }
   });
   $("#editar").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            var tr = $("#lista input[type='radio']:checked").parent().parent();
            var nombre = tr.find(".nombre").text();
            var url = $("#lista input[type='radio']:checked").attr("data-editar");
            window.location.href = url;
        }
        else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para editar un centro de costo.</p>');
            $("#alert-ok").modal();
        }
   });
   $("#estado").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            var tr = $("#lista input[type='radio']:checked").parent().parent();
            var nombre = tr.find(".nombre").text();
            var prox_estado = tr.find(".estado").attr("data-pro_estado");
            var url = $("#lista input[type='radio']:checked").attr("data-estado");
            $("#tit-alert").html("Cambiar Estado");
            $("#body-alert").html('<p>¿ Desea '+prox_estado+' el centro de costo:  "<strong>'+nombre+'</strong>"?</p>');
            $("#alert-yn").attr("data-href", url);
            $("#alert-yn").modal();
        }
        else
        {
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para cambiar de estado.</p>');
            $("#alert-ok").modal();
        }
   });
   $("#ok-alert").on("click",function(){
        var url = $("#alert-yn").attr("data-href");
        var id = $("#alert-yn").attr("data-id");
        window.location.href = url;
   });

});