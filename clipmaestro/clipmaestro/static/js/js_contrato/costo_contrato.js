/**
 * Created by Roberto on 25/11/2014.
 */

var ID_TABLA_COSTO_PROVEEDOR = "#tabla_contrato_detalle_proveedor";
var ID_TABLA_COSTO_EMPLEADO = "#tabla_contrato_detalle_empleado";

/***
 * Función que permite ocultar o mostrar la información
 * de empleado o proveedor según la mano de obra a seleccionar
 */
function showProveedorEmpleado(){
    var tipo = $("#id_tipo");   // Mano de Obra:
                                // 1 -> Externa == Poveedor
                                // 2 -> Interna == Empleado
    if ($(tipo).val() == 1){
        $("#tr_proveedor").show();
        $("#div_detalle_proveedor").show();
        $("#tr_factura").show();

        $("#tr_sueldo").hide();
        $("#tr_empleado").hide();
        $("#div_detalle_empleado").hide();

    }else{

        $("#tr_empleado").show();
        $("#div_detalle_empleado").show();
        $("#tr_sueldo").show();

        $("#tr_valor_cmp").hide();
        $("#tr_factura").hide();
        $("#tr_proveedor").hide();
        $("#div_detalle_proveedor").hide();
    }
}
/***
 * Función Ajax para obtener las facturas correspondientes
 * al proveedor
 * @param url
 */
function get_facturas_proveedor(url){
    var combo_num_facturas = $("#id_factura");
    combo_num_facturas.empty();

    $.ajax({
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'id': $("#id_proveedor").val(),
            'tipo': $("#id_tipo").val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(data){
            return true;
        },
        success: function(data){
            combo_num_facturas.append('<option value=""></option>'); // Para llenar en blanco la primera posición del select
            for(var i=0; i<data.length; i++){
                combo_num_facturas.append('<option value="' + data[i].id + '">' + data[i].num_doc+ '</option>');
            }
            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
    combo_num_facturas.selectpicker("refresh");
}

/***
 * Función Ajax para obtener el respectivo valor
 * de la compra
 * @param url
 */
function get_valor_mano_obra_externa(url){
    var valor_cmp = $("#id_valor_cmp");
    $.ajax({
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
             'id_compra': $("#id_factura").val()
        },
        error: function(data){
            return true;
        },
        success: function(data){

             if(data.status == 1){
                $(valor_cmp).val(data.valor);
             }
        }
    });
}


/***
 * Función que Agrega las Filas al formset
 * de detalle_costo_proveedor
 * @param e
 *
 */
function AgregarDetalleCostoProveedor(e){
    e.preventDefault();
    var clonar_tr = $(ID_TABLA_COSTO_PROVEEDOR+" tbody tr:first").clone();
    clonar_tr.find(".bootstrap-select").remove();

    clonar_tr.find(".eliminar_prov").click(function(e){
        EliminarFila(e,this,ID_TABLA_COSTO_PROVEEDOR);
    });

    clonar_tr.find(".valor").val("");
    clonar_tr.find(".numerico").each(function(){
        $(this).keydown(function(event){
            Numerico(event);
        }).change(function(event){
            redondear(this);
        });
    });
    clonar_tr.find(".contrato").prop('selectedIndex', 0);

    // Refresca los combos selectpicker
    $(clonar_tr).find(".selectpicker").selectpicker("refresh");

    $(ID_TABLA_COSTO_PROVEEDOR+" tbody").append(clonar_tr);
    recalcular_ids(ID_TABLA_COSTO_PROVEEDOR);
}
/***
 * Función que Agrega las Filas al formset
 * de detalle_costo_empleado
 * @param e
 *
 */
function AgregarDetalleCostoEmpleado(e){
    e.preventDefault();
    var clonar_tr = $(ID_TABLA_COSTO_EMPLEADO+" tbody tr:first").clone();
    clonar_tr.find(".bootstrap-select").remove();

    clonar_tr.find(".eliminar_emp").click(function(e){
        EliminarFila(e,this,ID_TABLA_COSTO_EMPLEADO);
    });

    clonar_tr.find(".horas").val("");
    clonar_tr.find(".valor").val("");

    clonar_tr.find(".horas").mask("99:99").each(function(){

    });

    // Refresca los combos selectpicker
    $(clonar_tr).find(".selectpicker").selectpicker("refresh");

    $(ID_TABLA_COSTO_EMPLEADO+" tbody").append(clonar_tr);
    recalcular_ids(ID_TABLA_COSTO_EMPLEADO);

}

$(document).ready(function(){
    if (flag_post == 0){
        $("#id_factura").empty();
        $("#tr_valor_cmp").hide();

    }else{

        if ($("#id_factura").val() != ""){
            $("#tr_valor_cmp").show();
            get_valor_mano_obra_externa(url_get_valor_costo_contrato);
        }
    }

    $('#id_fecha_reg').mask("9999-99-99").datepicker({
                 todayBtn: true,
                 autoclose: true
    }).on('changeDate', function (ev) {

    });

    $(".selectpicker").selectpicker();

    showProveedorEmpleado();
    $("#id_tipo").change(function(){
       showProveedorEmpleado();
    });

    $(".numerico").each(function(){
        $(this).keydown(function(event){
            Numerico(event);
        }).change(function(event){
            redondear(this);
        });
    });

    $("#id_proveedor").change(function(){
       get_facturas_proveedor(url_get_facturas_compras);

    });

    $("#id_factura").change(function(){

        if($(this).val()!= ""){
            // Presentar el input con el valor de la compra
            $("#tr_valor_cmp").show();

            // función ajax que me presente el valor actualizado
            get_valor_mano_obra_externa(url_get_valor_costo_contrato);


        }else{
            $("#tr_valor_cmp").hide();
        }

    });

    $("#id_empleado").change(function(){
        var sueldo = $("#id_sueldo");
        var tr = $(this).parent().parent();
        var valor_sueldo = $(tr).find("option:selected").attr("data-sueldo");

        if ($(this).val()!= ""){
            $(sueldo).val(valor_sueldo);
        }
    });

    $(".horas").mask("99:99").each(function(){

    });


    $("#add_prov").click(function(e){
        AgregarDetalleCostoProveedor(e);

    });

    $("#add_emp").click(function(e){
        AgregarDetalleCostoEmpleado(e);

    });

    $(".eliminar_emp").click(function(e){
        EliminarFila(e,this,ID_TABLA_COSTO_EMPLEADO);
    });

     $(".eliminar_prov").click(function(e){
        EliminarFila(e,this,ID_TABLA_COSTO_PROVEEDOR);
     });

});
