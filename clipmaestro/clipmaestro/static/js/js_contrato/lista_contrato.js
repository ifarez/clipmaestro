/**
 * Created by Roberto on 10/09/14.
 */
function ver_contrato(){
    if ($("#lista input[type='radio']:checked").length != 0){
        window.location.href = $("#lista input[type='radio']:checked").attr("data-ver");
    }else{
        $("#tit-alert-ok").html("ALERTA");
        $("#body-alert-ok").html('<p>Seleccione un item para ver el datalle del contrato</p>');
        $("#alert-ok").modal();
    }
}
function anular_contrato(){
    if ($("#lista input[type='radio']:checked").length != 0){
        var tr = $("#lista input[type='radio']:checked").parent().parent();
        var nombre = tr.find(".nombre").text();

        var url = $("#lista input[type='radio']:checked").attr("data-anular");
        var status = $("#lista input[type='radio']:checked").attr("data-status");
        if(status == "1")
        {
            $("#tit-alert").html("ANULAR CONTRATO");
            $("#body-alert").html('<p>¿ Desea anular el contrato: <strong>#'+nombre+' ?</strong></p>');
            $("#alert-yn").attr("data-href", url).modal();
        }
        else{
            $("#tit-alert-ok").html("ALERTA");
            $("#body-alert-ok").html('<p>El contrato que quiere anular ya se encuentra anulado</p>');
            $("#alert-ok").modal();
        }
    }
    else{
        $("#tit-alert-ok").html("ALERTA");
        $("#body-alert-ok").html('<p>Seleccione un item para anular un contrato</p>');
        $("#alert-ok").modal();
        }
}
function editar_contrato(){
    if ($("#lista input[type='radio']:checked").length != 0){
        var tr = $("#lista input[type='radio']:checked").parent().parent();
        var nombre = tr.find(".nombre").text();

        var url = $("#lista input[type='radio']:checked").attr("data-editar");
        var status = $("#lista input[type='radio']:checked").attr("data-status");
        if(status == "1")
        {
            $("#tit-alert").html("EDITAR CONTRATO");
            $("#body-alert").html('<p>Desea editar el contrato <strong>#'+nombre+'</strong></p>');
            $("#alert-yn").attr("data-href", url).modal();
        }
        else{
            $("#tit-alert-ok").html("ALERTA");
            $("#body-alert-ok").html('<p>El contrato que quiere editar ya se encuentra anulado</p>');
            $("#alert-ok").modal();
        }
    }
    else{
        $("#tit-alert-ok").html("ALERTA");
        $("#body-alert-ok").html('<p>Seleccione un item para editar un contrato</p>');
        $("#alert-ok").modal();
    }
}

$(document).ready(function(e){
    $("#cerrar").click(function(){
        $("#error").fadeOut("slow");
    });
    $(".opcion-buscar").bind('click', function(event) {
        $( ".container-buscador" ).slideToggle(1000).removeClass("container-buscador-hide");
        $(".opcion-buscar").addClass("opcion-buscar-hide");
    });
    $(".buscador-cerrar").click(function(){
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").removeClass("opcion-buscar-hide");
    });
    $("#ver").click(function(){ ver_contrato() });
    $("#anular").click(function(){ anular_contrato() });
    $("#editar").click(function(){ editar_contrato() });

    $("#ok-alert").on("click",function(){
        window.location.href = $("#alert-yn").attr("data-href");
    });

    $('#limpiar').click(function(){
       limpiar_form($("#formulario"));
    });

    $(".evento_page").each(function(){
        $(this).click(function(){
            var page = $(this).attr("data-page");
            $("#id_page").val(page);
            $("#buscar").click();
        });
    });

    $(".numerico").keydown(function(event) {Numerico(event)});
    $(".numerico_doc").keydown(function(event) {NumeroEntero(event)});
    $(".selectpicker").selectpicker();

    $(".datepicker").mask("9999-99-99");
});