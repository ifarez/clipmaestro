/**
 * Created by Roberto on 23/12/2014.
 */
var ID_TABLA_MANUAL = "#tabla_formset_venta_dev_m";
var ID_TABLA_AUTOMATICA ="#tabla_formset_venta_dev_a";
var URLSelectNumFacturas = getURLSeleccionarNumFacturas();
var URLCargarVenta = getURLCargarVenta();
var URLcantidadDevolver = getURLCantidadDevolucionVenta();
var URLExisteItemBodega = getExisteItemBodega();
/***
 * Función que  permite seleccionar todas las columnas de cantidad
 * y costo para realizar el proceso según se de el caso
 *
 */
function SelectedAllCantidadCosto(){
    var all_cantidad = $("#all_cantidad");
    var all_costo = $("#all_costo");

    if ($(all_cantidad).is(':checked')){
        // Radios Formset
        $(".cantidad").attr("checked", true);
        $(".cantidad").prop("checked", true);
        $(".costo").attr("checked", false);
        $(".costo").prop("checked", false);

        //Formset
        $(".cant_dev_a").attr("disabled", false);
        $(".cant_dev_a").attr("readonly", false);
        $(".subtotal").attr("disabled", false);
        $(".subtotal").attr("readonly", true);
        //$(".cant_dev_a").attr("readonly", false);

        $(".cant_dev_a").val("");
        $(".subtotal").val("");

        $(".cant_dev_a").addClass("campo_requerido");
        $(".subtotal").removeClass("campo_requerido");

    }else if ($(all_costo).is(':checked')) {
        $(".costo").attr("checked", true);
        $(".cantidad").attr("checked", false);
        $(".costo").prop("checked", true);
        $(".cantidad").prop("checked", false);

        //Formset
        $(".cant_dev_a").attr("disabled", true);
        //$(".cant_dev_a").attr("readonly", true);
        $(".cant_dev_a").val("");

        $(".subtotal").attr("disabled", false);
        $(".subtotal").attr("readonly", false);
        $(".subtotal").val("");

        $(".subtotal").addClass("campo_requerido");
        $(".cant_dev_a").removeClass("campo_requerido");

    }else{
        $(".cant_dev_a").attr("disabled", true);
        $(".subtotal").attr("disabled", true);
        $(".cant_dev").attr("disabled", true);
    }

}
/***
 * Función que permite la interacción de la cantidad a devolver y
 * el costo (valor) en una N/C que es manual(No se encuentra
 * registrada en el sistema)
 * @constructor
 */
function SelectAllCantidadCostoManual() {
    var all_cantidad_m = $("#all_cantidad_man");
    var all_costo_m = $("#all_costo_man");

    if ($(all_cantidad_m).is(':checked')) {
        // Radios Formset
        $(".cantidad_m").attr("checked", true);
        $(".cantidad_m").prop("checked", true);
        $(".costo").attr("checked", false);
        $(".costo").prop("checked", false);

        //Formset
        $(".cant_dev").attr("disabled", false);
        $(".cant_dev").attr("readonly", false);
        $(".subtotal").attr("disabled", false);
        $(".subtotal").attr("readonly", true);

        $(".cant_dev").val("");
        $(".subtotal").val("");

        $(".cant_dev").addClass("campo_requerido");
        $(".subtotal").removeClass("campo_requerido");

    } else if ($(all_costo_m).is(':checked')) {

        $(".costo_m").attr("checked", true);
        $(".cantidad_m").attr("checked", false);
        $(".costo_m").prop("checked", true);
        $(".cantidad_m").prop("checked", false);

        //Formset
        $(".cant_dev").attr("disabled", true);
        //$(".cant_dev_a").attr("readonly", true);
        $(".cant_dev").val("");

        $(".subtotal").attr("disabled", false);
        $(".subtotal").attr("readonly", false);
        $(".subtotal").val("");

        $(".subtotal").addClass("campo_requerido");
        $(".cant_dev").removeClass("campo_requerido");

    }else{
        $(".subtotal").attr("disabled", true);
        $(".cant_dev").attr("disabled", true);
    }
}
/***
 * Función que permite la interacción de cantidad - costo manual
 * en una N/C manual
 * @constructor
 */
function SelectOneCantidadCostoManual(obj){
    var tr = $(obj).parent().parent();
    // radios
    var cantidad_r_m = $(tr).find(".cantidad_m");
    var costo_r_m = $(tr).find(".costo_m");

    var all_cantidad_m = $("#all_cantidad_man");
    var all_costo_m = $("#all_costo_man");

    //inputs
    var cantidad_dev = $(tr).find(".cant_dev");



    if ($(cantidad_r_m).is(':checked')){


    }


}

function SelectOneCantidadCosto(obj){
    var tr = $(obj).parent().parent();
    //radios
    var cantidad_r = $(tr).find(".cantidad");
    var costo_r = $(tr).find(".costo");
    //inputs
    var cantidad_dev = $(tr).find(".cant_dev_a");
    var costo_subt = $(tr).find(".subtotal");

    //Radio General - En Blanco
    $("#all_cantidad").attr("checked", false);
    $("#all_costo").attr("checked", false);
    $("#all_cantidad").prop("checked", false);
    $("#all_costo").prop("checked", false);

    if ($(cantidad_r).is(':checked')){
        //Formset
        $(cantidad_dev).attr("disabled", false);
        $(cantidad_dev).attr("readonly", false);
        $(costo_subt).attr("disabled", false);
        $(costo_subt).attr("readonly", true);

        $(cantidad_dev).val("");
        $(costo_subt).val("");

        $(cantidad_dev).addClass("campo_requerido");
        $(costo_subt).removeClass("campo_requerido");

    }else if ($(costo_r).is(':checked')){
        //Formset
        $(cantidad_dev).attr("disabled", true);
        $(cantidad_dev).attr("readonly", true);
        $(costo_subt).attr("readonly", false);
        $(costo_subt).attr("disabled", false);
        $(cantidad_dev).val("");
        $(costo_subt).val("");

        $(costo_subt).addClass("campo_requerido");
        $(cantidad_dev).removeClass("campo_requerido");
    }

}

/***
 * Función que bloquea según si es servicio el checkbox Activa Inventario
 */
function DisabledActInventarioManual(obj){
     var tr = $(obj).parent().parent();
     var act_inven = $(tr).find(".act_inven");
     var tipo_item = $(tr).find("option:selected").attr("data-tipo");

    if (tipo_item == 2){
        $(act_inven).attr('checked', false);
        $(act_inven).attr("disabled", true);
    }else{
        $(act_inven).attr("disabled", false);
    }
}

/************************************************
 * Valida el documento y la serie Ns/C
 ************************************************/
function ValidaDocumentoSerieNotaCredito(url){
    var combo_serie = $("#id_serie");
    combo_serie.empty();

    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'fecha': $("#id_fecha").val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data){

            $("#id_serie").val(data[0].id);

            if(data[0].status == 0){
                $("#tit-alert-ok").html("No tiene block de documentos vigentes registrados para esta fecha de emisión. " +
                    "Por Favor antes de continuar, ingrese un block de nota crédito vigente.");

                $("#body-alert-ok").html("");

                $("#enviar").on("click",function(){
                    var url = $(this).attr("data-href");
                    window.location.href = url;
                });

                $("#alert-ok").modal();
            }

            if (data[0].status == 1){

                for(var i=0; i<data.length; i++){
                    combo_serie.append('<option value="' + data[i].id + '">' + data[i].serie + '</option>');
                }
            }


            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
    combo_serie.selectpicker("refresh");
}

/*************************************************
* Seleccion de datos según el documento N/C
**************************************************/
function SelectSerieNumDevolucion(){
    $.post(url_seleccionar_nota_credito, {
            'id': $("#id_serie").val()
        }, function(data){
            var id=data.id;
            var serie = data.serie;
            var autorizacion = data.autorizacion;
            var fecha_vencimiento = data.vencimiento;
            $("#autorizacion").text(autorizacion);
            $("#fecha_vencimiento").text(fecha_vencimiento);
            }
    );
}
/******************************************************
 *  Fución Validate Cantidades en Tabla Form Dev Manual
 ******************************************************/
function validateCantidadesDevManual(obj){
    var tr = $(obj).parent().parent();
    var cantidad_venta = $(tr).find(".cant_vendida_m");
    var cantidad_dev = $(tr).find(".cant_dev");

    if (parseFloat($(cantidad_venta).val()) < parseFloat($(cantidad_dev).val())){
        $("#tit-alert-ok").html("Alerta");
        $("#body-alert-ok").html('<p>Error, La cantidad ingresada de venta:  '+$(cantidad_venta).val()+'<br/>'
                                        +'  es menor a la cantidad a devolver: '+ $(cantidad_dev).val() +'</p>');
        $("#alert-ok").modal();
        $(cantidad_dev).val("");
    }
}
/***
 * Función limpia Totales
 */
function clearTotales(){
    $("#subtotal").text("$"+"0.0");
    $("#subtotal_cero").text("$"+"0.0");
    $("#ice").text("$"+"0.0");
    $("#descuento").text("$"+"0.0");
    $("#iva").text("$"+"0.0");
    $("#total_pagar").text("$"+"0.0");
}
/*****************************************************
* Función para Calcular Totales
******************************************************/
function CalcularTotalesDevolucion(tablaid){
    var total_pagar = 0.0;
    var total_descuento = 0.0;
    var total_iva = 0.0;
    var total_ice = 0.0;
    var subtotal = 0.0;
    var subtotal_cero = 0.0;
    var x = 0.0;
    var ice_tmp = 0.0;
    var iva_tmp = 0.0;
    var cantidad_dev_tmp = 0.0;
    var flag_manual = 0;
    var tipo_desc_tmp = 0;
    var desc_valor_tmp = 0.0;

    $(tablaid+" tbody tr").each(function(index){
        var cantidad_dev = $(this).find(".cant_dev");
        var cantidad_dev_a = $(this).find(".cant_dev_a");
        var precio = $(this).find(".precio");
        var desc_valor = $(this).find(".desc_valor");
        var subtotal_tabla = $(this).find(".subtotal");
        var desc_porc = $(this).find(".desc_porc");
        var ice = $(this).find("option:selected").attr("data-ice");
        var iva = $(this).find("option:selected").attr("data-iva");
        var tipo_desc = $(this).find(".tipo_desc"); // auto

        if (ice == undefined){
            ice_tmp = parseFloat($(this).find(".ice").val());
        }else{
            flag_manual = 1;
            ice_tmp = ice;
        }

        if (iva == undefined){
            iva_tmp = parseFloat($(this).find(".iva").val());
        }else{
            flag_manual = 1;
            iva_tmp = iva;
        }

        if ($(cantidad_dev).val() == undefined){
            cantidad_dev_tmp = parseFloat($(cantidad_dev_a).val()); // auto
        }else{
            flag_manual = 1;
            cantidad_dev_tmp = parseFloat($(cantidad_dev).val()); // manual
        }

        if (isNaN(parseInt($(tipo_desc).val()))){
             tipo_desc_tmp = 0;  //manual
             flag_manual = 1;
        }else{
            tipo_desc_tmp = parseInt($(tipo_desc).val());
        }

        if (parseFloat($(desc_valor).val()) == undefined){
             desc_valor_tmp = 0;  //manual
             flag_manual = 1;

        }else{
            desc_valor_tmp = parseFloat($(desc_valor).val());
        }

        if ($(precio).val() != "" && !(isNaN(cantidad_dev_tmp))){

            if(flag_manual == 1 || tipo_desc_tmp == 0){   // Sin Desc or Manual

                if (parseFloat(iva) != 0.0 ){
                    subtotal = subtotal + (cantidad_dev_tmp)*($(precio).val());

                    total_iva = total_iva +  (cantidad_dev_tmp*$(precio).val())*0.12;
                    total_ice = total_ice + (cantidad_dev_tmp*$(precio).val())*(parseFloat(ice_tmp)/100);
                    $(subtotal_tabla).val(subtotal);

                    $("#subtotal").text("$"+subtotal.toFixed(2));
                    $("#ice").text("$"+total_ice.toFixed(2));
                    $("#descuento").text("$"+0.0);
                    $("#iva").text("$"+total_iva.toFixed(2));
                    x = subtotal + total_ice + total_iva + subtotal_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));

                }else{

                     subtotal_cero = subtotal_cero + (cantidad_dev_tmp)*$(precio).val();
                     $(subtotal_tabla).val(subtotal);

                     $("#subtotal_cero").text("$"+subtotal_cero.toFixed(2));
                     $("#descuento").text("$"+0.0);
                     x = subtotal + total_ice + total_iva + subtotal_cero;
                     $("#total_pagar").text("$"+x.toFixed(2));
                }
            }

            if(flag_manual == 0 && tipo_desc_tmp != 0){   // con Desc. Auto


                if (parseFloat(iva) != 0.0 ){
                    subtotal = subtotal + (cantidad_dev_tmp)*($(precio).val());
                    total_descuento = total_descuento + subtotal*$(desc_porc).val()/100;

                    total_iva = total_iva +  (subtotal - total_descuento)*0.12;
                    total_ice = total_ice + (subtotal - total_descuento)*(parseFloat(ice_tmp)/100);

                    $(subtotal_tabla).val(subtotal);

                    $("#subtotal").text("$"+subtotal.toFixed(2));
                    $("#subtotal_cero").text("$"+subtotal_cero.toFixed(2));
                    $("#ice").text("$"+total_ice.toFixed(2));
                    $("#descuento").text("$"+total_descuento.toFixed(2));
                    $("#iva").text("$"+total_iva.toFixed(2));
                    x = subtotal + total_ice + total_iva + subtotal_cero - total_descuento;
                    $("#total_pagar").text("$"+x.toFixed(2));


                }else{
                     subtotal_cero = subtotal_cero + (cantidad_dev_tmp)*$(precio).val();
                     //total_descuento = total_descuento + desc_valor_tmp;
                     total_descuento = total_descuento + subtotal*$(desc_porc).val()/100;

                    $(subtotal_tabla).val(subtotal);

                     $("#subtotal_cero").text("$"+subtotal_cero.toFixed(2));
                     $("#descuento").text("$"+total_descuento.toFixed(2));
                     x = subtotal + total_ice + total_iva + subtotal_cero - total_descuento;
                     $("#total_pagar").text("$"+x.toFixed(2));
                }

            }

        }else{

                $("#subtotal").text("$"+subtotal.toFixed(2));
                $("#ice").text("$"+total_ice.toFixed(2));
                $("#descuento").text("$"+0.0);
                $("#iva").text("$"+total_iva.toFixed(2));
                x = subtotal + total_ice + total_iva + subtotal_cero;
                $("#total_pagar").text("$"+x.toFixed(2));

        }
    });

}
/*****************************************************
* Función para Calcular SubTotales
******************************************************/
function CalcularSubtotal(tablaid){
    var total_descuento = 0.0;
    var ice_tmp = 0.0;
    var iva_tmp = 0.0;
    var cantidad_dev_aut = 0.0;
    var cantidad_dev_man = 0.0;
    var x = 0.0;
    var total_iva = 0.0;
    var total_ice = 0.0;
    var subtotal_final = 0.0;
    var subtotal_final_cero = 0.0;
    var flag_manual = 0;

    $(tablaid+" tbody tr").each(function(index){
        var cantidad_dev = $(this).find(".cant_dev");            // Manual
        var cantidad_dev_a = $(this).find(".cant_dev_a");       // Automática
        var precio = $(this).find(".precio");
        var desc_valor = $(this).find(".desc_valor");
        var subtotal_tabla = $(this).find(".subtotal");         // Automática
        var desc_porc = $(this).find(".desc_porc");
        var ice = $(this).find("option:selected").attr("data-ice"); // Manual
        var iva = $(this).find("option:selected").attr("data-iva"); // Manual

        if (ice == undefined){
            ice_tmp = parseFloat($(this).find(".ice").val());
        }else{
            flag_manual = 1;
            ice_tmp = ice;
        }

        if (iva == undefined){
            iva_tmp = parseFloat($(this).find(".iva").val());
        }else{
            flag_manual = 1;
            iva_tmp = iva;
        }

        if ($(cantidad_dev).val() == undefined){
            cantidad_dev_aut = parseFloat($(cantidad_dev_a).val()); // auto
        }else{
            flag_manual = 1;
            cantidad_dev_man = parseFloat($(cantidad_dev).val()); // manual
        }

        if (!(isNaN(cantidad_dev_aut))){

            if (parseFloat(iva) != 0.0 ){
                subtotal_final = subtotal_final + (cantidad_dev_aut)*($(precio).val());

                $(subtotal_tabla).val((cantidad_dev_aut)*($(precio).val()));

                total_descuento = total_descuento + (subtotal_final)*($(desc_valor).val())/100;
                total_iva = total_iva +  ((cantidad_dev_aut)*($(precio).val()))*0.12;
                total_ice = total_ice + (subtotal_final - total_descuento)*(parseFloat(ice_tmp)/100);

                $("#subtotal").text("$"+subtotal_final.toFixed(2));
                $("#ice").text("$"+total_ice.toFixed(2));
                $("#descuento").text("$"+total_descuento.toFixed(2));
                $("#iva").text("$"+total_iva.toFixed(2));
                x = subtotal_final + total_ice + total_iva + subtotal_final_cero - total_descuento;
                $("#total_pagar").text("$"+ x.toFixed(2));

            }else{

                 subtotal_final_cero = subtotal_final_cero + (cantidad_dev_aut)*($(precio).val());
                 $(subtotal_tabla).val((cantidad_dev_aut)*($(precio).val()));
                 total_descuento = total_descuento + (subtotal_final_cero)*($(desc_valor).val())/100;
                 $("#subtotal_cero").text("$"+subtotal_final_cero.toFixed(2));
                 $("#descuento").text("$"+total_descuento.toFixed(2));
                 x = subtotal_final + total_ice + total_iva + subtotal_final_cero - total_descuento;
                 $("#total_pagar").text("$"+x.toFixed(2));
            }

        }else if ($(subtotal_tabla).val() != ""){

                if (parseFloat(iva) != 0.0 ){
                    subtotal_final = subtotal_final + parseFloat($(subtotal_tabla).val());
                    total_descuento = total_descuento + (subtotal_final)*($(desc_valor).val())/100;
                    total_iva = ((subtotal_final - total_descuento))*0.12;
                    total_ice = total_ice + (subtotal_final - total_descuento)*(parseFloat(ice_tmp)/100);

                    $("#subtotal").text("$"+subtotal_final);
                    $("#ice").text("$"+total_ice.toFixed(2));
                    $("#descuento").text("$"+total_descuento.toFixed(2));
                    $("#iva").text("$"+total_iva.toFixed(2));
                    x = subtotal_final + total_ice + total_iva + subtotal_final_cero - total_descuento;
                    $("#total_pagar").text("$"+ x.toFixed(2));

                }else{

                     subtotal_final_cero = subtotal_final_cero + $(subtotal_tabla).val();
                     total_descuento = total_descuento + (subtotal_final_cero)*($(desc_valor).val())/100;
                     $("#subtotal_cero").text("$"+subtotal_final_cero.toFixed(2));
                     $("#descuento").text("$"+total_descuento.toFixed(2));
                     x = subtotal_final + total_ice + total_iva + subtotal_final_cero - total_descuento;
                     $("#total_pagar").text("$"+x.toFixed(2));
                }
        }

    });
}
/**************************************************
* Funcion para AgregarFilas en la Devolución Manual
***************************************************/
function AgregarFilasDevolucionManual(e){
    e.preventDefault();
    var clonar_tr = $(ID_TABLA_MANUAL+" tbody tr:first").clone();
    clonar_tr.find(".bootstrap-select").remove();
    clonar_tr.find(".eliminar").click(function(e){
        EliminarFila(e, this, ID_TABLA_MANUAL);
    });
    clonar_tr.find("input").each(function( index ) {
        $(this).val("");
        $(this).parent().find(".error").remove();
    });
    $(clonar_tr).find(".act_inven").attr('checked', false);
    $(clonar_tr).find(".act_inven").attr('disabled', false);
    $(clonar_tr).find(".act_inven").removeAttr("value");
    $(clonar_tr.find(".numerico")).each(function(){
        $(this).keydown(function(event){
            Numerico(event);
        }).change(function(event){
                redondear(this);
        });
    });
    $(clonar_tr.find(".item")).change(function(){
        ValidateItemsRepetidos(this, ID_TABLA_MANUAL);
        DisabledActInventarioManual(this);
    });

    // Refresca los combos selectpicker
    $(clonar_tr).find(".selectpicker").selectpicker("refresh");

    $(clonar_tr).find(".cant_dev").change(function(){
        var tr = $(this).parent().parent();
        var cantidad_dev = $(tr).find(".cant_dev");

        if ($(cantidad_dev).val() != ""){
            validateCantidadesDevManual(this);
            CalcularTotalesDevolucion(ID_TABLA_MANUAL);
        }
    });

    $(clonar_tr).find(".precio").change(function(){
        var tr = $(this).parent().parent();
        var cantidad_dev = $(tr).find(".cant_dev");

        if ($(cantidad_dev).val() != ""){
            CalcularTotalesDevolucion(ID_TABLA_MANUAL);
        }
     });

    //Agrega la fila clonada y recalcula los ids de las filas
    $(ID_TABLA_MANUAL+" tbody").append(clonar_tr);
    recalcular_ids(ID_TABLA_MANUAL);
}
/**************************************************
* Llamado de Función de recalcular_ids
***************************************************/
$(function(){recalcular_ids(ID_TABLA_MANUAL);});

/****************************************************************
 * Si la función que controla los escenarios y presentación de
 * los formset
 ****************************************************************/
function showFormsetPost(){

    if (flag_post == 1){

        if ($("#id_num_documentos_venta").val() == 0){
            $("#div_detalle_dev_aut").hide();
            $("#div_detalle_dev_man").show();
            $("#grabar").show();
            $("#totales").show();

        }else if ($("#id_num_documentos_venta").val() != 0){
            $("#div_detalle_dev_aut").show();
            $("#div_detalle_dev_man").hide();
        }
    }
}



/*****************************************************
* Cargar los datos de la venta según el Núm_Documento
******************************************************/
function cargar_detalle_devolucion_venta_a(){

    $.ajax(
    {
        url: URLCargarVenta,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            id: $("#id_cliente").val(),
            id_venta: $("#id_num_documentos_venta").val()
        },
        beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data)
        {
            $("#div_detalle_dev_aut").html(data).hide();
            var div = $("#tiene_venta");

            if(div.attr("data-error") == "1"){
                $("#div_detalle_dev_aut").html(data).hide();
                $("#grabar").hide();
                $("#totales").hide();
            }else{
                $("#div_detalle_dev_aut").html(data).show();
                $("#div_detalle_dev_man").hide();
                $("#grabar").show();
                $("#totales").show();
                clearTotales();

                $('.selectpicker').selectpicker();

                SelectedAllCantidadCosto();

                $("#all_cantidad").click(function(){
                    SelectedAllCantidadCosto();
                    CalcularSubtotal(ID_TABLA_AUTOMATICA);
                });



                $("#all_costo").click(function(){
                    SelectedAllCantidadCosto();
                    CalcularSubtotal(ID_TABLA_AUTOMATICA);
                });

                $(".cantidad").click(function(){
                    SelectOneCantidadCosto(this);
                    CalcularSubtotal(ID_TABLA_AUTOMATICA);
                });

                $(".costo").click(function(){
                    SelectOneCantidadCosto(this);
                    CalcularSubtotal(ID_TABLA_AUTOMATICA);
                });

                $(".numerico").each(function(){
                   $(this).keydown(function(event){
                        Numerico(event);
                   }).change(function(event){
                        redondear(this);
                   });
                });
                // Inhabilita el boton de submit evitando que se haga más de una petición
                $("input[type='submit']").click(function(){
                    $(this).attr("disabled", false);
                    $("form").submit(function(){
                      $("input[type='submit']").attr("disabled", true).val("Por favor, espere...");
                      return true;
                    });
                });

                $(".cant_dev_a").change(function(){
                    var tr = $(this).parent().parent();
                    var cant_vta = $(tr).find(".cant_vendida");  //cantidad_venta

                    ValidaCantidadDEvolver(this);

                    if ($(this).val() > parseFloat($(cant_vta).val())){
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>Error, No puede devolver más de lo que se ha vendido.</p>');
                        $("#alert-ok").modal();
                        $(this).val("");
                    }

                    CalcularSubtotal(ID_TABLA_AUTOMATICA);
                });

                $(".subtotal").change(function(){
                    var tr = $(this).parent().parent();
                    var cantidad_vendida = $(tr).find(".cant_vendida");
                    var precio= $(tr).find(".precio");

                    if ($(this).val() <= $(cantidad_vendida).val()*$(precio).val()){
                        CalcularSubtotal(ID_TABLA_AUTOMATICA);
                    }else{
                        mensaje_alerta("Error el subtotal no puede ser mayor al costo total de la " +
                            "venta de este item");
                        $(this).val("");
                    }
                });
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
}

/**********************************************************
* Ocultar Opciones
***********************************************************/
function HideOpciones(){
    var cliente = $("#id_cliente");
    var combo_num_facturas = $("#id_num_documentos_venta");

    if ($(cliente).val() == ""){
        SelecNumFacturas();
        $("#div_detalle_dev_aut").hide();
        $("#div_detalle_dev_man").hide();
        $("#grabar").hide();
        $("#totales").hide();
    }

    if ($(combo_num_facturas).val() != ""){
        $("#div_detalle_dev_man").hide();
    }

    if (flag_post == 1 && $(combo_num_facturas).val() != ""){
         SelecNumFacturas();
         cargar_detalle_devolucion_venta_a();
    }

    if (flag_post == 1 && $(combo_num_facturas).val() == ""){
         $("#div_detalle_dev_man").hide();
         $("#div_detalle_dev_aut").hide();
         $("#grabar").hide();
         $("#totales").hide();
    }

    // MANUAL
    if (flag_post == 1 && $(combo_num_facturas).val() == "0"){
         $("#div_detalle_dev_man").show();
         $("#div_detalle_dev_aut").hide();
         $("#grabar").show();
         $("#totales").show();
    }

    $("#tr_documento_modifica").hide();
}
/**********************************************************
* Seleccion de Num. Facturas de la Venta
***********************************************************/
function SelecNumFacturas() {
    var combo_num_facturas = $("#id_num_documentos_venta");

    if(flag_post == 0){
        combo_num_facturas.empty();
    }

    $.ajax({
        url: URLSelectNumFacturas,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'id': $("#id_cliente").val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(data){
            return true;
        },
        success: function(data){

            if(data.status == 0){
                combo_num_facturas.empty();
                $("#div_detalle_dev_aut").hide();
                $("#div_detalle_dev_man").hide();
                $("#grabar").hide();
                $("#totales").hide();
            }

            if(data.status == 1){
                var lista = data.documentos;
                combo_num_facturas.append('<option value=""></option>'); // Para llenar en blanco la primera posición del select
                for(var i=0; i< lista.length; i++){
                    combo_num_facturas.append('<option value="' + lista[i].id + '">' + lista[i].num_doc+ '</option>');
                }
                $("#div_detalle_dev_man").hide();
            }
            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
    combo_num_facturas.selectpicker("refresh");
}
/**********************************************************
* Seleccion de Num. Facturas de la Venta
***********************************************************/
function ValidaCantidadDEvolver(obj){
    var tr = $(obj).parent().parent();
    var id_detalle = $(tr).find(".id_detalle_venta_a");
    var cant_dev = $(tr).find(".cant_dev_a");

    $.ajax({
        url: URLcantidadDevolver,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'id_detalle_venta': $(id_detalle).val(),
            'cantidad_dev': $(cant_dev).val()
        },

        error: function(data){
            return true;
        },
        success: function(data){


            if(data[0].status == 0){
                $("#tit-alert-ok").html("Alerta");
                $("#body-alert-ok").html('<p>Error: Ingrese una cantidad correcta, la cantidad ha devolver supera a la cantidad vendida.</p>');
                $("#alert-ok").modal();
                $(cant_dev).val("");
            }

            if(data[0].status == -1){
                $("#tit-alert-ok").html("Alerta");
                $("#body-alert-ok").html('<p>Error: Existe problemas con el servidor por favor intente nuevamente.</p>');
                $("#alert-ok").modal();
                $(cant_dev).val("");
            }

            if(data[0].status == 1){
                $("#tit-alert-ok").html("Alerta");
                $("#body-alert-ok").html('<p>Error: Todos los items vendidos han sido devueltos no puede ingresar una cantidad a devolver.</p>');
                $("#alert-ok").modal();
                $(cant_dev).val("");
            }

        }
    });
}

