/**
 * Created by Roberto on 29/12/2014.
 */

/***
 * Función para enviar la nota de crédito electrónica
 */
function enviar_nota_credito_electronica(){
    var input_check = $("#lista").find("input[type='radio']:checked");
    if (input_check.length != 0){
        var tr = input_check.parent().parent();
        var num_doc = tr.find(".num_doc").text();
        var url = input_check.attr("data-enviar_n_c_electronica");
        var status = input_check.attr("data-status");
        var estado_firma = input_check.attr("data-estado_firma");

        if(status == "1"){
            mensaje_alerta_yn("Desea autorizar la nota de crédito con  #"+num_doc, "AUTORIZAR NOTA DE CRÉDITO SRI", "data-href", url);
        }
        else{
            mensaje_alerta("La nota de crédito se encuentra anulada");
        }

    }else{
        mensaje_alerta("Seleccione una nota de crédito para enviarla al SRI");
    }
}

/***
 * Función para enviar al email nota de crédito electrónica
 */
function enviar_mail_nota_credito_electronica(){
    var input_check = $("#lista").find("input[type='radio']:checked");
    if (input_check.length != 0){
        var tr = input_check.parent().parent();
        var num_doc = tr.find(".num_doc").text();
        var cliente = tr.find(".razon_social").text();
        var url = input_check.attr("data-enviar_correo");
        var status = input_check.attr("data-status");
        var estado_firma = parseInt(input_check.attr("data-estado_firma"));
        var modal_correo = $("#envio_correo");

        $(".error-modal").hide();

        if(status == "1")
        {
            if(estado_firma >= 4){
                $.ajax(
                {
                    url: url_get_correo,
                    type: 'POST',
                    async: false,
                    cache: false,
                    timeout: 150,
                    data: {
                        'id': input_check.attr("value")
                    },
                    beforeSend: function(){
                    },
                    error: function(){
                        $("#correo_cliente").text("Hubo un error de conexión, por favor cierre la ventana e intente nuevamente");
                        $("#ok_send_mail").hide();
                    },
                    success: function(data)
                    {
                        if(data.status=1)
                        {
                            $("#correo_cliente").text(data.correo);
                        }
                        else
                        {
                            $("#correo_cliente").text(data.mensaje);
                        }
                    }
                });

                $("#id_id_nota_credito").val(input_check.attr("value"));
                modal_correo.modal();

            }else{
                mensaje_alerta("La nota de crédito que seleccionó no está autorizada por el SRI, " +
                    "por favor verifique")
            }



        }else{
            mensaje_alerta("La nota de crédito se encuentra anulada");

        }

    }else{
        mensaje_alerta("Seleccione una nota de crédito autorizada por el SRI " +
            "para enviarle la información al correo");
    }
}

/***
 * Funcionalidad que llama al detalle de la ver_nota_credito
 */
function ver_nota_credito(){
    var status = $("#lista input[type='radio']:checked").attr("data-status");

    if ($("#lista input[type='radio']:checked").length != 0){
        window.location.href = $("#lista input[type='radio']:checked").attr("data-ver");
    }else{
        mensaje_alerta("Seleccione un item para ver el datalle de la nota de crédito");
    }
}

/***
 * Funcionalidad que llama a la vista de anular_nota_credito
 */
function anular_nota_credito(){

    if ($("#lista input[type='radio']:checked").length != 0){
        var tr = $("#lista input[type='radio']:checked").parent().parent();
        var nombre = tr.find(".nombre").text();
        var num_doc = tr.find(".num_doc").text();
        var url = $("#lista input[type='radio']:checked").attr("data-anular-nota-credito");
        var status = $("#lista input[type='radio']:checked").attr("data-status");
        var estado_firma = $("#lista input[type='radio']:checked").attr("data-estado_firma");

        switch(parseInt(status)){
            case 1:
                if (estado_firma >= 4){
                    mensaje_alerta_yn("Esta seguro de anular la N/C con  # Doc.:"+ num_doc + " que ha sido autorizada por el SRI."+ " ? ",
                                      "ANULAR NOTA DE CRÉDITO", "data-href", url);
                }else{
                    mensaje_alerta_yn("Desea anular la nota de crédito con número de comprobante: "+ nombre+ " ?", "ANULAR NOTA DE CRÉDITO", "data-href", url);
                }

            break;
            case 2:
                mensaje_alerta("La nota de crédito ya se encuentra anulada");
            break;
            default :
            break;
        }

    }else{
        mensaje_alerta("Seleccione un item para anular una nota de crédito");
    }
}
/***
 * Función que llama a la vista copiar_nota_credito
 */
function copiar_nota_credito(){

    if ($("#lista input[type='radio']:checked").length != 0){
        var status = $("#lista input[type='radio']:checked").attr("data-status");

        switch(parseInt(status)){
            case 1:
                window.location.href = $("#lista input[type='radio']:checked").attr("data-copiar");
            break;
            case 2:
                mensaje_alerta("No puede copiar la nota de crédito, debido a que se encuentra anulada.");
            break;
            default:
            break;
        }
    }else{
        mensaje_alerta("Seleccione un item para copiar una nota de crédito");
    }
}
/***
 * Función para anular_generar_nota_credito
 */
function anular_generar_nota_credito(){

    if ($("#lista input[type='radio']:checked").length != 0){
        var tr = $("#lista input[type='radio']:checked").parent().parent();
        var nombre = tr.find(".nombre").text();
        var url = $("#lista input[type='radio']:checked").attr("data-anular_generar");
        var status = $("#lista input[type='radio']:checked").attr("data-status");
         switch(parseInt(status)){
             case 1:
                    $("#tit-alert").html("ANULAR GENERAR VENTA");
                    $("#body-alert").html('<p>Desea anular la nota de crédito  "<strong>'+nombre+'</strong>"  y generar otra?</p>');
                    $("#alert-yn").attr("data-tipo", 0);
                    $("#alert-yn").attr("data-href", url);
                    $("#alert-yn").modal();
             break;
             case 2:
                    mensaje_alerta("La nota de crédito que quiere anular y generar ya se encuentra anulada.");
             break;
            default :
            break;
         }

    }else{
        mensaje_alerta("Seleccione un item para anular y generar una nota crédito")
    }
}

$(document).ready(function(){
    $(".selectpicker").selectpicker();
    $(".datepicker").datepicker();
    $('#limpiar').click(function(){
       limpiar_form(formulario);
    });
    $(".opcion-buscar").bind('click', function(event) {
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").addClass("opcion-buscar-hide");
        $( ".container-buscador").removeClass("container-buscador-hide");
    });
    $(".buscador-cerrar").click(function(){
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").removeClass("opcion-buscar-hide");
    });
    $("#cerrar").click(function(){
        $("#error").fadeOut("slow");
     });
    $(".evento_page").each(function(){
        $(this).click(function(){
            var page = $(this).attr("data-page");
            $("#id_page").val(page);
            $("#buscar").click();
        });
    });

    $("#id_fecha").datepicker();
    $("#id_fecha").mask("9999-99-99");
    $("#id_fecha_final").datepicker();
    $("#id_fecha_final").mask("9999-99-99");

    /** Funcionalidades principales **/
    $("#ver").click(function(){ver_nota_credito()});
    $("#anular").click(function(){anular_nota_credito()});
    $("#copiar").click(function(){copiar_nota_credito()});
    $("#anular_generar").click(function(){anular_generar_nota_credito()});

    /*** Factura Electrónica ***/
    $("#nc_electronica").click(function(){enviar_nota_credito_electronica()});
    $("#enviar_mail_nc_electronica").click(function(){enviar_mail_nota_credito_electronica()});
    $(":file").filestyle({buttonText: "Buscar"});

    $("#ok-alert").on("click",function(){
        window.location.href = $("#alert-yn").attr("data-href");
    });
     /**************************
     * Enviar nota de crédito electrónica por mail
     */
     $("#ok_send_mail").click(function(){
        $(this).text("Por favor Espere ...").attr("disabled", "disabled");
        $("#cancel_envio_correo").hide();
        $.ajax(
        {
            url: url_enviar_correo,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data: new FormData($("#form_envio_correo")[0]),
            processData: false,
            contentType: false,
            beforeSend: function(){
            },
            error: function(){
                $("#ok_send_mail").text("Enviar al Correo").attr("disabled", false);
                $("#cancel_envio_correo").show();
                $("#error-envio").text("Existió un error en la conexión al servidor por favor intente más tarde");
                $(".error-modal").show()
            },
            success: function(data)
            {
                location.reload();
            }
        });
     });
      // Si existe algun error el botón de enviar correo se oculta, esta función lo muestra nuevamente
     $("#cancel_envio_correo").click(function(){
        $("#ok_send_mail").show();
     });
     // Cierra el div de error en el modal
     $(".close-error").click(function(){
       $(".error-modal").hide();
    });

});