var ID_TABLA_DETALLE = "#tabla_detalle_trans";

function numerico(event) {
    if ($.inArray(event.keyCode, [46, 8, 9, 27, 110, 13, 190]) !== -1 ||
        (event.keyCode == 65 && event.ctrlKey === true) ||
        (event.keyCode >= 35 && event.keyCode <= 39)) {
        return;
    }
    else {
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
}
/*************************************
 * Funcion para recalcular los ids
 */
function recalcular_ids(tableid){
    var nombre_form = "";
    $(tableid+" tbody tr").each(function(index)
    {
        var cont = index;
        $(this).find(":input:not(:button, .input-block-level)").each(function()
        {
            var name_campo = $(this).attr("name").split("-");
            nombre_form = name_campo[0];
            var nombre_campo = name_campo[0]+"-" + cont + "-"+name_campo[2];
            $(this).attr("name", nombre_campo);
            $(this).attr("id", "id_"+nombre_campo);
        });
    });
    $('#id_'+nombre_form+'-TOTAL_FORMS').val($(tableid+" tbody").children().length);
}
/***********************************
 * Agrega puntos decimales a los
 * inputs
 * @param e
 */
function redondear(e){
    var numero = $(e).val();

    if(numero!=""){
        if (!isNaN(numero))
            $(e).val(parseFloat(numero).toFixed(2));
        else
            $(e).val((0).toFixed(2));
    }
    else
        $(e).val((0).toFixed(2));
}

function show_data_cheque(){
    var tipo_mov = parseInt($("#id_tipo_movimiento").val());
    var check_prov = $("#id_check_provision");
    switch (tipo_mov){
        case 1:  // Ingreso
        {
            $(".paguese").show();
            $("#change_label").hide();
            $("#change_label2").show();
            $("#change_label3").hide();
            $("#datos_cheque").hide();
            $(".referencia").show();
            check_prov.hide();
            if (check_prov.is(":checked"))
            {
                $("#id_check_provision").trigger("click");  // Desmarca el check activando el evento click
            }

            break;
        }
        case 2:  // Egreso
        {
            $(".paguese").show();
            $(".referencia").hide();
            $("#change_label").show(); // paguese a la orden de:
            $("#change_label2").hide(); // recibí de:
            $("#change_label3").hide();  // Paguese/ Recibi
            $("#datos_cheque").show();
            check_prov.show();
            break;
        }
        case 3:  // Trasnferencia
        {
            $(".paguese").show();
            $(".referencia").hide();
            $("#change_label").hide(); // paguese a la orden de:
            $("#change_label2").hide(); // recibí de:
            $("#change_label3").show();  // Paguese/ Recibi
            $("#datos_cheque").hide();
            check_prov.hide();
            break;
        }
        default:  // N/D  y  N/C
        {
            $(".paguese").hide();
            $(".referencia").show();
            $("#datos_cheque").hide();
            check_prov.hide();
            break;
        }
    }
}


/***
 * Función que valida el escenario para llamar a
 * get_secuencia_ultimo_cheque(url_get_secuencia_cheque)
 * para obtener la secuencia de cheque
 */
function selectCtaBanco(){
    var tipo_mov = parseInt($("#id_tipo_movimiento").val());
    var cta_banco = $("#id_cuenta_bancaria");
    var secuencia_cheque = $("#secuencia_cheque");

    if (tipo_mov == 2 && $(cta_banco).val() != "") { // Si es Egreso

        get_secuencia_ultimo_cheque(url_get_secuencia_cheque);

    }else{
        $(secuencia_cheque).text("");
    }

}
/***
 * Función que bloque la ctas de banco tipo Ahorro
 * si el escenario es egreso por cheque al cargar
 * la pantalla
 */
function copiarAnularCtasValidate(){
    var tipo_mov = $("#id_tipo_movimiento").val();
    var motvo = $("#id_motivo").val();
    var cta_banco = $("#id_cuenta_bancaria");

    if (tipo_mov == 2 && motvo == 1){ // Si es Egreso y Cheque
        $(cta_banco).find("option").each(function(){
            // Cta.Ahorro
            if($(this).attr("data-tipo")=="1"){
                $(this).attr("disabled", true);
            }else{
                $(this).attr("disabled", false);
            }
        });
        $(cta_banco).selectpicker("refresh");
    }

    $(cta_banco).selectpicker("refresh");
}

function validar_fecha_actual_cheque(){
    var fecha_reg_ini = $("#id_fecha").val();
    var fecha_cheque = $("#id_fecha_cheque").val();

    try
    {
        var str_fecha = fecha_reg_ini.split("-");
        var date = new Date(parseInt(str_fecha[0]),parseInt(str_fecha[1]),parseInt(str_fecha[2]));
        var str_fecha_reg = fecha_cheque.split("-");
        var fecha_reg = new Date(parseInt(str_fecha_reg[0]),parseInt(str_fecha_reg[1]),parseInt(str_fecha_reg[2]));

        if(fecha_reg < date){
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Error: La fecha de cheque es menor a la fecha del registro.</p>');
            $("#alert-ok").modal();
            $("#id_fecha_cheque").val("");
        }
    }
    catch(err){

    }
}
function EliminarFila(e, fila, tableid){
    e.preventDefault();

    if($(tableid+" tbody > tr").not("td tr").length>1){
        $(fila).parent().parent().remove();
         recalcular_cliente_ids(tableid);
    }
    else{
        $("#tit-alert").html("Alerta");
        $("#body-alert").html("<strong>No se puede eliminar el Formulario.</strong>");
        $("#alert-yn").modal();
    }
}
function calcularTotal(){
    var total = 0.0;
    $(ID_TABLA_DETALLE+" tbody tr").each(function(){
        var valor = $(this).find(".valor");

        if ($(valor).val() != ""){
            total += parseFloat($(valor).val());
        }
        else{

            total += 0.0;
        }
    });

    $("#total").text("$ " +total.toFixed(2));
}


/***
 * Función que obtiene la fecha según el tipo de motivo seleccionado
 * Ingreso - Egreso
 * @param url
 */
function get_fecha_ultimo_tipo_motivo(url){
    var id_tipo_comprobante = $("#id_tipo_movimiento");
    var fecha_registro = $("#id_fecha");
    var fecha_cheque = $("#id_fecha_cheque");

    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'id_tipo_comp': $(id_tipo_comprobante).val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data){
            var fecha_reg = data.fecha_actual;

            if(data.status == 1){
                if (fecha_cheque.val() == "")
                    $(fecha_cheque).val(fecha_reg);
                $(fecha_registro).val(fecha_reg);
            }


            if(data.status == 2){
                $("#tit-alert-ok").html("Error del Servidor " +
                    "por favor comuníquese con el administrador");
                $("#body-alert-ok").html("");
                $("#alert-ok").modal();
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }

    });
}


/***
 * Función que obtiene la secuencia según la cta Bancaria
 *
 * @param url
 */
function get_secuencia_ultimo_cheque(url){
    var id_cuenta_bancaria = $("#id_cuenta_bancaria");
    var secuencia_cheque = $("#secuencia_cheque");
    var tr = $("#tabla_detalle_trans tbody").find("tr:first");
    var select = $(tr).find("select:first"); // Select de la cuenta de la primera fila

    if(id_cuenta_bancaria.val() != "")
    {
        $.ajax(
        {
            url: url,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data:{
                'id_cta_banco': $(id_cuenta_bancaria).val()
            },
             beforeSend: function(msj){
                $('#loading').show();
                $('body').css('display', '0.5');
            },
            error: function(){
                return true;
            },
            success: function(data){
                var secuencia = data.secuencia;

                if(data.status == 1){
                    $(secuencia_cheque).text(secuencia);
                }

                if(data.status == 2){
                    id_cuenta_bancaria.prop('selectedIndex', 0);
                    id_cuenta_bancaria.selectpicker("refresh");
                    $(select).prop('selectedIndex', 0);
                    $(select).selectpicker("refresh");

                    mensaje_alerta("La cuenta de banco que utilizó no es una cuenta corriente, por favor verifíque");
                    secuencia_cheque.text("");
                }

                $('#loading').fadeOut();
                $('body').css('opacity', '1');
            }

        });
    }
    else{
        mensaje_alerta("Debe seleccionar una cuenta bancaria, para obtener el número del último cheque girado")
    }
}

$(function(){

    var formulario = $("#formulario");

    $("#grabar_agregar").click(function(){
        var action = formulario.attr("action");
        formulario.attr("action", action+"?grabar_agregar=1");
        $("#grabar").click();
    });
    $("#grabar_imprimir").click(function(){
        var action = formulario.attr("action");
        formulario.attr("action", action+"?grabar_imprimir=1");
        $("#grabar").click();
    });



});

/*******************************************
 * Función para habilitar o deshabilitar las cuentas banco
 * de los dropdown dependiendo de el tipo de movimiento bancario
 */
function activar_des_cuenta_bancos_select()
{
    var tipo_mov = $("#id_tipo_movimiento");
    var cont = 0;
    if (tipo_mov.val() == "3")  // Tipo de movimiento Transferencia
    {
        $("#tabla_detalle_trans tbody").find("tr").each(function () {
            if(cont > 0)
            {
                var select = $(this).find("select:first");
                select.find("option").each(function(){
                    if($(this).attr("data-tipo") == "1"){
                        $(this).attr("disabled", false)
                    }
                });
                select.selectpicker("refresh");
            }
            cont += 1;
        });
    }
    else{  // Todas las demás opciones
        $("#tabla_detalle_trans tbody").find("tr").each(function () {
            if(cont > 0)
            {
                var select = $(this).find("select:first");
                $(select).find("option").each(function(){
                    if($(this).attr("data-tipo") == "1"){
                        $(this).attr("disabled", true);
                        if($(this).is(':selected')){
                            $(select).prop("selectedIndex", 0)
                        }
                    }
                });
                select.selectpicker("refresh");
            }
            cont += 1;
        });
    }
}
/*******************************
 * Función que inserta la cuenta de banco dentro del primer dropdown de
 * la transacción que eligió el usuario en la transacción
 * @param tipo
 */
function cuenta_banco_predeterminada(tipo){
    var obj = $("#id_cuenta_bancaria");
    var id = $(obj).val();
    var tipo_movi = parseInt($("#id_tipo_movimiento").val());
    var opt = $(obj).find("option:selected");
    var id_plan_cta = opt.attr("data-id");
    var tr = $("#tabla_detalle_trans tbody").find("tr:first");
    var debe = $(tr).find(".debe");
    var haber = $(tr).find(".haber");
    var select = $(tr).find("select:first");
    var td_select = select.parent();
    var btn_select_boots = $(td_select).find("div:first");

    $(select).attr("disabled", false);
    if (id != "")  // Escojió alguna cuenta bancaria
    {
        var cont = 0;
        $(select).find("option").each(function(){
            if($(this).attr("value") == id_plan_cta)
            {
                $(select).prop("selectedIndex", cont);
                $(select).removeClass("campo_requerido");
                $(btn_select_boots).removeClass("campo_requerido");
            }
            cont ++;
        });
    }
    else{
        $(select).prop("selectedIndex", 0);
    }

    $(select).attr("disabled", true);
    if(tipo == 1){  // Solo si cambia el tipo de movimiento se enceran los valores
        debe.val("0.00");
        haber.val("0.00");
    }

    switch (tipo_movi){
        case 1:  // Ingreso
        {
            debe.attr("disabled", false);
            haber.attr("disabled", true);
            haber.val("0.00");
            break;
        }
        case 2:  // Egreso
        {
            debe.attr("disabled", true);
            debe.val("0.00");
            haber.attr("disabled", false);
            break;
        }
        case 3:  // Transferencia
        {
            debe.attr("disabled", false);
            haber.attr("disabled", false);
            break;
        }
        case 4: // N/C
        {
            debe.attr("disabled", false);
            haber.attr("disabled", true);
            haber.val("0.00");
            break;
        }
        case 5:  // N/D
        {
            debe.attr("disabled", true);
            debe.val("0.00");
            haber.attr("disabled", false);
            break;
        }
        default :
        {
            break;
        }
    }
    $(select).selectpicker("refresh");
}

/**************************
 * Encera la columna del debe o del haber dependiendo de cual
 * de las dos haya elegido el usuario
 * @param obj
 */
function desactivar_debe_haber(obj){
    var tr = $(obj).parent().parent();
    var debe = $(tr).find(".debe");
    var haber = $(tr).find(".haber");
    if($(obj).hasClass("debe")){
        if(parseFloat($(obj).val()) > 0){
            haber.val("0.00");
        }
        else{
            haber.val("0.00");
            debe.val("0.00")
        }
    }
    else{
        if(parseFloat($(obj).val()) > 0){
            debe.val("0.00");
        }
        else{
            haber.val("0.00");
            debe.val("0.00")
        }
    }
}

/****************************
 * Función que calcula los totales del
 * debe y del haber
 */
function calcular_totales(){
    var total_debe = 0.0;
    var total_haber = 0.0;

    $("#tabla_detalle_trans tbody").find("tr").each(function () {
        var debe = parseFloat($(this).find(".debe").val());
        var haber = parseFloat($(this).find(".haber").val());
        if(!isNaN(debe))
        {
            total_debe += debe
        }
        if(!isNaN(haber)){
            total_haber += haber
        }
    });
    $("#total_debe").text(total_debe.toFixed(2));
    $("#total_haber").text(total_haber.toFixed(2));
}

$(document).ready(function(){
    // Bloquea la primera cuenta de la fila
    var select_first = $("#tabla_detalle_trans tbody").find("tr:first").find("select:first");
    select_first.attr("disabled", true);

    // Inhabilitar el borrado de la primera fila
    var btn_borrar = $("#tabla_detalle_trans tbody").find("tr:first").find(".btn-borrar");
    btn_borrar.hide();

    $('.selectpicker').selectpicker();
    cuenta_banco_predeterminada(2);  // Inhabilita la columna debe/haber dependiendo el tipo de movimiento
    calcular_totales();
    activar_des_cuenta_bancos_select();  // Habilita o deshabilita las cuentas del banco en el dropdown

    $("#tabla_detalle_trans").find("tbody tr").each(function(){
        var select_cta = $(this).find("select:first");
        var td = $(select_cta).parent();
        var div_seelct_boots = $(td).find("div:first");
        $(select_cta).change(function(){
            if( $(this).val() != ""){
                $(this).removeClass("campo_requerido");
                $(div_seelct_boots).removeClass("campo_requerido");
            }
        })
    });

    $("#id_tipo_movimiento").change(function(){
        var fecha_cheque = $("#id_fecha_cheque");
        show_data_cheque();
        get_fecha_ultimo_tipo_motivo(url_get_fecha_t);
        selectCtaBanco();
        activar_des_cuenta_bancos_select();
        cuenta_banco_predeterminada(1);
        calcular_totales();
    });

    $("#actualizar_secuencia_cheque").click(function(){
        get_secuencia_ultimo_cheque(url_get_secuencia_cheque);
    });

    $(".debe").change(function(){
        desactivar_debe_haber(this);
        redondear(this);
        calcular_totales();
    });
    $(".haber").change(function(){
        desactivar_debe_haber(this);
        redondear(this);
        calcular_totales()
    });
    $("#grabar").click(function(e){
        var tr = $("#tabla_detalle_trans tbody").find("tr:first");
        var cuenta_banco_princ = $(tr).find("select:first");
        $(cuenta_banco_princ).attr("disabled", false);
        if(!($("#total_debe").text() == $("#total_haber").text())){
           mensaje_alerta("Los valores del debe y haber no cuadran por favor verifique.");
            e.preventDefault();
        }
    });

    $("#id_paguese").change(function(){
        if($(this).val() != "")
            $(this).removeClass("campo_requerido")
    });

    $("#id_cuenta_bancaria").change(function()
    {
        var td = $(this).parent();
        var btn_select = $(td).find("div:first");
        if ($(this).val() != "")
        {
            $(this).removeClass("campo_requerido");
            $(btn_select).removeClass("campo_requerido");
        }

        selectCtaBanco();
        cuenta_banco_predeterminada(2);
        calcular_totales();
    });

    $(".datepicker").mask("9999-99-99");

    $('#id_fecha_cheque').datepicker({
         todayBtn: true,
         autoclose: true
    }).on('changeDate', function () {
        validar_fecha_actual_cheque();
    });

    // Inhabilita el boton de submit evitando que se haga más de una petición
    $("input[type='submit']").click(function(){
        $(this).attr("disabled", false);
        $("form").submit(function(){
          $("input[type='submit']").attr("disabled", true).val("Por favor, espere...");
          return true;
        });
    });

    /*********************************************
     * Inhabilitar el "enter" en los inputs para no enviar
     * el formulario con un enter
     */
        $('form :input').keypress(function(e){
            if ( e.which == 13 ) e.preventDefault();
        });

    $(".numerico").keydown(function(event) {numerico(event)});

    $(".eliminar").click(function(e){
        e.preventDefault();
        if($(ID_TABLA_DETALLE+" tbody").children().length>1){
            $(this).parent().parent().remove();
            recalcular_ids(ID_TABLA_DETALLE);
            calcular_totales();
        }
        else{
            mensaje_alerta("No se puede eliminar la fila")
        }
     });

});

$(document).ready(function(){

    $("#add").click(function(e){
        e.preventDefault();
        var clonar_tr = $(ID_TABLA_DETALLE+" tbody tr:first").clone();
        clonar_tr.find(".bootstrap-select").remove();
        clonar_tr.find(".debe").attr("disabled", false);
        clonar_tr.find(".haber").attr("disabled", false);
        clonar_tr.find(".btn-borrar").show();
        var select_cta = $(clonar_tr).find("select:first");
        var td = $(this).parent();
        var btn_select_boots = $(td).find("div:first");

        $(select_cta).removeClass("campo_requerido");
        $(btn_select_boots).removeClass("campo_requerido");

        $(select_cta).attr("disabled", false);
        $(select_cta).prop("selectedIndex", 0);
        $(select_cta).selectpicker("refresh");

        $(clonar_tr).find("select[data-name='combo_centro_costo']").prop('selectedIndex', 0);
        clonar_tr.find(".eliminar").click(function(e){
            e.preventDefault();

            if($(ID_TABLA_DETALLE+" tbody").children().length>1){
                $(this).parent().parent().remove();
                recalcular_ids("#tabla_detalle_trans");
                calcular_totales();
            }
            else{
                mensaje_alerta("No se puede eliminar la fila")
            }
        });
        clonar_tr.find("input").each(function( index ) {
            $(this).val("");
            $(this).parent().find(".error").remove();
        });

        clonar_tr.find(".debe").change(function(){
            desactivar_debe_haber(this);
            redondear(this);
            calcular_totales();
        });
        clonar_tr.find(".haber").change(function(){
            desactivar_debe_haber(this);
            redondear(this);
            calcular_totales()
        });
        /********************************  Númerico ************************************************/
         $(clonar_tr.find(".numerico")).keydown(function(event) {
            if ( $.inArray(event.keyCode,[46,8,9,27,110,13,190]) !== -1 ||
                (event.keyCode == 65 && event.ctrlKey === true) ||
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     return;
            }
            else {
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });
        /***************************************************************************************/

        // Refresca los combos selectpicker
        $(clonar_tr).find(".selectpicker").selectpicker("refresh");
        $(ID_TABLA_DETALLE+" tbody").append(clonar_tr);

        recalcular_ids(ID_TABLA_DETALLE);
        activar_des_cuenta_bancos_select();
    });

});