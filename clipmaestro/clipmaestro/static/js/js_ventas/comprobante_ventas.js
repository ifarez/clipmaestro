var URLbusquedaItem = getURLBusquedaItem();
var URLcantidadBodega = getCantidadBodega();
var URLseleccionDocumento = getURLSeleccionarDocumento();
var URLbusquedaDocumentos = getURLBusquedaDocuementos();
var URLclienteItemVenta = getClienteItemVenta();
var URLExisteItemBodega = getExisteItemBodega();
var contador = 0;
var border, outline, box_shadow;

//Desactiva los opciones de agregar concepto e items
function puede_agregar_fila(event){
    var filas = 40-1;
    var cantidad = $('input.detalle_vent').length;
    // Returna verdadero si puede agregar la fila del detalle de venta
    return cantidad <= filas;
}

$(function(){
    $("#grabar_facturar").click(function(){
        var action = $("#formulario").attr("action");
        $("#formulario").attr("action", action+"?facturar=1");
        $("#guardar").click();
    });

    $("#grabar_agregar").click(function(){
        var action = $("#formulario").attr("action");
        $("#formulario").attr("action", action+"?grabar_agregar=1");
        $("#guardar").click();
    });

    $("#grabar_imprimir").click(function(){
        var action = $("#formulario").attr("action");
        $("#formulario").attr("action", action+"?grabar_imprimir=1");
        $("#guardar").click();
    });
});

window.onload = function(){
    // Para mostrar la fecha de vencimiento al iniciar si es que existe un plazo ya definido "enviado desde el servidor"
    MostrarFechaVenci($("#id_plazo"));
    // Función para agregar clientes dentro de la venta
    $("#cerrar").click(function(){
        $("#error").fadeOut("slow");
    });
    $("#add_cliente").click(function(){
        formCliente($("#id_tipo_identificacion").val());

        $("#id_tipo_identificacion").change(function(){
           formCliente($("#id_tipo_identificacion").val());
        });

        $("#error").hide();
        $("#id_tipo_identificacion").prop('selectedIndex', 0).selectpicker("refresh");
        $("#id_Num_Identificacion").val("");
        $("#id_razon_social").val("");
        $("#id_ciudad_direccion").prop('selectedIndex', 0).selectpicker("refresh");
        $("#id_descripcion").val("");
        $("#id_direccion1").val("");
        $("#error-cliente").html("");
        $("#div-cliente").modal();
    });

    $("#editar_cliente").click(function(){
        formCliente($("#id_tipo_identificacion").val());
        $("#id_tipo_identificacion").change(function(){
           formCliente($("#id_tipo_identificacion").val());
        });
        $("#error").hide();
        $("#div-cliente-editar").modal();
    });


    $("#send_form_cliente").click(function(e){ AgregarCliente(e) });

    /****************************************** VALIDACIÓN DE FECHA CON DOCUMENTO VIGENTE *****************************/
    cantidadEntregada();

    $("#id_descuento_porc").change(function(){
        if (es_stock == 1){
            columnahide(ID_TABLA_VENTA_STOCK);
            iteraccionCombosDescuento(ID_TABLA_VENTA_STOCK);
            calcularSubtotalesStock();
            DescuetosPorVentaStock();
        }else{
            columnahide(ID_TABLA_VENTA);
            iteraccionCombosDescuento(ID_TABLA_VENTA);
            calcularSubtotalesPepa();
            DescuetosPorVenta();
        }
    });

    $("#id_tipo_descuento").change(function(){
        if (es_stock == 1){
            columnahide(ID_TABLA_VENTA_STOCK);
            iteraccionCombosDescuento(ID_TABLA_VENTA_STOCK);
            calcularSubtotalesStock();
            DescuetosPorVentaStock();
        }else{
            columnahide(ID_TABLA_VENTA);
            iteraccionCombosDescuento(ID_TABLA_VENTA);
            calcularSubtotalesPepa();
            DescuetosPorVenta();
        }

    });

    $("#id_monto_descuento").focusout(function(){
        $(this).removeClass("campo_requerido");
            if($(this).val() >100){
                $("#tit-alert-ok").html("Alerta");
                $("#body-alert-ok").html('<p>Por Favor ingrese un porcentaje válido.</p>');
                $("#alert-ok").modal();
                $(this).val("");
                $(this).addClass("campo_requerido");
            }else{

                if (es_stock == 1){
                    calcularSubtotalesStock();
                    DescuetosPorVentaStock();
                }else{
                    calcularSubtotalesPepa();
                    DescuetosPorVenta();
                }

            }

    });

    $("#id_monto_descuento_doce").focusout(function(){
        $(this).removeClass("campo_requerido");
        if (es_stock == 1){
            calcularSubtotalesStock();
            DescuetosPorVentaStock();
        }else{
            calcularSubtotalesPepa();
            DescuetosPorVenta();
        }

    });

    $("#id_monto_descuento_cero").focusout(function(){
        $(this).removeClass("campo_requerido");
            if ($(this).val() == ""){
                $(this).val(0.0);
            }
        if (es_stock == 1){
            calcularSubtotalesStock();
            DescuetosPorVentaStock();
        }else{
            calcularSubtotalesPepa();
            DescuetosPorVenta();
        }
    });

    $(".precio_u").focusout(function(){
        calcularSubtotalesPepa();
        DescuetosPorVenta();
        agregar_punto_VENTA(this);
    });

    $(".iva").focusin(function(){
        calcularSubtotalesPepa();
        DescuetosPorVenta();
    });

    $(".descuento").focusout(function(){
        if ($(this).val()==""){
            $(this).val(0.0);
        }

        if (es_stock == 1){
            calcularSubtotalesStock();
            DescuetosPorVentaStock();
        }else{
            calcularSubtotalesPepa();
            DescuetosPorVenta();
        }
    });

    $(".cantidad").change(function(){
        var tr = $(this).parent().parent();
        var item = $(tr).find(".item");
        var cant_entregada = $(tr).find(".cant_entregada");
        var subtotal = $(tr).find(".subtotal");

        $(this).removeClass("campo_requerido");
        $(subtotal).removeClass("campo_requerido");

        $(ID_TABLA_VENTA).find(".descuento").each(function(){
             if ($(this).val()==""){
                 $(this).val(0.0);
             }
        });

        if($(item).val() != ""){
            agregar_punto_VENTA(this);
            cantidadBodegaVenta(this, URLcantidadBodega);
        }

        else{

              $("#tit-alert-ok").html("Alerta");
              $("#body-alert-ok").html("Seleccione un item, para realizar la venta.");
              $(this).val("");
              $("#alert-ok").modal();
        }

         if (parseFloat($(this).val()) == 0.0){
              $("#tit-alert-ok").html("Alerta");
              $("#body-alert-ok").html("Ingrese una cantidad correcta, para realizar la venta.");
              $(this).val("");
              $(cant_entregada).val("");
              $("#alert-ok").modal();
         }

        cantidadEntregada();
        iteraccionCombosDescuento(ID_TABLA_VENTA);
        calcularSubtotalesPepa();
        DescuetosPorVenta();
    });

    $(".cant_entregada").change(function(){
        var tr =  $(this).parent().parent();
        var item = $(tr).find(".item");

        calcularSubtotalesPepa();
        DescuetosPorVenta();

        if($(item).val() != ""){
            agregar_punto_VENTA(this);
            cantidadEntregada();
            cantidadBodegaVenta(this, URLcantidadBodega);

            iteraccionCombosDescuento(ID_TABLA_VENTA);
            calcularSubtotalesPepa();
            DescuetosPorVenta();

        }else{
              $("#tit-alert-ok").html("Alerta");
              $("#body-alert-ok").html("Seleccione un item, para realizar la venta.");
              $(this).val("");
              $("#alert-ok").modal();
        }
    });

    $("#id_fecha_inicio" ).mask("9999-99-99").datepicker({
             todayBtn: true,
             autoclose: true

    }).on('changeDate', function(ev){
        $(this).datepicker("hide");
        ValidarFechaGuia();
        validar_fecha_actual(this);
    });

    $("#id_fecha_final").datepicker().on('changeDate', function(ev){
        $(this).datepicker("hide");
        ValidarFechaGuia();
    });


};

$(document).ready(function(){

    $("#add_cliente").show();
    $("#editar_cliente").hide();

    /***ejeeeee*/
    $(".item_stock").change(function(e){
       SelectITemParametros(this);
       ExisteItemBodegaStock(this, URLExisteItemBodega);
       //ValidateItemsRepetidosStock(this, ID_TABLA_VENTA_STOCK);
    });

    $(".eliminar_stock").click(function(e){
         EliminarFila(e,this,ID_TABLA_VENTA_STOCK);
    });

    $("#add_stock").click(function(e){
        AgregarDetalleVentaStock(e);

    });

     $(".precio_u_stock").change(function(){
        calcularSubtotalesStock();
        DescuetosPorVentaStock();
        agregar_punto_VENTA(this);
    });

    $(".cantidad_stock").change(function(){
        var tr = $(this).parent().parent();
        var item = $(tr).find(".item_stock");
        var subtotal = $(tr).find(".subtotal_stock");

        $(this).removeClass("campo_requerido");
        $(subtotal).removeClass("campo_requerido");

        $(ID_TABLA_VENTA_STOCK).find(".descuento").each(function(){
             if ($(this).val()==""){
                 $(this).val(0.0);
             }
        });

        if($(item).val() != ""){
            agregar_punto_VENTA(this);
            cantidadBodegaVentaStock(this, url_get_cantidad_disponible_stock);
        }

        else{

              $("#tit-alert-ok").html("Alerta");
              $("#body-alert-ok").html("Seleccione un item, para realizar la venta.");
              $(this).val("");
              $("#alert-ok").modal();
        }

         if (parseFloat($(this).val()) == 0.0){
              $("#tit-alert-ok").html("Alerta");
              $("#body-alert-ok").html("Ingrese una cantidad correcta, para realizar la venta.");
              $(this).val("");
              $("#alert-ok").modal();
         }

        iteraccionCombosDescuento(ID_TABLA_VENTA_STOCK);
        calcularSubtotalesStock();
        DescuetosPorVentaStock();
    });

    presentar_ocultar_anular_generar(tipo);

    $("#id_cliente").change(function(){
        flag_post_venta = 0;

        if($(this).val() != ""){
            $("#add_cliente").hide();
            $("#editar_cliente").show();

        }else{
            $("#add_cliente").show();
            $("#editar_cliente").hide();
        }

        seleccionarDirecciones();
        getSelectContratos(getContratos());
        SelectClienteItem(this, URLclienteItemVenta);

        /*** Mitigar ****/
        if (es_stock == 1){
            iteraccionCombosDescuento(ID_TABLA_VENTA_STOCK);
            calcularSubtotalesStock();
            DescuetosPorVentaStock();
        }else{
            iteraccionCombosDescuento(ID_TABLA_VENTA);
            calcularSubtotalesPepa();
            DescuetosPorVenta();
        }

    });

    $("#id_plazo").focusout(function(){MostrarFechaVenci(this)});
    /*************************************************************
     * Funcion para Credito o Contado
     * Credito cod 2
     * Contado cod 1
     */
    $("#id_forma_pago").change(function(){
       $(this).find("option").each(function(){
          if(this.selected){
           if($(this).attr("value")==2){
               $("#id_plazo").attr("disabled",false);
               $("#id_plazo").addClass("campo_requerido");
           }
           else{
                if($(this).attr("value")==1){
                    $("#id_plazo").val("");
                    $("#id_plazo").removeClass("campo_requerido");
                    $("#id_plazo").attr("disabled",true);
                    $("#dia_vencimiento").html("");

                }
           }
          }
       });
   });

    $(".tr-agregar-concepto").click(function(event){
        if(puede_agregar_fila())
            agregarconcepto(this);
     });

    $(".numerico").each(function(){
        $(this).keydown(function(event){
            Numerico(event);
        }).change(function(event){
                redondear(this);
        });
    });

    $(".selectpicker").change(function(){
        AgregarTitulo(this);
    });

    $("#id_serie").change(function(e){
        SelectSerieNum();
    });

    $(".item").change(function(e){
       var tr = $(this).parent().parent();
       var cantidad_t = $(tr).find(".cantidad");
       var cantidad_enntregada = $(tr).find(".cant_entregada");
       var subtotal_t = $(tr).find(".subtotal");
       SelectItem(this, URLbusquedaItem);
       //ValidateItemsRepetidos(this, ID_TABLA_VENTA);

       if($(this).val() != ""){
            cantidadEntregada();
            iteraccionCombosDescuento();

            calcularSubtotalesPepa();
            DescuetosPorVenta();
       }
       else{
            cantidad_t.val("");
            cantidad_enntregada.val("");
            subtotal_t.val("");
            cantidadEntregada();
            iteraccionCombosDescuento();
            calcularSubtotalesPepa();
            DescuetosPorVenta();
       }
   });

    /*********************************************
     * Inhabilitar el "enter" en los inputs para no enviar
     * el formulario con un enter
     */
    $('form :input').keypress(function(e){
            if ( e.which == 13 ) e.preventDefault();
       });

    if($("#id_plazo").attr("value")==undefined){
        if($("#id_forma_pago").val()==1)
            $("#id_plazo").attr("disabled",true);
    }
    $(".eliminar").click(function(e){
        e.preventDefault();
        if($(ID_TABLA_VENTA+" tbody").children().length>1){
            $(this).parent().parent().remove();
            cantidadEntregada();
            presentaForm();
            iteraccionCombosDescuento();
            calcularSubtotalesPepa();
            DescuetosPorVenta();
            recalcular_ids_Venta(ID_TABLA_VENTA);
        }
        else{
           validarEliminar(1);
        }
   });


    /******************************************* CLONAR DETALLE ***************************************************/
    $("#add").click(function(e){
        if(puede_agregar_fila())
        {
             var clonar_tr2 = $(ID_TABLA_VENTA+" tbody tr:first").clone();
             clonar_tr2.find(".bootstrap-select").remove();
             clonar_tr2.find(".eliminar").click(function(e){
                e.preventDefault();
                if($(ID_TABLA_VENTA+" tbody").children().length>1){
                    $(this).parent().parent().remove();
                    cantidadEntregada();
                    presentaForm();
                    iteraccionCombosDescuento();
                    calcularSubtotalesPepa();
                    DescuetosPorVenta();
                    recalcular_ids_Venta(ID_TABLA_VENTA);

                }else{
                     validarEliminar(1);
                }
            });

             $(clonar_tr2.find(".cantidad")).addClass("numerico").val("");
             $(clonar_tr2.find(".cant_entregada")).addClass("numerico").val("");
             $(clonar_tr2.find(".cant_entregada")).attr("disabled", false);

              var ul =   $(clonar_tr2.find("ul"));
              var i = 0;

            $(ul).find("li").each(function(){
                if (i!=0){
                    $(this).remove();
                }
                 i++;
             });
             $(ul).find(".tr-agregar-concepto").click(function(event){
                if(puede_agregar_fila())
                    agregarconcepto(this);
             });


             $(ul).find(".tr-eliminar-concepto").click(function(){
                eliminarconcepto(this);
             });

            $(clonar_tr2.find(".item")).attr("disabled", false);
            $(clonar_tr2).find("select[data-name='combo_centro_costo']").prop('selectedIndex', 0);
            $(clonar_tr2.find(".selectpicker")).change(function(){
                    AgregarTitulo(this);
             });
            $(clonar_tr2.find(".iva")).val("");
            $(clonar_tr2.find(".ice")).val("");
            $(clonar_tr2.find(".subtotal")).val("");
            $(clonar_tr2.find(".descuento")).val("");
            $(clonar_tr2.find(".concepto")).val("");
            $(clonar_tr2.find(".item")).val("");
            $(clonar_tr2.find('.precio_u').empty());
            $(clonar_tr2.find(".item")).change(function(e){
                   var tr = $(this).parent().parent();
                   var cantidad_t = $(tr).find(".cantidad");
                   var cantidad_enntregada = $(tr).find(".cant_entregada");
                   var subtotal_t = $(tr).find(".subtotal");

                   SelectItem(this, URLbusquedaItem);
                   //ValidateItemsRepetidos(this, ID_TABLA_VENTA);

                   if($(this).val() != ""){
                        cantidadEntregada();
                        iteraccionCombosDescuento();
                        calcularSubtotalesPepa();
                        DescuetosPorVenta();

                   }
                   else{
                        cantidad_t.val("");
                        cantidad_enntregada.val("");
                        subtotal_t.val("");
                        cantidadEntregada();
                        iteraccionCombosDescuento();
                        calcularSubtotalesPepa();
                        DescuetosPorVenta();
                   }
             });

            $(clonar_tr2.find(".selectpicker")).change(function(){
                AgregarTitulo(this);
            });

             /**************************************  Númerico ************************************************************/
                $(clonar_tr2.find(".numerico")).each(function(){
                    $(this).keydown(function(event){
                        Numerico(event);
                    }).change(function(event){
                            redondear(this);
                    });
                });

             $(clonar_tr2.find(".cant_entregada")).change(function(){
                 var tr = $(this).parent().parent();
                 var item = $(tr).find(".item");

                calcularSubtotalesPepa();
                DescuetosPorVenta();

                 if($(item).val() != ""){
                    agregar_punto_VENTA(this);
                    cantidadEntregada();
                    cantidadBodegaVenta(this, URLcantidadBodega);

                    iteraccionCombosDescuento();
                    calcularSubtotalesPepa();
                    DescuetosPorVenta();

                 }else{

                      $("#tit-alert-ok").html("Alerta");
                      $("#body-alert-ok").html("Seleccione un item, para realizar la venta.");
                      $(this).val("");
                      $("#alert-ok").modal();
                    }
            });

             $(clonar_tr2.find(".cantidad")).change(function(e){
                 var tr = $(this).parent().parent();
                 var item = $(tr).find(".item");
                 var cant_entregada = $(tr).find(".cant_entregada");
                 var subtotal = $(tr).find(".subtotal");

                $(this).removeClass("campo_requerido");
                $(subtotal).removeClass("campo_requerido");

                $(ID_TABLA_VENTA).find(".descuento").each(function(){
                     if ($(this).val()==""){
                         $(this).val(0.0);
                     }
                });

                if($(item).val() != ""){
                    agregar_punto_VENTA(this);
                    cantidadBodegaVenta(this, URLcantidadBodega);
                }
                else{

                      $("#tit-alert-ok").html("Alerta");
                      $("#body-alert-ok").html("Seleccione un item, para realizar la venta.");
                      $(this).val("");
                      $("#alert-ok").modal();
                }

                if (parseFloat($(this).val()) == 0.0){
                      $("#tit-alert-ok").html("Alerta");
                      $("#body-alert-ok").html("Ingrese una cantidad correcta, para realizar la venta.");
                      $(this).val("");
                      $(cant_entregada).val("");
                      $("#alert-ok").modal();
                }

                cantidadEntregada();
                calcularSubtotalesPepa();
                iteraccionCombosDescuento();
                DescuetosPorVenta();
             });

             $(clonar_tr2.find(".iva")).focusout(function(e){
                 iteraccionCombosDescuento();
                 calcularSubtotalesPepa();
                 DescuetosPorVenta();
             });
             $(clonar_tr2.find(".ice")).focusout(function(e){
                  iteraccionCombosDescuento();
                  calcularSubtotalesPepa();
                  DescuetosPorVenta();
             });
             $(clonar_tr2.find(".precio_u")).val("");
             $(clonar_tr2.find(".precio_u")).focusout(function(e){
                 iteraccionCombosDescuento();
                 calcularSubtotalesPepa();
                 DescuetosPorVenta();
                 agregar_punto_VENTA(this);
             });
             $(clonar_tr2.find(".concepto")).focusout(function(e){
                  calcularSubtotalesPepa();
                  DescuetosPorVenta();
             });
             $(clonar_tr2.find(".descuento")).focusout(function(e){
                if ($(this).val()==""){
                    $(this).val(0.0);
                }
                 calcularSubtotalesPepa();
                 DescuetosPorVenta();
             });

             // Refresca los combos selectpicker
             $(clonar_tr2).find(".selectpicker").selectpicker("refresh");
             $(ID_TABLA_VENTA+" tbody").append(clonar_tr2);
             recalcular_ids_Venta(ID_TABLA_VENTA);

            $(clonar_tr2).find(':input').keypress(function(e){
                if ( e.which == 13 ) e.preventDefault();
            });

            // LLama a las funciones que limitan la cantidad de texto que puede escribir
            clonar_tr2.find('.detalle_vent').keydown(function(event){
                var li = $(this).parent();
                adjust(event, li);
                liminar_fila_concepto(this);
            }).keyup(function(){
                liminar_fila_concepto(this);
                });
        }
   });

    $("#guardar").click(function(e){
        var tabla = $("#tabla_formset_venta_test");
        tabla.find("tbody tr").each(function(){
            var input_conc = $(this).find(".concepto_item");
            var text_input = "";
            $(this).find(".detalle_vent").each(function(index, element){
                if(index > 0)
                    text_input += deli;
                text_input += $(this).val();
            });
            input_conc.val(text_input);
        });
    });
    llenar_fila_concepto();
});

//Limita el ancho de la fila
function adjust(event, obj)
{
    if($(obj).find(".spacer").width() < 400){
        return true;
    }
    else{
        if(event.keyCode == 8 || event.keyCode == 46)
        {
            return true;
        }
        else
        {
            event.preventDefault();
        }
    }
}

/***********************************************
 * Copia el valor del input detalle_venta a un span
 * el cual va a crecer dependiendo de su contenido,
 * con ese valor me guío para limitar su escritura
 * @param obj
 */
function liminar_fila_concepto(obj){
    var li = $(obj).parent();
    $(li).find('.spacer').html( $(li).find('.detalle_vent').val() );
    $(li).find('.ancho').html( $(li).find(".spacer").width()+"px" );
}

/************************************************
 * Agrega una fila de detalle de concepto de la venta del item
 * retorna un objeto de tipo input ("detalle venta") es para obtener un input
 * y llenarlo cuando se haya hecho un envio de datos y nos retorne los valores
 * @param obj
 * @returns {*}
 */
function agregarconcepto(obj)
{
    var ul = $(obj).parent().parent();
    var li_first = ul.find("li:first").clone();
    li_first.find(".tr-agregar-concepto").hide();
    li_first.find(".tr-eliminar-concepto").show().click(function(){
        eliminarconcepto(this);
    });
    li_first.find(".detalle_vent").val("");
    li_first.find(".spacer").html("");
    li_first.find(".ancho").html("0px");
    ul.append(li_first);


    $(li_first.find(".detalle_vent")).focus().keydown(function(event){
        adjust(event, li_first);
        liminar_fila_concepto(this);
    }).keyup(function(){
            liminar_fila_concepto(this);
        });

    num_filas++;
    return li_first.find(".detalle_vent");
}
/***************************
 * Elimina la fila del detalle del concepto del item
 * @param obj
 */
function eliminarconcepto(obj)
{
    var li = $(obj).parent();
    var ul = $(obj).parent().parent();
    if(ul.find("li").length > 1)
    {
        $(li).remove();
        num_filas--;

    }
    activar_filas();
}
/**********************************
 * Función que llena los detalles del concepto del item
 * cuando hay un regreso de datos desde el servidor
 **********************************/
function llenar_fila_concepto()
{
    var tabla = $(ID_TABLA_VENTA);
    tabla.find("tbody tr").each(function(){
        var input_conc = $(this).find(".concepto_item");
        var detalle_vent = $(this).find(".detalle_vent");
        var btn_agregar = $(this).find(".tr-agregar-concepto");
        var text_input = "";
        var split_det_vent = input_conc.val().split(deli);
        for(var i=0; i<split_det_vent.length; i++ )
        {
            if(i==0)
                detalle_vent.val(split_det_vent[i]);
            if(i>0)
            {
                var concepto = agregarconcepto(btn_agregar);
                concepto.val(split_det_vent[i]);
            }
        }
    });
}




