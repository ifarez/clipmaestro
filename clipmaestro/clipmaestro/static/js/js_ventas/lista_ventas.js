/**
 * Created by Roberto on 08/09/14.
 */
function tiene_ret_venta(){
    var estado = null;
    $.ajax(
    {
        url: url_tiene_retencion_venta,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {
            'id': $("#lista input[type='radio']:checked").val()
        },
        beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            estado = null;
            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        },
        success: function(data)
        {
            estado = parseInt(data.status);
            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
    return estado;
}

function tiene_guia_remision_venta(){
    var estado= null;
    $.ajax({
        url: url_tiene_guia_remision,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {
            'id': $("#lista input[type='radio']:checked").val()
        },
         error: function(){
             estado = null;
        },
         success: function(data)
        {
            estado = parseInt(data.status);
        }
    });
    return estado
}

/***
 * Función que verifica y llama a la vista que envía la factura al SRI
 */
function enviar_factura_electronica(){
    var input_check = $("#lista").find("input[type='radio']:checked");

    if (input_check.length != 0){
        var tr = input_check.parent().parent();
        var nombre = tr.find(".nombre").text();
        var num_doc = tr.find(".num_doc").text();
        var url = input_check.attr("data-enviar_factura_electronica");
        var status = input_check.attr("data-status");
        var estado_firma = input_check.attr("data-estado_firma");

        if(status == "1"){
            mensaje_alerta_yn("Desea autorizar la factura con  #"+num_doc+" , de la venta #" + nombre+ " ?", "AUTORIZAR FACTURA SRI", "data-href", url);
        }
        else{
            mensaje_alerta("La venta se encuentra anulada");
        }

    }else{
        mensaje_alerta("Seleccione una factura para enviarla al SRI");
    }
}

/***
 * Función para enviar al email factura electrónica
 */
function enviar_mail_factura_electronica(){
    var input_check = $("#lista").find("input[type='radio']:checked");

    if (input_check.length != 0){
        var tr = input_check.parent().parent();
        var nombre = tr.find(".nombre").text();
        var num_doc = tr.find(".num_doc").text();
        var cliente = tr.find(".razon_social").text();
        var url = input_check.attr("data-enviar_correo");
        var status = input_check.attr("data-status");
        var estado_firma = parseInt(input_check.attr("data-estado_firma"));
        var modal_correo = $("#envio_correo");
        $(".error-modal").hide();

        if(status == "1")
        {
            /*
            mensaje_alerta_yn("Desea enviar la venta con  #: "+num_doc + " autorizada por el SRI, al cliente: "+cliente,
                "ENVIAR FACTURA ELECTRÓNICA AL CORREO", "data-href", url);
            */
            if(estado_firma >= 4){
                $.ajax(
                {
                    url: url_get_correo,
                    type: 'POST',
                    async: false,
                    cache: false,
                    timeout: 150,
                    data: {
                        'id': input_check.attr("value")
                    },
                    beforeSend: function(){
                    },
                    error: function(){
                        $("#correo_cliente").text("Hubo un error de conexión, por favor cierre la ventana e intente nuevamente");
                        $("#ok_send_mail").hide();
                    },
                    success: function(data)
                    {
                        if(data.status=1)
                        {
                            $("#correo_cliente").text(data.correo);
                        }
                        else
                        {
                            $("#correo_cliente").text(data.mensaje);
                        }
                    }
                });

                $("#id_id_venta").val(input_check.attr("value"));
                modal_correo.modal();

            }else{
                mensaje_alerta("La venta que seleccionó no tiene su factura autorizada por el SRI, " +
                    "por favor verifique")
            }

        }else{
            mensaje_alerta("La venta se encuentra anulada");
        }

    }else{
        mensaje_alerta("Seleccione una factura firmada electrónicamente y autorizada por el SRI " +
            "para enviarla al correo del cliente");
    }
}
/***
 * Funcionalidad que llama al detalle de la venta
 */
function ver_venta(){
    var status = $("#lista input[type='radio']:checked").attr("data-status");

    if ($("#lista input[type='radio']:checked").length != 0){
        window.location.href = $("#lista input[type='radio']:checked").attr("data-ver");
    }else{
        mensaje_alerta("Seleccione un item para ver el datalle de la venta");
    }
}
/***
 * Funcionalidad que llama a la vista para usar la venta provisionada
 */
function provision_venta(){
    if ($("#lista input[type='radio']:checked").length != 0){
        var tr = $("#lista input[type='radio']:checked").parent().parent();
        var status = $("#lista input[type='radio']:checked").attr("data-status");
        var url = $("#lista input[type='radio']:checked").attr("data-provision");
        var nombre = tr.find(".nombre").text();

        if(status==3){
             mensaje_alerta_yn("Desea usar esta venta provisionada  " + nombre+" ?", "PROVISIÓN VENTA", "data-href", url);
        }else{

            if(status == 2){
                mensaje_alerta("La venta que quiere provisionar se encuentra anulada");
            }else{
                mensaje_alerta("La venta que seleccionó no se puede usar para la provisión");
            }
        }
    }else{
        mensaje_alerta("Seleccione un item para ver el datalle de la venta");
    }
}

/***
 * Funcionalidad que llama a la vista de anular la venta
 */
function anular_venta(){
    if ($("#lista input[type='radio']:checked").length != 0){
        var tr = $("#lista input[type='radio']:checked").parent().parent();
        var nombre = tr.find(".nombre").text();
        var url = $("#lista input[type='radio']:checked").attr("data-anular");
        var status = $("#lista input[type='radio']:checked").attr("data-status");
        var flag_n_c = $("#lista input[type='radio']:checked").attr("data-flag-n-c");
        var sw = false;

        if (flag_n_c == "True"){
            sw = true;
        }
        if (flag_n_c == "False"){
            sw = false;
        }

        switch(parseInt(status)){
            case 1:
                if (sw){
                        mensaje_alerta("La Venta seleccionada: "+nombre+"no puede ser anulada, ya que se generó N/C con la misma.");
                }else{
                        mensaje_alerta_yn("Desea anular la venta provisionada con número de comprobante: "+ nombre+ " ?", "ANULAR VENTA", "data-href", url);
                }
            break;
            case 2:
                mensaje_alerta("La Venta ya se encuentra anulada");
            break;

            case 3:
                mensaje_alerta_yn("Desea anular la venta provisionada con número de comprobante: "+ nombre+ " ?", "ANULAR VENTA", "data-href", url);
            break;
            default :
            break;
        }

    }else{
        mensaje_alerta("Seleccione un item para anular una venta");
    }
}
/***
 * Función que llama a la vista copiar venta
 */
function copiar_venta(){
    if ($("#lista input[type='radio']:checked").length != 0){
        var status = $("#lista input[type='radio']:checked").attr("data-status");
        switch(parseInt(status)){
            case 1:
                window.location.href = $("#lista input[type='radio']:checked").attr("data-copiar");
            break;
            case 2:
                mensaje_alerta("No puede copiar La Venta, debido a que se encuentra anulada.");
            break;
            case 3:
                 mensaje_alerta("No puede copiar una venta provisionada.");
            break;
                default:
            break;
        }
    }else{
        mensaje_alerta("Seleccione un item para copiar una venta");
    }
}

/***
 * Función para anular generar venta
 */
function anular_generar_venta(){
    if ($("#lista input[type='radio']:checked").length != 0){
        var tr = $("#lista input[type='radio']:checked").parent().parent();
        var nombre = tr.find(".nombre").text();
        var url = $("#lista input[type='radio']:checked").attr("data-anular_generar");
        var status = $("#lista input[type='radio']:checked").attr("data-status");
        var flag_n_c = $("#lista input[type='radio']:checked").attr("data-flag-n-c");
        var sw = false;

        if (flag_n_c == "True"){
            sw = true;
        }
        if (flag_n_c == "False"){
            sw = false;
        }

         switch(parseInt(status)){
             case 1:
                 if(sw){
                        mensaje_alerta("La Venta seleccionada: "+ nombre + "no puede ser anulada, tampoco generar otra, ya que se realizó N/C con la misma.");
                 }else{
                        $("#tit-alert").html("ANULAR GENERAR VENTA");
                        $("#body-alert").html('<p>Desea anular la venta  "<strong>'+nombre+'</strong>"  y generar otra?</p>');
                        $("#alert-yn").attr("data-tipo", 0);
                        $("#alert-yn").attr("data-href", url);
                        $("#alert-yn").modal();
                 }
             break;
             case 2:
                    mensaje_alerta("La Venta que quiere anular y generar, ya se encuentra anulada.");
             break;
             case 3:
                 mensaje_alerta_yn("Desea anular la venta provisionada con número de comprobante: "+ nombre + "?", "ANULAR GENERAR VENTA", "data-href", url);
                    
            break;
            default :
            break;
         }

    }else{
        mensaje_alerta("Seleccione un item para anular y generar una venta")
    }
}

$(document).ready(function(e){
    $(".selectpicker").selectpicker();
    $("#id_fecha_ret").mask("9999-99-99");
    $('#limpiar').click(function(){
       limpiar_form(formulario);
    });
    $(".opcion-buscar").bind('click', function(event) {
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").addClass("opcion-buscar-hide");
        $( ".container-buscador").removeClass("container-buscador-hide");
    });
    $(".buscador-cerrar").click(function(){
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").removeClass("opcion-buscar-hide");
    });
    $("#cerrar").click(function(){
        $("#error").fadeOut("slow");
     });
    $(".evento_page").each(function(){
        $(this).click(function(){
            var page = $(this).attr("data-page");
            $("#id_page").val(page);
            $("#buscar").click();
        });
    });
    $("#id_fecha_reg").mask("9999-99-99").datepicker({
                     todayBtn: true,
                     autoclose: true
    }).on('changeDate', function(ev){
        $(this).datepicker("hide");

    });

    /** Funcionalidades principales **/
    $("#ver").click(function(){ver_venta()});
    $("#provision").click(function(){provision_venta()});
    $("#anular").click(function(){anular_venta()});
    $("#copiar").click(function(){copiar_venta()});
    $("#anular_generar").click(function(){anular_generar_venta()});

    /*** Factura Electrónica ***/
    $("#factura_electronica").click(function(){enviar_factura_electronica()});
    $("#enviar_mail_factura_electronica").click(function(){enviar_mail_factura_electronica()});

    $(":file").filestyle({buttonText: "Buscar"});

    /**************************
     * Enviar factura electrónica por mail
     */
     $("#ok_send_mail").click(function(){
        $(this).text("Por favor Espere ...").attr("disabled", "disabled");
        $("#cancel_envio_correo").hide();

        $.ajax(
        {
            url: url_enviar_correo,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data: new FormData($("#form_envio_correo")[0]),
            processData: false,
            contentType: false,
            error: function(){
                $("#ok_send_mail").text("Enviar al Correo").attr("disabled", false);
                $("#cancel_envio_correo").show();
                $("#error-envio").text("Existió un error en la conexión al servidor por favor intente más tarde");
                $(".error-modal").show()
            },
            success: function(data)
            {
                location.reload();
            }
        });
     });

     // Si existe algun error el botón de enviar correo se oculta, esta función lo muestra nuevamente
    $("#cancel_envio_correo").click(function(){
        $("#ok_send_mail").show();
    });

    // Cierra el div de error en el modal
    $(".close-error").click(function(){
       $(".error-modal").hide();
    });


    /******************************************** FUNCIONALIDADES VENTAS - GUIA REMISION ****************************/
    $("#agregar-guia-rem").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            var tr = $("#lista input[type='radio']:checked").parent().parent();
            var nombre = tr.find(".nombre").text();
            var status = $("#lista input[type='radio']:checked").attr("data-status");
            var flag = $("#lista input[type='radio']:checked").attr("data-flag-guia");
            var sw = false;

                if (flag == "True"){
                    sw=true;
                }
                if (flag == "False"){
                    sw=false;
                }

                switch(parseInt(status)){
                     case 1:
                         if (sw){
                            window.location.href = $("#lista input[type='radio']:checked").attr("data-guia-rem-venta");
                         }
                         else{
                            $("#tit-alert-ok").html("ALERTA");
                            $("#body-alert-ok").html('<p>La Venta:  "<strong>'+nombre+'</strong>", no puede generar guía debido a que no posee items que entregar .</p>');
                            $("#alert-ok").modal();
                         }
                     break;
                     case 2:
                            $("#tit-alert-ok").html("ALERTA");
                            $("#body-alert-ok").html('<p>No puede agregar Guía de Remisión, a una venta anulada.</p>');
                            $("#alert-ok").modal();
                     break;
                     case 3:
                            $("#tit-alert-ok").html("ALERTA");
                            $("#body-alert-ok").html('<p>No puede agregar una Guía de Remisión, a una venta provisionada.</p>');
                            $("#alert-ok").modal();
                     break;
                     default :
                     break;
                }

        }else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para agregar una Guía de Remisión a la Venta.</p>');
            $("#alert-ok").modal();
        }

        });

    $("#ver_guia_rem").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            var tr = $("#lista input[type='radio']:checked").parent().parent();
            var nombre = tr.find(".nombre").text();
            var status = $("#lista input[type='radio']:checked").attr("data-status");
            var url = $("#lista input[type='radio']:checked").attr("data-ver-guia-rem-venta");

            switch(parseInt(status)){
                case 1:
                        $.ajax({
                            url: url_tiene_guia_remision,
                            type: 'POST',
                            async: false,
                            cache: false,
                            timeout: 300,
                            data: {
                                'id': $("#lista input[type='radio']:checked").val()
                            },
                             error: function(){
                                return true;
                            },
                             success: function(data){
                                if(data.status == 0)
                                {
                                    $("#tit-alert-ok").html("Alerta");
                                    $("#body-alert-ok").html('<p>La venta seleccionada no tiene ' +
                                            'registrada una guía de remisión </p>');
                                    $("#alert-ok").modal();
                                }
                                else
                                {
                                    window.location.href = $("#lista input[type='radio']:checked").attr("data-ver-guia-rem-venta");
                                }
                                }
                        });
                break;
                case 2:
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>No puede ver la guia remisión de una venta anulada.</p>');
                        $("#alert-ok").modal();
                break;
                case 3:
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>No puede ver la guia remisión de una venta provisionada.</p>');
                        $("#alert-ok").modal();
                break;
                default :
                break;
            }

        }else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para ver la guía de remisión de la venta</p>');
            $("#alert-ok").modal();
        }
        });

    /******************************************** FUNCIONALIDADES VENTA - RETENCIÓN *********************************/
        $(".numerico").keydown(function(event){
            Numerico(event);
        });
        $(".eliminar").click(function(e){
            EliminarFilaRetencion(e,this, "#retencion")
        });
        $("#id_venta_iva").removeClass("form-control");
        $("#id_venta_sin_iva").removeClass("form-control");
        $("#id_reembolso").removeClass("form-control");
        $(".valor_retenido").change(function(){
            redondear(this)
        });

        var now = new Date(2000,1,1, 0, 0, 0, 0);
        var checkin = $('#id_fecha').mask("9999-99-99").datepicker({
                onRender: function(date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
                }
        }).on('changeDate', function(ev){
            if (ev.date.valueOf() > checkout.date.valueOf()){
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate());
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#id_fecha_final')[0].focus();

        }).data('datepicker');
        var checkout = $('#id_fecha_final').mask("9999-99-99").datepicker({
            onRender: function(date)
            {
                return date.valueOf() <= checkin.date.valueOf() ? '' : '';
            }
            }).on('changeDate', function(ev) {
                checkout.hide();
            }).data('datepicker');

        $("#agregar_fila").click(function(){
            var clonar = $("#retencion tbody tr:first").clone();
            clonar.find(".btn-group").remove();
            clonar.find(".numerico").keydown(function(e){
                Numerico(e);
            });
            clonar.find(".eliminar").click(function(e){
                EliminarFilaRetencion(e, this, "#retencion")
            });
            clonar.find(".valor_retenido").val("0.00").change(function(){redondear(this)});
            clonar.find('select[data-name="cuenta_ret"]').prop('Selectindex', 0).selectpicker("refresh");
            $("#retencion tbody").append(clonar);
            recalcular_ids_Venta("#retencion");
        });
        $("#ok-ret").click(function(){
            var datastring = $("#form_retencion").serialize();
            $.ajax(
            {
                url: url_agregar_retencion_venta,
                type: 'POST',
                async: false,
                cache: false,
                timeout: 300,
                data: datastring,
                error: function(){
                    return true;
                },
                success: function(data)
                {
                    if (data.status == 2 || data.status == 3)
                    {
                        $("#error-pop_up").html('<p>' + data.mensaje + '</p>');
                        $("#error").fadeIn("slow");
                    }
                    else
                    {
                        window.location.href = url_lista_ventas;
                    }
                }
            });
        });
        $("#agregar_ret").click(function(){
            if ($("#lista input[type='radio']:checked").length != 0){
                $("#id_venta").val($("#lista input[type='radio']:checked").val());
                $("#error").hide(); // Esconde el div de error en agregar retención

                switch(tiene_ret_venta())
                {
                    case 0:
                        $("#agregar_retencion").modal();
                        break;
                    case 1:
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>La venta seleccionada ya tiene ' + 'registrada una retención </p>');
                        $("#alert-ok").modal();
                        break;
                    case 2:
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>No se puede agregar una retención a una ' +
                                'venta anulada o pendiente.</p>');
                        $("#alert-ok").modal();
                        break;
                    case 3:
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>La venta no esta asociada a una cuenta por pagar</p>');
                        $("#alert-ok").modal();
                        break;
                    default :
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>Error en el servidor, intente más tarde o comuníquese con ' +
                                'el administrador del sistema </p>');
                        $("#alert-ok").modal();
                        break;
                }
            }
            else{
                $("#tit-alert-ok").html("Alerta");
                $("#body-alert-ok").html('<p>Seleccione un item para agregar una retención</p>');
                $("#alert-ok").modal();
            }
        });
        $("#ver_ret").click(function(){
            if ($("#lista input[type='radio']:checked").length != 0)
            {
                switch (tiene_ret_venta())
                {
                    case 0:
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>La venta seleccionada no tiene ' +
                                'registrada una retención </p>');
                        $("#alert-ok").modal();
                        break;
                    case 1:
                        var url = $("#lista input[type='radio']:checked").attr("data-ver_ret");
                        window.open(url,'_blank');
                        break;
                    case 2:
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>La venta se encuentra anulada</p>');
                        $("#alert-ok").modal();
                        break;
                    default :
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>Error en el servidor, intente más tarde o comuníquese con ' +
                                'el administrador del sistema </p>');
                        $("#alert-ok").modal();
                        break;
                }
            }
            else{
                $("#tit-alert-ok").html("Alerta");
                $("#body-alert-ok").html('<p>Seleccione un item para ver la retención de la venta</p>');
                $("#alert-ok").modal();
            }

        });
        $("#anular_ret").click(function(){
            if ($("#lista input[type='radio']:checked").length != 0)
            {
                switch (tiene_ret_venta())
                {
                    case 0:
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>La venta seleccionada no tiene ' +
                                'registrada una retención </p>');
                        $("#alert-ok").modal();
                        break;
                    case 1:
                        var url = $("#lista input[type='radio']:checked").attr("data-anular_ret");
                        $("#tit-alert").html("Anualar Retención");
                        $("#body-alert").html('<p>Desea anular la retención asignada a esta factura?</p>');
                        $("#alert-yn").attr("data-href", url);
                        $("#alert-yn").modal();
                        break;
                    case 2:
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>La venta se encuentra anulada o en estado pendiente.</p>');
                        $("#alert-ok").modal();
                        break;
                    default :
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>Error en el servidor, intente más tarde o comuníquese con ' +
                                'el administrador del sistema </p>');
                        $("#alert-ok").modal();
                        break;
                }
            }
            else
            {
                $("#tit-alert-ok").html("Alerta");
                $("#body-alert-ok").html('<p>Seleccione una venta para anular su retención</p>');
                $("#alert-ok").modal();
            }
        });



    $("#ok-alert").on("click",function(){
        window.location.href = $("#alert-yn").attr("data-href");
    });

    /********************************************************************
     * Anular Factura
     ********************************************************************/
        $("#ok-doc-anul").click(function(){
            anular_factura_venta(this);
        });

        $("#anular-factura").click(function(){
            get_ultimo_comprobante_venta();

            $("#ultima_factura").text(get_ultima_factura());
            $("#ultimo_comprobante").text(get_ultimo_comprobante_venta());
            $(".error-modal").hide();
            $("#anular_venta_modal").modal();

        });

        $("#id_documento").change(function(){
            $("#ultima_factura").text(get_ultima_factura());
        });
        $(".retencion-reload").click(function(){
            $("#ultima_factura").text(get_ultima_factura());
            $("#ultimo_comprobante").text(get_ultimo_comprobante_venta());
        });
     /*******************************************************************/


});