#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from django.core.urlresolvers import reverse, resolve
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern, Resolver404, get_resolver
from clipmaestro import *

register = template.Library()


@register.simple_tag(name='tabla_retenciones')
def tabla_retenciones(form):
    return {"forms": form}
register.inclusion_tag('tags/compras/tabla_retenciones.html')(tabla_retenciones)
