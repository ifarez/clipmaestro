__author__ = 'Clip Maestro'
#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from django.core.urlresolvers import reverse, resolve
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern, Resolver404, get_resolver
from clipmaestro import *

register = template.Library()


@register.simple_tag(name='t_cruce_de_cuentas')
def t_cruce_de_cuentas(form):
    return {"detalle_docs": form, "snnipet": False}
register.inclusion_tag('Cuentas_Cobrar/Clientes/tabla_cruce_cuenta_cobros.html')(t_cruce_de_cuentas)


@register.simple_tag(name='t_cruce_de_doc')
def t_cruce_de_doc(form1, form2):
    return {"detalle_docs": form1,
            "detalle_doc_cobrar": form2,
            "snnipet": True}
register.inclusion_tag('Cuentas_Cobrar/Clientes/tabla_cruce_doc_cta.html')(t_cruce_de_doc)

@register.simple_tag(name='t_pago_clientes')
def t_pago_clientes(form1):
    return {"forms": form1}
register.inclusion_tag('Cuentas_Cobrar/Clientes/tabla_cuentas_cobrar.html')(t_pago_clientes)