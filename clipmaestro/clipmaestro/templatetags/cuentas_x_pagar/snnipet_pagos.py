import datetime

__author__ = 'Clip Maestro'
#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from django.core.urlresolvers import reverse, resolve
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern, Resolver404, get_resolver
from clipmaestro import *

register = template.Library()

@register.simple_tag(name='t_cruce_de_cuentas_pago')
def t_cruce_de_cuentas_pago(form):
    fecha_act = datetime.datetime.now().date()
    return {"formularios_ctas": form, "fecha_act":fecha_act, "snnipet": False}
register.inclusion_tag('Cuentas_Pagar/pago_proveedores/tabla_cruce_cta.html')(t_cruce_de_cuentas_pago)


@register.simple_tag(name='t_cruce_de_doc_pago')
def t_cruce_de_doc_pago(form1, form2):
    fecha_act = datetime.datetime.now().date()
    return {"formulario_facts": form1,
            "formularios": form2,
            "fecha_act":fecha_act,
            "snnipet": True}
register.inclusion_tag('Cuentas_Pagar/pago_proveedores/tabla_cruce_doc.html')(t_cruce_de_doc_pago)


@register.simple_tag(name='t_pago_proveedores')
def t_pago_proveedores(form1):
    return {"forms": form1,
            "flag": True}
register.inclusion_tag('Cuentas_Pagar/pago_proveedores/tabla_cuentas_pagar.html')(t_pago_proveedores)

