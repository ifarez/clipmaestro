#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from django.core.urlresolvers import reverse, resolve
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern, Resolver404, get_resolver
from clipmaestro import *

register = template.Library()

@register.simple_tag(name='formulario_comp_caja_chica')
def formulario_comp_caja_chica(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/formulario_comp_caja_chica.html')(formulario_comp_caja_chica)

