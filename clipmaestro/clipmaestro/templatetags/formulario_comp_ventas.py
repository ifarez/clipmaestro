#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from django.core.urlresolvers import reverse, resolve
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern, Resolver404, get_resolver
from clipmaestro import *

register = template.Library()


########################## Formulario Nuevo para realizar las ventas stock #############################################
@register.simple_tag(name='formulario_detalle_venta_stock')
def formulario_detalle_venta_stock(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/ventas/formulario_detalle_venta_stock.html')(formulario_detalle_venta_stock)

@register.simple_tag(name='formulario_detalle_forma_pago')
def formulario_detalle_forma_pago(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/ventas/formulario_forma_pago_vta.html')(formulario_detalle_forma_pago)



########################################################################################################################


########################## Formulario Nuevo para realizar las ventas TEst ##############################################
@register.simple_tag(name='formulario_detalle_venta_test')
def formulario_detalle_venta_test(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/ventas/formulario_detalle_venta_test.html')(formulario_detalle_venta_test)
########################################################################################################################


@register.simple_tag(name='formulario_guia_remision_venta')
def formulario_guia_remision_venta(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/formulario_transportista.html')(formulario_guia_remision_venta)

@register.simple_tag(name='formulario_transportista_venta')
def formulario_transportista_venta(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/formulario_transportista.html')(formulario_transportista_venta)

@register.simple_tag
def subtract(value, arg):
    return value - arg
