#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from django.core.urlresolvers import reverse, resolve
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern, Resolver404, get_resolver
from clipmaestro import *

register = template.Library()

@register.simple_tag(name='formulario_detalle_comprobante_asiento')
def formulario_detalle_comprobante_asiento(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/contabilidad/formulario_detalle_comprobante_asiento.html')(formulario_detalle_comprobante_asiento)

@register.simple_tag(name='formulario_simple_cabecera')
def formulario_simple_cabecera(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/formulario_simple_cabecera.html')(formulario_simple_cabecera)
