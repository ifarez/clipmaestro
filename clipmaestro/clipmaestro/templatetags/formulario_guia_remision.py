#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from django.core.urlresolvers import reverse, resolve
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern, Resolver404, get_resolver
from clipmaestro import *

register = template.Library()

__author__ = 'Roberto'

@register.simple_tag(name='formulario_detalle_guia')
def formulario_detalle_guia(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/Guia_remision/formulario_detalle_guia_remision.html')(formulario_detalle_guia)
