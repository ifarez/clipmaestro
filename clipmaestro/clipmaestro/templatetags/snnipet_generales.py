#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from librerias.funciones.funciones_vistas import *

register = template.Library()


@register.simple_tag(name='get_nombre_doc_sri_by_cod_tag')
def get_nombre_doc_sri_by_cod_tag(cod):
    return get_nombre_doc_sri_by_cod(cod)


@register.simple_tag(name='paginacion_html')
def paginacion_html(objetos, arreglo_paginado, muestra_paginacion):
    return {"objetos": objetos, "arreglo_paginado": arreglo_paginado, "muestra_paginacion": muestra_paginacion}
register.inclusion_tag('tags/snnipets_general/paginacion.html')(paginacion_html)


@register.simple_tag(name='get_estado')
def get_estado(cod):
    try:
        estado = Estados.objects.get(codigo=cod).descripcion
        return estado
    except Estados.DoesNotExist:
        return "No Encontrado"


@register.simple_tag(name='float_con_comas_snippet')
def float_con_comas_snippet(cod):
    return float_con_comas(cod)


@register.simple_tag(name='sumar_fecha_dias')
def sumar_fecha_dias(fecha_registro, dias):

    try:
        if fecha_registro is not None and dias is not None:

            fecha_registro = fecha_registro.strftime("%Y-%m-%d")
            fecha_vencimiento = fecha_registro + datetime.timedelta(days=dias)
            return fecha_vencimiento

        else:
            return ""
    except:
        return ""

@register.simple_tag(name='dia_actual')
def dia_actual():
    return datetime.datetime.now().date()


@register.simple_tag(name='mensaje_no_data')
def mensaje_no_data(request):
    return {"request": request}
register.inclusion_tag('tags/snnipets_general/mensaje_no_data.html')(mensaje_no_data)

@register.simple_tag(name='get_avatar')
def get_avatar(url):
    return validar_url_avatar(url)

@register.simple_tag(name='get_logotipo')
def get_logotipo(url):
    return validar_url_logotipo(url)


@register.simple_tag(name='getDelimitadorConcepto')
def getDelimitadorConcepto(concepto):
    limitador = "!=.$"
    result_concepto = unicode(concepto).split(limitador)

    return {"lista": result_concepto}
register.inclusion_tag('tags/snnipet_delimitador.html')(getDelimitadorConcepto)


@register.simple_tag(name='activar_grupo')
def activar_grupo(id_persona, id_grupo):
    if Persona.objects.filter(id=id_persona, grupo=id_grupo).exists():
        return "checked"
    else:
        return ""


@register.simple_tag(name='activar_permiso')
def activar_permiso(lista, id_permiso):
    try:
        if id_permiso in lista:
            return "checked"
        else:
            return ""
    except Permisos.DoesNotExist:
        return ""

@register.simple_tag(name='get_mes_num')
def get_mes_num(num):
    return get_mes(num)
