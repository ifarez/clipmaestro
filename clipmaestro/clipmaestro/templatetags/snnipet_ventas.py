#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from contabilidad.models import *
from administracion.models import *
from django.contrib import messages
from clipmaestro.templatetags.formulario_base import *
from reportes.vistas.funciones import get_monto_cobrado_fecha_func, \
    get_monto_saldo_cxc_fecha_func, get_monto_pagado_fecha_func, get_monto_saldo_cxp_fecha_func

register = template.Library()

@register.simple_tag(name='tiene_retencion')
def tiene_retencion(id_venta):
    try:
        venta = Venta_Cabecera.objects.get(id=id_venta)
        cta_por_cobrar = Cuentas_por_Cobrar.objects.get(venta_cabecera=venta, naturaleza=1)
        tipo_comp = TipoComprobante.objects.get(id=16)
        if CXC_cobro.objects.filter(cuentas_x_cobrar=cta_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1).exists():
            return "Si"
        else:
            return "No"
    except:
        return "No"


@register.simple_tag(name='tiene_guia_remision')
def tiene_guia_remision(id_venta):
    venta = Venta_Cabecera.objects.get(id=id_venta)

    if CabeceraGuiaRemision.objects.filter(venta_cabecera=venta, status=1).exists():
        return "Si"
    else:
        return "No"


@register.simple_tag(name='get_monto_cobrado_fecha')
def get_monto_cobrado_fecha(id_cta_x_cobrar, fecha_final):
    return "%.2f" % get_monto_cobrado_fecha_func(id_cta_x_cobrar, fecha_final)


@register.simple_tag(name='get_monto_saldo_cxc_fecha')
def get_monto_saldo_cxc_fecha(id_cta_x_cobrar, fecha_final):
    return "%.2f" % get_monto_saldo_cxc_fecha_func(id_cta_x_cobrar, fecha_final)


@register.simple_tag(name='get_monto_pagado_fecha')
def get_monto_pagado_fecha(id_cta_x_pagar, fecha_final):
    return "%.2f" % get_monto_pagado_fecha_func(id_cta_x_pagar, fecha_final)


@register.simple_tag(name='get_monto_saldo_cxp_fecha')
def get_monto_saldo_cxp_fecha(id_cta_x_pagar, fecha_final):
    return "%.2f" % get_monto_saldo_cxp_fecha_func(id_cta_x_pagar, fecha_final)
