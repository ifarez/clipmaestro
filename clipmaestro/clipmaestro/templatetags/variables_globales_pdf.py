#! /usr/bin/python
# -*- coding: UTF-8 -*-
from clipmaestro.templatetags.formulario_base import *
from django.conf import settings
from administracion.models import Empresa, Empresa_Parametro

register = template.Library()


@register.simple_tag(name='URL_MEDIA')
def URL_MEDIA():
    return settings.URL_MEDIA


@register.simple_tag(name='unicode_str')
def unicode_str(str):
    return unicode(str)


@register.simple_tag(name='get_height_logo')
def get_height_logo():
    id_empresa = Empresa.objects.filter(status=1)[0].empresa_general_id
    empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

    if empresa.height_logo is None:
        return 100
    else:
        return empresa.height_logo


@register.simple_tag(name='get_width_logo')
def get_width_logo():
    id_empresa = Empresa.objects.filter(status=1)[0].empresa_general_id
    empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

    if empresa.width_logo is None:
        return 300
    else:
        return empresa.width_logo
