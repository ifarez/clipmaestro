__author__ = 'Clip Maestro'

class EmpresaXML():
    razon_social = u""
    nombre_comer = u""
    ruc = u""
    direccion_matriz = u""
    logo = u""
    contribuyente_especial = False


class DatosClienteXML():
    razon_social = u""
    identificacion = u""
    fecha_emision = u""
    guia_remision = u""


class DatosFacturaXML():
    num_factura = u""
    num_autorizacion = u""
    fecha_autorizacion = u""
    ambiente = u""
    emision = u""
    clave_acceso = u""
    total_sin_impuesto = 0.00
    total_descuento = 0.00
    propina = 0.00
    importe_total = 0.00
    moneda = ""


class DetallesFactura():
    codigo_principal = ""
    codigo_auxiliar = ""
    descripcion = ""
    cantidad = ""
    precio_unit = 0.00
    descuento = 0.00
    precio_tot_sin_imp = 0.00
    impuestos = []


class ImpuestoDetalle():
    codigo = ""
    codigo_porc = ""
    tarifa = 0.00
    base_imponible = 0.00
    valor = 0.00


class InfoTributariaRetencionXml():
    ambiente = ""
    tipo_emision = ""
    razon_social = ""
    nombre_comercial = ""
    ruc = ""
    clave_acceso = ""
    cod_doc = ""
    establecimiento = ""
    pto_emision = ""
    secuencial = ""
    dir_matriz = ""


class InfoCompRetencion():
    fecha_emi = ""
    dir_establecimiento = ""
    obligado_contabilidad = ""
    tipo_ident_suj_rete = ""
    razon_social_sujet_retenido = ""
    identificacion_suj_ret = ""
    periodo_fiscal = ""


class ImpuestoRetencionXml():
    codigo = ""
    codigo_ret = ""
    base_imponible = 0.00
    porcentaje_retener = 0.00
    valor_retenido = 0.00
    cod_doc_sustento = 0.00
    num_doc_sustento = ""
    fecha_emi_sust = ""


class InfoNotaCredito():
    fecha_emision = ""
    dir_establecimiento = ""
    tipo_identificacion_comprador = ""
    razon_social_comprador = ""
    identificacion_comprador = ""
    obligado_contabilidad = ""
    cod_doc_modificado = ""
    num_doc_modificado = ""
    fecha_emision_doc_sustento = ""
    total_sin_imp = 0.0
    valor_modificacion = 0.0
    moneda = ""
    motivo = ""


class DetalleNotaCredito():
    codigo_interno = ""
    descripcion = ""
    cantidad = ""
    precio_unit = 0.00
    descuento = 0.00
    precio_total_sin_imp = 0.00
    impuestos = []