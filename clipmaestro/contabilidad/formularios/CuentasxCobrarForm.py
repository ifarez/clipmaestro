#! /usr/bin/python
# -*- coding: UTF-8 -*-

from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from contabilidad.models import *
from administracion.models import Estados
import datetime
from datetime import date, timedelta
from django.forms import ModelForm
from django.db.models import Q
__author__ = 'Clip Maestro'

def validar_cliente(value):
    if Cliente.objects.filter(id=value, status=1).exists():
        return True
    else:
        raise ValidationError(u"Cliente inválido o no existe")

def validar_fecha(value):
    try:
        str_fecha = str(value).split("-")
        fecha_cab = datetime.datetime(int(str_fecha[0]), int(str_fecha[1]), int(str_fecha[2]))
        if fecha_cab.date() > datetime.datetime.now().date():
            raise ValidationError(u"Fecha de registro superior a la fecha actual")
    except:
        raise ValidationError(u"Fecha no válida")


class CabeceraCobrosForm(forms.Form):
    cliente = forms.ChoiceField(choices=[], label="Cliente", validators=[validar_cliente])
    fecha = forms.DateField(label="Fecha Registro", widget=forms.DateInput(format=("%Y-%m-%d")))
    observaciones = forms.CharField(max_length=255, widget=forms.Textarea, required=False)

    def getFecha(self):
        return self.cleaned_data["fecha"]

    def getCliente(self):
        return self.data["cliente"]

    def getObservacion(self):
        return self.data["observaciones"]

    def __init__(self, *args, **kwargs):
        super(CabeceraCobrosForm, self).__init__(*args, **kwargs)
        self.fields['observaciones'].widget.attrs['rows'] = 3
        self.fields['observaciones'].widget.attrs['maxlength'] = 255
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['class'] = "numerico control-fecha"
        self.fields['cliente'].choices = [("", "")] + [(str(x.id), unicode(x.razon_social[0:25])) for x in Cliente.objects.filter(status=1)]
        self.fields['cliente'].widget.attrs['class'] = "selectpicker dropup show-tick"
        self.fields['cliente'].widget.attrs['title'] = "Seleccione un Cliente"
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-live-search'] = True


class DetalleCobro(forms.Form):
    id_cuenta_cobrar = forms.IntegerField(label="id_cuenta_cobrar")
    check = forms.BooleanField(label="check", required=False)
    tipo_doc = forms.CharField(max_length=50, required=False)
    numero_doc = forms.CharField(required=False)
    fecha_reg = forms.CharField(required=False)
    fecha_ven = forms.CharField(required=False)

    total = forms.FloatField(label="total", required=False)
    saldo = forms.FloatField(label="saldo", required=False)
    referencia = forms.CharField(max_length=1000, required=False)
    valor = forms.FloatField(label="valor", required=False)

    def getIdCuentaPagar(self):
        return self.data["id_cuenta_cobrar"]
    def getCheck(self):
        return self.data["check"]
    def getValor(self):
        return self.data["valor"]

    def __init__(self, *args, **kwargs):
        super(DetalleCobro, self).__init__(*args, **kwargs)
        self.fields['check'].widget.attrs['class']="checks"
        self.fields['valor'].widget.attrs['class']="valor numerico_valor numerico input-right"
        self.fields['tipo_doc'].widget.attrs['readonly']= True
        self.fields['total'].widget.attrs['readonly'] = True
        self.fields['fecha_ven'].widget.attrs['readonly'] = True
        self.fields['fecha_ven'].widget.attrs['class']= "fecha_vencimiento"
        self.fields['saldo'].widget.attrs['readonly'] = True
        self.fields['numero_doc'].widget.attrs['readonly'] = True
        self.fields['saldo'].widget.attrs['class']= "saldo"
        self.fields['referencia'].widget.attrs['readonly'] = True
        self.fields['id_cuenta_cobrar'].widget.attrs['class'] = "tipo"


class FormaCobroForm(forms.Form):

    terminos = (('1','Corriente'),('2','Diferido'))
    plazos = (('3','3'),('6','6'),('9','9'),('12','12'),('18','18'),('24','24'),('36','36'))

    forma_pago = forms.ChoiceField(choices=[], label="forma_pago", required=False)
    bancos = forms.ChoiceField(choices=[], required=False)
    tarjetas = forms.ChoiceField(choices=[], required=False)
    termino = forms.ChoiceField(choices=terminos, required=False)
    caja = forms.ChoiceField(choices=[], required=False)
    plazo = forms.ChoiceField(choices=plazos, required=False)
    interes = forms.FloatField(required=False)
    referencia = forms.CharField(max_length=20, label="referencia", required=False)

    def getFormaPago(self):
        return self.data["forma_pago"]
    def getBanco(self):
        return self.data["bancos"]
    def getTarjetas(self):
        return self.data["tarjetas"]
    def getTermino(self):
        return self.data["termino"]
    def getPlazo(self):
        return self.data["plazo"]
    def getInteres(self):
        return self.data["interes"]
    def getReferencia(self):
        return self.data["referencia"]
    def getCajaChica(self):
        return self.data["caja"]

    def __init__(self, *args, **kwargs):
        super(FormaCobroForm, self).__init__(*args, **kwargs)
        self.fields['forma_pago'].widget.attrs['class']="selectpicker show-tick"
        self.fields['forma_pago'].widget.attrs['data-name']="forma_de_pago"
        self.fields['forma_pago'].widget.attrs['data-style']="slc-primary"
        self.fields['forma_pago'].widget.attrs['data-width']="100%"
        self.fields['forma_pago'].choices = [(x.id, x.descripcion) for x in FormaPago.objects.exclude(id__gte=5).filter(status=1)]

        self.fields['bancos'].widget.attrs['class']="selectpicker show-tick dropup"
        self.fields['bancos'].widget.attrs['data-style']="slc-primary"
        self.fields['bancos'].widget.attrs['data-width']="100%"
        self.fields['bancos'].widget.attrs['title'] = "Seleccione una Cuenta de Banco"

        self.fields['caja'].widget.attrs['class']="selectpicker show-tick dropup"
        self.fields['caja'].widget.attrs['data-style']="slc-primary"
        self.fields['caja'].widget.attrs['data-width']="100%"
        self.fields['caja'].widget.attrs['title'] = "Seleccione una Cuenta de Caja"

        self.fields['tarjetas'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tarjetas'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tarjetas'].widget.attrs['data-width'] = "100%"
        self.fields['tarjetas'].widget.attrs['title'] = "Seleccione una Tarjeta"
        self.fields['bancos'].choices = [("", "")] + [(x.id, x.descripcion + " " + x.tipo_cuenta_banco.descripcion + " " + x.numero_cuenta) for x in Cuenta_Banco.objects.filter(status=1)]
        self.fields['caja'].choices = [("", "")] + [(x.id, x.descripcion + " - " + x.codigo) for x in PlanCuenta.objects.filter(status=1, nivel=5, tipo=TipoCuenta.objects.get(alias=12))]
        self.fields['tarjetas'].choices = [("", "")] + [(x.id, x.descripcion + " " + x.numero_tarjeta) for x in Tarjeta_Credito.objects.filter(status=1)]

        self.fields['termino'].widget.attrs['class']="selectpicker show-tick"
        self.fields['termino'].widget.attrs['data-style']="slc-primary"
        self.fields['termino'].widget.attrs['data-width']="100%"
        self.fields['plazo'].widget.attrs['class']="selectpicker show-tick numerico"
        self.fields['plazo'].widget.attrs['data-style']="slc-primary"
        self.fields['plazo'].widget.attrs['data-width']="100%"
        self.fields['interes'].widget.attrs['class']="numerico"

        self.fields['referencia'].widget.attrs['class'] = "referencia"

forma_pago_cruce = ((1, "Cruce de Documentos"), (2, "Cruce de Cuentas"))


class CabeceraCurceDocFormCobro(forms.Form):
    fecha = forms.DateField(label="Fecha", widget=forms.DateInput(format=("%Y-%m-%d")))
    forma_pago = forms.ChoiceField(label=u"Forma de Pago", choices=forma_pago_cruce)
    observaciones = forms.CharField(max_length=255, widget=forms.Textarea, required=False)
    cliente = forms.ChoiceField(choices=[], label=u"Cliente")

    def getFecha(self):
        return self.cleaned_data["fecha"]

    def getFormaPago(self):
        return self.cleaned_data["forma_pago"]

    def getObservacion(self):
        return self.data["observaciones"]

    def getCliente(self):
        return Cliente.objects.get(id=self.data["cliente"])

    def __init__(self, *args, **kwargs):
        super(CabeceraCurceDocFormCobro, self).__init__(*args, **kwargs)
        self.fields['observaciones'].widget.attrs['rows'] = 3
        self.fields['observaciones'].widget.attrs['maxlength'] = 255
        self.fields['observaciones'].widget.attrs['class'] = 'textarea textarea-cruce'

        self.fields['forma_pago'].widget.attrs['data-style'] = "slc-primary"
        self.fields['forma_pago'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['forma_pago'].widget.attrs['title'] = "Seleccione un cliente"

        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cliente'].widget.attrs['title'] = "Seleccione un cliente"
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        q1 = Cuentas_por_Cobrar.objects.filter(status=1).extra(where=["monto > cobrado"])
        self.fields['cliente'].choices = [("", u"")] + [(x.id, unicode(x.razon_social[0:25])) for x in Cliente.objects.filter(status=1).filter(cuentas_por_cobrar__in=q1).distinct()]

    def clean(self):
        """
            Función para vbalidar el formulario
        """
        id_cliente = self.cleaned_data.get('cliente', None)

        # validar cliente
        try:
            if not Cliente.objects.filter(id=id_cliente, status=1).exists():
                self._errors["cliente"] = u"El cliente que ingresó esta inactivo o no existe"
        except (Cliente.DoesNotExist):
            self._errors["cliente"] = u"El cliente que ingresó esta inactivo o no existe"


        return self.cleaned_data

class DetalleCobroDocCruzarForm(forms.Form):
    id_cuenta_pagar = forms.IntegerField(label="id_cuenta_pagar")
    check = forms.BooleanField(label="check", required=False)
    tipo_doc = forms.CharField(max_length=50, required=False)
    numero_doc = forms.CharField(required=False)
    fecha_reg = forms.CharField(required=False)
    fecha_ven = forms.CharField(required=False)

    total = forms.FloatField(label="total", required=False)
    saldo = forms.FloatField(label="saldo", required=False)
    referencia = forms.CharField(required=False)
    valor = forms.FloatField(label="valor", required=False)

    def __init__(self, *args, **kwargs):
        super(DetalleCobroDocCruzarForm, self).__init__(*args, **kwargs)
        self.fields['check'].widget.attrs['class']="checks"
        self.fields['valor'].widget.attrs['class']="valor numerico"
        self.fields['saldo'].widget.attrs['class']="saldo"
        self.fields['tipo_doc'].widget.attrs['readonly']= True
        self.fields['total'].widget.attrs['readonly'] = True
        self.fields['total'].widget.attrs['class'] = "total"
        self.fields['fecha_ven'].widget.attrs['readonly'] = True
        self.fields['fecha_ven'].widget.attrs['class']= "fecha_vencimiento"
        self.fields['numero_doc'].widget.attrs['readonly'] = True
        self.fields['referencia'].widget.attrs['readonly'] = True
        self.fields['saldo'].widget.attrs['readonly'] = True
        self.fields['id_cuenta_pagar'].widget.attrs['class'] = "tipo"


    def clean(self):
        """
            Función para validar el formulario
        """

        valor = self.cleaned_data.get('valor', None)

        # validar valor
        try:
            if float(valor) < 0:
                self._errors["valor"] = u"El valor de este documento no es válido"
        except:
            self._errors["valor"] = u"Error en el valor"

        return self.cleaned_data


class DetalleCobroCruceCta(forms.Form):
    id_cuenta_cobrar = forms.IntegerField(label="id_cuenta_cobrar")
    check = forms.BooleanField(label="check", required=False)
    tipo_doc = forms.CharField(max_length=50, required=False)
    numero_doc = forms.CharField(required=False)
    fecha_reg = forms.CharField(required=False)
    fecha_ven = forms.CharField(required=False)

    total = forms.FloatField(label="total", required=False)
    saldo = forms.FloatField(label="saldo", required=False)
    referencia = forms.CharField(required=False)
    cuenta = forms.ChoiceField(choices=[], required=False)
    valor = forms.FloatField(label="valor", required=False)

    def __init__(self, *args, **kwargs):
        super(DetalleCobroCruceCta, self).__init__(*args, **kwargs)
        self.fields['valor'].widget.attrs['class'] = "valor numerico"
        self.fields['tipo_doc'].widget.attrs['readonly']= True
        self.fields['total'].widget.attrs['readonly'] = True
        self.fields['saldo'].widget.attrs['readonly'] = True

        self.fields['fecha_reg'].widget.attrs['readonly'] = True
        self.fields['fecha_ven'].widget.attrs['readonly'] = True
        self.fields['fecha_ven'].widget.attrs['class']= "fecha_vencimiento"

        self.fields['saldo'].widget.attrs['class'] = "saldo"

        self.fields['check'].widget.attrs['class'] = "checks"

        self.fields['numero_doc'].widget.attrs['readonly'] = True
        self.fields['referencia'].widget.attrs['readonly'] = True
        self.fields['id_cuenta_cobrar'].widget.attrs['class'] = "tipo"
        self.fields['cuenta'].widget.attrs['class'] = "cuenta selectpicker show-tick"
        self.fields['cuenta'].widget.attrs['title'] = "Seleccione una Cuenta"
        self.fields['cuenta'].choices = [(0, "")] + [(x.id, x.codigo + "-" + x.descripcion) for x in PlanCuenta.objects.filter(status=1, nivel=5).exclude(tipo=TipoCuenta.objects.get(alias=1)).exclude(tipo=TipoCuenta.objects.get(alias=2)).exclude(tipo=TipoCuenta.objects.get(alias=14)).order_by("codigo")]

        self.fields['cuenta'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuenta'].widget.attrs['data-width'] = "100%"
        self.fields['cuenta'].widget.attrs['data-live-search'] = True

    def clean(self):
        """
            Función para vbalidar el formulario
        """
        cuenta = self.cleaned_data.get('cuenta', 0)
        valor = self.cleaned_data.get('valor', 0)
        # validar usuername
        try:
            if valor > 0:
                if not PlanCuenta.objects.filter(id=cuenta, status=1).exists():
                    self._errors["cuenta"] = u"El nombre del ususario " \
                                            u"ya se encuetra registrado"
        except:
            self._errors["cuenta"] = u"El nombre del ususario " \
                                        u"ya se encuetra registrado"
        return self.cleaned_data


class BuscadorCobros(forms.Form):
    fecha_ini = forms.DateTimeField(label=u"F. Inicial")
    fecha_final = forms.DateTimeField(label=u"F. Final")
    num_comp = forms.CharField(max_length=25, label=u"# Comprobante")
    num_doc = forms.CharField(max_length=25, label=u"# Doc.")
    cliente = forms.ChoiceField(choices=[], label=u"Cliente")
    valor_ini = forms.FloatField(label=u"Rango de monto")
    valor_final = forms.FloatField(label=u"Hasta")
    estado = forms.ChoiceField(choices=[], label=u"Estado")
    page = forms.IntegerField()

    def getFechaIni(self):
        try:
            value = self.data["fecha_ini"]
            split_date = str(value).split("-")
            return datetime.datetime(int(split_date[0]), int(split_date[1]), int(split_date[2]))
        except:
            return None

    def getFechaFinal(self):
        try:
            value = self.data["fecha_final"]
            split_date = str(value).split("-")
            return datetime.datetime(int(split_date[0]), int(split_date[1]), int(split_date[2]))
        except:
            return datetime.datetime.now().date()

    def getNumComp(self):
        try:
            value = self.data["num_comp"]
            return value
        except:
            return ""

    def getNumDoc(self):
        try:
            value = self.data["num_doc"]
            return value
        except:
            return ""

    def getCliente(self):
        try:
            value = self.data["cliente"]
            return Cliente.objects.get(id=value)
        except:
            return None

    def getValorIni(self):
        try:
            value = float((self.data["valor_ini"]))
            return value
        except:
            return 0
    def getValorFinal(self):
        try:
            value = float((self.data["valor_final"]))
            return value
        except:
            return 1000000000
    def getEstado(self):
        try:
            value = int(self.data["estado"])
            return value
        except:
            return ""
    def getPage(self):
        try:
            value = int(self.data["page"])
            return value
        except:
            return 1

    def __init__(self, *args, **kwargs):
        super(BuscadorCobros, self).__init__(*args, **kwargs)
        self.fields['fecha_ini'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_ini'].widget.attrs['class'] = "datepicker"
        self.fields['fecha_ini'].widget.attrs['data-mask'] = "9999-99-99"

        self.fields['fecha_final'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['class'] = "datepicker"
        self.fields['fecha_final'].widget.attrs['data-mask'] = "9999-99-99"

        self.fields['valor_ini'].widget.attrs['class'] = "numerico"
        self.fields['valor_final'].widget.attrs['class'] = "numerico"

        self.fields['estado'].widget.attrs['class'] = "cuenta selectpicker show-tick"
        self.fields['estado'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado'].widget.attrs['data-width'] = "100%"
        self.fields['estado'].widget.attrs['title'] = "Seleccione un estado"
        self.fields['estado'].choices = [("", "")] + [(x.id, x.descripcion[0:30]) for x in Estados.objects.filter(status=1)]

        self.fields['cliente'].widget.attrs['class'] = "cuenta selectpicker show-tick"
        self.fields['cliente'].choices = [("", "")] + [(x.id, x.razon_social[0:30]) for x in Cliente.objects.filter(status=1)]
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['title'] = "Seleccione un cliente"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-live-search'] = True


class CabeceraCXCInicialForm(forms.Form):
    cliente = forms.ChoiceField(choices=[], label="Cliente")
    fecha_reg = forms.CharField(label=u"Fecha de Registro", required=False)

    def getCliente(self):
        return self.data["cliente"]

    def __init__(self, *args, **kwargs):
        super(CabeceraCXCInicialForm, self).__init__(*args, **kwargs)
        self.fields['cliente'].choices = [("", "")] + [(x.id, x.razon_social) for x in Cliente.objects.filter(status=1).order_by("razon_social")]
        self.fields['cliente'].widget.attrs['class'] = "selectpicker dropup show-tick"
        self.fields['cliente'].widget.attrs['title'] = "Seleccione un Cliente"
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-live-search'] = True


class CuentasCobrarInicialForm(forms.Form):
    tipo_doc = forms.ChoiceField(choices=[], label=u"Tipo Documento")
    num_doc = forms.CharField(max_length=20, label=u"# Documento")
    fecha = forms.CharField(max_length=100, label=u"Fecha", required=False)
    monto = forms.FloatField(label="Monto")
    plazo = forms.IntegerField(required=False, initial=0)

    def clean(self):
        monto = self.cleaned_data.get('monto', 0)

        if monto <= 0:
            self._errors["monto"] = "Campo obligatorio"

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(CuentasCobrarInicialForm, self).__init__(*args, **kwargs)
        q = Documento.objects.filter(status=1).order_by("codigo_documento")
        self.fields['tipo_doc'].choices = [("", "")] + [(x.id, x.codigo_documento+" - "+x.descripcion_documento) for x in q]
        self.fields['tipo_doc'].widget.attrs['class'] = "selectpicker dropup show-tick tipo_doc"
        self.fields['tipo_doc'].widget.attrs['title'] = "Seleccione el tipo de documento"
        self.fields['tipo_doc'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_doc'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_doc'].widget.attrs['data-live-search'] = True

        self.fields['monto'].widget.attrs['class'] = "numerico monto"
        self.fields['num_doc'].widget.attrs['class'] = "numerico num_doc"
        self.fields['plazo'].widget.attrs['class'] = "numero_natural plazo"


class ReversionCuentasCobrarForm(forms.Form):
    fecha = forms.DateField(label="Fecha Registro:", widget=forms.DateInput(format=("%Y-%m-%d")))
    concepto = forms.CharField(max_length=255, widget=forms.Textarea, required=False, label="Concepto:")

    def getFecha(self):
        return self.cleaned_data["fecha"]

    def getCconcepto(self):
        return self.data["concepto"]

    def __init__(self, *args, **kwargs):
        super(ReversionCuentasCobrarForm, self).__init__(*args, **kwargs)
        self.fields['concepto'].widget.attrs['rows'] = 3
        self.fields['concepto'].widget.attrs['maxlength'] = 255
        self.fields['concepto'].widget.attrs['class'] = 'concepto_reversar'
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['class'] = "numerico control-fecha"


class BuscadorCuentasxCobrarinicialPagos(forms.Form):
    tipo_doc = forms.ChoiceField(choices=[], label=u"Tipo Documento")
    num_doc = forms.CharField(max_length=25, label=u"# Documento")
    cliente = forms.ChoiceField(choices=[], label=u"Proveedor")
    monto_ini = forms.FloatField(label=u"Monto desde")
    monto_final = forms.FloatField(label=u"Hasta")

    cobrado_ini = forms.FloatField(label=u"Cobrado desde")
    cobrado_final = forms.FloatField(label=u"Hasta")

    plazo = forms.IntegerField(label=u"Plazo")
    page = forms.IntegerField()

    def get_tipo_doc(self):
        try:
            value = self.data["tipo_doc"]
            return int(value)
        except:
            return ""

    def get_num_doc(self):
        try:
            value = self.data["num_doc"]
            return value
        except:
            return ""

    def get_cliente(self):
        try:
            value = self.data["cliente"]
            return value
        except:
            return ""

    def get_monto_ini(self):
        try:
            value = self.data["monto_ini"]
            return float(value)
        except:
            return 0

    def get_monto_final(self):
        try:
            value = self.data["monto_final"]
            return float(value)
        except:
            return 999999999

    def get_cobrado_ini(self):
        try:
            value = self.data["cobrado_ini"]
            return float(value)
        except:
            return 0

    def get_cobrado_final(self):
        try:
            value = self.data["cobrado_final"]
            return float(value)
        except:
            return 999999999

    def get_plazo(self):
        try:
            value = self.data["plazo"]
            return int(value)
        except:
            return 0

    def __init__(self, *args, **kwargs):
        super(BuscadorCuentasxCobrarinicialPagos, self).__init__(*args, **kwargs)
        q = Documento.objects.filter(status=1).order_by("codigo_documento")
        self.fields['tipo_doc'].choices = [("", "")] + [(x.id, x.codigo_documento+" - "+x.descripcion_documento) for x in q]
        self.fields['tipo_doc'].widget.attrs['class'] = "selectpicker dropup show-tick tipo_doc"
        self.fields['tipo_doc'].widget.attrs['title'] = "Seleccione el tipo de documento"
        self.fields['tipo_doc'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_doc'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_doc'].widget.attrs['data-live-search'] = True

        q = Proveedores.objects.filter(status=1).order_by("razon_social")
        self.fields['cliente'].choices = [("", "")] + [(x.id, x.razon_social+" - "+x.ruc) for x in q]
        self.fields['cliente'].widget.attrs['class'] = "selectpicker dropup show-tick tipo_doc"
        self.fields['cliente'].widget.attrs['title'] = "Seleccione un Cliente"
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-live-search'] = True

        self.fields['monto_ini'].widget.attrs['class'] = "numerico"
        self.fields['monto_ini'].widget.attrs['placeholder'] = "0.00"

        self.fields['monto_final'].widget.attrs['class'] = "numerico"
        self.fields['monto_final'].widget.attrs['placeholder'] = "0.00"

        self.fields['cobrado_ini'].widget.attrs['class'] = "numerico"
        self.fields['cobrado_ini'].widget.attrs['placeholder'] = "0.00"

        self.fields['cobrado_final'].widget.attrs['class'] = "numerico"
        self.fields['cobrado_final'].widget.attrs['placeholder'] = "0.00"

        self.fields['plazo'].widget.attrs['class'] = "numero_natural"
        self.fields['plazo'].widget.attrs['placeholder'] = "0"
