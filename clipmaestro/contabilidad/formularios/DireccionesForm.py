#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from librerias.funciones.validacion_formularios import *
from django.forms.formsets import BaseFormSet

__author__ = 'Roberto'


# Clase que me ayuda a validar los formset vacios
class DireccionesFormSetOnlyRead(forms.Form):
    descripcion = forms.CharField(widget=forms.Textarea,label=u"Descripción", validators=[validador_espacios])
    direccion1 = forms.CharField(widget=forms.Textarea,label=u"Dirección1", validators=[validador_espacios])
    direccion2 = forms.CharField(widget=forms.Textarea,label=u"Dirección2", required=False)
    atencion = forms.CharField(label="Atención", required=False)
    ciudad_direccion = forms.ChoiceField(choices=[], label="Ciudad")
    telefono = forms.CharField(label="Teléfono", required=False)

    def __init__(self, *args, **kwargs):
        super(DireccionesFormSetOnlyRead, self).__init__(*args, **kwargs)
        self.fields['descripcion'].widget.attrs['maxlength'] = "150"
        self.fields['direccion1'].widget.attrs['maxlength'] = "150"
        self.fields['direccion2'].widget.attrs['maxlength'] = "150"
        self.fields['descripcion'].widget.attrs['style'] = "height: 110px;"
        self.fields['direccion1'].widget.attrs['style'] = "height: 110px;"
        self.fields['direccion2'].widget.attrs['style'] = "height: 110px;"
        self.fields['atencion'].widget.attrs['maxlength'] = "70"

        self.fields['ciudad_direccion'].choices = [("", "")]+[(x.id, x.descripcion) for x in Ciudad.objects.filter(status=1)]
        self.fields['ciudad_direccion'].widget.attrs['class'] = "selectpicker show-tick ciudad"
        self.fields['ciudad_direccion'].widget.attrs['data-style'] = "slc-primary"
        self.fields['ciudad_direccion'].widget.attrs['data-size'] = "15"
        self.fields['ciudad_direccion'].widget.attrs['data-live-search'] = True
        self.fields['ciudad_direccion'].widget.attrs['data-name'] = "combo_ciudad_direccion"
        self.fields['telefono'].widget.attrs['placeholder'] = "(99)-9999-9999"
        self.fields['descripcion'].widget.attrs['class'] = "descripcion"
        self.fields['direccion1'].widget.attrs['class'] = "direccion1"
        self.fields['direccion2'].widget.attrs['class'] = "direccion2"
        self.fields['atencion'].widget.attrs['class'] = "atencion"
        self.fields['telefono'].widget.attrs['class'] = "telefono_direccion"
        self.fields['descripcion'].widget.attrs['readonly'] = True
        self.fields['ciudad_direccion'].widget.attrs['readonly'] = True
        self.fields['ciudad_direccion'].widget.attrs['disabled'] = True
        self.fields['direccion1'].widget.attrs['readonly'] = True
        self.fields['direccion2'].widget.attrs['readonly'] = True
        self.fields['atencion'].widget.attrs['readonly'] = True
        self.fields['telefono'].widget.attrs['readonly'] = True

class FormDireccion(forms.Form):
    descripcion = forms.CharField(label="Descripción", validators=[validador_espacios])
    direccion1 = forms.CharField(label="Dirección1", validators=[validador_espacios])
    direccion2 = forms.CharField(label="Dirección2", required=False)
    atencion = forms.CharField(label="Atención", required=False)
    ciudad_direccion = forms.ChoiceField(choices=[], label="Ciudad")
    telefono = forms.CharField(label="Teléfono", required=False)

    def getAtencion(self):
        return self.data["atencion"]
    def getDireccion2(self):
        return self.data["direccion2"]
    def getDireccion1(self):
        return self.data["direccion1"]
    def getDescripcion(self):
        return self.data["descripcion"]
    def getCiudad(self):
        return self.data["ciudad_direccion"]
    def getTelefono(self):
        return self.data["telefono"]

    def __init__(self, *args, **kwargs):
        super(FormDireccion, self).__init__(*args, **kwargs)
        self.fields['descripcion'].widget.attrs['maxlength'] = "80"
        self.fields['direccion1'].widget.attrs['maxlength'] = "80"
        self.fields['direccion2'].widget.attrs['maxlength'] = "80"
        self.fields['atencion'].widget.attrs['maxlength'] = "70"

        self.fields['ciudad_direccion'].choices = [("", "")]+[(x.id, x.descripcion) for x in Ciudad.objects.filter(status=1)]
        self.fields['ciudad_direccion'].widget.attrs['class'] = "selectpicker show-tick ciudad"
        self.fields['ciudad_direccion'].widget.attrs['data-style'] = "slc-primary"
        self.fields['ciudad_direccion'].widget.attrs['data-size'] = "15"
        self.fields['ciudad_direccion'].widget.attrs['data-live-search'] = True
        self.fields['ciudad_direccion'].widget.attrs['data-name'] = "combo_ciudad_direccion"

        self.fields['telefono'].widget.attrs['placeholder'] = "(99)-9999-9999"
        self.fields['telefono'].widget.attrs['maxlength'] = "12"

        self.fields['descripcion'].widget.attrs['class'] = "descripcion"
        self.fields['direccion1'].widget.attrs['class'] = "direccion1"
        self.fields['direccion2'].widget.attrs['class'] = "direccion2"
        self.fields['atencion'].widget.attrs['class'] = "atencion"
        self.fields['telefono'].widget.attrs['class'] = "numerico telefono_direccion"

