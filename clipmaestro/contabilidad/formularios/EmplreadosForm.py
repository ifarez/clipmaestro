#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from datetime import date, timedelta
from librerias.funciones.validacion_formularios import *
from django.contrib import messages
from librerias.funciones.validacion_rucs import *

__author__ = 'Roberto'

solo_letras = u'^[A-Za-zÑñáéíóúÁÉÍÓÚ_\\s]+$'
solo_numeros = u'^[0-9]+$'
solo_alfanumericas = u'[a-zA-Z0-9_\\s]+$'

class FormEmpleados(forms.Form):
    num_id = forms.CharField(max_length=20, label=u"# Identificación")
    nombres = forms.RegexField(max_length=70, label=u"Nombres", regex=solo_letras, validators=[validador_espacios])
    apellidos = forms.RegexField(max_length=70, label=u"Apellidos", regex=solo_letras, validators=[validador_espacios])
    sueldo = forms.FloatField(label=u"Sueldo", validators=[validar_valor])

    def getNumID(self):
        return self.data["num_id"]

    def getNombres(self):
        return self.data["nombres"]

    def getApellidos(self):
        return self.data["apellidos"]

    def getSueldo(self):
        return self.data["sueldo"]

    def __init__(self, *args, **kwargs):
        super(FormEmpleados, self).__init__(*args, **kwargs)
        self.fields['sueldo'].widget.attrs['class'] = "numerico"
        self.fields['sueldo'].widget.attrs['maxlength'] = "13"
        self.fields['num_id'].widget.attrs['class'] = "numerico"
        self.fields['num_id'].widget.attrs['maxlength'] = "13"

class FormEmpleadoOnly(forms.Form):
    num_id = forms.CharField(max_length=20, label=u"Cédula")
    nombres = forms.CharField(max_length=70, label=u"Nombres")
    apellidos = forms.CharField(max_length=70, label=u"Apellidos")
    sueldo = forms.FloatField(label=u"Sueldo")

    def __init__(self, *args, **kwargs):
        super(FormEmpleadoOnly, self).__init__(*args, **kwargs)
        self.fields['sueldo'].widget.attrs['readonly'] = True
        self.fields['apellidos'].widget.attrs['readonly'] = True
        self.fields['nombres'].widget.attrs['readonly'] = True
        self.fields['num_id'].widget.attrs['readonly'] = True

class FormBuscadorEmpleados(forms.Form):
    num_id = forms.CharField(max_length=20, label=u"# Identificación")
    nombres = forms.RegexField(max_length=70, label=u"Nombres", regex=solo_letras, validators=[validador_espacios])
    apellidos = forms.RegexField(max_length=70, label=u"Apellidos", regex=solo_letras, validators=[validador_espacios])
    page = forms.IntegerField()

    def getNombres(self):
        try:
            value = self.data['nombres']
            return value
        except:
            return ""

    def getApellidos(self):
        try:
            value = self.data['apellidos']
            return value
        except:
            return ""

    def getNumID(self):
        try:
            value = self.data['num_id']
            return value
        except:
            return ""

meses = (("", ""), (1, "Enero"), (2, "Febrero"), (3, "Marzo"), (4, "Abril"), (5, "Mayo"), (6, "Junio"), (7, "Julio"),
                   (8, "Agosto"), (9, "Septiembre"), (10, "Octubre"), (11, "Noviembre"), (12, "Diciembre"))

class FormGenerarSueldoEmpleados(forms.Form):
    anio = forms.IntegerField(label=u"Año")
    mes = forms.CharField(label=u"Mes")
    sueldo = forms.FloatField(label=u"Sueldo")

    def getSueldo(self):
        return self.data["sueldo"]

    def getMes(self):
        value = self.cleaned_data["mes"]
        try:
            return int(value)
        except:
            return 0

    def getAnio(self):
        value = self.cleaned_data["anio"]
        try:
            return int(value)
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(FormGenerarSueldoEmpleados, self).__init__(*args, **kwargs)
        self.fields['mes'].widget.attrs['data-width'] = "100%"
        self.fields['mes'].widget.attrs['data-size'] = "auto"
        self.fields['sueldo'].widget.attrs['class'] = "numerico"
        self.fields['sueldo'].widget.attrs['maxlength'] = "13"

class EmpleadoDetalleForm(forms.Form):
    id_empleado = forms.IntegerField(label=u"id_empleado")
    empleado = forms.CharField(label=u"Empleado")
    sueldo = forms.FloatField(label=u"Valor", validators=[validar_valor])

    def __init__(self, *args, **kwargs):
        super(EmpleadoDetalleForm, self).__init__(*args, **kwargs)
        self.fields['sueldo'].widget.attrs['style'] = "width: 100%"
        self.fields['sueldo'].widget.attrs['class'] = "numerico sueldo"
        self.fields['sueldo'].widget.attrs['maxlength'] = "13"