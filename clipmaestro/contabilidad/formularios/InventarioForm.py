#! /usr/bin/python
# -*- coding: UTF-8 -*-

from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
import datetime
from django.forms import ModelForm
from django.db.models import Q
from django.forms.formsets import BaseFormSet
from datetime import date, timedelta
from django.forms.util import ErrorList
from django.core.urlresolvers import reverse
__author__ = 'Clip Maestro'
#################
#  Constantes   #
#################
tipo_movimiento_inv = (("", ""), (14, "Ingreso"), (15, "Egreso"))


class BaseDetalleInventFormSet(BaseFormSet):
    """
    Clase que ayuda a validar los formset de los items
    """
    def __init__(self, *args, **kwargs):
        super(BaseDetalleInventFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False
    def clean(self):
        """Checks that no two articles have the same title."""
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return

        for form in self.forms:
            cuenta = form.cleaned_data.get("cuenta")
            if cuenta is not None:
                if not PlanCuenta.objects.filter(id=cuenta, status=1, nivel=5).exists():
                    errors = form._errors.setdefault("cuenta", ErrorList())
                    errors.append(u"Ingrese una cuenta valida")

            valor = form.cleaned_data.get("valor", None)
            if valor is not None:
                try:
                    if float(valor) <= 0:
                        errors = form._errors.setdefault("valor", ErrorList())
                        errors.append(u"valor no válido")
                except:
                    errors = form._errors.setdefault("valor", ErrorList())
                    errors.append(u"valor no válido")

            cantidad = form.cleaned_data.get("cantidad")
            if cantidad is not None:
                try:
                    if float(cantidad) <= 0:
                        errors = form._errors.setdefault("cantidad", ErrorList())
                        errors.append(u"valor no válido")
                except:
                    errors = form._errors.setdefault("cantidad", ErrorList())
                    errors.append(u"valor no válido")



tipo_inventario = ((1, "Ingreso"), (2, "Egreso"))


class CabeceraInvForm(forms.Form):
    fecha_reg = forms.DateField(label=u"Fecha Registro", widget=forms.DateInput(format=("%Y-%m-%d")))
    tipo = forms.ChoiceField(choices=tipo_inventario, label=u"Tipo")
    motivo = forms.ChoiceField(choices=[], label=u"Motivo")
    motivo_egreso = forms.ChoiceField(choices=[], label=u"Motivo")
    proveedor = forms.IntegerField(label=u"Proveedor", required=False)
    num_contrato = forms.IntegerField(label=u"# Contrato", required=False)
    numero_guia_r = forms.CharField(max_length=17, label=u"# de Guia", required=False)
    num_doc = forms.CharField(max_length=17, label=u"# de Documento", required=False)
    tipo_doc = forms.ChoiceField(choices=[], label=u"Tipo de Doc.")
    concepto = forms.CharField(max_length=220, label="Concepto", widget=forms.Textarea, required=False)

    # Nuevo
    def getFechaReg(self):
        return self.cleaned_data['fecha_reg']

    def getTipo(self):
        return self.data["tipo"]

    def getMotivo(self):
        if self.getTipo() == "1":
            return self.data["motivo"]
        else:
            return self.data["motivo_egreso"]

    def getProveedorCliente(self):
        return self.data["proveedor"]

    def getNumContrato(self):
        return self.data["num_contrato"]

    def getNumDoc(self):
        return self.data["num_doc"]

    def getTipoDoc(self):
        return self.data["tipo_doc"]

    def getNumGuiaR(self):
        return self.data["numero_guia_r"]

    def getConcepto(self):
        return self.data["concepto"]

    def __init__(self, *args, **kwargs):
        super(CabeceraInvForm, self).__init__(*args, **kwargs)
        self.fields['fecha_reg'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_reg'].widget.attrs['class'] = "datepicker"
        self.fields['fecha_reg'].widget.attrs['text-align'] = "left"
        self.fields['fecha_reg'].widget.attrs['data-date-format'] = "yyyy-mm-dd"

        self.fields['concepto'].widget.attrs['placeholder'] = "Concepto"
        self.fields['concepto'].widget.attrs['rows'] = 3
        self.fields['concepto'].widget.attrs['maxlength'] = "220"

        self.fields['tipo'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo'].widget.attrs['data-width'] = "100px"
        self.fields['tipo'].widget.attrs['data-size'] = "auto"

        self.fields['tipo_doc'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_doc'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_doc'].widget.attrs['data-width'] = "100px"
        self.fields['tipo_doc'].widget.attrs['data-size'] = "auto"
        self.fields['tipo_doc'].widget.attrs['data-live-search'] = True
        self.fields['tipo_doc'].choices = [(x.id, x.descripcion_documento[0:30]) for x in Documento.objects.filter(status=1).order_by("codigo_documento")]

        self.fields['motivo'].choices = [(x.id, x.descripcion) for x in Tipo_Mov_Inventario.objects.filter(status=1,
                                                                                                           alias=1).filter(Q(tipo_inventario=1) | Q(tipo_inventario=3)).order_by("id")]
        self.fields['motivo'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['motivo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['motivo'].widget.attrs['data-width'] = "100px"
        self.fields['motivo'].widget.attrs['data-size'] = "auto"

        self.fields['motivo_egreso'].choices = [(x.id, x.descripcion) for x in Tipo_Mov_Inventario.objects.filter(status=1,
                                                                                                                  alias=1).filter(Q(tipo_inventario=2) | Q(tipo_inventario=3)).order_by("id")]
        self.fields['motivo_egreso'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['motivo_egreso'].widget.attrs['data-style'] = "slc-primary"
        self.fields['motivo_egreso'].widget.attrs['data-width'] = "100px"
        self.fields['motivo_egreso'].widget.attrs['data-size'] = "auto"

        self.fields['num_contrato'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['num_contrato'].widget.attrs['data-url'] = reverse("buscar_contratos")

        self.fields['proveedor'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['proveedor'].widget.attrs['data-url'] = reverse("buscar_proveedor")



class SelectCostoItem(forms.Select):
    allow_multiple_selected = False

    def __init__(self, attrs=None, choices=()):
        super(SelectCostoItem, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)

    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''

        if option_value != "":
            try:
                costo = Item.objects.get(id=option_value).costo
                iva = Item.objects.get(id=option_value).iva
                if costo is None:
                    costo = 0
                if iva is None:
                    iva = 0
            except:
                costo = 0
                iva = 0

            return format_html(u'<option value="{0}" {1} data-costo={2} data-iva={3}> {4} </option>',
                               option_value,
                               selected_html,
                               costo,
                               iva,
                               unicode(force_text(option_label)))
        else:
            return format_html('<option value="{0}" {1}>{2}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label))


class FormDetalleInventarioContrato(forms.Form):
    item = forms.IntegerField()
    cantidad = forms.FloatField()
    costo = forms.FloatField(label=u"Costo/Unidad")
    costo_total = forms.FloatField(label=u"Costo Total", required=False)

    def __init__(self, *args, **kwargs):
        super(FormDetalleInventarioContrato, self).__init__(*args, **kwargs)
        self.id_button_add_form_name = "add_det_contrato"
        self.id_table = "tabla_item_contrato"
        self.formset_prefix = "detalle_inv_contrato"
        self.url_get_row = reverse('get_fila_form_produccion_inventario')

        self.fields['item'].widget.attrs['class'] = "ajaxcombobox item"
        self.fields['item'].widget.attrs['data-url'] = reverse("buscar_items")
        self.fields['item'].widget.attrs['placeholder'] = "Seleccionar un Item"

        self.fields['cantidad'].widget.attrs['class'] = "cantidad_contrato numerico"
        self.fields['cantidad'].widget.attrs['style'] = "width: 100%"
        self.fields['cantidad'].widget.attrs['placeholder'] = "0.00"

        self.fields['costo'].widget.attrs['readonly'] = True
        self.fields['costo'].widget.attrs['class'] = "mostrar_costo text-right deshabilitado"
        self.fields['costo'].widget.attrs['style'] = "width: 100%"

        self.fields['costo_total'].widget.attrs['readonly'] = True
        self.fields['costo_total'].widget.attrs['class'] = "mostrar_costo_total text-right deshabilitado"
        self.fields['costo_total'].widget.attrs['style'] = "width: 100%"

class DetalleInventarioGRForm(forms.Form):
    item = forms.IntegerField()
    cantidad = forms.FloatField()

    def __init__(self, *args, **kwargs):
        super(DetalleInventarioGRForm, self).__init__(*args, **kwargs)
        self.id_button_add_form_name = "add_item_gr"
        self.id_table = "tabla_item_gr"
        self.formset_prefix = "detalle_inv_gr_form"
        self.url_get_row = reverse('get_fila_form_guia_rem_inventario')

        self.fields['item'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['item'].widget.attrs['data-url'] = reverse("buscar_items")
        self.fields['item'].widget.attrs['placeholder'] = "Seleccione un item"

        self.fields['cantidad'].widget.attrs['class'] = "cantidad numerico"
        self.fields['cantidad'].widget.attrs['style'] = "width: 100%"

# Clase que me ayuda a validar los formset vacios
class RequiredFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(RequiredFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


class AjusteCantidadForm(forms.Form):
    item = forms.IntegerField()
    cantidad = forms.FloatField()
    unidad_medida = forms.CharField(max_length=100, required=False)
    cuenta = forms.IntegerField()

    def __init__(self, *args, **kwargs):
        super(AjusteCantidadForm, self).__init__(*args, **kwargs)
        self.id_button_add_form_name = "add_ajuste_cant"
        self.id_table = "tabla_ajuste_cantidad"
        self.formset_prefix = "detalle_inv_aju_cant_form"
        self.url_get_row = reverse('get_fila_form_ajuste_cantidad_inventario')

        self.fields['item'].widget.attrs['class'] = "ajaxcombobox item"
        self.fields['item'].widget.attrs['data-url'] = reverse("buscar_items")
        self.fields['item'].widget.attrs['placeholder'] = "Seleccione un item"

        self.fields['cantidad'].widget.attrs['class'] = "cantidad numerico"
        self.fields['cantidad'].widget.attrs['style'] = "width: 100%"

        self.fields['unidad_medida'].widget.attrs['readonly'] = True
        self.fields['unidad_medida'].widget.attrs['class'] = "unidad"

        self.fields['cuenta'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['cuenta'].widget.attrs['data-url'] = reverse("buscar_cuenta_codigo")
        self.fields['cuenta'].widget.attrs['placeholder'] = "Seleccione una cuenta"


class AjusteCostoForm(forms.Form):
    item = forms.IntegerField()
    total = forms.FloatField()
    unidad_medida = forms.CharField(max_length=100, required=False)
    cuenta = forms.IntegerField()

    def __init__(self, *args, **kwargs):
        super(AjusteCostoForm, self).__init__(*args, **kwargs)

        self.id_button_add_form_name = "add_ajuste_costo"
        self.id_table = "tabla_ajuste_costo"
        self.formset_prefix = "detalle_inv_aju_costo_form"
        self.url_get_row = reverse('get_fila_form_ajuste_costo_inventario')

        self.fields['item'].widget.attrs['class'] = "ajaxcombobox item"
        self.fields['item'].widget.attrs['data-url'] = reverse("buscar_items")
        self.fields['item'].widget.attrs['placeholder'] = "Seleccione un item"

        self.fields['total'].widget.attrs['class'] = "total numerico"
        self.fields['total'].widget.attrs['style'] = "width: 100%"
        self.fields['unidad_medida'].widget.attrs['readonly'] = True
        self.fields['unidad_medida'].widget.attrs['class'] = "unidad"

        self.fields['cuenta'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['cuenta'].widget.attrs['data-url'] = reverse("buscar_cuenta_codigo")
        self.fields['cuenta'].widget.attrs['placeholder'] = "Seleccione una cuenta"

def validate_cantidad(value):
    if value != "":
        if float(value) <= 0:
            raise ValidationError(u'La cantidad debe de ser mayor a cero')
    else:
        raise ValidationError(u'Ingrese una cantidad válida')

def validate_costo(value):
    if value != "":
        if float(value) <= 0:
            raise ValidationError(u'El costo debe de ser mayor a cero')
    else:
        raise ValidationError(u'Ingrese un costo válido')


class SelectItemIva(forms.Select):
    allow_multiple_selected = False

    def __init__(self, attrs=None, choices=()):
        super(SelectItemIva, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)

    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''

        if option_value != "":
            try:
                porc_iva = Item.objects.get(id=option_value).iva
            except:
                porc_iva = 0

            return format_html(u'<option value="{0}" {1} data-iva={2}> {3} </option>',
                               option_value,
                               selected_html,
                               porc_iva,
                               unicode(force_text(option_label)))
        else:
            return format_html('<option value="{0}" {1}>{2}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label))


class CompraInventario(forms.Form):
    item = forms.IntegerField(label="Item")
    cantidad = forms.FloatField(validators=[validate_cantidad], label="Cantidad")
    costo = forms.FloatField(validators=[validate_costo], label="Costo Total")
    p_unitario = forms.FloatField(required=False, label="P. Unitario")

    def __init__(self, *args, **kwargs):
        super(CompraInventario, self).__init__(*args, **kwargs)
        self.id_button_add_form_name = "add_detalle_compra"
        self.id_table = "tabla_detalle_compra"
        self.formset_prefix = "detalle_compra_inventario"
        self.url_get_row = reverse('get_fila_form_compra_inventario')

        self.fields['item'].widget.attrs['class'] = "ajaxcombobox item"
        self.fields['item'].widget.attrs['data-url'] = reverse("buscar_items")
        self.fields['item'].widget.attrs['placeholder'] = "Seleccionar un Item"

        self.fields['cantidad'].widget.attrs['class'] = "cantidad numerico"
        self.fields['cantidad'].widget.attrs['style'] = "width: 100%"
        self.fields['costo'].widget.attrs['class'] = "costo numerico"
        self.fields['costo'].widget.attrs['style'] = "width: 100%"
        self.fields['p_unitario'].widget.attrs['class'] = "p_unitario numerico deshabilitado"
        self.fields['p_unitario'].widget.attrs['style'] = "width: 100%"
        self.fields['p_unitario'].widget.attrs['readonly'] = True


class CompraInventarioInicial(forms.Form):
    item = forms.IntegerField()
    cantidad = forms.FloatField(validators=[validate_cantidad])
    costo = forms.FloatField(validators=[validate_costo])
    p_unitario = forms.FloatField(required=False)
    cuenta = forms.IntegerField()

    def __init__(self, *args, **kwargs):
        super(CompraInventarioInicial, self).__init__(*args, **kwargs)
        self.id_button_add_form_name = "add_detalle_inventario_inicial"
        self.id_table = "tabla_detalle_inventario_inicial"
        self.formset_prefix = "detalle_compra_inventario_inicial"
        self.url_get_row = reverse('get_fila_form_compra_inicial_inventario')

        self.fields['item'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['item'].widget.attrs['data-url'] = reverse("buscar_items")
        self.fields['item'].widget.attrs['placeholder'] = "Seleccionar un Item"

        self.fields['cuenta'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['cuenta'].widget.attrs['data-url'] = reverse("buscar_cuenta_codigo")
        self.fields['cuenta'].widget.attrs['placeholder'] = "Seleccione una cuenta"

        self.fields['cantidad'].widget.attrs['class'] = "cantidad numerico"
        self.fields['cantidad'].widget.attrs['style'] = "width: 100%"
        self.fields['costo'].widget.attrs['class'] = "costo numerico"
        self.fields['costo'].widget.attrs['style'] = "width: 100%"
        self.fields['p_unitario'].widget.attrs['class'] = "p_unitario numerico"
        self.fields['p_unitario'].widget.attrs['style'] = "width: 100%"
        self.fields['p_unitario'].widget.attrs['readonly'] = True


class CompraEgresoInventario(forms.Form):
    id_det_inv = forms.IntegerField(label="id_det_inv")
    item = forms.CharField(max_length=100)
    cant_vend = forms.CharField(max_length=100)
    cantidad = forms.FloatField(validators=[validate_cantidad])
    costo = forms.FloatField(validators=[validate_costo])
    p_unitario = forms.FloatField(required=False)

    def __init__(self, *args, **kwargs):
        super(CompraEgresoInventario, self).__init__(*args, **kwargs)
        self.fields['item'].widget.attrs['style'] = "width: 100%"

        self.fields['cantidad'].widget.attrs['class'] = "cantidad_vend numerico"
        self.fields['cantidad'].widget.attrs['style'] = "width: 100%"

        self.fields['cantidad'].widget.attrs['class'] = "cantidad numerico"
        self.fields['cantidad'].widget.attrs['style'] = "width: 100%"

        self.fields['costo'].widget.attrs['class'] = "costo numerico"
        self.fields['costo'].widget.attrs['style'] = "width: 100%"

        self.fields['p_unitario'].widget.attrs['class'] = "p_unitario numerico"
        self.fields['p_unitario'].widget.attrs['style'] = "width: 100%"
        self.fields['p_unitario'].widget.attrs['readonly'] = True

class DetalleCompraInvForm(forms.Form):
    item = forms.ChoiceField(choices=[])
    cantidad = forms.FloatField()
    unidad_medida = forms.ChoiceField(choices=[])
    subtotal = forms.FloatField()
    porc_desc = forms.FloatField()
    p_unitario = forms.FloatField()

    def __init__(self, *args, **kwargs):
        super(DetalleCompraInvForm, self).__init__(*args, **kwargs)
        self.fields['item'].widget.attrs['style'] = "width: 100%"
        self.fields['item'].widget.attrs['class'] = "item selectpicker show-tick"
        self.fields['item'].widget.attrs['data-style'] = "slc-primary"
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['title'] = "Seleccione un Producto"
        self.fields['item'].choices = [("", "")] + [(x.id, unicode(x.codigo)+ "-"+unicode(x.nombre[0:50]))
                                                      for x in Item.objects.filter(status=1, categoria_item__tipo_item__id=1).order_by("nombre") ]

        self.fields['cantidad'].widget.attrs['class'] = "cantidad numerico"
        self.fields['cantidad'].widget.attrs['style'] = "width: 100%"
        self.fields['unidad_medida'].choices = [(x.id, x.descripcion[0:30]) for x in Unidad.objects.filter(status=1)]
        #self.fields['unidad_medida'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['unidad_medida'].widget.attrs['data-style'] = "slc-primary"
        self.fields['unidad_medida'].widget.attrs['data-width'] = "100px"
        self.fields['unidad_medida'].widget.attrs['data-size'] = "auto"
        self.fields['subtotal'].widget.attrs['class'] = "subtotal numerico"
        self.fields['subtotal'].widget.attrs['style'] = "width: 100%"
        self.fields['porc_desc'].widget.attrs['class'] = "porc_desc numerico"
        self.fields['porc_desc'].widget.attrs['style'] = "width: 100%"
        self.fields['p_unitario'].widget.attrs['class'] = "p_unitario numerico"
        self.fields['p_unitario'].widget.attrs['style'] = "width: 100%"
        self.fields['p_unitario'].widget.attrs['readonly'] = True


class GuiaCompraCabeceraForm(forms.Form):
    proveedor = forms.ChoiceField(choices=[], label=u"Proveedor")
    tipo_doc = forms.ChoiceField(choices=[], label=u"Tipo de Doc.")
    fecha_reg = forms.DateField(label=u"Fecha de registro", widget=forms.DateInput(format=("%Y-%m-%d")))
    num_doc = forms.CharField(max_length=17, label=u"#")
    concepto = forms.CharField(max_length=250, label=u"Concepto", widget=forms.Textarea, required=False)

    def getProveedor(self):
        return self.data["proveedor"]

    def getTipoDoc(self):
        return self.data["tipo_doc"]

    def getFechaReg(self):
        return self.cleaned_data['fecha_reg']

    def getNumDoc(self):
        return self.data["num_doc"]

    def getConcepto(self):
        return self.data["concepto"]

    def __init__(self, *args, **kwargs):
        super(GuiaCompraCabeceraForm, self).__init__(*args, **kwargs)
        self.fields['proveedor'].choices = [("", "")]+\
                                           [(x.id, x.razon_social[0:30]) for x in Proveedores.objects.filter(proveedor_item__in=
                                                                                                             Proveedor_Item.objects.filter(status=1, cantidad_provision__gt=0), status=1).distinct()]
        self.fields['proveedor'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['proveedor'].widget.attrs['data-style'] = "slc-primary"
        self.fields['proveedor'].widget.attrs['title'] = "Seleccione un proveedor"
        self.fields['proveedor'].widget.attrs['data-width'] = "100px"
        self.fields['proveedor'].widget.attrs['data-size'] = "auto"

        self.fields['tipo_doc'].choices = [(x.id, x.descripcion_documento) for x in Documento.objects.filter(status=1).order_by("codigo_documento")]
        self.fields['tipo_doc'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_doc'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_doc'].widget.attrs['data-width'] = "100px"
        self.fields['tipo_doc'].widget.attrs['data-size'] = "auto"

        self.fields['fecha_reg'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_reg'].widget.attrs['class'] = "datepicker calendario-gris"
        self.fields['fecha_reg'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_reg'].widget.attrs['readonly'] = True


        self.fields['num_doc'].widget.attrs['class'] = "num_doc numerico"

        self.fields['concepto'].widget.attrs['rows'] = "3"
        self.fields['concepto'].widget.attrs['max_length'] = 250
        self.fields['concepto'].widget.attrs['style'] = "max-width: 455px"


class GuiaCompraForm(forms.Form):
    item = forms.ChoiceField(choices=[])
    cantidad_guia = forms.FloatField()
    cantidad_documento = forms.FloatField(required=False)
    costo = forms.FloatField(required=False)
    p_unitario = forms.FloatField(required=False)

    def __init__(self, *args, **kwargs):
        super(GuiaCompraForm, self).__init__(*args, **kwargs)
        self.fields['item'].widget.attrs['style'] = "width: 100%"
        self.fields['item'].widget.attrs['class'] = "item selectpicker show-tick"
        self.fields['item'].widget.attrs['data-style'] = "slc-primary"
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['title'] = "Seleccione un Producto"
        self.fields['item'].choices = [("", "")] + [(x.id, unicode(x.codigo)+ "-"+unicode(x.nombre[0:50]))
                                                      for x in Item.objects.filter(status=1, categoria_item__tipo_item__id=1).order_by("nombre") ]

        self.fields['item'].widget.attrs['readonly'] = True
        self.fields['cantidad_guia'].widget.attrs['class'] = "cantidad_guia"
        self.fields['cantidad_guia'].widget.attrs['style'] = "width: 100%"
        self.fields['cantidad_guia'].widget.attrs['readonly'] = True
        self.fields['cantidad_documento'].widget.attrs['class'] = "cantidad numerico"
        self.fields['cantidad_documento'].widget.attrs['style'] = "width: 100%"
        self.fields['costo'].widget.attrs['class'] = "costo numerico"
        self.fields['costo'].widget.attrs['style'] = "width: 100%"
        self.fields['p_unitario'].widget.attrs['class'] = "p_unitario"
        self.fields['p_unitario'].widget.attrs['style'] = "width: 100%"
        self.fields['p_unitario'].widget.attrs['readonly'] = True

class BuscadorInventarioForm(forms.Form):
    fecha = forms.DateField(label='Fecha Inicial: ', required=False, widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha_final = forms.DateField(label='Fecha Final: ', required=False, widget=forms.DateInput(format=("%Y-%m-%d")))
    prov = forms.ChoiceField(choices=[], label="Proveedor: ", required=False)
    cliente = forms.ChoiceField(choices=[], label="Cliente: ", required=False)
    tipo_documento = forms.ChoiceField(choices=[], label="Tipo de Documento: ", required=False)
    item = forms.ChoiceField(choices=[], label=u"Item: ", required=False)
    ndocumento = forms.CharField(label="# Documento: ", required=False)
    motivo = forms.ChoiceField(choices=[], label="Motivo: ", required=False)
    tipo_movimiento = forms.ChoiceField(choices=tipo_movimiento_inv, label="Tipo Movimiento: ", required=False)
    num_comp = forms.CharField(max_length=30, label="# de Comprobante:", required=False)
    concepto = forms.CharField(max_length=250, label="Concepto")
    page = forms.IntegerField(required=False)

    def getFecha(self):
        value = self.cleaned_data['fecha']
        return value

    def getFechaFinal(self):
        value = self.cleaned_data['fecha_final']
        if value is not None:
            return value
        else:
            return datetime.datetime.now().date()

    def getProveedor(self):
        try:
            value = self.data['prov']
            return Proveedores.objects.get(id=value)
        except:
            return None

    def getItem(self):
        try:
            value = self.data['item']
            return Item.objects.get(id=value)
        except:
            return None

    def getCliente(self):
        try:
            value = self.data['cliente']
            return Cliente.objects.get(id=value)
        except:
            return None

    def getTipoDoc(self):
        try:
            value = self.data['tipo_documento']
            return Documento.objects.get(id=value)
        except:
            return None

    def getMotivo(self):
        try:
            value = self.data['motivo']
            return Tipo_Mov_Inventario.objects.get(id=value)
        except:
            return None

    def getTipoMovimiento(self):
        try:
            value = self.data['tipo_movimiento']
            return value
        except:
            return None

    def getNumeroDocumento(self):
        try:
            value = self.data['ndocumento']
            return value
        except:
            return ""

    def getNumComprobante(self):
        try:
            value = self.data["num_comp"]
            return str(value).strip(' \t\n\r')
        except:
            return ""

    def getConcepto(self):
        try:
            value = self.data['concepto']
            return value
        except:
            return ""

    def __init__(self, *args, **kwargs):
        super(BuscadorInventarioForm, self).__init__(*args, **kwargs)
        self.fields['fecha'].widget.attrs['data-date-format']="yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['placeholder']="aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['class']="control-fecha numerico"

        self.fields['fecha_final'].widget.attrs['data-date-format']="yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_final'].widget.attrs['class'] = "control-fecha numerico"

        self.fields['prov'].widget.attrs['class']="selectpicker show-tick"
        self.fields['prov'].widget.attrs['data-style'] = "slc-primary"
        self.fields['prov'].widget.attrs['data-width'] = "100%"
        self.fields['prov'].widget.attrs['data-live-search'] = True
        self.fields['prov'].choices = [('', '')] + [(x.id, x.razon_social[0:30]) for x in Proveedores.objects.exclude(status=0)]

        self.fields['cliente'].widget.attrs['class']="selectpicker show-tick"
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        self.fields['cliente'].choices = [('', '')] + [(x.id, x.razon_social[0:30]) for x in Cliente.objects.exclude(status=0)]


        self.fields['item'].choices = [("", "")] + [(x.id, x.codigo+" - "+x.nombre) for x in Item.objects.filter(status=1, nombre__isnull=False, descripcion__isnull=False).order_by("codigo")]
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['item'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['item'].widget.attrs['data-width'] = "100%"

        self.fields['tipo_documento'].choices = [("","")] + [(x.id, x.codigo_documento+" - "+x.descripcion_documento) for x in Documento.objects.filter(status=1).order_by("codigo_documento")]
        self.fields['tipo_documento'].widget.attrs['class']="selectpicker show-tick"
        self.fields['tipo_documento'].widget.attrs['data-style']="slc-primary"
        self.fields['tipo_documento'].widget.attrs['data-width']="100%"

        self.fields['motivo'].choices = [("", "")] + [ ( x.id, x.descripcion ) for x in Tipo_Mov_Inventario.objects.filter(status=1) ]
        self.fields['motivo'].widget.attrs['class']="selectpicker show-tick"
        self.fields['motivo'].widget.attrs['data-style']="slc-primary"
        self.fields['motivo'].widget.attrs['data-width']="100%"

        self.fields['tipo_movimiento'].widget.attrs['class']="selectpicker show-tick"
        self.fields['tipo_movimiento'].widget.attrs['data-style']="slc-primary"
        self.fields['tipo_movimiento'].widget.attrs['data-width']="100%"

        self.fields['ndocumento'].widget.attrs['placeholder']="001-001-000000000"
        self.fields['ndocumento'].widget.attrs['class'] = "numerico_doc"

        self.fields['num_comp'].widget.attrs['placeholder'] = "Xin20XX000000"

        self.fields['concepto'].widget.attrs['max_lenght'] = "250"


class FormGuiaRemisionCabecera(forms.Form):
    serie = forms.ChoiceField(choices=[], label=u"Serie")
    fecha_inicio = forms.DateField(label=u'F. Inicio', widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha_final = forms.DateField(label=u'F. Final ', widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha_emision = forms.DateField(label=u"Fecha de Emisión", widget=forms.DateInput(format=("%Y-%m-%d")))
    cliente = forms.IntegerField(label="Cliente")
    motivo_guia = forms.ChoiceField(label="Motivo")

    compania = forms.CharField(max_length=220, label=u"Compañía")
    ruc_remision = forms.CharField(max_length=100, label="RUC")
    ciudad = forms.IntegerField(label="Ciudad")
    direccion_guia = forms.ChoiceField(choices=[], label=u"Dirección")
    direccion_conductor = forms.CharField(label=u"Dirección")

    def getSerie(self):
        try:
            value = self.data['serie']
            return VigenciaDocEmpresa.objects.get(id=value)
        except VigenciaDocEmpresa.DoesNotExist:
            return None

    def getFechaEmision(self):
        value = self.cleaned_data['fecha_emision']
        return value

    def getFechaInicio(self):
        value = self.cleaned_data['fecha_inicio']
        return value

    def getFechaFinal(self):
        value = self.cleaned_data['fecha_final']
        return value

    def getCliente(self):
        try:
            value = self.data['cliente']
            return Cliente.objects.get(id=value)
        except Cliente.DoesNotExist:
            return None

    def getCiudad(self):
        try:
            value = self.data['ciudad']
            return Ciudad.objects.get(id=value)
        except Ciudad.DoesNotExist:
            return None

    def getMotivoGuia(self):
        try:
            value = self.data['motivo_guia']
            return TipoMovGuiaRemision.objects.get(id=value)
        except TipoMovGuiaRemision.DoesNotExist:
            return None

    def getCompania(self):
        return self.data['compania']

    def getRuc(self):
        return self.data['ruc_remision']

    def getDireccionGuia(self):
        return self.data['direccion_guia']

    def getDireccionConductor(self):
        return self.data['direccion_conductor']

    def __init__(self, *args, **kwargs):
        super(FormGuiaRemisionCabecera, self).__init__(*args, **kwargs)
        self.fields['serie'].choices = [(x.id, x.serie) for x in VigenciaDocEmpresa.objects.filter(status=1).filter(fecha_emi__lte=datetime.datetime.now(), fecha_vencimiento__gte=datetime.datetime.now()).filter(documento_sri=25).order_by("serie")]
        self.fields['serie'].widget.attrs['class'] = "selectpicker show-tick serie_guia_rem"
        self.fields['serie'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['serie'].widget.attrs['data-width'] = "100%"

        self.fields['compania'].widget.attrs['style'] = "width: 100%"

        self.fields['fecha_inicio'].widget.attrs['style'] = "width: 100%"
        self.fields['fecha_inicio'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_inicio'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_inicio'].widget.attrs['class'] = "input-calendario calendario-gris datepicker"

        self.fields['fecha_emision'].widget.attrs['style'] = "width: 100%"
        self.fields['fecha_emision'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_emision'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_emision'].widget.attrs['class'] = "input-calendario calendario-gris datepicker"

        self.fields['fecha_final'].widget.attrs['style'] = "width: 100%"
        self.fields['fecha_final'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_final'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['class'] = "input-calendario calendario-gris datepicker"

        self.fields['cliente'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['cliente'].widget.attrs['data-url'] = reverse("buscar_clientes")
        self.fields['cliente'].widget.attrs['placeholder'] = "Seleccionar un Cliente"

        self.fields['ciudad'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['ciudad'].widget.attrs['data-url'] = reverse("buscar_ciudad")
        self.fields['ciudad'].widget.attrs['placeholder'] = "Seleccionar una ciudad"

        self.fields['direccion_guia'].widget.attrs['placeholder'] = "Búsqueda Cod - CI - Nombre"
        self.fields['direccion_guia'].widget.attrs['class']="selectpicker show-tick"
        self.fields['direccion_guia'].widget.attrs['data-live-search'] = True
        self.fields['direccion_guia'].choices = [("", "")]+[(x.id, x.descripcion) for x in Cliente_Direccion.objects.filter(status=1)]
        self.fields['direccion_guia'].widget.attrs['data-style'] = "slc-primary"
        self.fields['direccion_guia'].widget.attrs['title'] = u"Seleccione una Dirección"
        self.fields['direccion_guia'].widget.attrs['data-width'] = "100%"
        self.fields['direccion_guia'].widget.attrs['data-size'] = "auto"

        self.fields['motivo_guia'].widget.attrs['placeholder'] = "Búsqueda Cod - CI - Nombre"
        self.fields['motivo_guia'].choices = [("", "")]+[(x.id, x.descripcion) for x in TipoMovGuiaRemision.objects.filter(status=1)]
        self.fields['motivo_guia'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['motivo_guia'].widget.attrs['data-live-search'] = True
        self.fields['motivo_guia'].widget.attrs['data-style'] = "slc-primary"
        self.fields['motivo_guia'].widget.attrs['data-width'] = "100%"
        self.fields['motivo_guia'].widget.attrs['data-size'] = "auto"


class SelectItemConInterno(forms.Select):
    allow_multiple_selected = False

    def __init__(self, attrs=None, choices=()):
        super(SelectItemConInterno, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)

    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''

        if option_value != "":
            costo = Item.objects.get(id=option_value).costo
            if costo is None:
                costo = 0.0
            try:
                unidad = Item.objects.get(id=option_value).unidad.descripcion
            except:
                unidad = ""

            return format_html(u'<option value="{0}" {1} data-costo={2} data-unidad={3}> {4} </option>',
                               option_value,
                               selected_html,
                               costo,
                               unidad,
                               unicode(force_text(option_label)))
        else:
            return format_html('<option value="{0}" {1}>{2}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label))


class ConsumoInternoForm(forms.Form):
    item = forms.IntegerField()
    cantidad = forms.FloatField()
    unidad_medida = forms.CharField(max_length=100, required=False)
    costo = forms.FloatField()
    cuenta = forms.IntegerField()

    def __init__(self, *args, **kwargs):
        super(ConsumoInternoForm, self).__init__(*args, **kwargs)
        self.id_button_add_form_name = "add_con_interno"
        self.id_table = "tabla_con_interno"
        self.formset_prefix = "detalle_con_interno"
        self.url_get_row = reverse('get_fila_form_consumo_interno_inventario')

        self.fields['item'].widget.attrs['class'] = "ajaxcombobox item"
        self.fields['item'].widget.attrs['data-url'] = reverse("buscar_items")
        self.fields['item'].widget.attrs['placeholder'] = "Seleccionar un Item"

        self.fields['cantidad'].widget.attrs['class'] = "cantidad numerico"
        self.fields['cantidad'].widget.attrs['style'] = "width: 100%"

        self.fields['unidad_medida'].widget.attrs['readonly'] = True
        self.fields['unidad_medida'].widget.attrs['class'] = "unidad inhabilitado"

        self.fields['costo'].widget.attrs['readonly'] = True
        self.fields['costo'].widget.attrs['class'] = "mostrar_costo inhabilitado"

        self.fields['cuenta'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['cuenta'].widget.attrs['data-url'] = reverse("buscar_cuenta_codigo")
        self.fields['cuenta'].widget.attrs['placeholder'] = "Seleccione una cuenta"


class CierreAnualForm(forms.Form):
    anio = forms.IntegerField(label=u"Años", required=False)

    def getAnio(self):
        try:
            value = self.data["anio"]
            return int(value)
        except:
            return 0

    def __init__(self, *args, **kwargs):
        super(CierreAnualForm, self).__init__(*args, **kwargs)
        self.fields['anio'].widget.attrs['class'] = "anio"