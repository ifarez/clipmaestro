#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from contabilidad.models import *
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.forms import ModelForm
from django.db.models import Q
from librerias.funciones.validacion_formularios import *
from librerias.funciones.funciones_vistas import emite_docs_electronicos

solo_letras = u'^[A-Za-zÑñáéíóúÁÉÍÓÚ ]+$'
solo_numeros = u'^[0-9]+$'
solo_alfanumericas = u'[a-zA-Z0-9_ \\ s]+$'
solo_decimal = u'^\d+(\.\d{1,2})?$'

def validate_ruc_unico(value):
    try:
        if(value.index(' ')!=None):
            raise ValidationError(u'%s No es permitido porque tiene espacios en blanco' % value)
    except ValueError:
        pass
    if Proveedores.objects.filter(Q(ruc=value), Q(status=1) | Q(status=2)).exists():
        raise ValidationError(u'El Nº de identificación ya ha sido ingresado')
    else:
        return True


class ProveedorFormOnlyRead(forms.Form):
    grupo = forms.ChoiceField(choices=[], label="Grupo Proveedor")
    tipo_identificacion = forms.ChoiceField(choices=[], label="Tipo de Identificación")
    num_identificacion = forms.CharField(label=u"Nº de Identificación")
    tipo_proveedor = forms.ChoiceField(choices=[], label=u"Tipo Proveedor", required=False)
    razon_social = forms.CharField(widget=forms.Textarea, label=u"Razon Social", validators=[validador_espacios])
    nombre = forms.CharField(widget=forms.Textarea, label=u"Nombre Comercial", required=False)
    ciudad = forms.ChoiceField(choices=[],  label=u"Ciudad", required=False)
    direccion = forms.CharField(widget=forms.Textarea, label=u"Dirección", required=False)
    telefono = forms.CharField(label=u"Teléfono", required=False, validators=[validador_espacios])
    actividad = forms.ChoiceField(choices=[], label=u"Sector", required=False)
    forma_pago = forms.ChoiceField(choices=[], label=u"Forma de Pago", required=False)
    plazo = forms.CharField(label=u"Plazo", required=False)
    monto_credito = forms.RegexField(label=u"Monto Crédito", required=False, regex=solo_decimal)

    if emite_docs_electronicos():
        email = forms.EmailField(max_length=54, required=False, label=u"Email")
    else:
        email = forms.EmailField(max_length=54, required=False, label=u"Email")


    def __init__(self, *args, **kwargs):
        super(ProveedorFormOnlyRead, self).__init__(*args, **kwargs)
        self.fields['tipo_identificacion'].choices = [(x.codigo, x.descripcion) for x in Identificacion.objects.filter(status=1).exclude(codigo="F")]
        self.fields['num_identificacion'].widget.attrs['maxlength'] = "13"
        self.fields['tipo_identificacion'].widget.attrs['data-name'] = "combo_tipo_id"

        self.fields['telefono'].widget.attrs['maxlength'] = "15"
        self.fields['razon_social'].widget.attrs['maxlength'] = "150"
        self.fields['nombre'].widget.attrs['maxlength'] = "150"
        self.fields['razon_social'].widget.attrs['style'] = "height: 110px;"
        self.fields['nombre'].widget.attrs['style'] = "height: 110px;"
        self.fields['direccion'].widget.attrs['style'] = "height: 110px;"

        self.fields['direccion'].widget.attrs['maxlength'] = "150"
        self.fields['plazo'].widget.attrs['maxlength'] = "4"
        self.fields['monto_credito'].widget.attrs['maxlength'] = "7"

        self.fields['grupo'].choices = [(x.id, x.codigo+"-"+x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(status=1, tipo__alias=3, nivel=5)]
        self.fields['grupo'].widget.attrs['class'] = "selectpicker"
        self.fields['grupo'].widget.attrs['data-style'] = "slc-primary"

        self.fields['tipo_proveedor'].widget.attrs['class']="selectpicker tipo_proveedor"
        self.fields['tipo_proveedor'].widget.attrs['data-style']="slc-primary"
        self.fields['tipo_proveedor'].widget.attrs['data-name'] = "combo_tipo_proveedor"
        self.fields['tipo_proveedor'].choices = [(x.id, x.descripcion) for x in TipoPersona.objects.filter(status=1)]

        self.fields['actividad'].choices = [("", "")]+[(x.id, x.descripcion) for x in Sector.objects.filter(status=1)]
        self.fields['actividad'].widget.attrs['class'] = "selectpicker"
        self.fields['actividad'].widget.attrs['data-style'] = "slc-primary"

        self.fields['forma_pago'].choices = [("", "")]+[(x.id, x.descripcion) for x in TipoPago.objects.filter(status=1)]
        self.fields['forma_pago'].widget.attrs['class']="selectpicker"
        self.fields['forma_pago'].widget.attrs['data-style'] = "slc-primary"

        self.fields['ciudad'].choices = [("", "")]+[(x.id, x.descripcion) for x in Ciudad.objects.filter(status=1)]
        self.fields['ciudad'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['ciudad'].widget.attrs['data-style'] = "slc-primary"
        self.fields['ciudad'].widget.attrs['data-size'] = "15"
        self.fields['ciudad'].widget.attrs['data-live-search'] = True

        self.fields['tipo_identificacion'].widget.attrs['class']="selectpicker"
        self.fields['tipo_identificacion'].widget.attrs['data-style']="slc-primary"

        self.fields['monto_credito'].widget.attrs['style'] = "text-align: right;"
        self.fields['monto_credito'].widget.attrs['placeholder']="0.00"
        self.fields['telefono'].widget.attrs['placeholder'] = "(99) 9999-9999"
        self.fields['num_identificacion'].widget.attrs['placeholder'] = ""
        self.fields['plazo'].widget.attrs['placeholder'] = "Número de días, Ej: 15"

        self.fields['plazo'].widget.attrs['style'] = "text-align: right;"
        self.fields['plazo'].widget.attrs['class'] = "numerico"

        self.fields['grupo'].widget.attrs['readonly'] = True
        self.fields['grupo'].widget.attrs['disabled'] = True

        self.fields['email'].widget.attrs['readonly'] = True
        self.fields['email'].widget.attrs['disabled'] = True

        self.fields['tipo_identificacion'].widget.attrs['readonly'] = True
        self.fields['tipo_identificacion'].widget.attrs['disabled'] = True

        self.fields['tipo_proveedor'].widget.attrs['readonly'] = True
        self.fields['tipo_proveedor'].widget.attrs['disabled'] = True

        self.fields['razon_social'].widget.attrs['readonly'] = True
        self.fields['nombre'].widget.attrs['readonly'] = True

        self.fields['telefono'].widget.attrs['readonly'] = True
        self.fields['direccion'].widget.attrs['readonly'] = True

        self.fields['actividad'].widget.attrs['readonly'] = True
        self.fields['actividad'].widget.attrs['disabled'] = True

        self.fields['ciudad'].widget.attrs['readonly'] = True
        self.fields['ciudad'].widget.attrs['disabled'] = True

        self.fields['forma_pago'].widget.attrs['readonly'] = True
        self.fields['forma_pago'].widget.attrs['disabled'] = True

        self.fields['plazo'].widget.attrs['readonly'] = True
        self.fields['monto_credito'].widget.attrs['readonly'] = True
        self.fields['num_identificacion'].widget.attrs['readonly'] = True


class ProveedorForm(forms.Form):
    grupo = forms.ChoiceField(choices=[], label="Grupo Proveedor")
    tipo_identificacion = forms.ChoiceField(choices=[], label="Tipo de Identificación")
    num_identificacion = forms.CharField(label="Nº de Identificación")
    tipo_proveedor = forms.ChoiceField(choices=[], label="Tipo Proveedor", required=False)
    razon_social = forms.CharField(label=u"Razon Social", max_length=220, validators=[validador_espacios])
    nombre = forms.CharField(label=u"Nombre Comercial",  max_length=220, required=False)
    ciudad = forms.ChoiceField(choices=[],  label=u"Ciudad", required=False)
    direccion = forms.CharField(label=u"Dirección", required=False, max_length=220)
    telefono = forms.CharField(label="Teléfono", required=False, validators=[validador_espacios])
    actividad = forms.ChoiceField(choices=[], label=u"Sector", required=False)

    if emite_docs_electronicos():
        email = forms.EmailField(max_length=54, required=True, label=u"Email")
    else:
        email = forms.EmailField(max_length=54, required=False, label=u"Email")

    forma_pago = forms.ChoiceField(choices=[], label="Forma de Pago", required=False)
    plazo = forms.CharField(label="Plazo", required=False)
    monto_credito = forms.RegexField(label="Monto Crédito", required=False, regex=solo_decimal)

    def getRuc(self):
        return self.data["num_identificacion"]

    def getNombre(self):
        try:
            value = self.data['nombre']
            return value
        except:
            return ""

    def getRazonSocial(self):
        return self.data["razon_social"]

    def getDireccion(self):
        return self.data["direccion"]

    def getMontoCredito(self):
        return self.data["monto_credito"]

    def getTelefono(self):
        return self.data["telefono"]

    def getCiudad(self):
        return self.data["ciudad"]

    def getTipoProveedor(self):
        return self.data["tipo_proveedor"]

    def getGrupoProveedor(self):
        return self.data["grupo"]

    def getPlazo(self):
        return self.data["plazo"]

    def getActividad(self):
        return self.data["actividad"]

    def getFormaPago(self):
        return self.data["forma_pago"]

    def getTipoIdentificacion(self):
        return self.data["tipo_identificacion"]

    def get_email(self):
        return self.data["email"]

    def clean(self):
        """
            Función para validar el formulario
        """
        from django.core.validators import validate_email
        from django.core.exceptions import ValidationError

        email = self.cleaned_data.get('email')
        if emite_docs_electronicos():
            try:
                validate_email(email)
            except ValidationError:
                self._errors["email"] = u"El email es una dato obligatorio"

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(ProveedorForm, self).__init__(*args, **kwargs)
        self.fields['tipo_identificacion'].choices = [(x.codigo, x.descripcion) for x in Identificacion.objects.filter(status=1).exclude(codigo="F")]
        self.fields['num_identificacion'].widget.attrs['maxlength'] = "13"
        self.fields['num_identificacion'].widget.attrs['placeholder'] = ""
        self.fields['num_identificacion'].widget.attrs['class'] = "numero_natural"
        self.fields['tipo_identificacion'].widget.attrs['data-name'] = "combo_tipo_id"

        self.fields['telefono'].widget.attrs['maxlength'] = "15"

        #self.fields['razon_social'].widget.attrs['maxlength'] = "200"

        self.fields['nombre'].widget.attrs['maxlength'] = "150"
        self.fields['direccion'].widget.attrs['maxlength'] = "150"
        self.fields['plazo'].widget.attrs['maxlength'] = "4"
        self.fields['monto_credito'].widget.attrs['maxlength'] = "7"

        self.fields['grupo'].choices = [(x.id, x.codigo+"-"+x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(status=1, tipo__alias=3, nivel=5)]
        self.fields['grupo'].widget.attrs['class'] = "selectpicker"
        self.fields['grupo'].widget.attrs['data-style'] = "slc-primary"

        self.fields['email'].widget.attrs['placeholder'] = "info@dominio.com"

        self.fields['tipo_proveedor'].widget.attrs['class']="selectpicker tipo_proveedor"
        self.fields['tipo_proveedor'].widget.attrs['data-style']="slc-primary"
        self.fields['tipo_proveedor'].widget.attrs['data-name'] = "combo_tipo_proveedor"
        self.fields['tipo_proveedor'].choices = [(x.id, x.descripcion) for x in TipoPersona.objects.filter(status=1)]


        self.fields['actividad'].choices = [("", "")]+[(x.id, x.descripcion) for x in Sector.objects.filter(status=1)]
        self.fields['actividad'].widget.attrs['class'] = "selectpicker"
        self.fields['actividad'].widget.attrs['data-style'] = "slc-primary"

        self.fields['forma_pago'].choices = [("", "")]+[(x.id, x.descripcion) for x in TipoPago.objects.filter(status=1)]
        self.fields['forma_pago'].widget.attrs['class']="selectpicker"
        self.fields['forma_pago'].widget.attrs['data-style'] = "slc-primary"

        self.fields['ciudad'].choices = [("", "")]+[(x.id, x.descripcion) for x in Ciudad.objects.filter(status=1)]
        self.fields['ciudad'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['ciudad'].widget.attrs['data-style'] = "slc-primary"
        self.fields['ciudad'].widget.attrs['data-size'] = "15"
        self.fields['ciudad'].widget.attrs['data-live-search'] = True

        self.fields['tipo_identificacion'].widget.attrs['class']="selectpicker"
        self.fields['tipo_identificacion'].widget.attrs['data-style']="slc-primary"

        self.fields['monto_credito'].widget.attrs['style'] = "text-align: right;"
        self.fields['monto_credito'].widget.attrs['placeholder']="0.00"
        self.fields['telefono'].widget.attrs['placeholder'] = "(99) 9999-9999"

        self.fields['plazo'].widget.attrs['placeholder'] = "Número de días, Ej: 15"

        self.fields['plazo'].widget.attrs['style'] = "text-align: right;"
        self.fields['plazo'].widget.attrs['class'] = "numerico"


class BuscadorFormProveedor(forms.Form):
    tipo_identificacion = forms.ChoiceField(choices=[], label=u"Tipo de Identificación")
    num_id = forms.CharField(label=u"# Identificación")
    razon_social = forms.CharField(label=u"Razón Social")
    nombre = forms.CharField(label=u"Nombre Comercial")


    def getNombre(self):
        return self.data["nombre"]

    def getNumId(self):
        return self.data["num_id"]

    def getTipoIdentificacion(self):
        return self.data["tipo_identificacion"]

    def getRazonSocial(self):
        return self.data["razon_social"]


    def __init__(self, *args, **kwargs):
        super(BuscadorFormProveedor, self).__init__(*args, **kwargs)
        self.fields['tipo_identificacion'].choices = [(x.codigo, x.descripcion) for x in Identificacion.objects.exclude(status=0)]
        self.fields['tipo_identificacion'].widget.attrs['class']="selectpicker"
        self.fields['tipo_identificacion'].widget.attrs['data-style'] = "slc-primary"

        self.fields['num_id'].widget.attrs['maxlength'] = "13"
        self.fields['razon_social'].widget.attrs['maxlength'] = "65"
        self.fields['nombre'].widget.attrs['maxlength'] = "75"

