#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'

from contabilidad.formularios.ProveedorForm import *
from librerias.funciones.funciones_vistas import *
import operator
from django.contrib import messages
from contabilidad.formularios.ComprasForm import *
from django.db import connection
from librerias.funciones.comprobantes_electronicos import *

class ErrorCompras(Exception):
    def __init__(self, valor):
        self.valor = unicode(valor)

    def __str__(self):
        return self.valor


def busqueda_compras(buscador):
    """
    function busqueda_compras(buscador)
    @ Return CompraCabecera Object
    """
    predicates = []

    if buscador.getFecha() is not None:
        predicates.append(('fecha_reg__gte', buscador.getFecha()))

    predicates.append(('fecha_reg__lte', buscador.getFechaFinal()))
    predicates.append(('num_comp__icontains', buscador.getNumComprobante().replace(" ", "")))
    predicates.append(('num_doc__icontains', buscador.getNumeroDocumento().replace(" ", "")))
    predicates.append(('concepto__icontains', buscador.getConcepto()))

    if buscador.getEstado() != "":
        estado = Estados.objects.get(id=buscador.getEstado())
        predicates.append(('status', estado.codigo))
    if buscador.getNumRetencion() != "":
        try:
            split_ret = str(buscador.getNumRetencion()).replace(" ", "").split("-")
            serie_ret = split_ret[0]+"-"+split_ret[1]
            num_ret = split_ret[2]
        except:
            serie_ret = ""
            num_ret = buscador.getNumRetencion()
        predicates.append(('retencioncompracabecera__numero_ret__contains', num_ret))
        predicates.append(('retencioncompracabecera__vigencia_doc_empresa__serie__contains', serie_ret))

    if emite_docs_electronicos():
        if buscador.getEstadoDocElect() != "":
            predicates.append(('retencioncompracabecera__estado_firma', buscador.getEstadoDocElect()))



    if buscador.getProveedor() is not None:
        predicates.append(('proveedor', buscador.getProveedor()))
    if buscador.getTipoDoc() is not None:
        predicates.append(('documento', buscador.getTipoDoc()))
    if buscador.getFormaPago() is not None:
        predicates.append(('tipo_pago', buscador.getFormaPago()))
    if buscador.getCuenta() is not None:
        predicates.append(('compradetalle__plan_cuenta', buscador.getCuenta()))
    if buscador.getCodigoIva() is not None:
        predicates.append(('compradetalle__codigo_iva', buscador.getCodigoIva()))
    if buscador.getCodigoRetFte() is not None:
        predicates.append(('compradetalle__codigo_ret_fte__codigo', buscador.getCodigoRetFte().codigo))
    if buscador.getCodigoRetIVA() is not None:
        predicates.append(('retencioncompracabecera__retencioncompradetalle__codigo_sri__codigo', buscador.getCodigoRetIVA().codigo))


    # reate the list of Q objects and run the queries as above..
    q_list = [Q(x) for x in predicates]
    entries = CompraCabecera.objects.extra(where=["baseiva + base0 >= %s AND baseiva + base0 <= %s"], params=[buscador.getRangoMontoIni(), buscador.getRangoMontoHasta()]).filter(reduce(operator.and_, q_list)).order_by("-fecha_reg", "-num_comp").distinct()

    return entries


def busqueda_compras_old(proveedor, fecha_ini, fecha_fin, n_doc, t_doc, contcred, local_ext):
    campos_string = [proveedor, n_doc]
    campos_obj = [t_doc]
    campos_exactos = [contcred, local_ext]
    fields_campos_exactos = ['pago_cont_cred','local_ext']
    bandera = True
    i = 0
    while bandera == True:
        if campos_exactos[i] == "":
            campos_exactos.remove(campos_exactos[i])
            fields_campos_exactos.remove(fields_campos_exactos[i])
        else:
            i += 1
        if i == (len(campos_exactos)):
            bandera = False

    fields = ['proveedor__nombre', 'numero_doc_sri']  # campos de búsqueda pertenecientes al modelo Contacto
    fields_obj = ['tipo_documento_sri']
    where_string = reduce(operator.and_, [Q(**{fields[i] + '__icontains': campos_string[i]}) for i in range(0, len(campos_string))])
    where_obj = reduce(operator.or_, [Q(**{field: v}) for field in fields_obj for v in campos_obj])  # conformando la consulta
    if len(campos_exactos) != 0:
        where_campos_exactos = reduce(operator.and_, [Q(**{fields_campos_exactos[i]: campos_exactos[i]}) for i in range(0,len(campos_exactos))]) # conformando la consulta
    else:
        where_campos_exactos = reduce(operator.or_, [Q(**{})])

    if t_doc is not None:
        return CompraCabecera.objects.exclude(status=0).filter(where_string).filter(where_obj).filter(where_campos_exactos).filter(fecha_emision__range=(fecha_ini, fecha_fin)).order_by("-fecha_emision")
    else:
        return CompraCabecera.objects.exclude(status=0).filter(where_string).filter(where_campos_exactos).filter(fecha_emision__range = (fecha_ini, fecha_fin)).order_by("-fecha_emision")


def CalcularTotales(detalleform):
    """
    La función retorna los totales de la compra que se utilizarán en la
    transacción
    @Return (total_iva, tmonto0, tmontoi, tmontoi_cta_iva_pagado, tmontoi_sin_cred_trib, total_ret_fte, total_ret_iva)
    """
    tmonto0 = 0.0
    tmontoi = 0.0
    tmontoi_sin_cred_trib = 0.0
    tmontoi_cta_iva_pagado = 0.0
    total_ret_fte = 0.0
    total_ret_iva = 0.0
    retenciones_fte = []  # Variable que contendra las retenciones sumarizadas
    retenciones_iva = []  # Variable que contendra las retenciones sumarizadas
    id_ret = []  # Variable que contendra los id retenciones sumarizadas
    id_ret_iva = []
    if detalleform.is_valid():  # Línea para que el formulario sea validado y tenga el atributo cleaned_data
        for form in detalleform:
            informacion = form.cleaned_data
            valor = 0.0

            if informacion.get("valor") != "" and informacion.get("valor") is not None:
                try:
                    valor = redondeo(float(informacion.get("valor")), 2)
                except:
                    errors = form._errors.setdefault("valor", ErrorList())
                    errors.append(u"Valor no válido")
            else:
                errors = form._errors.setdefault("valor", ErrorList())
                errors.append(u"Valor no válido")

            codigo_sri_iva = Codigo_SRI.objects.get(id=informacion.get("valor_iva"))
            iva = codigo_sri_iva.porcentaje

            if iva == 0:
                tmonto0 += valor
            else:
                tmontoi += valor

            #############################################
            #   Sin crédito Tributario iva al gasto     #
            #############################################
            if codigo_sri_iva.iva_al_gasto:
                tmontoi_sin_cred_trib += ((valor * iva)/100)
            elif iva != 0:
                tmontoi_cta_iva_pagado += ((valor * iva)/100)

            #################################################
            # Te totaliza los valores de ret_fte por codigo #
            #################################################

            try:
                cod_ret_fte_id = informacion.get("fte")
                cod_ret_fte = Codigo_SRI.objects.get(id=cod_ret_fte_id)
                if cod_ret_fte.porc_variable:
                    porc_ret_fte = informacion.get("porc_ret_iva")
                else:
                    porc_ret_fte = cod_ret_fte.porcentaje

                if porc_ret_fte not in id_ret:
                    retenciones_fte.append([porc_ret_fte, valor])
                    id_ret.append(porc_ret_fte)
                else:
                    for obj in retenciones_fte:
                        if obj[0] == porc_ret_fte:
                            obj[1] += valor
            except (Codigo_SRI.DoesNotExist, ValueError):
                pass

            #################################################
            # Te totaliza los valores de ret_iva por codigo #
            #################################################

            try:
                cod_ret_iva_id = informacion.get("iva", None)
                cod_ret_iva = Codigo_SRI.objects.get(id=cod_ret_iva_id)
                if cod_ret_iva.porc_variable:
                    porc_ret_iva = informacion.get("porc_ret_iva")
                else:
                    porc_ret_iva = cod_ret_iva.porcentaje

                if porc_ret_iva not in id_ret_iva:
                    retenciones_iva.append([porc_ret_iva, (valor * iva / 100)])
                    id_ret_iva.append(porc_ret_iva)
                else:
                    for obj in retenciones_iva:
                        if obj[0] == porc_ret_iva:
                            obj[1] += (valor * iva / 100)
            except (Codigo_SRI.DoesNotExist, ValueError):
                pass

    tmontoi_cta_iva_pagado = redondeo(tmontoi_cta_iva_pagado, 2)
    tmontoi_sin_cred_trib = redondeo(tmontoi_sin_cred_trib, 2)

    total_iva = tmontoi_cta_iva_pagado

    for obj in retenciones_fte:
        total_ret_fte += redondeo((obj[1] * obj[0] / 100), 2)

    for obj in retenciones_iva:
        total_ret_iva += redondeo((obj[1] * obj[0] / 100), 2)

    return total_iva, tmonto0, tmontoi, tmontoi_cta_iva_pagado, tmontoi_sin_cred_trib, total_ret_fte, total_ret_iva


def inst_form_cab_compra(compra, ats, now, request):
    """
    Instancia el formulario compra cabecera
    :param compra:
    :param ats:
    :param now:
    :param request:
    :return:
    """
    if compra.tipo_pago.id == 2:
        plazo = compra.plazo
    else:
        plazo = ""
    if ats.tipo_pago_sri.id == 2:
        pais = ats.pais.id
    else:
        pais = ""

    if es_nota_credito(compra.documento.id) or es_nota_debito(compra.documento.id):
        documento_modi = Doc_Modifica.objects.get(compra_cabecera=compra)
        tipo_doc_mod = documento_modi.tipo_documento.codigo_documento
        serie_doc_modif = documento_modi.numero_documento
        autorizacion_modif = documento_modi.autorizacion
    else:
        #### Cambiarle a tipo de doc que modifica un id
        tipo_doc_mod = ""
        serie_doc_modif = ""
        autorizacion_modif = ""

    if ats.vence is not None:
        fecha_vence = ats.vence.strftime('%Y-%m-%d')
    else:
        fecha_vence = ""

    # Contrato
    try:
        contrato_det = ContratoDetalle.objects.filter(modulo_id=id, tipo_comprobante_id=1)
        if len(contrato_det) == 0:
            id_contrato = ""
        elif len(contrato_det) == 1:
            id_contrato = contrato_det[0].contrato_cabecera.id
        else:
            id_contrato = ""
            messages.success(request, u"La compra que está queriendo anular tiene asignados varios contratos a ella, si la anula perderá"
                                      u" la relación a dichos contratos")
    except:
        id_contrato = ""

    try:
        secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=1)  # Secuencia de Compras
        fecha_comp = secuencia.fecha_actual
    except SecuenciaTipoComprobante.DoesNotExist:
        fecha_comp = get_parametros_empresa().fecha_inicio_sistema
        if fecha_comp is None:
            fecha_comp = now.strftime("%Y-%m-%d")

    return {
        "fecha_asiento": fecha_comp,
        "fecha": fecha_comp,
        "proveedor": compra.proveedor.id,
        "sust_tribut": ats.sustento_tributario_sri.codigo,
        "tipo_de_doc": compra.documento.id,
        "serie": compra.num_doc,
        "autorizacion": ats.autorizacion,
        "vence": fecha_vence,
        "forma_pago": compra.tipo_pago.id,
        "plazo": plazo,
        "pago": ats.tipo_pago_sri.id,
        "pais": pais,
        "convenio_doble_tr": ats.doble_tributacion,
        "sujeto_retencion": ats.sujeto_retencion,
        "contrato": id_contrato,
        "tipo_de_doc_modif": tipo_doc_mod,
        "serie_doc_modif": serie_doc_modif,
        "autorizacion_modif": autorizacion_modif,
        "concepto": compra.concepto
    }


def inst_comp_detalle(compra_detalle, det_form, compra):
    """
    Instancia el formulario detalle de compra
    :param compra_detalle:
    :param det_form:
    :return:
    """
    ats = ATS.objects.filter(compra_cabecera=compra)[0]
    sustento = ats.sustento_tributario_sri_id

    for obj in compra_detalle:
        try:
            codigo_ret_fte = Codigo_SRI.objects.get(id=obj.codigo_ret_fte.id)
            id_cod_ret_fte = codigo_ret_fte.id
            porc_ret_fte = obj.porc_ret_fte if codigo_ret_fte.porc_variable else codigo_ret_fte.porcentaje
        except:
            id_cod_ret_fte = ""
            porc_ret_fte = ""

        if obj.centro_costo is not None:
            centro_costo_id = obj.centro_costo.id
        else:
            centro_costo_id = ""

        try:
            codigo_ret_iva = Codigo_SRI.objects.get(id=obj.codigo_ret_iva.id)
            id_cod_ret_iva = codigo_ret_iva.id
            porc_ret_iva = obj.porc_ret_iva if codigo_ret_iva.porc_variable else codigo_ret_iva.porcentaje
        except:

            id_cod_ret_iva = ""
            porc_ret_iva = ""

        if sustento == obj.sustento_tributario_id:
            check = False
        else:
            check = True

        det_form.append({"cuenta": obj.plan_cuenta.id,
                         "centro_costo": centro_costo_id, "valor_iva": obj.codigo_iva.id,
                         "sust_tribut": obj.sustento_tributario.codigo, "cambio_sust": check,
                         "fte": id_cod_ret_fte, "porc_ret_fte": porc_ret_fte, "iva": id_cod_ret_iva,
                         "porc_ret_iva": porc_ret_iva, "bs": obj.tipo_iva, "valor": obj.valor})


def inst_reembolso_compra(reembolsos, det_reem_form):
    """
    Instancia el formulario reembolso en compras
    :param reembolsos:
    :param det_reem_form:
    :return:
    """
    for obj in reembolsos:
        det_reem_form.append({"tipo_identificacion": obj.identificacion, "proveedor": obj.num_id,
                              "tipo_documento": obj.documento.id, "serie": obj.num_doc,
                              "fecha_emision": obj.fecha_emi.strftime('%Y-%m-%d'), "num_autorizacion": obj.autorizacion,
                              "base_cero": obj.base0, "base_iva": obj.baseiva})


def inst_cab_retencion_compra(retencion_cabecera):
    """
    Instancia el formulario Cabecera de Retención en la compra
    :param retencion_cabecera:
    :return:
    """
    return {"numero_retencion": retencion_cabecera.numero_ret,
            "fecha_emision_retencion": retencion_cabecera.fecha_emi.strftime('%Y-%m-%d'),
            "autorizacion_retencion": retencion_cabecera.vigencia_doc_empresa.autorizacion,
            "serie_ret": retencion_cabecera.vigencia_doc_empresa_id}


def inst_det_retencion_compra(detalle_retencion, det_ret_form):
    """
    Instancia el detalle de retención de la compra
    :param detalle_retencion:
    :param det_ret_form:
    :return:
    """
    for obj in detalle_retencion:
        det_ret_form.append({"codigo_ret": "Cod Ret. " + obj.codigo_sri.codigo, "tipo": obj.tipo_iva,
                         "cod_sri": obj.codigo_sri.id,
                         "cod_cta": obj.plan_cuenta.id,
                         "base": obj.base,
                         "porcentaje": obj.codigo_sri.porcentaje,
                         "total": obj.valor_retenido})


def CalcularTotalesReembolso(reembolso_gasto):
    """
    Calcula los totales base0 y base_iva del reembolso
    :param reembolso_gasto:
    :return:
    """
    base0 = 0
    baseiva = 0
    reembolso_gasto.is_valid()
    for form in reembolso_gasto:
        informacion = form.cleaned_data
        try:
            base0 += float(informacion.get("base_cero"))
        except ValueError:
            pass
        try:
            baseiva += float(informacion.get("base_iva"))
        except ValueError:
            pass
    return base0, baseiva


def GuardarCabecera(cabecera_compra, mayor_cabecera, detalleform, formulario, tmonto0, total_iva, tmontoi, contador, now, id_compra, reembolso_gasto, tipo, request):
    """
    function GuardarCabecera(cabecera_compra, formulario, cursor, tmonto0, tmontoi, contador, request)
    @Return None
    Details: Guarda los datos de la cabecera de la compra
    """
    if tipo != 4:  # Solo si la compra no es provisionada se puede cambiar la fecha de registro
        cabecera_compra.fecha_reg = formulario.getFecha_asiento()

    cabecera_compra.fecha_emi = formulario.getFecha()

    ##############################################################
    # Se valida las fechas del asiento con el de emisión del doc #
    ##############################################################

    if formulario.getFecha() > cabecera_compra.fecha_reg:
        contador += 1
        messages.error(request, u"La fecha de emisión es mayor a la del asiento, por "
                                u"favor corríja las fechas")

    if formulario.getFecha() > datetime.datetime.now().date():
        contador += 1
        messages.error(request, u"La fecha de emisión es mayor a la fecha actual, por favor verifique la fecha.")

    if cabecera_compra.fecha_reg > datetime.datetime.now().date():
        contador += 1
        messages.error(request, u"La fecha de registro es mayor a la fecha actual, por favor verifique la fecha.")

    cabecera_compra.base0 = tmonto0
    cabecera_compra.baseiva = tmontoi
    cabecera_compra.monto_iva = total_iva
    cabecera_compra.documento = Documento.objects.filter(status=1).get(id=formulario.getTipo_de_documento())

    # Contrato
    if formulario.getContrato() is not None:
        cabecera_compra.contrato = formulario.getContrato()

    #try:
    proveedor = Proveedores.objects.get(id=formulario.getProveedor(), status=1)
    cabecera_compra.proveedor = proveedor

    # Guardar los datos del block de facturas del proveedor
    try:
        vigencia_doc_persona = VigenciaDocPersona.objects.get(tipo_comprobante=TipoComprobante.objects.get(id=1), cliente_proveedor_id=cabecera_compra.proveedor.id, documento_sri=cabecera_compra.documento)
        if str(formulario.getSerie())[0:7] != vigencia_doc_persona.serie:
            vigencia_doc_persona.serie = str(formulario.getSerie())[0:7]
            vigencia_doc_persona.fecha_emi = cabecera_compra.fecha_emi
            vigencia_doc_persona.fecha_vencimiento = formulario.getVence()
            vigencia_doc_persona.autorizacion = formulario.getAutorizacion()
            vigencia_doc_persona.fecha_actualizacion = now
            vigencia_doc_persona.usuario_actualizacion = request.user.username
            vigencia_doc_persona.save()
    except:
        vigencia_doc_persona = VigenciaDocPersona()
        vigencia_doc_persona.cliente_proveedor_id = cabecera_compra.proveedor.id
        vigencia_doc_persona.tipo_comprobante = TipoComprobante.objects.get(id=1)
        vigencia_doc_persona.autorizacion = formulario.getAutorizacion()
        vigencia_doc_persona.documento_sri = cabecera_compra.documento
        vigencia_doc_persona.fecha_emi = cabecera_compra.fecha_emi
        vigencia_doc_persona.serie = str(formulario.getSerie())[0:7]
        vigencia_doc_persona.fecha_vencimiento = formulario.getVence()
        vigencia_doc_persona.fecha_creacion = now
        vigencia_doc_persona.usuario_creacion = request.user.username
        vigencia_doc_persona.save()

    cabecera_compra.tipo_pago = TipoPago.objects.get(id=formulario.getForma_pago())

    if int(formulario.getForma_pago()) == 2:
        cabecera_compra.plazo = formulario.getPlazo()

    ##################################################
    #  validación de posibles documentos repetidos.  #
    ##################################################
    if CompraCabecera.objects.filter(status=1).exclude(id=id_compra).filter(documento=cabecera_compra.documento, num_doc=formulario.getSerie(), proveedor=cabecera_compra.proveedor).exists():
        errors = formulario._errors.setdefault("serie", ErrorList())
        errors.append(u"Número de Documento ya ingresado, por favor verifique el número.")
        messages.error(request, u"Número de Documento ya ingresado, por favor verifique el número.")
        contador += 1

    cabecera_compra.num_doc = formulario.getSerie()
    cabecera_compra.base_ice = 0.0
    cabecera_compra.monto_ice = 0.0

    if formulario.getConcepto() == "" or formulario.getConcepto().isspace():
        cabecera_compra.concepto = proveedor.razon_social + " #Doc: " + \
                                   formulario.getSerie()
    else:
        cabecera_compra.concepto = formulario.getConcepto()

    if tipo == 4:  # Si es compra Provisionada
        cabecera_compra.status = 1
        cabecera_compra.fecha_actualizacion = now
        cabecera_compra.usuario_actualizacion = request.user.username
    else:
        ###################################
        #  Llama a la función num_comp    #
        ###################################
        num_comp = get_num_comp(1, cabecera_compra.fecha_reg, True, request)
        if num_comp != -1:
            cabecera_compra.num_comp = num_comp
        else:
            secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=1)
            messages.error(request, u"La fecha de registro de asiento es menor al último registro de compras, "
                                    u"por favor verifique la fecha de asiento, la última fecha de registro fue: "
                                    u"" + secuencia_tipo.fecha_actual.strftime("%Y-%m-%d"))
            errors = formulario._errors.setdefault("fecha_asiento", ErrorList())
            errors.append(u"La fecha de registro de asiento es menor al último registro de compras, "
                          u"por favor verifique la fecha de asiento")
            contador += 1
        cabecera_compra.fecha_creacion = now
        cabecera_compra.usuario_creacion = request.user.username

    cabecera_compra.save()

    # Guardar Mayor
    mayor_cabecera.concepto_comprobante = cabecera_compra.concepto
    mayor_cabecera.fecha = cabecera_compra.fecha_reg
    mayor_cabecera.numero_comprobante = cabecera_compra.num_comp
    mayor_cabecera.tipo_comprobante = TipoComprobante.objects.get(id=1)  # Comprobante de Compras

    mayor_cabecera.fecha_creacion = now
    mayor_cabecera.usuario_creacion = request.user.username
    mayor_cabecera.save()

    ##################
    #    N/D N/C     #
    ##################
    if es_nota_credito(formulario.getTipo_de_documento()) or es_nota_debito(formulario.getTipo_de_documento()):
        doc_modifica = Doc_Modifica()
        doc_modifica.compra_cabecera = cabecera_compra
        doc_modifica.numero_documento = str(formulario.getSerieMod())
        doc_modifica.autorizacion = formulario.getAutorMod()
        doc_modifica.tipo_documento = Documento.objects.get(id=formulario.getTipoDocMod())

        doc_modifica.fecha_creacion = now
        doc_modifica.usuario_creacion = request.user.username
        doc_modifica.save()

    ################################################
    #             Reembolso de Gastos              #
    ################################################
    iva = 0
    for form in detalleform:
        informacion = form.cleaned_data
        codigo_sri_iva = Codigo_SRI.objects.get(id=informacion.get("valor_iva"))
        if codigo_sri_iva.porcentaje != 0:
            iva = codigo_sri_iva.porcentaje

    if es_reembolso_gasto(formulario.getTipo_de_documento()):
        base0, baseiva = CalcularTotalesReembolso(reembolso_gasto)

        if base0 != cabecera_compra.base0 and baseiva != cabecera_compra.baseiva:
            messages.error(request, u"Los valores de base0 y baseIva del reembolso no cuadran con los de la compra")
            contador += 1
        if reembolso_gasto.is_valid() and contador == 0:
            for form in reembolso_gasto:
                informacion = form.cleaned_data
                reembolso = Reembolso_gasto_compra()
                reembolso.autorizacion = informacion.get("num_autorizacion")
                try:
                    reembolso.base0 = redondeo(float(informacion.get("base_cero")))
                except ValueError:
                    reembolso.base0 = 0
                try:
                    reembolso.baseiva = redondeo(float(informacion.get("base_iva")), 2)
                except ValueError:
                    reembolso.baseiva = 0

                reembolso.monto_iva = redondeo((reembolso.baseiva * iva)/100, 2)
                reembolso.compra_cabecera = cabecera_compra
                reembolso.num_id = informacion.get("proveedor")
                reembolso.num_doc = informacion.get("serie")
                reembolso.fecha_emi = informacion.get("fecha_emision")
                reembolso.documento = Documento.objects.filter(status=1).get(id=informacion.get("tipo_documento"))
                reembolso.identificacion = Identificacion.objects.get(codigo=informacion.get("tipo_identificacion"))

                ########################################
                #  Validación con la fecha del asiento #
                ########################################
                reembolso.fecha_creacion = now
                reembolso.usuario_creacion = request.user.username
                reembolso.save()
                if reembolso.fecha_emi > cabecera_compra.fecha_reg:
                    raise ErrorCompras(u"Error la fecha de emisión de uno de los documentos "
                                       u"del reembolso es mayor a la del asiento, por "
                                       u"favor corríja los datos")
        else:
            contador += 1
            lista_errores = "Por favor verifique los siguientes campos del reembolso: "
            for i in reembolso_gasto.errors:
                lista_errores = lista_errores +(unicode(i)) + " "
            if reembolso_gasto.errors:
                messages.error(request, u"Por favor llenar todos los campos del reembolso de gasto")

    return contador


def GuardarATS(ats, formulario, cabecera_compra, detalleform, contador, now, tmonto0, tmontoi, total_iva,
               codigo_sustento, request):
    """
    function GuardarATS(ats, formulario, reembolso_gasto, cabecera_compra, contador, request)
    @Return None
    Details: Guarda los datos del ATS, Reembolso o N/D o N/C si las hubiera
    """
    ats.tipo_comprobante = TipoComprobante.objects.get(id=1) # Tipo de Comprobante compras
    ats.compra_cabecera_id = cabecera_compra.id
    proveedor = Proveedores.objects.get(id=formulario.getProveedor())
    ats.identificacion = proveedor.tipo_identificacion
    ats.sustento_tributario_sri_id = Sustento_Tributario.objects.get(codigo=codigo_sustento).id
    ats.num_id = proveedor.ruc
    ats.documento = Documento.objects.get(id=formulario.getTipo_de_documento())
    ats.num_doc = formulario.getSerie()
    ats.fecha_reg = formulario.getFecha_asiento()
    ats.fecha_emi = formulario.getFecha()
    ats.autorizacion = formulario.getAutorizacion()
    
    if formulario.getVence() != "":
        ats.vence = formulario.getVence()

    if tmonto0 + tmontoi + total_iva >= 1000:
        ats.forma_pago_sri = Forma_Pago_SRI.objects.get(codigo=request.POST.get("forma_pago_exc"))

    ats.tipo_pago_sri = Tipo_Pago_SRI.objects.get(id=formulario.getPago())

    if formulario.getPago() == "2":  # Compras al exterior
        ats.pais = Pais.objects.get(id=formulario.getPais())
        try:
            ats.sujeto_retencion = formulario.getSujeto_Retencion()
        except:
            pass
        try:
            ats.doble_tributacion = formulario.getConvenioDobleT()
        except:
            pass

    ats.base0 = tmonto0
    ats.baseiva = tmontoi

    ats.monto_iva = total_iva
    ats.base_ice = 0
    ats.monto_ice = 0

    ats.fecha_creacion = now
    ats.usuario_creacion = request.user.username
    ats.save()
    return contador


def GuardarCuentasPagar(cuentas_pagar, cabecera_compra, total, now, request):
    """
    function GuardarCuentasPagar(cuentas_pagar, cabecera_compra, request)
    @Return None
    Details: Guarda los datos de las cuentas por pagar
    """
    ##################################
    #       Cuentas por Pagar        #
    ##################################
    cuentas_pagar.proveedor = cabecera_compra.proveedor

    if es_nota_credito(cabecera_compra.documento.id):
        cuentas_pagar.naturaleza = 1
    else:
        cuentas_pagar.naturaleza = 2

    cuentas_pagar.documento = cabecera_compra.documento
    cuentas_pagar.num_doc = cabecera_compra.num_doc
    cuentas_pagar.compra_cabecera = cabecera_compra

    cuentas_pagar.fecha_reg = cabecera_compra.fecha_reg
    cuentas_pagar.fecha_emi = cabecera_compra.fecha_emi
    cuentas_pagar.plazo = cabecera_compra.plazo
    cuentas_pagar.monto = redondeo(total)
    cuentas_pagar.pagado = 0.0

    cuentas_pagar.fecha_creacion = now
    cuentas_pagar.usuario_creacion = request.user.username
    cuentas_pagar.save()


def GuardarDetalleCompraAsiento(detalleform, formulario, mayor_cabecera, cabecera_compra, now, contador, request):
    """
    function GuardarDetalleCompraAsiento(detalleform, formulario, mayor_cabecera, cabecera_compra, request)
    @Return None
    Details: Guarda los datos de los detalles del asiento contable
    """
    monto_costo_dist_comp = 0.0
    monto_cuenta_mano_obra = 0.0

    lista_sustento = []
    lista_codigo_sustento = []

    for form in detalleform:
        informacion = form.cleaned_data
        detalle_compra = CompraDetalle()

        detalle_compra.compra_cabecera = cabecera_compra
        plan_cuenta = PlanCuenta.objects.filter(status=1).get(id=informacion.get("cuenta"))
        valor = informacion.get("valor")
        detalle_compra.plan_cuenta = plan_cuenta
        tmonto0 = 0
        tmontoi = 0

        try:
            iva = Codigo_SRI.objects.get(id=informacion.get("valor_iva"))
            valor_iva = (valor * iva.porcentaje) / 100
            if iva.porcentaje == 0:
                tmonto0 = valor
            else:
                tmontoi = valor
        except (Codigo_SRI.DoesNotExist, ValueError):
            valor_iva = 0

        try:
            cod_ret_iva = Codigo_SRI.objects.get(id=informacion.get("iva"))
            porcentaje = cod_ret_iva.porcentaje if not cod_ret_iva.porc_variable else informacion.get("porc_ret_iva")
            valor_ret_iva = (valor_iva * porcentaje) / 100
        except (Codigo_SRI.DoesNotExist, ValueError):
            valor_ret_iva = 0

        try:
            cod_ret_fte = Codigo_SRI.objects.get(id=informacion.get("fte"))
            porcentaje = cod_ret_fte.porcentaje if not cod_ret_fte.porc_variable else informacion.get("porc_ret_fte")
            valor_ret_fte = (valor * porcentaje) / 100
        except (Codigo_SRI.DoesNotExist, ValueError):
            valor_ret_fte = 0

        if plan_cuenta.tipo_id == 19:  # Mano de Obra Transitoria
            monto_costo_dist_comp += (valor + valor_iva) - (valor_ret_iva + valor_ret_fte)
        elif plan_cuenta.tipo_id == 17:  # Mano de Obra Producción
            monto_cuenta_mano_obra += (valor + valor_iva) - (valor_ret_iva + valor_ret_fte)

        try:
            detalle_compra.centro_costo = Centro_Costo.objects.get(id=informacion.get("centro_costo"))
        except (Centro_Costo.DoesNotExist, ValueError):
            pass

        ##########################################
        #  LISTANDO LOS SUSTENTOS TRIBUTARIOS    #
        ##########################################
        cod_sustento_trib = informacion.get("sust_tribut")
        if cod_sustento_trib not in lista_codigo_sustento:
            lista_codigo_sustento.append(cod_sustento_trib)
            lista_sustento.append([cod_sustento_trib, tmonto0, tmontoi, valor_iva])
        else:
            for obj in lista_sustento:
                if obj[0] == cod_sustento_trib:
                    obj[1] += tmonto0
                    obj[2] += tmontoi
                    obj[3] += valor_iva

        detalle_compra.valor = valor
        detalle_compra.codigo_iva = Codigo_SRI.objects.get(id=informacion.get("valor_iva"))
        detalle_compra.monto_iva = redondeo((detalle_compra.valor * detalle_compra.codigo_iva.porcentaje)/100, 2)
        detalle_compra.tipo_iva = informacion.get("bs")

        detalle_compra.sustento_tributario = Sustento_Tributario.objects.get(codigo=cod_sustento_trib)

        ###################################################
        #   Compras con codigo de retención obligatoria   #
        #    si es diferente a un reembolso, N/C, N/D     #
        ###################################################
        tipo_doc = int(formulario.getTipo_de_documento())

        if not es_reembolso_gasto(tipo_doc):
            try:
                cod_ret_fte = Codigo_SRI.objects.get(id=informacion.get("fte"))
                porcentaje = cod_ret_fte.porcentaje if not cod_ret_fte.porc_variable else informacion.get("porc_ret_fte")

                detalle_compra.codigo_ret_fte = cod_ret_fte
                detalle_compra.monto_ret_fte = redondeo((detalle_compra.valor * porcentaje)/100, 2)
                detalle_compra.porc_ret_fte = porcentaje

                if es_nota_credito(tipo_doc):
                    if porcentaje > 0:
                        messages.error(request, u"En una nota de crédito no puede haber retención mayor a 0%")
                        errors = form._errors.setdefault("fte", ErrorList())
                        errors.append(u"Valor error")
                        errors = form._errors.setdefault("porc_ret_fte", ErrorList())
                        errors.append(u"Valor error")
                        contador += 1

            except (Codigo_SRI.DoesNotExist, ValueError):
                errors = form._errors.setdefault("fte", ErrorList())
                errors.append(u"Elija una retención")
                contador += 1

            id_combo_iva = informacion.get("iva", None)

            if id_combo_iva is not None and id_combo_iva != "":
                cod_ret_iva = Codigo_SRI.objects.get(id=informacion.get("iva"))
                porcentaje = cod_ret_iva.porcentaje if not cod_ret_iva.porc_variable else informacion.get("porc_ret_iva")
                detalle_compra.codigo_ret_iva = cod_ret_iva
                detalle_compra.monto_ret_iva = redondeo((detalle_compra.valor *
                                                         detalle_compra.codigo_iva.porcentaje * porcentaje)/10000, 2)
                detalle_compra.porc_ret_iva = porcentaje
        else:
            vg = VigenciaRetencionSRI.objects.filter(tipo_codigo_sri_id=1).get(fecha_inicio__lte=cabecera_compra.fecha_reg,
                                                                               fecha_final__gte=cabecera_compra.fecha_reg)

            cod_ret_fte = Codigo_SRI.objects.filter(status=1).filter(vigencia_retencion=vg).get(codigo="332")
            detalle_compra.codigo_ret_fte = cod_ret_fte  # Codigo de ret Fte 332
            detalle_compra.monto_ret_fte = 0

        detalle_compra.fecha_creacion = now
        detalle_compra.usuario_creacion = request.user.username
        detalle_compra.save()
        ###############################################
        #         Detalle de comprobante Contable     #
        ###############################################

        detalle_comp_cont = Detalle_Comp_Contable()
        detalle_comp_cont.cabecera_contable = mayor_cabecera

        try:
            detalle_comp_cont.centro_costo = Centro_Costo.objects.get(id=informacion.get("centro_costo"), status=1)
        except (Centro_Costo.DoesNotExist, ValueError):
            pass

        detalle_comp_cont.plan_cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
        detalle_comp_cont.valor = redondeo(informacion.get("valor"))

        # Cuando es una nota de crédito se valora los datos en el haber
        if es_nota_credito(tipo_doc):
            detalle_comp_cont.dbcr = "H"
        else:
            detalle_comp_cont.dbcr = "D"

        detalle_comp_cont.detalle = mayor_cabecera.concepto_comprobante
        detalle_comp_cont.fecha_asiento = mayor_cabecera.fecha

        detalle_comp_cont.fecha_creacion = now
        detalle_comp_cont.usuario_creacion = request.user.username
        detalle_comp_cont.save()

        #######################################################
        #    Cuenta de Gastos cuando el iva se va al gasto    #
        #######################################################

        if detalle_compra.codigo_iva.iva_al_gasto:
            detalle_comp_gastos = Detalle_Comp_Contable()
            detalle_comp_gastos.cabecera_contable = mayor_cabecera
            try:
                detalle_comp_gastos.centro_costo = Centro_Costo.objects.get(id=informacion.get("centro_costo"))
            except:
                pass
            detalle_comp_gastos.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(id=informacion.get("cuenta"))

            impuesto = Codigo_SRI.objects.get(id=informacion.get("valor_iva")).porcentaje
            valor = float(informacion.get("valor"))
            detalle_comp_gastos.valor = redondeo((valor*float(impuesto))/100, 2)
            # Cuando es una nota de crédito se valora los datos en el haber
            if es_nota_credito(tipo_doc):
                detalle_comp_gastos.dbcr = "H"
            else:
                detalle_comp_gastos.dbcr = "D"

            detalle_comp_gastos.detalle = mayor_cabecera.concepto_comprobante
            detalle_comp_gastos.fecha_asiento = mayor_cabecera.fecha

            detalle_comp_gastos.fecha_creacion = now
            detalle_comp_gastos.usuario_creacion = request.user.username
            detalle_comp_gastos.save()

    #################################################################
    #  Se guarda una instancia de CostoDistribuirCompras para los   #
    #                   proyectos o contratos                       #
    #################################################################
    if formulario.getContrato() is not None:
        if monto_cuenta_mano_obra == 0.0:
            messages.error(request, u"Debe seleccionar una cuenta de Mano de Obra Producción cuando escoje un "
                                    u"contrato")
            contador += 1
        else:
            detalle_cont = ContratoDetalle()
            detalle_cont.contrato_cabecera = formulario.getContrato()
            detalle_cont.tipo_comprobante = TipoComprobante.objects.get(id=1)  # Comprobante de Compras
            detalle_cont.valor = redondeo(monto_cuenta_mano_obra)
            detalle_cont.modulo_id = cabecera_compra.id

            detalle_cont.usuario_creacion = request.user.username
            detalle_cont.fecha_creacion = now
            detalle_cont.save()
    else:
        if monto_cuenta_mano_obra > 0.0:
            messages.error(request, u"Debe seleccionar un contrato, cuando utiliza una cuenta Mano de Obra Producción, "
                                    u"por favor verifíque")
            errors = formulario._errors.setdefault("contrato", ErrorList())
            errors.append(u"Debe seleccionar un contrato, cuando utiliza una cuenta Mano de Obra Producción, "
                                    u"por favor verifíque")
            contador += 1

    if monto_costo_dist_comp > 0:
        costo_dist_compra = CostoDistribuirCompras()
        costo_dist_compra.compra_cabecera = cabecera_compra
        costo_dist_compra.monto = monto_costo_dist_comp
        costo_dist_compra.distribuido = 0
        costo_dist_compra.fecha_creacion = now
        costo_dist_compra.usuario_creacion = request.user.username
        costo_dist_compra.save()

    # Guardar los ATS de la compra
    for obj in lista_sustento:
        ats = ATS()
        contador += GuardarATS(ats, formulario, cabecera_compra, detalleform, contador, now, obj[1], obj[2], obj[3],
                               obj[0], request)
    return contador


def GuardarAsientoProveedor(detalle_comp_prov, mayor_cabecera, formulario, cabecera_compra, tmonto0, tmontoi, total_iva, total_ret_fte, total_ret_iva, now, request):
    """
    function GuardarAsientoProveedor(detalle_comp_prov, mayor_cabecera, cabecera_compra, tmonto0, tmontoi, total_iva, total_ret_fte, total_ret_iva, request)
    @Return None
    Details: Guarda los datos de los detalles del asiento para la cuenta de proveedor
    """
    tipo_doc = int(formulario.getTipo_de_documento())

    detalle_comp_prov.cabecera_contable = mayor_cabecera
    detalle_comp_prov.plan_cuenta = cabecera_compra.proveedor.plan_cuenta
    detalle_comp_prov.valor = redondeo((tmonto0 + tmontoi + total_iva) - (total_ret_fte + total_ret_iva))

    if es_nota_credito(tipo_doc):  # Cuando es nota de credito el asiento va al débito
        detalle_comp_prov.dbcr = "D"
    else:
        detalle_comp_prov.dbcr = "H"

    detalle_comp_prov.detalle = cabecera_compra.concepto
    detalle_comp_prov.fecha_asiento = mayor_cabecera.fecha

    detalle_comp_prov.fecha_creacion = now
    detalle_comp_prov.usuario_creacion = request.user.username
    detalle_comp_prov.save()


def GuardarAsientoIvaPagado(detalle_comp_iva, mayor_cabecera, formulario, tmontoi_cta_iva_pagado, now, request):
    """
    function GuardarAsientoIvaPagado(detalle_comp_iva, mayor_cabecera, tmontoi_cta_iva_pagado, request)
    @Return None
    Details: Guarda los datos de los detalles del asiento para la cuenta Iva pagado
    """
    detalle_comp_iva.cabecera_contable = mayor_cabecera
    detalle_comp_iva.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(tipo=11)  # Tipo Iva PAgado
    detalle_comp_iva.valor = redondeo(tmontoi_cta_iva_pagado)
    tipo_doc = int(formulario.getTipo_de_documento())

    # Cuando es una nota de crédito el iva va al haber
    if es_nota_credito(tipo_doc):
        detalle_comp_iva.dbcr = "H"
    else:
        detalle_comp_iva.dbcr = "D"

    detalle_comp_iva.detalle = mayor_cabecera.concepto_comprobante
    detalle_comp_iva.fecha_asiento = mayor_cabecera.fecha

    detalle_comp_iva.fecha_creacion = now
    detalle_comp_iva.usuario_creacion = request.user.username
    detalle_comp_iva.save()


def GuardarRetencionCabecera(retencion_cabecera, retencion, cabecera_compra, empresa, total_ret_fte, total_ret_iva, contador, now, tipo, request):
    """
    function GuardarRetencionCabecera(retencion_cabecera, retencion, cabecera_compra, empresa, total_ret_fte, total_ret_iva, contador, request)
    @Return None
    Details: Guarda los datos de la retencion cabecera
    """

    retencion_cabecera.fecha_emi = retencion.getFechaEmisionRetencion()
    if tipo == 4:  # Si es una provisión de compras
        if retencion_cabecera is not None:
            retencion_cabecera.status = 1
            retencion_cabecera.fecha_actualizacion = now
            retencion_cabecera.usuario_actualizacion = request.user.username
        else:
            contador += 1
            messages.error(request, u"La compra provisionada es sin retención por favor verifique")
    else:
        if retencion_cabecera.id:
            # Se anula el detalle de la retencion generada anteriormente
            for obj in RetencionCompraDetalle.objects.filter(retencion_cabecera_compra=retencion_cabecera):
                obj.status = 0
                obj.fecha_actualizacion = now
                obj.usuario_actualizacion = request.user.username
                obj.save()
        else:
            if retencion_cabecera.id is None:  # Si no hay una retención asignada a la compra
                try:
                    retencion_cabecera.vigencia_doc_empresa = VigenciaDocEmpresa.objects.get(id=retencion.getSerie())
                    num_doc = get_num_doc(retencion_cabecera.vigencia_doc_empresa, cabecera_compra.fecha_reg, request)
                    if num_doc == -1:
                        contador += 1
                        messages.error(request, u"Error: La secuencia actual es mayor a la secuencia final del block de documento")
                    else:
                        retencion_cabecera.numero_ret = num_doc
                    #####################################################
                    #  Validación de la fecha de vencimiento del block  #
                    #####################################################
                    if retencion_cabecera.vigencia_doc_empresa.fecha_vencimiento < retencion_cabecera.fecha_emi:
                        messages.error(request, u"La fecha de emisión de la retención es mayor a la vecha "
                                                u"de vencimiento del block de retenciones")
                        errors = retencion._errors.setdefault("fecha_emision_retencion", ErrorList())
                        errors.append(u"La fecha de emisión de la retención es mayor a la vecha "
                                      u"de vencimiento del block de retenciones")
                        contador += 1

                except (VigenciaDocEmpresa.DoesNotExist, ValueError):
                    messages.error(request, u"Por favor seleccione el campo de la serie de retención")
                    errors = retencion._errors.setdefault("serie_ret", ErrorList())
                    errors.append(u"Por favor seleccione el campo de la serie de retención")
                    contador += 1
                    retencion_cabecera.numero_ret = 0
                retencion_cabecera.fecha_creacion = now
                retencion_cabecera.usuario_creacion = request.user.username
            else:
                if retencion_cabecera.vigencia_doc_empresa.fecha_vencimiento < retencion_cabecera.fecha_emi:
                    messages.error(request, u"La fecha de emisión de la retención es mayor a la fecha "
                                            u"de vencimiento del block de retenciones")
                    errors = retencion._errors.setdefault("fecha_emision_retencion", ErrorList())
                    errors.append(u"La fecha de emisión de la retención es mayor a la fecha "
                                  u"de vencimiento del block de retenciones")
                    contador += 1


    ########################################
    #  Validación con la fecha del asiento #
    ########################################
    if retencion_cabecera.fecha_emi > cabecera_compra.fecha_reg:
        errors = retencion._errors.setdefault("fecha_emision_retencion", ErrorList())
        errors.append(u"Ls fecha de emisión de la retención es mayor a la del"
                      u"asiento, por favor corríja las fechas")
        messages.error(request, u"La fecha de emisión de la retención es mayor a la del "
                           u"asiento, por favor corríja las fechas")
        contador += 1

    ##########################################
    #  Validación con la fecha del documento #
    ##########################################

    if retencion_cabecera.fecha_emi < cabecera_compra.fecha_emi:
        errors = retencion._errors.setdefault("fecha_emision_retencion", ErrorList())
        errors.append(u"La fecha de emisión de la retención es menor a la fecha de "
                      u"emisión del documento, por favor corríja las fechas")
        messages.error(request, u"La fecha de emisión de la retención es menor a la fecha de "
                           u"emisión del documento, por favor corríja las fechas")
        contador += 1

    retencion_cabecera.compra_cabecera = cabecera_compra
    retencion_cabecera.direccion = empresa.direccion
    retencion_cabecera.totalretfte = redondeo(total_ret_fte)
    retencion_cabecera.totalretiva = redondeo(total_ret_iva)

    retencion_cabecera.save()

    return contador


def GuardarDetalleRetencion(detalle_ret, retencion_cabecera, mayor_cabecera, now, request):
    """
    function GuardarDetalleRetencion(detalle_ret, retencion_cabecera, mayor_cabecera, request)
    @Return None
    Details: Guarda los datos de los detalles de la retención
    """
    for form in detalle_ret:
        informacion = form.cleaned_data
        detalle_retenciones = RetencionCompraDetalle()
        detalle_retenciones.retencion_cabecera_compra = retencion_cabecera
        id_cta_ret = int(informacion.get("cod_cta"))
        detalle_retenciones.plan_cuenta = PlanCuenta.objects.filter(status=1).get(codigo=Cuenta_Retencion.objects.get(id=id_cta_ret).plan_cuenta.codigo)
        codigo_sri = Codigo_SRI.objects.get(id=informacion.get("cod_sri"), status=1)

        detalle_retenciones.codigo_sri = codigo_sri
        porcentaje = codigo_sri.porcentaje if not codigo_sri.porc_variable else informacion.get("porcentaje")

        if int(informacion.get("tipo")) == 1:  # RET FTE
            detalle_retenciones.porc_ret_fte = porcentaje
        else:  # RET IVA
            detalle_retenciones.tipo_iva = informacion.get("tipo")
            detalle_retenciones.porc_ret_iva = porcentaje

        detalle_retenciones.base = informacion.get("base")
        detalle_retenciones.valor_retenido = redondeo(((detalle_retenciones.base * porcentaje)/100), 2)
        detalle_retenciones.fecha_creacion = now
        detalle_retenciones.usuario_creacion = request.user.username
        detalle_retenciones.save()

        ############################################################
        #         Detalle de comprobante Contable retenciones      #
        ############################################################
        detalle_comp_cont = Detalle_Comp_Contable()
        detalle_comp_cont.cabecera_contable = mayor_cabecera
        detalle_comp_cont.plan_cuenta = detalle_retenciones.plan_cuenta
        detalle_comp_cont.valor = redondeo(informacion.get("total"))
        detalle_comp_cont.dbcr = "H"
        detalle_comp_cont.detalle = mayor_cabecera.concepto_comprobante
        detalle_comp_cont.fecha_asiento = mayor_cabecera.fecha

        detalle_comp_cont.fecha_creacion = now
        detalle_comp_cont.usuario_creacion = request.user.username
        detalle_comp_cont.save()


def ValidacionFormsCompras(formulario, detalleform, contador):
    """
    function ValidacionFormsCompras(formulario, detalleform, contador)
    @Return None
    Details: Guarda los datos de los detalles de la retención
    """

    # Validación de N/D N/C
    if es_nota_credito(formulario.getTipo_de_documento()) or es_nota_credito(formulario.getTipo_de_documento()):
        if formulario.getSerieMod() == "":
            errors = formulario._errors.setdefault("serie_modif", ErrorList())
            errors.append(u"Usted debe ingresar el número de documento que modifica")
            contador += 1
        if formulario.getAutorMod() == "":
            errors = formulario._errors.setdefault("autorizacion_modif", ErrorList())
            errors.append(u"Usted debe ingresar el número de autorización del documento que modifica")
            contador += 1
    return contador


def guardar_contrato(formulario, detalleform, cabecera_compra, request, now):
    """
    Funcion para guardar el contrato
    :param formulario:
    :param cabecera_compra:
    :param request:
    :param now:
    :return:
    """
    ##################################################################################
    #  Valido que al menos tenga una cuenta mano de obra en el detalle de la compra  #
    ##################################################################################
    cont = 0
    monto_cuenta_mano_obra = 0.0
    for form in detalleform:
        informacion = form.cleaned_data
        cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
        valor = informacion.get("valor")
        if cuenta.tipo_id == 17:
            cont += 1
            monto_cuenta_mano_obra += valor

    if cont > 0:
        # Detalle de Contrato
        detalle_cont = ContratoDetalle()
        detalle_cont.contrato_cabecera = formulario.getContrato()
        detalle_cont.tipo_comprobante = TipoComprobante.objects.get(id=1)  # Comprobante de Compras
        detalle_cont.valor = redondeo(monto_cuenta_mano_obra)
        detalle_cont.modulo_id = cabecera_compra.id

        detalle_cont.usuario_creacion = request.user.username
        detalle_cont.fecha_creacion = now
        detalle_cont.save()

        messages.success(request, u"Se ha registrado la compra al contrato: " +
                         str(detalle_cont.contrato_cabecera.num_cont) + u" del cliente: " +
                         unicode(detalle_cont.contrato_cabecera.cliente.razon_social))
        return 0
    else:
        messages.error(request, u"Cuando se asigna la compra un contrato, se debe utilizar en el detalle al menos "
                                u"una cuenta mano de obra, por favor verifique")
        return 1


def redireccionar_compra(request, cabecera_compra):
    """
    redirecciona dependiendo de la acción que desee el usuario
    :return:
    """
    if request.GET.get("pagar", "") == "1":
        redirect_url = reverse("pago_proveedores")
        extra_params = '?prov=%s' % cabecera_compra.proveedor.id
        full_redirect_url = '%s%s' % (redirect_url, extra_params)

    elif request.GET.get("grabar_agregar", "") == "1":
        full_redirect_url = reverse("agregarcompras")

    elif request.GET.get("grabar_imprimir", "") == "1":
        redirect_url = reverse("agregarcompras")
        extra_params = '?imp=%s' % cabecera_compra.id
        full_redirect_url = '%s%s' % (redirect_url, extra_params)

    else:
        full_redirect_url = reverse("lista_compras")

    return full_redirect_url
