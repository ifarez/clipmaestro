#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt, csrf_protect
import json
from django.forms.formsets import formset_factory
from django.db import transaction
from django.db import connection
from administracion.models import *

from contabilidad.formularios.ContratoForm import *
import operator

from django.template import RequestContext
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt, csrf_protect
import json

from django.forms.formsets import formset_factory
from django.contrib.auth.decorators import login_required
from contabilidad.formularios.RetencionesCuentaForm import *
from contabilidad.formularios.ComprasForm import *

from django.template.loader import get_template
from django.template import Context
from django.core.paginator import *
from django import template
from django.contrib import messages

__author__ = 'Roberto'

def busqueda_contrato(buscador):
    '''
    Funcion que realiza la busqueda de los registros
    sobre contrato
    :param buscador:
    :return:
    '''
    predicates = []
    if buscador.getFechaRegIni() is not None and buscador.getFechaRegFin() is not None:
        predicates.append(('fecha_reg__range', (buscador.getFechaRegIni(), buscador.getFechaRegFin())))

    if buscador.getFechaEntIni() is not None and buscador.getFechaEntFin() is not None:
        predicates.append(('fecha_entrega__range', (buscador.getFechaEntIni(), buscador.getFechaEntFin())))

    if buscador.getCliente() is not None:
        predicates.append(('cliente', buscador.getCliente()))

    if buscador.getVendedor() is not None:
        predicates.append(('vendedor', buscador.getVendedor()))

    if buscador.getTipoPago() is not None:
        predicates.append(('tipo_pago', buscador.getTipoPago()))

    predicates.append(('responsable__icontains', buscador.getResponsable()))

    # reate the list of Q objects and run the queries as above..
    q_list = [Q(x) for x in predicates]

    entries = ContratoCabecera.objects.exclude(status=0).filter(reduce(operator.and_, q_list)).order_by("-fecha_reg", "-num_cont")
    return entries


def Guardar_costo_contrato_cabecera(formulario, costo_contrato_cabecera, request, now):
    '''
    Función que registra la transacción
    en costo contrato cabecera
    :param formulario:
    :param costo_contrato_cabecera:
    :param request:
    :param now:
    :return:
    '''
    contador = 0.0

    tipo_comp = TipoComprobante.objects.get(id=21)  # Tipo Comprobante Costo Contrato
    costo_contrato_cabecera.tipo_comprobante = tipo_comp
    costo_contrato_cabecera.fecha_reg = formulario.getFechaReg()
    costo_contrato_cabecera.concepto = formulario.getConcepto()


    if formulario.getTipo() == "01":    # MAno de Obra Externa - Proveedores
        costo_contrato_cabecera.compra_cabecera = CompraCabecera.objects.get(id=formulario.getFactura())
        costo_contrato_cabecera.proveedor = formulario.getProveedor()
    else:                               # Mano de Obra Interna - Empleados
        costo_contrato_cabecera.empleado = formulario.getEmpleado()

    if formulario.getFechaReg() > datetime.datetime.now().date():
        contador += 1
        messages.error(request, u"Error: La fecha de registro no puede ser mayor a "
                                u"la fecha actual.")

    num_comp = get_num_comp(tipo_comp.id, formulario.getFechaReg(), True, request)

    if num_comp != -1:
        costo_contrato_cabecera.num_comp = num_comp
    else:
        contador += 1
        messages.error(request, u"La fecha de registro no puede ser menor a la "
                                u"fecha del último comprobante emitido.")

    if contador == 0.0:
        costo_contrato_cabecera.usuario_creacion = request.user.username
        costo_contrato_cabecera.fecha_creacion = now
        costo_contrato_cabecera.save()

    return contador

# Por Implementar
def Guardar_detalle_costo_contrato_interna(formset, costo_contrato_cabecera, request, now):
    '''

    :return:
    '''


def Guardar_detalle_costo_contrato_externa(detalle_contrato_costo_proveedor, costo_contrato_cabecera,  request, now):
    '''
    Funcion que me permite
    :param detalle_contrato_costo_proveedor:
    :return:
    '''
    total = 0.0
    for form in detalle_contrato_costo_proveedor:
        informacion2 = form.cleaned_data
        compra_detalle_contrato = CostoContratoDetalle()
        contrato_detalle = ContratoDetalle()

        # Guardar en Costo_Contrato_Detalle
        compra_detalle_contrato.costo_contrato_cabecera = costo_contrato_cabecera
        compra_detalle_contrato.contrato_cabecera = ContratoCabecera.objects.get(id=informacion2.get("contrato"))
        compra_detalle_contrato.valor = informacion2.get("valor")

        compra_detalle_contrato.usuario_creacion = request.user.username
        compra_detalle_contrato.fecha_creacion = now
        compra_detalle_contrato.save()

        if informacion2.get("valor") != "":
            total += float(informacion2.get("valor"))


        # Guardar en Contrato Detalle
        contrato_detalle.contrato_cabecera = ContratoCabecera.objects.get(id=informacion2.get("contrato"))
        contrato_detalle.tipo_comprobante = TipoComprobante.objects.get(id=21)  # Costo de Contrato
        contrato_detalle.modulo_id = costo_contrato_cabecera.compra_cabecera_id
        contrato_detalle.valor = informacion2.get("valor")
        contrato_detalle.usuario_creacion = request.user.username
        contrato_detalle.fecha_creacion = now
        contrato_detalle.save()

    return redondeo(total, 2)

def Guardar_asiento_contable_cabecera_costo_contrato(cabecera_asiento, costo_contrato_cabecera, request, now):
    '''
    Función que registra la cabecera del asiento
    contable de la transacción costos de contrato
    :param cabecera_asiento:
    :param costo_contrato_cabecera:
    :param request:
    :param now:
    :return:
    '''
    cabecera_asiento.numero_comprobante = costo_contrato_cabecera.num_comp
    cabecera_asiento.fecha = costo_contrato_cabecera.fecha_reg
    cabecera_asiento.tipo_comprobante = costo_contrato_cabecera.tipo_comprobante
    cabecera_asiento.concepto_comprobante = costo_contrato_cabecera.concepto
    cabecera_asiento.fecha_creacion = now
    cabecera_asiento.usuario_creacion = request.user.username
    cabecera_asiento.save()


def Guardar_detalle_asiento_contable_costo_contrato(formset, cabecera_asiento, request, now):
    '''
    Función que realiza el asiento contable
    en la transacción costos de contrato
    :param formset:
    :param cabecera_asiento:
    :param request:
    :param now:
    :return:
    '''
    valor = 0.0
    for form in formset:
        informacion = form.cleaned_data

        if informacion.get("valor") != "":
            valor += float(informacion.get("valor"))


    detalle_comp_contable_debe = Detalle_Comp_Contable()
    detalle_comp_contable_debe.cabecera_contable = cabecera_asiento
    detalle_comp_contable_debe.fecha_asiento = cabecera_asiento.fecha
    detalle_comp_contable_debe.plan_cuenta = PlanCuenta.objects.filter(status=1).get(tipo__id=17)  # Cta. Mano de Obra - Inv. en Proceso
    detalle_comp_contable_debe.detalle = cabecera_asiento.concepto_comprobante
    detalle_comp_contable_debe.dbcr = "D"
    detalle_comp_contable_debe.valor = valor
    detalle_comp_contable_debe.usuario_creacion = request.user.username
    detalle_comp_contable_debe.fecha_creacion = now
    detalle_comp_contable_debe.save()

    detalle_comp_contable_haber = Detalle_Comp_Contable()
    detalle_comp_contable_haber.cabecera_contable = cabecera_asiento
    detalle_comp_contable_haber.fecha_asiento = cabecera_asiento.fecha
    detalle_comp_contable_haber.plan_cuenta = PlanCuenta.objects.filter(status=1).get(codigo="110302002")  # Cta. Mano de obra Transitoria
    detalle_comp_contable_haber.detalle = cabecera_asiento.concepto_comprobante
    detalle_comp_contable_haber.dbcr = "H"
    detalle_comp_contable_haber.valor = valor
    detalle_comp_contable_haber.usuario_creacion = request.user.username
    detalle_comp_contable_haber.fecha_creacion = now
    detalle_comp_contable_haber.save()



################################################ Funciones AJAX ########################################################
@login_required(login_url="/")
@csrf_exempt
def get_facturas_costo_contrato(request):
    '''
    Funcion Ajax que me obtiene las facturas
    correspondiente al escenario
    :param request:
    :return:
    '''
    respuesta = []
    tipo_mano_obra = request.POST.get("tipo", "")
    id = request.POST.get("id", "")

    if tipo_mano_obra == "01" and id != "":
        try:
            proveedor = Proveedores.objects.get(id=id)
        except Proveedores.DoesNotExist:
            proveedor = None

        for c in CostoDistribuirCompras.objects.filter(compra_cabecera__in=CompraCabecera.objects.filter(proveedor=proveedor, status=1)):
            respuesta.append({"id": c.compra_cabecera_id, "num_doc": c.compra_cabecera.num_doc})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")

@login_required(login_url="/")
@csrf_exempt
def get_valor_costo_contrato(request):
    '''
    Funcion Ajax que me devuelve el saldo
    del total de la compra detalle
    :param request:
    :return:
    '''
    id_compra = request.POST.get("id_compra", "")
    valor = 0.0
    try:
        costo_distribuido = CostoDistribuirCompras.objects.filter(status=1).get(compra_cabecera__id=id_compra)
        valor = costo_distribuido.monto - costo_distribuido.distribuido
        resultado = json.dumps({"status": 1,  "valor": valor})
    except CostoDistribuirCompras.DoesNotExist:
        resultado = json.dumps({"status": 0,  "valor": valor})

    return HttpResponse(resultado, content_type="application/json; charset=utf-8")