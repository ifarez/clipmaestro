#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
from librerias.funciones.funciones_vistas import *
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.forms.formsets import formset_factory
from django.db.models import Q
from contabilidad.models import *
from django.contrib import messages
from contabilidad.formularios.InventarioForm import *
from django.forms.util import ErrorList
from django.template import RequestContext
from django.shortcuts import render_to_response
import operator
from librerias.funciones.excel_response_mod import *
msj_ult_fecha_invent = u"La fecha ingresada es menor a la fecha de la última transacción de inventario"


class Detalle_Cont_Inven():
    """
    Clase que me ayuda a agrupar los valores de las cuentas
    repetidas con sus costos
    """
    def __init__(self, id_cuenta, costo):
        self.id_cuenta = id_cuenta
        self.costo = costo


def busqueda_inventario(buscador):
    """
    Función del buscador de movientos de inventarios
    :param buscador:
    :return:
    """
    buscador.is_valid()
    predicates = []

    if buscador.getFecha() is not None:
        predicates.append(('fecha_reg__gte', buscador.getFecha()))

    predicates.append(('fecha_reg__lte', buscador.getFechaFinal()))
    predicates.append(('num_comp__icontains', buscador.getNumComprobante()))

    if buscador.getNumeroDocumento() != "":
        predicates.append(('num_doc__icontains', buscador.getNumeroDocumento()))
    predicates.append(('detalle__icontains', buscador.getConcepto()))

    predicates_prov = []
    if buscador.getProveedor() is not None:
        # predicates_prov estan en otro query ya que a estos se les debe hacer la consulta como or
        predicates_prov.append(('tipo_mov_inventario', Tipo_Mov_Inventario.objects.get(id=1)))
        predicates_prov.append(('tipo_mov_inventario', Tipo_Mov_Inventario.objects.get(id=6)))
        predicates.append(('cliente_proveedor_id', buscador.getProveedor().id))

    predicates_clie = []
    if buscador.getCliente() is not None:
        predicates_clie.append(('tipo_mov_inventario', Tipo_Mov_Inventario.objects.get(id=7)))
        predicates_clie.append(('cliente_proveedor_id', buscador.getCliente().id))

    predicates_item = []
    if buscador.getItem() is not None:
        predicates_item.append(('inventario_detalle__item_id', buscador.getItem().id))

    if buscador.getMotivo() is not None:
        predicates.append(('tipo_mov_inventario', buscador.getMotivo()))

    if buscador.getTipoMovimiento() is not None:
        if buscador.getTipoMovimiento() != "":
            predicates.append(('tipo_comprobante_id', buscador.getTipoMovimiento()))

    if buscador.getTipoDoc() is not None:
        predicates.append(('documento', buscador.getTipoDoc()))


    # reate the list of Q objects and run the queries as above..
    q_list = [Q(x) for x in predicates]
    q_list_prov = [Q(x) for x in predicates_prov]
    q_list_clie = [Q(x) for x in predicates_clie]
    q_list_items = [Q(x) for x in predicates_item]

    if len(predicates_prov) > 0:
        prov = reduce(operator.or_, q_list_prov)
    else:
        prov = Q()

    if len(predicates_clie) > 0:
        clien = reduce(operator.and_, q_list_clie)
    else:
        clien = Q()

    if len(predicates_item) > 0:
        items = reduce(operator.or_, q_list_items)
    else:
        items = Q()


    entries = Inventario_Cabecera.objects.filter(reduce(operator.and_, q_list),
                                                 status=1).filter(items).filter(prov).filter(clien).order_by("-fecha_reg", "-num_comp").distinct()

    return entries

                #######################################################################
                #               FUNCIONES DE INGRESO DE INVENTARIO                    #
                #######################################################################


def calcular_costo_promedio(costo, existencia):
    """
    calcula el costo promedio del item
    :param costo:
    :param existencia:
    :return:
    """
    try:
        return costo / existencia
    except ZeroDivisionError:
        return 0

#######################################################################
#               FUNCIONES PARA INVENTARIO COMPRAS                     #
#######################################################################


def validar_num_doc_inventario(cabecera_inv):
    """
    Función que valida si el usuario esta ingresando dos veces el mismo
    documento al agregar una compra por inventario
    :param cabecera_inv:
    :return:
    """
    if not str(cabecera_inv.getNumDoc()).isspace():
        tipo_doc = Documento.objects.get(id=cabecera_inv.getTipoDoc())
        proveedor = cabecera_inv.getProveedorCliente()
        tipo_moviento_inv = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())
        if Inventario_Cabecera.objects.filter(documento=tipo_doc, num_doc=cabecera_inv.getNumDoc(), tipo_mov_inventario=tipo_moviento_inv, cliente_proveedor_id=proveedor).exists():
            return False
        else:
            return True
    else:
        return False


def GuardarCabeceraInventarioCompras(inventario_cab, cabecera_inv, cabecera_asiento, empresa_parametro, cursor, contador, now, request):
    """
    Función que guarda la cabecera de inventario y la cabecera del asiento
    contable en la opción de compras de inventario
    :return contador
    """
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.detalle = cabecera_inv.getConcepto()
    inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=14)  # Ingreso de Inventario

    # Validar la fecha de ingreso de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1

    if cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la fecha")
        contador += 1
    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()

    if inventario_cab.fecha_reg < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, u"La fecha de registro es menor al último registro de inventario, "
                                u"por favor verifique la fecha")
        contador += 1
    else:
        inventario_cab.num_comp = get_num_comp(inventario_cab.tipo_comprobante.id, inventario_cab.fecha_reg, False, request)

    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())

    try:
        Proveedores.objects.get(id=cabecera_inv.getProveedorCliente())
        inventario_cab.cliente_proveedor_id = cabecera_inv.getProveedorCliente()
        if cabecera_inv.getNumDoc() == "":
            messages.error(request, u"El número de documento es un campo requerido, por favor revise")
            errors = cabecera_inv._errors.setdefault("num_doc", ErrorList())
            errors.append(u"El número de documento es un campo requerido, por favor revise")
            contador += 1
        else:
            inventario_cab.num_doc = cabecera_inv.getNumDoc()
            inventario_cab.documento = Documento.objects.get(id=cabecera_inv.getTipoDoc())

        if validar_num_doc_inventario(cabecera_inv):
            inventario_cab.num_doc = cabecera_inv.getNumDoc()
            inventario_cab.documento = Documento.objects.get(id=cabecera_inv.getTipoDoc())
        else:
            messages.error(request, u"El documento que ingresó ya se encuentra registrado en el sistema o no es válido")
            errors = cabecera_inv._errors.setdefault("num_doc", ErrorList())
            errors.append(u"El documento que ingresó ya se encuentra registrado en el sistema o no es válido")
            contador += 1
    except:
        messages.error(request, u"El proveedor que eligió no se encuentra activo")
        errors = cabecera_inv._errors.setdefault("proveedor", ErrorList())
        errors.append(u"El proveedor que eligió no se encuentra activo")
        contador += 1

    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    cabecera_asiento.tipo_comprobante = inventario_cab.tipo_comprobante
    cabecera_asiento.numero_comprobante = inventario_cab.num_comp
    cabecera_asiento.concepto_comprobante = inventario_cab.detalle
    cabecera_asiento.fecha = inventario_cab.fecha_reg

    cabecera_asiento.fecha_creacion = now
    cabecera_asiento.usuario_creacion = request.user.username
    cabecera_asiento.save()

    return contador


def GuardarDetalleCompraInventario(detalle_compra_inventario, inventario_cab, cabecera_asiento, contador, now, request):
    """
    Función que me guarda el detalle inventario, item bodega
    y el detalle del asiento de la compra en inventario
    """
    cuentas_item_ids = []
    detalle_asiento_list = []

    total_inventario = 0.0
    for forms in detalle_compra_inventario:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        costo = redondeo(float(informacion.get("costo")))
        cantidad = redondeo(float(informacion.get("cantidad")))

        item = Item.objects.get(id=id_item)

        detalle_inventario.item = item
        detalle_inventario.costo = costo/cantidad
        detalle_inventario.cantidad = cantidad
        detalle_inventario.inventario_cabecera = inventario_cab

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1)  # Bodega creada para pruebas
        detalle_inventario.save()

        total_inventario += costo

        if costo <= 0:
            contador += 1
            errors = forms._errors.setdefault("costo", ErrorList())
            errors.append(u"El costo debe ser mayor a $ 0.00")
        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)

            costo_act = (item_bodega.cantidad_actual * item.costo)

            item_bodega.cantidad_actual += cantidad
            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
            item_bodega.save()

            item.costo = calcular_costo_promedio((costo_act + costo), item_bodega.cantidad_actual)

        except Item_Bodega.DoesNotExist:
            item.costo = calcular_costo_promedio(costo, cantidad)

            item_bodega = Item_Bodega()
            item_bodega.item = detalle_inventario.item
            item_bodega.bodega = detalle_inventario.bodega

            item_bodega.cantidad_actual = cantidad
            item_bodega.fecha_creacion = now
            item_bodega.usuario_creacion = request.user.username
            item_bodega.save()

        item.fecha_actualizacion = now
        item.usuario_actualizacion = request.user.username
        item.save()

        if item_bodega.item.categoria_item.plan_cuenta_compra.id in cuentas_item_ids:  # Si ya se encuentra un id nuevo de item
            for obj in detalle_asiento_list:
                if obj.id_cuenta == item_bodega.item.categoria_item.plan_cuenta_compra.id:
                    obj.costo += costo
        else:
            cuentas_item_ids.append(item_bodega.item.categoria_item.plan_cuenta_compra.id)
            detalle_asiento_list.append(Detalle_Cont_Inven(item_bodega.item.categoria_item.plan_cuenta_compra.id, costo))

    for obj in detalle_asiento_list:
        detalle_asiento_inventario = Detalle_Comp_Contable()
        detalle_asiento_inventario.cabecera_contable = cabecera_asiento
        detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
        detalle_asiento_inventario.dbcr = 'D'
        detalle_asiento_inventario.detalle = "Compras de inventario"

        detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
        detalle_asiento_inventario.valor = obj.costo
        detalle_asiento_inventario.fecha_creacion = now
        detalle_asiento_inventario.usuario_creacion = request.user.username
        detalle_asiento_inventario.save()

    cuenta = PlanCuenta.objects.get(tipo=TipoCuenta.objects.get(id=16))
    detalle_asiento = Detalle_Comp_Contable()
    detalle_asiento.cabecera_contable = cabecera_asiento
    detalle_asiento.fecha_asiento = cabecera_asiento.fecha
    detalle_asiento.dbcr = 'H'
    detalle_asiento.detalle = "Transitoria en compras de inventario"
    detalle_asiento.valor = total_inventario
    detalle_asiento.plan_cuenta = cuenta

    detalle_asiento.fecha_creacion = now
    detalle_asiento.usuario_creacion = request.user.username
    detalle_asiento.save()
    return contador

#######################################################################
#               FUNCIONES PARA AJUSTE DE CANTIDAD Y COSTO             #
#######################################################################
def GuardarCabeceraInventarioAjustes(inventario_cab, cabecera_inv, cabecera_asiento, empresa_parametro, cursor, contador, now, request):
    """
    Guarda un regístro en la tabla inventario cabecera, cabecera de asiento contable y
    empresa parámetro.
    :param inventario_cab:
    :param cabecera_inv:
    :param cabecera_asiento:
    :param empresa_parametro:
    :param cursor:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.detalle = cabecera_inv.getConcepto()
    inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=14) #Ingreso de Inventario

    # Validar la fecha de ingreso de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1
    else:
        inventario_cab.num_comp = get_num_comp(inventario_cab.tipo_comprobante.id, inventario_cab.fecha_reg, False, request)

    if cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la misma")
        contador += 1

    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()

    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())
    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    cabecera_asiento.tipo_comprobante = inventario_cab.tipo_comprobante
    cabecera_asiento.numero_comprobante = inventario_cab.num_comp
    cabecera_asiento.concepto_comprobante = inventario_cab.detalle
    cabecera_asiento.fecha = inventario_cab.fecha_reg

    cabecera_asiento.fecha_creacion = now
    cabecera_asiento.usuario_creacion = request.user.username
    cabecera_asiento.save()

    return contador


def GuardarDetalleInventarioAC(detalle_inv_aju_cant, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request):
    """
    Guarda los registros del detalle de la opción en movimiento de inventario: "ajuste a la cantidad"
    :param detalle_inv_aju_cant:
    :param inventario_cab:
    :param cabecera_asiento:
    :param cont_mns_form:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    for forms in detalle_inv_aju_cant:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        cantidad = redondeo(float(informacion.get("cantidad")), 2)

        try:
            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
        except PlanCuenta.DoesNotExist:
            cuenta = None
            messages.error(request, u"Existió un error al encontrar la cuenta ingresada, por favor verifique")
            contador += 1

        item = Item.objects.get(id=id_item)

        if item.costo == 0:
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"Error")
            messages.error(request, u"El item: " + unicode(item.codigo) + "-" + unicode(item.nombre) + u", tiene costo $ 0.00. "
                                                                                     u"No es posible hacer el ajuste a "
                                                                                     u"la cantidad")
        detalle_inventario.item = item
        detalle_inventario.cantidad = cantidad
        detalle_inventario.costo = item.costo
        detalle_inventario.inventario_cabecera = inventario_cab

        if cuenta is not None:
            detalle_inventario.plan_cuenta = cuenta

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1)  # Bodega creada para pruebas
        detalle_inventario.save()

        # Si no existe en bodega registrado el item

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
            cant_act = convert_cero_none(item_bodega.cantidad_actual)
            item_bodega.cantidad_actual = cant_act + cantidad
            item_bodega.save()

            costo = convert_cero_none(item.costo)

            if costo <= 0 and cant_act <= 0:  #  Validar si el item no tiene costo y si tiene una cantidad en bodega previa
                contador += 1
                messages.error(request, u"La cantidad actual del item: " + item.nombre + "-" + item.codigo +
                               u" en bodega es de: " + str(cant_act) +
                               u" y su costo actual es de: "+str(item.costo)+u". No se puede realizar esta transacción")

            detalle_asiento_inventario = Detalle_Comp_Contable()
            detalle_asiento_inventario.cabecera_contable = cabecera_asiento
            detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento_inventario.dbcr = 'D'
            detalle_asiento_inventario.detalle = "Ajuste de inventario Ingreso"

            detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=item_bodega.item.categoria_item.plan_cuenta_compra.id)
            detalle_asiento_inventario.valor = (cantidad * costo)
            detalle_asiento_inventario.fecha_creacion = now
            detalle_asiento_inventario.usuario_creacion = request.user.username
            detalle_asiento_inventario.save()

            detalle_asiento = Detalle_Comp_Contable()
            detalle_asiento.cabecera_contable = cabecera_asiento
            detalle_asiento.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento.dbcr = 'H'
            detalle_asiento.detalle = "Ajuste de inventario Ingreso"
            detalle_asiento.valor = (cantidad * costo)
            if cuenta is not None:
                detalle_asiento.plan_cuenta = cuenta

            detalle_asiento.fecha_creacion = now
            detalle_asiento.usuario_creacion = request.user.username
            detalle_asiento.save()

        except Item_Bodega.DoesNotExist:
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item: " + item.codigo + "-"
                          + item.nombre + u" al que quiere hacer mantenimiento de inventario no existe en bodega.")
            messages.error(request, u"El Item: " + item.nombre + "-" + item.codigo + u", no se encuentra registrado en "
                                                                                     u"bodega")
    return cont_mns_form, contador

#######################################################################
#               FUNCIONES PARA AJUSTE DE COSTO                        #
#######################################################################


def GuardarDetalleInventarioACosto(detalle_inv_aju_costo, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request):
    """
    Guarda los registros del detalle de la opción en movimiento de inventario: "ajuste al costo"
    :param detalle_inv_aju_costo:
    :param inventario_cab:
    :param cabecera_asiento:
    :param cont_mns_form:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    for forms in detalle_inv_aju_costo:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        total = redondeo(float(informacion.get("total")), 2)

        try:
            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
        except PlanCuenta.DoesNotExist:
            cuenta = None
            messages.error(request, u"Existió un error al encontrar la cuenta ingresada, por favor verifique")
            contador += 1

        item = Item.objects.get(id=id_item)
        detalle_inventario.item = item
        detalle_inventario.costo = total
        detalle_inventario.inventario_cabecera = inventario_cab

        if cuenta is not None:
            detalle_inventario.plan_cuenta = cuenta

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1)  # Bodega creada para pruebas
        detalle_inventario.save()

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username

            item_bodega.save()
            costo_act = convert_cero_none(detalle_inventario.item.costo)
            cant_act = convert_cero_none(item_bodega.cantidad_actual)

            if cant_act <= 0 or total <= 0:
                contador += 1
                if cant_act <= 0:
                    errors = forms._errors.setdefault("item", ErrorList())
                    messages.error(request, u"No se puede hacer el ajuste. El item: " + item.codigo + "-" + item.nombre +
                                   u" no tiene existencia en bodega, por favor verifique.")
                    errors.append(u"No hay existencias del producto en la bodega, por favor verifique.")
                if total <= 0:
                    contador += 1
                    errors = forms._errors.setdefault("total", ErrorList())
                    messages.error(request, u"Por favor ingrese un valor mayor a cero para el ajuste")
                    errors.append(u"Por favor ingrese un valor mayor a cero para el ajuste")
            else:
                item.costo = calcular_costo_promedio(((cant_act * costo_act) + total), cant_act)

            item.fecha_actualizacion = now
            item.usuario_actualizacion = request.user.username
            item.save()

            detalle_asiento_inventario = Detalle_Comp_Contable()
            detalle_asiento_inventario.cabecera_contable = cabecera_asiento
            detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento_inventario.dbcr = 'D'
            detalle_asiento_inventario.detalle = "Ajuste de inventario Costo Ingreso"

            detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=item_bodega.item.categoria_item.plan_cuenta_compra.id)
            detalle_asiento_inventario.valor = total
            detalle_asiento_inventario.fecha_creacion = now
            detalle_asiento_inventario.usuario_creacion = request.user.username
            detalle_asiento_inventario.save()

            detalle_asiento = Detalle_Comp_Contable()
            detalle_asiento.cabecera_contable = cabecera_asiento
            detalle_asiento.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento.dbcr = 'H'
            detalle_asiento.detalle = "Ajuste de inventario Costo Ingreso"
            detalle_asiento.valor = total

            if cuenta is not None:
                detalle_asiento.plan_cuenta = cuenta

            detalle_asiento.fecha_creacion = now
            detalle_asiento.usuario_creacion = request.user.username
            detalle_asiento.save()

        except Item_Bodega.DoesNotExist:
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item: " + item.codigo + "-"
                          + item.nombre + u" al que quiere hacer mantenimiento de inventario no existe en bodega.")
            messages.error(request, u"El Item: " + item.nombre + "-" + item.codigo + u", no se encuentra registrado en "
                                                                                     u"bodega")
    return cont_mns_form, contador


#######################################################################
#               FUNCIONES PARA GUIA DE REMISIÓN                       #
#######################################################################
def GuardarCabeceraInventarioGR(inventario_cab, cabecera_inv, empresa_parametro, cursor, contador, now, request):
    """
    Guarda un registro en la tabla inventario cabecera, empresa parametro, en la opción
    "guía de remisión"
    :param inventario_cab:
    :param cabecera_inv:
    :param empresa_parametro:
    :param cursor:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.cliente_proveedor_id = cabecera_inv.getProveedorCliente()
    inventario_cab.detalle = cabecera_inv.getConcepto()

    # Validar la fecha de egreso de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1

    if cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la misma")
        contador += 1
    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()

    if Inventario_Cabecera.objects.filter(documento=Documento.objects.get(id=25), num_doc=cabecera_inv.getNumGuiaR()).exists():
        messages.error(request, u"El número de guia de remisión ingresado ya existe para ese proveedor, "
                                u"por favor verifique")
        errors = cabecera_inv._errors.setdefault("numero_guia_r", ErrorList())
        errors.append(u"El número de guia de remisión ingresado ya existe para ese proveedor, "
                      u"por favor verifique")
        contador += 1
    else:
        inventario_cab.documento = Documento.objects.get(id=25) #Guia de remision
    inventario_cab.num_doc = cabecera_inv.getNumGuiaR()
    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())
    inventario_cab.tipo_comprobante_id = 14  # Ingreso de inventario

    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    return contador


def GuardarDetalleInventarioGR(detalle_inv_gr_form, inventario_cab, now, request):
    """
    Guarda un registro del detalle en la opción "Guia de remisión"
    :param detalle_inv_gr_form:
    :param inventario_cab:
    :param now:
    :param request:
    :return:
    """
    for forms in detalle_inv_gr_form:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        cantidad = redondeo(float(informacion.get("cantidad")))
        detalle_inventario.item = Item.objects.get(id=id_item)
        detalle_inventario.cantidad = cantidad
        detalle_inventario.inventario_cabecera = inventario_cab

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1) # Bodega creada para pruebas
        detalle_inventario.save()
        try:
            proveedor_item = Proveedor_Item.objects.get(proveedor=inventario_cab.getProveedor(), item=id_item)
            proveedor_item.cantidad_provision += cantidad
            proveedor_item.fecha_ultima_compra = inventario_cab.fecha_reg
            proveedor_item.fecha_actualizacion = now
            proveedor_item.usuario_actualizacion = request.user.username
            proveedor_item.save()
        except:
            proveedor_item = Proveedor_Item()
            proveedor_item.proveedor = inventario_cab.getProveedor()
            proveedor_item.item = Item.objects.get(id=id_item)
            proveedor_item.cantidad_provision = cantidad
            proveedor_item.fecha_ultima_compra = inventario_cab.fecha_reg
            proveedor_item.fecha_creacion = now
            proveedor_item.usuario_creacion = request.user.username
            proveedor_item.save()
        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
        except:
            item_bodega = Item_Bodega()
            item_bodega.fecha_creacion = now
            item_bodega.usuario_creacion = request.user.username

        item_bodega.bodega = detalle_inventario.bodega
        item_bodega.item = detalle_inventario.item
        if item_bodega.cantidad_provisionada is not None:
            item_bodega.cantidad_provisionada += cantidad
        else:
            item_bodega.cantidad_provisionada = cantidad

        item_bodega.save()

#######################################################################
#               FUNCIONES PARA GUIA DE REMISIÓN VENTA                 #
#######################################################################


def GuardarCabeceraInventarioVentaGR(inventario_cab, guia, guiaform, cabecera_inv, empresa_parametro, cursor, contador, now, request):
    """
    Guarda un registro de la cabecera de inventario en la opción venta por guía de remisión
    :param inventario_cab:
    :param guia:
    :param guiaform:
    :param cabecera_inv:
    :param empresa_parametro:
    :param cursor:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.cliente_proveedor_id = guiaform.getCliente().id
    inventario_cab.detalle = cabecera_inv.getConcepto()

    # Validar la fecha de egreso de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1

    if cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la misma")
        contador += 1
    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()

    ###########################################
    #           Guia de Remisión              #
    ###########################################
    guia.cliente = guiaform.getCliente()
    guia.fecha_emi = guiaform.getFechaEmision()
    guia.ciudad = guiaform.getCiudad()
    guia.concepto = cabecera_inv.getConcepto()
    guia.fecha_final = guiaform.getFechaFinal()
    guia.fecha_inicio = guiaform.getFechaInicio()
    num_doc = get_num_doc(guiaform.getSerie(), inventario_cab.fecha_reg, request)
    guia.num_doc = num_doc

    guia.direccion = Cliente_Direccion.objects.get(id=guiaform.getDireccionGuia()).descripcion + Cliente_Direccion.objects.get(id=guiaform.getDireccionGuia()).direccion1

    guia.nombre_conductor = guiaform.getCompania()
    guia.identificacion_conductor = guiaform.getRuc()
    guia.direccion_conductor = guiaform.getDireccionConductor()
    guia.vigencia_doc_empresa = guiaform.getSerie()
    guia.tipo_mov_guia_rem = guiaform.getMotivoGuia()

    guia.fecha_creacion = now
    guia.usuario_creacion = request.user.username
    guia.save()

    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())
    inventario_cab.documento = Documento.objects.get(id=25)  # Guia de remisión
    inventario_cab.num_doc = num_doc

    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    return contador


def GuardarDetalleInventarioVentaGR(detalle_inv_gr_form, guia, inventario_cab, now, contador, request):
    """
    Guarda el detall de la opcion de guía de remision por venta
    :param detalle_inv_gr_form:
    :param guia:
    :param inventario_cab:
    :param now:
    :param contador:
    :param request:
    :return:
    """
    for forms in detalle_inv_gr_form:
        detalle_inventario = Inventario_Detalle()
        detalle_guia = DetalleGuiaRemision()
        informacion = forms.cleaned_data

        item = Item.objects.get(id=informacion.get("item"))

        cantidad = redondeo(float(informacion.get("cantidad")))
        detalle_inventario.item = item
        detalle_inventario.cantidad = cantidad
        detalle_inventario.inventario_cabecera = inventario_cab

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1) # Bodega creada para pruebas
        detalle_inventario.save()

        detalle_guia.cantidad = cantidad
        detalle_guia.guia_rem_cabecera = guia
        detalle_guia.item = item

        detalle_guia.fecha_creacion = now
        detalle_guia.usuario_creacion = request.user.username
        detalle_guia.save()

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
            item_bodega.bodega = detalle_inventario.bodega
            item_bodega.item = detalle_inventario.item

            if item_bodega.cantidad_actual is not None:
                if item_bodega.cantidad_actual - cantidad < 0:
                    cant_act = str(item_bodega.cantidad_actual)
                    messages.error(request, u"La cantidad del item ingresado para el egreso excede a la que se "
                                        u"encuentra en bodega. La cantidad actual en bodega es de " + cant_act + u" unidades")
                    errors = forms._errors.setdefault("cantidad", ErrorList())
                    errors.append(u"La cantidad del item ingresado para el egreso excede a la que se "
                                  u"encuentra en bodega")
                    contador += 1
                else:
                    item_bodega.cantidad_actual -= cantidad
            else:
                contador += 1
                cant_act = "0"
                messages.error(request, u"La cantidad del item ingresado para el egreso excede a la que se "
                                        u"encuentra en bodega. La cantidad actual en bodega es de " + cant_act + u" unidades")
                errors = forms._errors.setdefault("cantidad", ErrorList())
                errors.append(u"La cantidad del item ingresado para el egreso excede a la que se "
                              u"encuentra en bodega")
            item_bodega.save()

        except Item_Bodega.DoesNotExist:
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item: " + item.codigo + "-"
                          + item.nombre + u" al que quiere hacer mantenimiento de inventario no existe en bodega.")
            messages.error(request, u"El Item: " + item.nombre + "-" + item.codigo + u", no se encuentra registrado en "
                                                                                     u"bodega")
    return contador


##########################################################
#           FUNCIONES PARA CONSUMO INTERNO               #
##########################################################
def GuardarCabeceraInventarioConsumoInterno(inventario_cab, cabecera_inv, asiento_cabecera, empresa_parametro, contador, now, request):
    """
    Guarda el registro de la tabla inventario cabecera, asiento cabecera y empresa parámetro
    :param inventario_cab:
    :param cabecera_inv:
    :param asiento_cabecera:
    :param empresa_parametro:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    # Validar la fecha de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1

    if cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la misma")
        contador += 1

    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()

    # Inventario Cabecera
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.detalle = cabecera_inv.getConcepto()
    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())

    if cabecera_inv.getTipo() == "1":
        inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=14)  # Ingreso de inventario
    else:
        inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=15)  # Egreso de inventario

    inventario_cab.num_comp = get_num_comp(inventario_cab.tipo_comprobante_id, inventario_cab.fecha_reg, False, request)
    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    # Cabecera de asiento contable

    asiento_cabecera.fecha = inventario_cab.fecha_reg
    asiento_cabecera.concepto_comprobante = inventario_cab.detalle
    asiento_cabecera.numero_comprobante = inventario_cab.num_comp
    asiento_cabecera.tipo_comprobante = inventario_cab.tipo_comprobante
    asiento_cabecera.fecha_creacion = now
    asiento_cabecera.usuario_creacion = request.user.username
    asiento_cabecera.save()

    return contador


def GuardarDetalleInventarioConsumoInternoI(detalle_con_interno, inventario_cab, asiento_cabecera, contador, now, request):
    """
    Guarda un detalle en base de la opción en inventario: "Consumo Interno"
    :param detalle_con_interno:
    :param inventario_cab:
    :param asiento_cabecera:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    cuentas_item_ids = []
    cuentas_ids = []

    detalle_asiento_list = []  # Items
    detalle_asiento_list2 = []  # Cuentas
    for forms in detalle_con_interno:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        cantidad = redondeo(float(informacion.get("cantidad")), 2)
        id_cuenta = informacion.get("cuenta")
        item = Item.objects.get(id=id_item)

        try:
            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
        except PlanCuenta.DoesNotExist:
            cuenta = None
            messages.error(request, u"Existió un error al encontrar la cuenta ingresada, por favor verifique")
            contador += 1

        detalle_inventario.item = item
        detalle_inventario.cantidad = cantidad
        detalle_inventario.costo = item.costo
        detalle_inventario.inventario_cabecera = inventario_cab

        if cuenta is not None:
            detalle_inventario.plan_cuenta = cuenta

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1)  # Bodega creada para pruebas
        detalle_inventario.save()

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
            item_bodega.bodega = detalle_inventario.bodega
            item_bodega.item = detalle_inventario.item

            if item_bodega.cantidad_actual is not None:
                item_bodega.cantidad_actual += cantidad
            else:
                item_bodega.cantidad_actual = cantidad

            item_bodega.save()
            costo = detalle_inventario.costo

            if costo is None or costo == 0.0:
                errors = forms._errors.setdefault("item", ErrorList())
                errors.append(u"No se puede utilizar este item ya que tiene costo cero")
                messages.error(request, u"No puede utilizar el item: " + unicode(item_bodega.item.nombre) +
                               u", ya que tiene costo 0.0")
                contador += 1

            # Asientos de cuenta

            if Item.objects.get(id=id_item).categoria_item.plan_cuenta_compra.id in cuentas_item_ids:  # Si ya se encuentra un id nuevo de item
                for obj in detalle_asiento_list:
                    if obj.id_cuenta == Item.objects.get(id=id_item).categoria_item.plan_cuenta_compra.id:
                        obj.costo += (costo * detalle_inventario.cantidad)
            else:
                cuentas_item_ids.append(item_bodega.item.categoria_item.plan_cuenta_compra.id)
                detalle_asiento_list.append(Detalle_Cont_Inven(item_bodega.item.categoria_item.plan_cuenta_compra.id,
                                                               (costo * detalle_inventario.cantidad)))

            # Asientos de cuenta ingresada por el usuario

            if id_cuenta in cuentas_ids:  # Si ya se encuentra un id nuevo de item
                for obj in detalle_asiento_list2:
                    if obj.id_cuenta == id_cuenta:
                        obj.costo += (detalle_inventario.costo * detalle_inventario.cantidad)
            else:
                cuentas_ids.append(id_cuenta)
                detalle_asiento_list2.append(Detalle_Cont_Inven(id_cuenta, (detalle_inventario.costo * detalle_inventario.cantidad)))

        except Item_Bodega.DoesNotExist:
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item: " + item.codigo + "-"
                          + item.nombre + u" al que quiere hacer mantenimiento de inventario no existe en bodega.")
            messages.error(request, u"El Item: " + item.nombre + "-" + item.codigo + u", no se encuentra registrado en "
                                                                                     u"bodega")

    # Asiento de cuentas de items
    for obj in detalle_asiento_list:
        detalle_asiento_inventario = Detalle_Comp_Contable()
        detalle_asiento_inventario.cabecera_contable = asiento_cabecera
        detalle_asiento_inventario.fecha_asiento = asiento_cabecera.fecha
        detalle_asiento_inventario.dbcr = 'D'
        detalle_asiento_inventario.detalle = "Ingreso de Item por consumo"

        detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
        detalle_asiento_inventario.valor = obj.costo
        detalle_asiento_inventario.fecha_creacion = now
        detalle_asiento_inventario.usuario_creacion = request.user.username
        detalle_asiento_inventario.save()

    # Asientos de cuentas ingresada por los usuarios
    for obj in detalle_asiento_list2:
        detalle_asiento_inventario = Detalle_Comp_Contable()
        detalle_asiento_inventario.cabecera_contable = asiento_cabecera
        detalle_asiento_inventario.fecha_asiento = asiento_cabecera.fecha
        detalle_asiento_inventario.dbcr = 'H'
        detalle_asiento_inventario.detalle = "Ingreso de Item por consumo"

        detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
        detalle_asiento_inventario.valor = obj.costo
        detalle_asiento_inventario.fecha_creacion = now
        detalle_asiento_inventario.usuario_creacion = request.user.username
        detalle_asiento_inventario.save()

    return contador


def GuardarDetalleInventarioConsumoInternoE(detalle_con_interno, inventario_cab, asiento_cabecera, cont_mns_form, contador, now, request):
    """
    Guarda un registro del detall de la opción en inventario: "Consumo interno" (Egreso)
    :param detalle_con_interno:
    :param inventario_cab:
    :param asiento_cabecera:
    :param cont_mns_form:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    cuentas_item_ids = []
    cuentas_ids = []

    detalle_asiento_list = []  # Items
    detalle_asiento_list2 = []  # Cuentas

    for forms in detalle_con_interno:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        cantidad = redondeo(float(informacion.get("cantidad")), 2)
        id_cuenta = informacion.get("cuenta")
        item = Item.objects.get(id=id_item)
        try:
            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
        except PlanCuenta.DoesNotExist:
            cuenta = None
            messages.error(request, u"Existió un error al encontrar la cuenta ingresada, por favor verifique")
            contador += 1

        detalle_inventario.item = item
        detalle_inventario.cantidad = cantidad
        detalle_inventario.costo = item.costo
        detalle_inventario.inventario_cabecera = inventario_cab

        if cuenta is not None:
            detalle_inventario.plan_cuenta = cuenta

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1)  # Bodega creada para pruebas
        detalle_inventario.save()

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username

            item_bodega.bodega = detalle_inventario.bodega
            item_bodega.item = detalle_inventario.item
            print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            print "item_bodega.cantidad_actual: ", item_bodega.cantidad_actual

            if item_bodega.cantidad_actual is not None:
                print "item_bodega.cantidad_actual - cantidad: ",
                if (item_bodega.cantidad_actual - cantidad) >= 0:
                    item_bodega.cantidad_actual -= cantidad
                else:
                    errors = forms._errors.setdefault("cantidad", ErrorList())
                    errors.append(u"La cantidad del item ingresado para el egreso excede a la que se "
                                  u"encuentra en bodega")
                    cont_mns_form += 1
                    messages.success(request, u"Existe en bodega la cantidad de: " + str(item_bodega.cantidad_actual)
                                     + u" del item: " + item_bodega.item.codigo + "-" + item_bodega.item.nombre)
                    contador += 1
            else:
                errors = forms._errors.setdefault("cantidad", ErrorList())
                errors.append(u"La cantidad del item ingresado para el egreso excede a la que se "
                              u"encuentra en bodega")
                messages.success(request, u"Existe en bodega la cantidad de: " + str(0)
                                     + u" del item: " + item_bodega.item.codigo + "-" + item_bodega.item.nombre)
                cont_mns_form += 1
                contador += 1

            item_bodega.save()

            costo = detalle_inventario.costo
            if costo is None or costo == 0.0:
                errors = forms._errors.setdefault("item", ErrorList())
                errors.append(u"No se puede utilizar este item ya que tiene costo cero")
                messages.error(request, u"No puede utilizar el item: " + str(item_bodega.item.nombre)
                                 + u", ya que tiene costo 0.0")
                contador += 1

            # Asientos de cuenta

            if Item.objects.get(id=id_item).categoria_item.plan_cuenta_compra.id in cuentas_item_ids:  # Si ya se encuentra un id nuevo de item
                for obj in detalle_asiento_list:
                    if obj.id_cuenta == Item.objects.get(id=id_item).categoria_item.plan_cuenta_compra.id:
                        obj.costo += (detalle_inventario.costo * detalle_inventario.cantidad)
            else:
                cuentas_item_ids.append(item_bodega.item.categoria_item.plan_cuenta_compra.id)
                detalle_asiento_list.append(Detalle_Cont_Inven(item_bodega.item.categoria_item.plan_cuenta_compra.id, (detalle_inventario.costo * detalle_inventario.cantidad)))

            # Asientos de cuenta ingresada por el usuario

            if id_cuenta in cuentas_ids:  # Si ya se encuentra un id nuevo de item
                for obj in detalle_asiento_list2:
                    if obj.id_cuenta == id_cuenta:
                        obj.costo += (detalle_inventario.costo * detalle_inventario.cantidad)
            else:
                cuentas_ids.append(id_cuenta)
                detalle_asiento_list2.append(Detalle_Cont_Inven(id_cuenta, (detalle_inventario.costo * detalle_inventario.cantidad)))

        except Item_Bodega.DoesNotExist:
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item: " + item.codigo + "-"
                          + item.nombre + u" al que quiere hacer mantenimiento de inventario no existe en bodega.")
            messages.error(request, u"El Item: " + item.nombre + "-" + item.codigo + u", no se encuentra registrado en "
                                                                                     u"bodega")

    # Asiento de cuentas de items
    for obj in detalle_asiento_list:
        detalle_asiento_inventario = Detalle_Comp_Contable()
        detalle_asiento_inventario.cabecera_contable = asiento_cabecera
        detalle_asiento_inventario.fecha_asiento = asiento_cabecera.fecha
        detalle_asiento_inventario.dbcr = 'H'
        detalle_asiento_inventario.detalle = "Egreso de Item por consumo"

        detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
        detalle_asiento_inventario.valor = obj.costo
        detalle_asiento_inventario.fecha_creacion = now
        detalle_asiento_inventario.usuario_creacion = request.user.username
        detalle_asiento_inventario.save()

    # Asientos de cuentas ingresada por los usuarios
    for obj in detalle_asiento_list2:
        detalle_asiento_inventario = Detalle_Comp_Contable()
        detalle_asiento_inventario.cabecera_contable = asiento_cabecera
        detalle_asiento_inventario.fecha_asiento = asiento_cabecera.fecha
        detalle_asiento_inventario.dbcr = 'D'
        detalle_asiento_inventario.detalle = "Egreso de Item por consumo"

        detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
        detalle_asiento_inventario.valor = obj.costo
        detalle_asiento_inventario.fecha_creacion = now
        detalle_asiento_inventario.usuario_creacion = request.user.username
        detalle_asiento_inventario.save()

    return contador, cont_mns_form


###################################################
#         FUNCIONES PARA MANTENIMIENTO            #
###################################################

def GuardarCabeceraInventarioMantenimiento(inventario_cab, cabecera_inv, cabecera_asiento, empresa_parametro, cursor, contador, now, request):
    """
    Guarda un registro de la opción de inventario: "Mantenimiento"
    :param inventario_cab:
    :param cabecera_inv:
    :param cabecera_asiento:
    :param empresa_parametro:
    :param cursor:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.detalle = cabecera_inv.getConcepto()
    inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=14)  # Ingreso de Inventario

    # Validar la fecha de ingreso de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1
    else:
        inventario_cab.num_comp = get_num_comp(inventario_cab.tipo_comprobante.id, inventario_cab.fecha_reg, False, request)

    if cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la misma")
        contador += 1

    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()


    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())
    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    cabecera_asiento.tipo_comprobante = inventario_cab.tipo_comprobante
    cabecera_asiento.numero_comprobante = inventario_cab.num_comp
    cabecera_asiento.concepto_comprobante = inventario_cab.detalle
    cabecera_asiento.fecha = inventario_cab.fecha_reg

    cabecera_asiento.fecha_creacion = now
    cabecera_asiento.usuario_creacion = request.user.username
    cabecera_asiento.save()

    return contador


def GuardarDetalleInventarioMantenimiento(detalle_inv_aju_cant, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request):
    """
    Guarda un registro de la opción en inventario: "Mantenimiento"
    :param detalle_inv_aju_cant:
    :param inventario_cab:
    :param cabecera_asiento:
    :param cont_mns_form:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    for forms in detalle_inv_aju_cant:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        cantidad = redondeo(float(informacion.get("cantidad")))

        try:
            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
        except PlanCuenta.DoesNotExist:
            cuenta = None
            messages.error(request, u"Existió un error al encontrar la cuenta ingresada, por favor verifique")
            contador += 1

        item = Item.objects.get(id=id_item)

        if item.costo == 0:
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"Error")
            messages.error(request, u"El item: " + item.codigo + "-" + item.nombre + u", tiene costo $ 0.00. "
                                                                                     u"No es posible hacer el ajuste a "
                                                                                     u"la cantidad")
        detalle_inventario.item = item
        detalle_inventario.cantidad = cantidad
        detalle_inventario.costo = item.costo
        detalle_inventario.inventario_cabecera = inventario_cab

        if cuenta is not None:
            detalle_inventario.plan_cuenta = cuenta

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1)  # Bodega creada para pruebas
        detalle_inventario.save()

        # Si no existe en bodega registrado el item

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
            cant_act = convert_cero_none(item_bodega.cantidad_actual)
            item_bodega.cantidad_actual = cant_act + cantidad
            item_bodega.save()

            costo = convert_cero_none(item.costo)

            if costo <= 0 and cant_act <= 0:  # Validar si el item no tiene costo y si tiene una cantidad en bodega previa
                contador += 1
                messages.error(request, u"La cantidad actual del item: " + item.nombre + "-" + item.codigo +
                               u" en bodega es de: " + str(cant_act) +
                               u" y su costo actual es de: "+str(item.costo)+u". No se puede realizar esta transacción")

            detalle_asiento_inventario = Detalle_Comp_Contable()
            detalle_asiento_inventario.cabecera_contable = cabecera_asiento
            detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento_inventario.dbcr = 'D'
            detalle_asiento_inventario.detalle = "Mantenimento por Inventario"

            detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=item_bodega.item.categoria_item.plan_cuenta_compra.id)
            detalle_asiento_inventario.valor = (cantidad * costo)
            detalle_asiento_inventario.fecha_creacion = now
            detalle_asiento_inventario.usuario_creacion = request.user.username
            detalle_asiento_inventario.save()

            detalle_asiento = Detalle_Comp_Contable()
            detalle_asiento.cabecera_contable = cabecera_asiento
            detalle_asiento.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento.dbcr = 'H'
            detalle_asiento.detalle = "Mantenimento por Inventario"
            detalle_asiento.valor = (cantidad * costo)

            if cuenta is not None:
                detalle_asiento.plan_cuenta = cuenta

            detalle_asiento.fecha_creacion = now
            detalle_asiento.usuario_creacion = request.user.username
            detalle_asiento.save()

        except Item_Bodega.DoesNotExist:
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item: " + item.codigo + "-"
                          + item.nombre + u" al que quiere hacer mantenimiento de inventario no existe en bodega.")
            messages.error(request, u"El Item: " + item.nombre + "-" + item.codigo + u", no se encuentra registrado en "
                                                                                     u"bodega")

    return cont_mns_form, contador


##############################################
#         FUNCIONES PARA GARANTIA            #
##############################################

def GuardarCabeceraInventarioGarantia(inventario_cab, cabecera_inv, cabecera_asiento, empresa_parametro, cursor, contador, now, request):
    """
    Guarda el registro de inventario cabecera, asiento cabecera, empresa parametro en la
    opción de inventario: "Garantia"
    :param inventario_cab:
    :param cabecera_inv:
    :param cabecera_asiento:
    :param empresa_parametro:
    :param cursor:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.detalle = cabecera_inv.getConcepto()
    inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=14)  # Ingreso de Inventario

    # Validar la fecha de ingreso de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1
    else:
        inventario_cab.num_comp = get_num_comp(inventario_cab.tipo_comprobante.id, inventario_cab.fecha_reg, False, request)

    if cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la misma")
        contador += 1

    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()


    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())
    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    cabecera_asiento.tipo_comprobante = inventario_cab.tipo_comprobante
    cabecera_asiento.numero_comprobante = inventario_cab.num_comp
    cabecera_asiento.concepto_comprobante = inventario_cab.detalle
    cabecera_asiento.fecha = inventario_cab.fecha_reg

    cabecera_asiento.fecha_creacion = now
    cabecera_asiento.usuario_creacion = request.user.username
    cabecera_asiento.save()

    return contador


def GuardarDetalleInventarioGarantia(detalle_inv_aju_cant, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request):
    """
    :param detalle_inv_aju_cant:
    :param inventario_cab:
    :param cabecera_asiento:
    :param cont_mns_form:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    for forms in detalle_inv_aju_cant:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        cantidad = redondeo(float(informacion.get("cantidad")))

        try:
            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
        except PlanCuenta.DoesNotExist:
            cuenta = None
            messages.error(request, u"Existió un error al encontrar la cuenta ingresada, por favor verifique")
            contador += 1

        item = Item.objects.get(id=id_item)

        if item.costo == 0:
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"Error")
            messages.error(request, u"El item: " + item.codigo + "-" + item.nombre + u", tiene costo $ 0.00. "
                                                                                     u"No es posible hacer el ajuste a "
                                                                                     u"la cantidad")

        detalle_inventario.item = item
        detalle_inventario.cantidad = cantidad
        detalle_inventario.costo = item.costo
        detalle_inventario.inventario_cabecera = inventario_cab

        if cuenta is not None:
            detalle_inventario.plan_cuenta = cuenta

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1)  # Bodega creada para pruebas
        detalle_inventario.save()

        # Si no existe en bodega registrado el item

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
            cant_act = convert_cero_none(item_bodega.cantidad_actual)
            item_bodega.cantidad_actual = cant_act + cantidad

            item_bodega.save()

            costo = convert_cero_none(item.costo)

            if costo <= 0 and cant_act <= 0:  # Validar si el item no tiene costo y si tiene una cantidad en bodega previa
                contador += 1
                messages.error(request, u"La cantidad actual del item: " + item.nombre + "-" + item.codigo +
                               u" en bodega es de: " + str(cant_act) +
                               u" y su costo actual es de: "+str(item.costo)+u". No se puede realizar esta transacción")

            detalle_asiento_inventario = Detalle_Comp_Contable()
            detalle_asiento_inventario.cabecera_contable = cabecera_asiento
            detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento_inventario.dbcr = 'D'
            detalle_asiento_inventario.detalle = "Garantia Inventario"

            detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=item_bodega.item.categoria_item.plan_cuenta_compra.id)
            detalle_asiento_inventario.valor = (cantidad * costo)
            detalle_asiento_inventario.fecha_creacion = now
            detalle_asiento_inventario.usuario_creacion = request.user.username
            detalle_asiento_inventario.save()

            detalle_asiento = Detalle_Comp_Contable()
            detalle_asiento.cabecera_contable = cabecera_asiento
            detalle_asiento.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento.dbcr = 'H'
            detalle_asiento.detalle = "Garantia Inventario"
            detalle_asiento.valor = (cantidad * costo)
            if cuenta is not None:
                detalle_asiento.plan_cuenta = cuenta

            detalle_asiento.fecha_creacion = now
            detalle_asiento.usuario_creacion = request.user.username
            detalle_asiento.save()
        except Item_Bodega.DoesNotExist:
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item: " + item.codigo + "-"
                          + item.nombre + u" al que quiere hacer mantenimiento de inventario no existe en bodega.")
            messages.error(request, u"El Item: " + item.nombre + "-" + item.codigo + u", no se encuentra registrado en "
                                                                                     u"bodega")

    return cont_mns_form, contador


######################################################
#         FUNCIONES PARA INVENTARIO INICIAL          #
######################################################

def GuardarCabeceraInventarioInicial(inventario_cab, cabecera_inv, cabecera_asiento, empresa_parametro, cursor, contador, now, request):
    """
    Guarda el registro de inventario cabecera, asiento cabecera, empresa parametro en la
    opción de inventario: "Garantia"
    :param inventario_cab:
    :param cabecera_inv:
    :param cabecera_asiento:
    :param empresa_parametro:
    :param cursor:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.detalle = cabecera_inv.getConcepto()
    inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=14)  # Ingreso de Inventario

    # Validar la fecha de ingreso de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1
    elif cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la misma")
        contador += 1
    else:
        inventario_cab.num_comp = get_num_comp(inventario_cab.tipo_comprobante.id, inventario_cab.fecha_reg, False, request)

    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()

    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())
    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    cabecera_asiento.tipo_comprobante = inventario_cab.tipo_comprobante
    cabecera_asiento.numero_comprobante = inventario_cab.num_comp
    cabecera_asiento.concepto_comprobante = inventario_cab.detalle
    cabecera_asiento.fecha = inventario_cab.fecha_reg

    cabecera_asiento.fecha_creacion = now
    cabecera_asiento.usuario_creacion = request.user.username
    cabecera_asiento.save()

    return contador


def GuardarDetalleCompraInventarioInicial(detalle_compra_inventario, inventario_cab, cabecera_asiento, contador, now, request):
    """
    Función que me guarda el detalle inventario, item bodega
    y el detalle del asiento de la compra en inventario
    """
    cuentas_item_ids = []
    detalle_asiento_list = []
    cuentas_ingresadas_ids = []
    detalle_cuenta_ing_list = []

    for forms in detalle_compra_inventario:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        costo = redondeo(float(informacion.get("costo")))
        cantidad = redondeo(float(informacion.get("cantidad")))
        cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))

        item = Item.objects.get(id=id_item)
        detalle_inventario.item = item
        detalle_inventario.costo = costo/cantidad
        detalle_inventario.cantidad = cantidad
        detalle_inventario.inventario_cabecera = inventario_cab
        detalle_inventario.plan_cuenta = cuenta

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1)  # Bodega creada para pruebas
        detalle_inventario.save()

        if costo <= 0:
            contador += 1
            errors = forms._errors.setdefault("costo", ErrorList())
            errors.append(u"El costo debe ser mayor a $ 0.00")

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)

            cant_act = item_bodega.cantidad_actual

            item_bodega.cantidad_actual += cantidad
            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
            item_bodega.save()

            costo_act = (cant_act * convert_cero_none(item.costo))

            if cant_act == 0:
                item.costo = calcular_costo_promedio((costo_act + costo), (cant_act + cantidad))
                item.fecha_actualizacion = now
                item.usuario_actualizacion = request.user.username
                item.save()

                if item_bodega.item.categoria_item.plan_cuenta_compra.id in cuentas_item_ids:  # Si ya se encuentra un id nuevo de item
                    for obj in detalle_asiento_list:
                        if obj.id_cuenta == item_bodega.item.categoria_item.plan_cuenta_compra.id:
                            obj.costo += costo
                else:
                    cuentas_item_ids.append(item_bodega.item.categoria_item.plan_cuenta_compra.id)
                    detalle_asiento_list.append(Detalle_Cont_Inven(item_bodega.item.categoria_item.plan_cuenta_compra.id, costo))

                if cuenta.id in cuentas_ingresadas_ids:  # Si ya se encuentra un id de una cuenta
                    for obj in detalle_cuenta_ing_list:
                        if obj.id_cuenta == cuenta.id:
                            obj.costo += costo
                else:
                    cuentas_ingresadas_ids.append(cuenta.id)
                    detalle_cuenta_ing_list.append(Detalle_Cont_Inven(cuenta.id,
                                                                      costo))
            else:
                contador += 1
                messages.error(request, u"Solo se puede hacer un ingreso por inventario inicial, a items que no tengan"
                                        u" existencia en bodega")
                errors = forms._errors.setdefault("item", ErrorList())
                errors.append(u"Solo se puede hacer un ingreso por inventario inicial, a items que no tengan"
                              u" existencia en bodega")

        except Item_Bodega.DoesNotExist:
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item: " + item.codigo + "-"
                          + item.nombre + u" al que quiere hacer mantenimiento de inventario no existe en bodega.")
            messages.error(request, u"El Item: " + item.nombre + "-" + item.codigo + u", no se encuentra registrado en "
                                                                                     u"bodega")

    for obj in detalle_asiento_list:
        detalle_asiento_inventario = Detalle_Comp_Contable()
        detalle_asiento_inventario.cabecera_contable = cabecera_asiento
        detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
        detalle_asiento_inventario.dbcr = 'D'
        detalle_asiento_inventario.detalle = "Compras de inventario Inicial"

        detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
        detalle_asiento_inventario.valor = obj.costo
        detalle_asiento_inventario.fecha_creacion = now
        detalle_asiento_inventario.usuario_creacion = request.user.username
        detalle_asiento_inventario.save()

    for obj in detalle_cuenta_ing_list:
        detalle_asiento_inventario = Detalle_Comp_Contable()
        detalle_asiento_inventario.cabecera_contable = cabecera_asiento
        detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
        detalle_asiento_inventario.dbcr = 'H'
        detalle_asiento_inventario.detalle = "Compras de inventario Inicial"

        detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
        detalle_asiento_inventario.valor = obj.costo
        detalle_asiento_inventario.fecha_creacion = now
        detalle_asiento_inventario.usuario_creacion = request.user.username
        detalle_asiento_inventario.save()

    return contador


                #######################################################################
                #               FUNCIONES DE EGRESO DE INVENTARIO                     #
                #######################################################################

#######################################################################
#               FUNCIONES PARA INVENTARIO COMPRAS                     #
#######################################################################
def GuardarCabeceraInventarioComprasE(inventario_cab, cabecera_inv, cabecera_asiento, empresa_parametro, cursor, contador, now, request):
    """
    Compras en Egreso (Revisar ese concepto)
    :param inventario_cab:
    :param cabecera_inv:
    :param cabecera_asiento:
    :param empresa_parametro:
    :param cursor:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.detalle = cabecera_inv.getConcepto()
    inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=15)  # Egreso de Inventario

    # Validar la fecha de ingreso de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1
    else:
        inventario_cab.num_comp = get_num_comp(15, inventario_cab.fecha_reg, False, request)

    if cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la misma")
        contador += 1

    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()

    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())

    if not str(cabecera_inv.getNumDoc()).isspace():
        inventario_cab.num_doc = cabecera_inv.getNumDoc()
        inventario_cab.documento = Documento.objects.get(id=cabecera_inv.getTipoDoc())
    try:
        Proveedores.objects.get(id=cabecera_inv.getProveedorCliente())
        inventario_cab.cliente_proveedor_id = cabecera_inv.getProveedorCliente()
    except:
        messages.error(request, u"El proveedor que eligió no se encuentra activo")
        errors = cabecera_inv._errors.setdefault("proveedor", ErrorList())
        errors.append(u"El proveedor que eligió no se encuentra activo")
        contador += 1

    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    cabecera_asiento.tipo_comprobante = inventario_cab.tipo_comprobante
    cabecera_asiento.numero_comprobante = inventario_cab.num_comp
    cabecera_asiento.concepto_comprobante = inventario_cab.detalle
    cabecera_asiento.fecha = inventario_cab.fecha_reg

    cabecera_asiento.fecha_creacion = now
    cabecera_asiento.usuario_creacion = request.user.username
    cabecera_asiento.save()

    return contador


def GuardarDetalleCompraInventarioE(detalle_compra_inventario, inventario_cab, cabecera_asiento, contador, cont_mns_form, now, request):
    """
    Detalle compras en Egreso inventario (Revisar ese comcepto)
    :param detalle_compra_inventario:
    :param inventario_cab:
    :param cabecera_asiento:
    :param contador:
    :param cont_mns_form:
    :param now:
    :param request:
    :return:
    """
    total_inventario = 0.0
    cuentas_item_ids = []  # Variable que guardara el id de cada plan de cuenta de los items
    detalle_asiento_list = []  # Variable que guardara la cuenta y el valor total de pago

    for forms in detalle_compra_inventario:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        costo = redondeo(float(informacion.get("costo")))
        cantidad = redondeo(float(informacion.get("cantidad")))

        item = Item.objects.get(id=id_item)
        detalle_inventario.item = item
        detalle_inventario.costo = costo / cantidad
        detalle_inventario.cantidad = cantidad
        detalle_inventario.inventario_cabecera = inventario_cab

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1) # Bodega creada para pruebas
        detalle_inventario.save()
        total_inventario += costo

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)

            if item_bodega.cantidad_actual is not None:
                if item_bodega.cantidad_actual - cantidad >= 0:
                    cant_ant = item_bodega.cantidad_actual
                    item_bodega.cantidad_actual -= cantidad

                    if ((cant_ant * item.costo) - costo) < 0:
                        contador += 1
                        errors = forms._errors.setdefault("cantidad", ErrorList())
                        errors.append(u"El valor ingresado del costo execede al costo actual")
                        messages.error(request, u"El valor ingresado del costo execede al costo actual, el costo "
                                                u"del item unitario es de : $" + str(item.costo))

                    else:
                        costo_total_ant = (cant_ant * item.costo)
                        item.costo = calcular_costo_promedio((costo_total_ant - costo), item_bodega.cantidad_actual)
                else:
                    contador += 1
                    cont_mns_form += 1
                    errors = forms._errors.setdefault("cantidad", ErrorList())
                    errors.append(u"La cantidad del item ingresado para el egreso excede a la que se "
                                  u"encuentra en bodega")
                    messages.error(request, u"Existe en bodega la cantidad de: " + str(item_bodega.cantidad_actual)
                                     + u" del item: " + item_bodega.item.codigo + "-" + item_bodega.item.nombre)

            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
            item_bodega.save()

            item.fecha_actualizacion = now
            item.usuario_actualizacion = request.user.username
            item.save()

            if item_bodega.item.categoria_item.plan_cuenta_compra.id in cuentas_item_ids:  # Si ya se encuentra un id nuevo de item
                for obj in detalle_asiento_list:
                    if obj.id_cuenta == item_bodega.item.categoria_item.plan_cuenta_compra.id:
                        obj.costo += costo
            else:
                cuentas_item_ids.append(item_bodega.item.categoria_item.plan_cuenta_compra.id)
                detalle_asiento_list.append(Detalle_Cont_Inven(item_bodega.item.categoria_item.plan_cuenta_compra.id, costo))
        except Item_Bodega.DoesNotExist:
            cont_mns_form += 1
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item: " + detalle_inventario.item.codigo + "-"
                          + detalle_inventario.item.codigo + u" al que quiere hacer ajuste no existe en bodega.")
            contador += 1

    for obj in detalle_asiento_list:
        detalle_asiento_inventario = Detalle_Comp_Contable()
        detalle_asiento_inventario.cabecera_contable = cabecera_asiento
        detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
        detalle_asiento_inventario.dbcr = 'H'
        detalle_asiento_inventario.detalle = "Compras de inventario"

        detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
        detalle_asiento_inventario.valor = obj.costo
        detalle_asiento_inventario.fecha_creacion = now
        detalle_asiento_inventario.usuario_creacion = request.user.username
        detalle_asiento_inventario.save()

    cuenta = PlanCuenta.objects.get(tipo=TipoCuenta.objects.get(id=16))

    detalle_asiento = Detalle_Comp_Contable()
    detalle_asiento.cabecera_contable = cabecera_asiento
    detalle_asiento.fecha_asiento = cabecera_asiento.fecha
    detalle_asiento.dbcr = 'D'
    detalle_asiento.detalle = "Transitoria en compras de inventario"
    detalle_asiento.valor = total_inventario
    detalle_asiento.plan_cuenta = cuenta

    detalle_asiento.fecha_creacion = now
    detalle_asiento.usuario_creacion = request.user.username
    detalle_asiento.save()

    return contador

#######################################################################
#               FUNCIONES PARA AJUSTE DE CANTIDAD                     #
#######################################################################


def GuardarCabeceraInventarioAjustesE(inventario_cab, cabecera_inv, cabecera_asiento, empresa_parametro, cursor, contador, now, request):
    """
    Ajuste de inventario Egreso (Esta función sirve tanto para ajuste al costo como para cantidad)
    :param inventario_cab:
    :param cabecera_inv:
    :param cabecera_asiento:
    :param empresa_parametro:
    :param cursor:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.detalle = cabecera_inv.getConcepto()
    inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=15) #Ingreso de Inventario

    # Validar la fecha de ingreso de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1
    else:
        inventario_cab.num_comp = get_num_comp(15, inventario_cab.fecha_reg, False, request)

    if cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la misma")
        contador += 1

    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()

    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())
    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    cabecera_asiento.tipo_comprobante = inventario_cab.tipo_comprobante
    cabecera_asiento.numero_comprobante = inventario_cab.num_comp
    cabecera_asiento.concepto_comprobante = inventario_cab.detalle
    cabecera_asiento.fecha = inventario_cab.fecha_reg

    cabecera_asiento.fecha_creacion = now
    cabecera_asiento.usuario_creacion = request.user.username
    cabecera_asiento.save()

    return contador


def GuardarDetalleInventarioAjusteCantidadE(detalle_inv_aju_cant, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request):
    """
    Detalle ajuste de cantidad en Egreso
    :param detalle_inv_aju_cant:
    :param inventario_cab:
    :param cabecera_asiento:
    :param cont_mns_form:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    for forms in detalle_inv_aju_cant:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        cantidad = float(informacion.get("cantidad"))
        item = Item.objects.get(id=id_item)

        try:
            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
        except PlanCuenta.DoesNotExist:
            cuenta = None
            messages.error(request, u"Existió un error al encontrar la cuenta ingresada, por favor verifique")
            contador += 1

        detalle_inventario.item = item
        detalle_inventario.cantidad = cantidad
        detalle_inventario.costo = detalle_inventario.item.costo
        detalle_inventario.inventario_cabecera = inventario_cab

        if cuenta is not None:
            detalle_inventario.plan_cuenta = cuenta

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1)  # Bodega creada para pruebas
        detalle_inventario.save()

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.bodega = detalle_inventario.bodega
            item_bodega.item = detalle_inventario.item
            cant_act = item_bodega.cantidad_actual

            if item_bodega.cantidad_actual - cantidad < 0:
                contador += 1
                cont_mns_form += 1
                errors = forms._errors.setdefault("cantidad", ErrorList())
                errors.append(u"La cantidad del item ingresado para el egreso excede a la que se "
                              u"encuentra en bodega")
                messages.success(request, u"Existe en bodega la cantidad de: " + str(item_bodega.cantidad_actual)
                                     + u" del item: " + item_bodega.item.codigo + "-" + item_bodega.item.nombre)
            else:
                item_bodega.cantidad_actual -= cantidad

            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
            item_bodega.save()

            costo = convert_cero_none(item.costo)

            if costo <= 0 and cant_act <= 0:  # Validar si el item no tiene costo y si tiene una cantidad en bodega previa
                messages.error(request, u"No se puede haceer el ajuste. EL item: " + item.codigo + "-" + item.nombre +
                               u"tiene costo cero.")
                contador += 1

            detalle_asiento_inventario = Detalle_Comp_Contable()
            detalle_asiento_inventario.cabecera_contable = cabecera_asiento
            detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento_inventario.dbcr = 'H'
            detalle_asiento_inventario.detalle = "Ajuste de inventario Egreso"
            detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=item_bodega.item.categoria_item.plan_cuenta_compra.id)
            detalle_asiento_inventario.valor = (cantidad * costo)
            detalle_asiento_inventario.fecha_creacion = now
            detalle_asiento_inventario.usuario_creacion = request.user.username
            detalle_asiento_inventario.save()

            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
            detalle_asiento = Detalle_Comp_Contable()
            detalle_asiento.cabecera_contable = cabecera_asiento
            detalle_asiento.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento.dbcr = 'D'
            detalle_asiento.detalle = "Ajuste de inventario Egreso"
            detalle_asiento.valor = (cantidad * costo)
            detalle_asiento.plan_cuenta = cuenta

            detalle_asiento.fecha_creacion = now
            detalle_asiento.usuario_creacion = request.user.username
            detalle_asiento.save()

        except Item_Bodega.DoesNotExist:
            cont_mns_form += 1
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item: " + item.codigo + "-"
                          + item.nombre + u" al que quiere hacer ajuste no existe en bodega.")
    return cont_mns_form, contador

#######################################################################
#               FUNCIONES PARA AJUSTE DE COSTO                        #
#######################################################################


def GuardarDetalleInventarioAjusteCostoE(detalle_inv_aju_costo, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request):
    """
    Detalle ajuste de costo en Egreso
    :param detalle_inv_aju_costo:
    :param inventario_cab:
    :param cabecera_asiento:
    :param cont_mns_form:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    for forms in detalle_inv_aju_costo:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        total = redondeo(float(informacion.get("total")))
        item = Item.objects.get(id=id_item)

        try:
            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
        except PlanCuenta.DoesNotExist:
            cuenta = None
            messages.error(request, u"Existió un error al encontrar la cuenta ingresada, por favor verifique")
            contador += 1

        detalle_inventario.item = item
        detalle_inventario.costo = total
        detalle_inventario.inventario_cabecera = inventario_cab

        if cuenta is not None:
            detalle_inventario.plan_cuenta = cuenta

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1) # Bodega creada para pruebas
        detalle_inventario.save()

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
            item_bodega.save()
            costo_act = item.costo
            cant_act = item_bodega.cantidad_actual

            if cant_act <= 0 or ((cant_act * costo_act) - total) < 0:
                contador += 1
                errors = forms._errors.setdefault("total", ErrorList())
                if cant_act <= 0:
                    messages.error(request, u"No hay existencias del producto en la bodega, por favor verifique.")
                    errors.append(u"No hay existencias del producto en la bodega, por favor verifique.")
                if ((cant_act * costo_act) - total) <= 0:
                    contador += 1
                    messages.error(request, u"El valor ingresado del costo del item: " + item_bodega.item.codigo +
                                            u"-" + item_bodega.item.nombre +
                                            u", execede al costo actual")
                    errors.append(u"El valor ingresado del costo execede al costo actual")
            else:
                item.costo = calcular_costo_promedio(((cant_act * costo_act) - total), cant_act)

            item.fecha_actualizacion = now
            item.usuario_actualizacion = request.user.username
            item.save()

            #####################################
            #   DETALLE DEL ASIENTO CONTABLE    #
            #####################################
            detalle_asiento_inventario = Detalle_Comp_Contable()
            detalle_asiento_inventario.cabecera_contable = cabecera_asiento
            detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento_inventario.dbcr = 'H'
            detalle_asiento_inventario.detalle = "Ajuste de inventario Costo Egreso"

            detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=item_bodega.item.categoria_item.plan_cuenta_compra.id)
            detalle_asiento_inventario.valor = total
            detalle_asiento_inventario.fecha_creacion = now
            detalle_asiento_inventario.usuario_creacion = request.user.username
            detalle_asiento_inventario.save()

            #####################################################
            #   Cuenta ingresada por el cliente para el ajuste  #
            #####################################################
            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
            detalle_asiento = Detalle_Comp_Contable()
            detalle_asiento.cabecera_contable = cabecera_asiento
            detalle_asiento.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento.dbcr = 'D'
            detalle_asiento.detalle = "Ajuste de inventario Costo Egreso"
            detalle_asiento.valor = total
            detalle_asiento.plan_cuenta = cuenta

            detalle_asiento.fecha_creacion = now
            detalle_asiento.usuario_creacion = request.user.username
            detalle_asiento.save()
        except:
            cont_mns_form += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item al que quiere hacer ajuste no existe en bodega.")
            contador += 1

    return cont_mns_form, contador


#######################################################################
#               FUNCIONES PARA GUIA DE REMISIÓN                       #
#######################################################################

def GuardarCabeceraInventarioGRE(inventario_cab, cabecera_inv, cursor, empresa_parametro, contador, now, request):
    """
    Ingreso por Guía de Remisión
    :param inventario_cab:
    :param cabecera_inv:
    :param cursor:
    :param empresa_parametro:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.cliente_proveedor_id = cabecera_inv.getProveedor()
    inventario_cab.detalle = cabecera_inv.getConcepto()
    # Validar la fecha de egreso de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1

    if cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la misma")
        contador += 1
    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()

    if Inventario_Cabecera.objects.filter(documento=Documento.objects.get(id=25), num_doc=cabecera_inv.getNumGuiaR()).exists():
        messages.error(request, u"El número de guia de remisión ingresado ya existe para ese proveedor, "
                                u"por favor verifique")
        errors = cabecera_inv.g._errors.setdefault("numero_guia_r", ErrorList())
        errors.append(u"El número de guia de remision ingresado ya existe para ese proveedor, "
                      u"por favor verifique")
        contador += 1
    else:
        inventario_cab.documento = Documento.objects.get(id=25) #Guia de remision
    inventario_cab.num_doc = cabecera_inv.getNumGuiaR()
    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())
    inventario_cab.tipo_comprobante_id = 15  # Egreso de Inventario

    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    return contador


def GuardarDetalleInventarioGRE(detalle_inv_gr_form, inventario_cab, cont_mns_form, contador, now, request):
    """
    Detalle Guía de Remisión Ingreso de inventario
    :param detalle_inv_gr_form:
    :param inventario_cab:
    :param cont_mns_form:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    for forms in detalle_inv_gr_form:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        cantidad = redondeo(float(informacion.get("cantidad")))
        detalle_inventario.item = Item.objects.get(id=id_item)
        detalle_inventario.cantidad = cantidad
        detalle_inventario.inventario_cabecera = inventario_cab

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1) # Bodega creada para pruebas
        detalle_inventario.save()

        try:
            proveedor_item = Proveedor_Item.objects.get(proveedor=inventario_cab.getProveedor(), item=id_item)
            proveedor_item.fecha_ultima_compra = inventario_cab.fecha_reg
            proveedor_item.fecha_actualizacion = now
            proveedor_item.usuario_actualizacion = request.user.username
        except:
            proveedor_item = Proveedor_Item()
            proveedor_item.proveedor = inventario_cab.getProveedor()
            proveedor_item.item = Item.objects.get(id=id_item)
            proveedor_item.fecha_ultima_compra = inventario_cab.fecha_reg
            proveedor_item.fecha_creacion = now
            proveedor_item.usuario_creacion = request.user.username

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
        except:
            item_bodega = Item_Bodega()
            item_bodega.fecha_creacion = now
            item_bodega.usuario_creacion = request.user.username
        item_bodega.bodega = detalle_inventario.bodega

        item_bodega.item = detalle_inventario.item
        if item_bodega.cantidad_provisionada is not None:
            if item_bodega.cantidad_provisionada - cantidad < 0:
                messages.success(request, u"Existe en bodega la cantidad de: " + str(item_bodega.cantidad_actual)
                                     + u" del item: " + item_bodega.item.codigo + "-" + item_bodega.item.nombre)
                errors = forms._errors.setdefault("cantidad", ErrorList())
                errors.append(u"La cantidad del item ingresado para el egreso excede a la que se "
                              u"encuentra en bodega")
                contador += 1
                cont_mns_form += 1
            else:
                item_bodega.cantidad_provisionada -= cantidad
                proveedor_item.cantidad_provision -= cantidad
        else:
            contador += 1
            cont_mns_form += 1
            messages.success(request, u"Existe en bodega la cantidad de: " + str(item_bodega.cantidad_actual)
                                     + u" del item: " + item_bodega.item.codigo + "-" + item_bodega.item.nombre)
            errors = forms._errors.setdefault("cantidad", ErrorList())
            errors.append(u"La cantidad del item ingresado para el egreso excede a la que se "
                          u"encuentra en bodega")
        item_bodega.save()
        proveedor_item.save()
    return cont_mns_form, contador


def GuardarCabeceraMantenimientoE(inventario_cab, cabecera_inv, cabecera_asiento, empresa_parametro, cursor, contador, now, request):
    """
    Egreso por Mentenimiento Inventario
    :param inventario_cab:
    :param cabecera_inv:
    :param cabecera_asiento:
    :param empresa_parametro:
    :param cursor:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.detalle = cabecera_inv.getConcepto()
    inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=15)  # Egreso de Inventario

    # Validar la fecha de ingreso de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1
    else:
        inventario_cab.num_comp = get_num_comp(15, inventario_cab.fecha_reg, False, request)

    if cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la misma")
        contador += 1

    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()

    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())
    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    cabecera_asiento.tipo_comprobante = inventario_cab.tipo_comprobante
    cabecera_asiento.numero_comprobante = inventario_cab.num_comp
    cabecera_asiento.concepto_comprobante = inventario_cab.detalle
    cabecera_asiento.fecha = inventario_cab.fecha_reg

    cabecera_asiento.fecha_creacion = now
    cabecera_asiento.usuario_creacion = request.user.username
    cabecera_asiento.save()

    return contador


def GuardarDetalleMantenimientoE(detalle_inv_aju_cant, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request):
    """
    Detalle en Egreso de Mantenimiento en Inventario
    :param detalle_inv_aju_cant:
    :param inventario_cab:
    :param cabecera_asiento:
    :param cont_mns_form:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    for forms in detalle_inv_aju_cant:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        cantidad = redondeo(float(informacion.get("cantidad")))
        item = Item.objects.get(id=id_item)

        try:
            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
        except PlanCuenta.DoesNotExist:
            cuenta = None
            messages.error(request, u"Existió un error al encontrar la cuenta ingresada, por favor verifique")
            contador += 1

        detalle_inventario.item = item
        detalle_inventario.cantidad = cantidad
        detalle_inventario.costo = detalle_inventario.item.costo
        detalle_inventario.inventario_cabecera = inventario_cab

        if cuenta is not None:
            detalle_inventario.plan_cuenta = cuenta

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1)  # Bodega creada para pruebas
        detalle_inventario.save()

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.bodega = detalle_inventario.bodega
            item_bodega.item = detalle_inventario.item
            cant_act = item_bodega.cantidad_actual

            if item_bodega.cantidad_actual - cantidad < 0:
                contador += 1
                cont_mns_form += 1
                errors = forms._errors.setdefault("cantidad", ErrorList())
                errors.append(u"La cantidad del item ingresado para el egreso excede a la que se "
                              u"encuentra en bodega")
                messages.success(request, u"Existe en bodega la cantidad de: " + str(item_bodega.cantidad_provisionada)
                                     + u" del item: " + item_bodega.item.codigo + "-" + item_bodega.item.nombre)
            else:
                item_bodega.cantidad_actual -= cantidad

            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
            item_bodega.save()

            costo = convert_cero_none(item.costo)

            if costo <= 0 and cant_act <= 0:  # Validar si el item no tiene costo y si tiene una cantidad en bodega previa
                messages.error(request, u"No se puede hacer el mantenimiento de inventario. EL item: " + item.codigo + "-" + item.nombre +
                               u"tiene costo cero.")
                contador += 1

            detalle_asiento_inventario = Detalle_Comp_Contable()
            detalle_asiento_inventario.cabecera_contable = cabecera_asiento
            detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento_inventario.dbcr = 'H'
            detalle_asiento_inventario.detalle = "Mantenimiento de inventario Egreso"
            detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=item_bodega.item.categoria_item.plan_cuenta_compra.id)
            detalle_asiento_inventario.valor = (cantidad * costo)
            detalle_asiento_inventario.fecha_creacion = now
            detalle_asiento_inventario.usuario_creacion = request.user.username
            detalle_asiento_inventario.save()

            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
            detalle_asiento = Detalle_Comp_Contable()
            detalle_asiento.cabecera_contable = cabecera_asiento
            detalle_asiento.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento.dbcr = 'D'
            detalle_asiento.detalle = "Mantenimiento de inventario Egreso"
            detalle_asiento.valor = (cantidad * costo)
            detalle_asiento.plan_cuenta = cuenta

            detalle_asiento.fecha_creacion = now
            detalle_asiento.usuario_creacion = request.user.username
            detalle_asiento.save()
        except Item_Bodega.DoesNotExist:
            cont_mns_form += 1
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item: " + item.codigo + "-"
                          + item.nombre + u" al que quiere hacer mantenimiento de inventario no existe en bodega.")
    return cont_mns_form, contador


def GuardarCabeceraGarantiaE(inventario_cab, cabecera_inv, cabecera_asiento, empresa_parametro, cursor, contador, now, request):
    """
    Egreso de Garantía en Inventario
    :param inventario_cab:
    :param cabecera_inv:
    :param cabecera_asiento:
    :param empresa_parametro:
    :param cursor:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.detalle = cabecera_inv.getConcepto()
    inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=15)  # Egreso de Inventario

    # Validar la fecha de ingreso de inventario
    if cabecera_inv.getFechaReg() < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, msj_ult_fecha_invent)
        contador += 1
    else:
        inventario_cab.num_comp = get_num_comp(15, inventario_cab.fecha_reg, False, request)

    if cabecera_inv.getFechaReg() > datetime.datetime.now().date():
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, por favor verifique la misma")
        contador += 1

    empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
    empresa_parametro.save()

    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())
    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    cabecera_asiento.tipo_comprobante = inventario_cab.tipo_comprobante
    cabecera_asiento.numero_comprobante = inventario_cab.num_comp
    cabecera_asiento.concepto_comprobante = inventario_cab.detalle
    cabecera_asiento.fecha = inventario_cab.fecha_reg

    cabecera_asiento.fecha_creacion = now
    cabecera_asiento.usuario_creacion = request.user.username
    cabecera_asiento.save()

    return contador


def GuardarDetalleGarantiaE(detalle_inv_aju_cant, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request):
    """
    Detalle en Egreso de Garantía en Inventario
    :param detalle_inv_aju_cant:
    :param inventario_cab:
    :param cabecera_asiento:
    :param cont_mns_form:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    for forms in detalle_inv_aju_cant:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        cantidad = redondeo(float(informacion.get("cantidad")))
        item = Item.objects.get(id=id_item)

        try:
            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
        except PlanCuenta.DoesNotExist:
            cuenta = None
            messages.error(request, u"Existió un error al encontrar la cuenta ingresada, por favor verifique")
            contador += 1

        detalle_inventario.item = item
        detalle_inventario.cantidad = cantidad
        detalle_inventario.costo = detalle_inventario.item.costo
        detalle_inventario.inventario_cabecera = inventario_cab

        if cuenta is not None:
            detalle_inventario.plan_cuenta = cuenta

        detalle_inventario.fecha_creacion = now
        detalle_inventario.usuario_creacion = request.user.username
        detalle_inventario.bodega = Bodega.objects.get(id=1)  # Bodega creada para pruebas
        detalle_inventario.save()

        try:
            item_bodega = Item_Bodega.objects.get(bodega=detalle_inventario.bodega, item=detalle_inventario.item)
            item_bodega.bodega = detalle_inventario.bodega
            item_bodega.item = detalle_inventario.item
            cant_act = item_bodega.cantidad_actual

            if item_bodega.cantidad_actual - cantidad < 0:
                contador += 1
                cont_mns_form += 1
                errors = forms._errors.setdefault("cantidad", ErrorList())
                errors.append(u"La cantidad del item ingresado para el egreso excede a la que se "
                              u"encuentra en bodega")
                messages.success(request, u"Existe en bodega la cantidad de: " + str(item_bodega.cantidad_actual)
                                     + u" del item: " + item_bodega.item.codigo + "-" + item_bodega.item.nombre)
            else:
                item_bodega.cantidad_actual -= cantidad

            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username
            item_bodega.save()

            costo = convert_cero_none(item.costo)

            if costo <= 0 and cant_act <= 0:  # Validar si el item no tiene costo y si tiene una cantidad en bodega previa Egreso
                messages.error(request, u"No se puede hacer la Garantía de inventario. EL item: " + item.codigo + "-" + item.nombre +
                               u"tiene costo cero.")
                contador += 1

            detalle_asiento_inventario = Detalle_Comp_Contable()
            detalle_asiento_inventario.cabecera_contable = cabecera_asiento
            detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento_inventario.dbcr = 'H'
            detalle_asiento_inventario.detalle = u"Garantía de inventario Egreso"
            detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=item_bodega.item.categoria_item.plan_cuenta_compra.id)
            detalle_asiento_inventario.valor = redondeo(cantidad * costo)
            detalle_asiento_inventario.fecha_creacion = now
            detalle_asiento_inventario.usuario_creacion = request.user.username
            detalle_asiento_inventario.save()

            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
            detalle_asiento = Detalle_Comp_Contable()
            detalle_asiento.cabecera_contable = cabecera_asiento
            detalle_asiento.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento.dbcr = 'D'
            detalle_asiento.detalle = u"Garantía de inventario Egreso"
            detalle_asiento.valor = redondeo(cantidad * costo)
            detalle_asiento.plan_cuenta = cuenta

            detalle_asiento.fecha_creacion = now
            detalle_asiento.usuario_creacion = request.user.username
            detalle_asiento.save()
        except:
            cont_mns_form += 1
            contador += 1
            errors = forms._errors.setdefault("item", ErrorList())
            errors.append(u"El item: " + item.codigo + "-"
                          + item.nombre + u" al que quiere hacer mantenimiento de inventario no existe en bodega.")
    return cont_mns_form, contador


###############################################################################################################
###############################################################################################################
###############################################################################################################
###############################################################################################################
###############################################################################################################

def validar_num_doc_inventario_guia(cabecera_form):
    """
    Función para validar si ya existe registrado ese número de doc en inventario por Guía de remisión
    :param cabecera_form:
    :return:
    """
    if not str(cabecera_form.getNumDoc()).isspace():
        tipo_doc = Documento.objects.get(id=cabecera_form.getTipoDoc())
        proveedor = cabecera_form.getProveedor()
        tipo_moviento_inv = Tipo_Mov_Inventario.objects.get(id=6)  # Guia de remisión
        if Inventario_Cabecera.objects.filter(documento=tipo_doc, num_doc=cabecera_form.getNumDoc(), tipo_mov_inventario=tipo_moviento_inv, cliente_proveedor_id=proveedor).exists():
            return False
        else:
            return True
    else:
        return False


def validarTotalCompra(cabecera_form, total):
    """
    Valida si el total de la compra en inventario es igual al total ingresada en el módulo de compras
    :param cabecera_form:
    :param total:
    :return:
    """
    tipo_doc = Documento.objects.get(id=cabecera_form.getTipoDoc())
    proveedor = cabecera_form.getProveedor()
    num_doc = cabecera_form.getNumDoc()
    if CompraCabecera.objects.filter(num_doc=num_doc, documento=tipo_doc, proveedor=proveedor).exists():
        compra = CompraCabecera.objects.get(num_doc=num_doc, documento=tipo_doc, proveedor=proveedor)
        if compra.getSubtotal() != total:
            return True
        else:
            return False
    else:
        return False


def GuardarCabeceraConvGuiaCompra(inventario_cab, cabecera_form, cabecera_asiento, cursor, contador, now, request):
    """
    Convertir Guía a Compra
    :param inventario_cab:
    :param cabecera_form:
    :param cabecera_asiento:
    :param cursor:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    inventario_cab.fecha_reg = cabecera_form.getFechaReg()
    inventario_cab.detalle = cabecera_form.getConcepto()
    inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=14) #Ingreso de Inventario
    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=1) # Tipo Compra

    inventario_cab.num_comp = get_num_comp(14, inventario_cab.fecha_reg, False, request)

    if cabecera_form.getNumDoc() == "":
        errors = cabecera_form._errors.setdefault("num_doc", ErrorList())
        errors.append(u"El documento que ingresó ya se encuentra registrado en el sistema o no es válido")
        contador += 1
        messages.error(request, u"El documento que ingresó ya se encuentra registrado en el sistema o no es válido")

    elif validar_num_doc_inventario_guia(cabecera_form):
        inventario_cab.num_doc = cabecera_form.getNumDoc()
        inventario_cab.documento = Documento.objects.get(id=cabecera_form.getTipoDoc())

    else:
        messages.error(request, u"El documento que ingresó ya se encuentra registrado en el sistema o no es válido")
        errors = cabecera_form._errors.setdefault("num_doc", ErrorList())
        errors.append(u"El documento que ingresó ya se encuentra registrado en el sistema o no es válido")
        contador += 1

    try:
        Proveedores.objects.get(id=cabecera_form.getProveedor())
        inventario_cab.cliente_proveedor_id = cabecera_form.getProveedor()
    except:
        messages.error(request, u"El proveedor que eligió no se encuentra activo")
        errors = cabecera_form._errors.setdefault("proveedor", ErrorList())
        errors.append(u"El proveedor que eligió no se encuentra activo")
        contador += 1

    inventario_cab.fecha_creacion = now
    inventario_cab.usuario_creacion = request.user.username
    inventario_cab.save()

    cabecera_asiento.tipo_comprobante = inventario_cab.tipo_comprobante
    cabecera_asiento.numero_comprobante = inventario_cab.num_comp
    cabecera_asiento.concepto_comprobante = inventario_cab.detalle
    cabecera_asiento.fecha = inventario_cab.fecha_reg

    cabecera_asiento.fecha_creacion = now
    cabecera_asiento.usuario_creacion = request.user.username
    cabecera_asiento.save()

    return contador


def agregar_cantidad_detalle_inventario(obj, cantidad, username):
    """
    Funcion para agregar la cantidad recibida de los items
    revisa cada detalle de inventario y compara si es igual al item
    entonces le agrega la cantidad entregada por el proveedor
    y cambia de estado si se le ha asignado el total del producto
    en esa guia
    """
    if obj.costo is not None:
        if cantidad + obj.costo >= obj.cantidad:
            x = cantidad - (cantidad - obj.costo)
            obj.costo = obj.cantidad
            obj.status = 3
            obj.fecha_actualizacion = datetime.datetime.now()
            obj.usuario_actualizacion = username
            obj.save()
            return x
        else:
            obj.costo += cantidad
            obj.fecha_actualizacion = datetime.datetime.now()
            obj.usuario_actualizacion = username
            obj.save()
            return 0
    else:
        if cantidad >= obj.cantidad:
            obj.costo = cantidad
            obj.fecha_actualizacion = datetime.datetime.now()
            obj.usuario_actualizacion = username
            obj.save()
            return 0
        else:
            obj.costo = cantidad
            obj.fecha_actualizacion = datetime.datetime.now()
            obj.usuario_actualizacion = username
            obj.save()
            return obj.cantidad - cantidad


def actualizar_detalle_inventario(cabecera_inventario, id_item, cantidad, username):
    """
    Funcion para actualizar las tablas de cabecera de
    inventario sus detalles, y le cambia de estado si ha sido
    entregado todo de la guia
    """
    for obj_cab in cabecera_inventario:
        detalle_inventario = Inventario_Detalle.objects.filter(inventario_cabecera=obj_cab)
        for obj in detalle_inventario:
            if id_item == obj.item.id:
                cantidad = agregar_cantidad_detalle_inventario(obj, cantidad, username)
            if cantidad == 0:
                break
        tam_detalle_inv = len(detalle_inventario)
        cont = 0
        for obj in detalle_inventario:
            if obj.status == 3:
                cont += 1
        if tam_detalle_inv == cont:
            obj_cab.status = 3
            obj_cab.save()

        if cantidad == 0:
            break


def cambiar_valor_items(id, id_item, cantidad, username):
    """
    Recibe el item y la cantidad para actualizar el detalle
    del inventario y la cabecera
    """
    tipo_mov = Tipo_Mov_Inventario.objects.get(id=6)
    cabecera_inventario = Inventario_Cabecera.objects.filter(cliente_proveedor_id=id, tipo_mov_inventario=tipo_mov)
    actualizar_detalle_inventario(cabecera_inventario, id_item, cantidad, username)


def GuardarDetalleConvertirGuiaCompra(id, cabecera_form, detalle_form, inventario_cab, cabecera_asiento, contador, now, request):
    """
    Detalle de Convertir Guía a Compra
    :param id:
    :param cabecera_form:
    :param detalle_form:
    :param inventario_cab:
    :param cabecera_asiento:
    :param contador:
    :param now:
    :param request:
    :return:
    """
    total_inventario = 0.0
    for forms in detalle_form:
        detalle_inventario = Inventario_Detalle()
        informacion = forms.cleaned_data
        id_item = informacion.get("item")
        if informacion.get("costo") is not None:
            costo = redondeo(float(informacion.get("costo")))
        else:
            costo = 0
        if informacion.get("cantidad_documento") is not None:
            cantidad = float(informacion.get("cantidad_documento"))
        else:
            cantidad = 0

        if cantidad > 0 and costo > 0:
            detalle_inventario.item = Item.objects.get(id=id_item)
            detalle_inventario.costo = costo
            detalle_inventario.cantidad = cantidad
            detalle_inventario.inventario_cabecera = inventario_cab

            detalle_inventario.fecha_creacion = now
            detalle_inventario.usuario_creacion = request.user.username
            detalle_inventario.bodega = Bodega.objects.get(id=1) # Bodega creada para pruebas
            detalle_inventario.save()

            # Actualiza la tabla de Cabecera de inventario y detalle de inventario
            cambiar_valor_items(id, id_item, cantidad, request.user.username)

            total_inventario += costo
            item_bodega = Item_Bodega.objects.get(item=detalle_inventario.item, bodega=detalle_inventario.bodega)
            proveedor_item = Proveedor_Item.objects.get(proveedor=inventario_cab.getProveedor(), item=item_bodega.item)

            if proveedor_item.cantidad_provision - cantidad < 0:
                contador += 1
                messages.error(request, u"No puede ingresar cantidad mayor a la que se encuentra registrada en el producto")
                errors = forms._errors.setdefault("cantidad_documento", ErrorList())
                errors.append(u"No puede ingresar cantidad mayor a la que se encuentra registrada en el producto")
            else:
                item_bodega.cantidad_provisionada -= cantidad
                proveedor_item.cantidad_provision -= cantidad
                proveedor_item.ultimo_costo = costo

                proveedor_item.fecha_actualizacion = now
                proveedor_item.usuario_actualizacion = request.user.username
                proveedor_item.save()

            item_bodega.fecha_actualizacion = now
            item_bodega.usuario_actualizacion = request.user.username

            cantidad_total = cantidad + item_bodega.cantidad_actual

            # Valida que el item tenga costo promedio
            if detalle_inventario.item.costo is not None and detalle_inventario.item.costo > 0:
                costo_total = ((detalle_inventario.item.costo * item_bodega.cantidad_actual) + costo)
            else:
                costo_total = costo

            item = Item.objects.get(id=detalle_inventario.item_id)
            item.costo = calcular_costo_promedio(costo_total, cantidad_total)
            item.fecha_actualizacion = now
            item.usuario_actualizacion = request.user.username
            item.save()

            item_bodega.cantidad_actual += cantidad
            item_bodega.save()

            detalle_inventario.item.fecha_actualizacion = now
            detalle_inventario.item.usuario_actualizacion = request.user.username
            detalle_inventario.item.save()

            detalle_asiento_inventario = Detalle_Comp_Contable()
            detalle_asiento_inventario.cabecera_contable = cabecera_asiento
            detalle_asiento_inventario.fecha_asiento = cabecera_asiento.fecha
            detalle_asiento_inventario.dbcr = 'D'
            detalle_asiento_inventario.detalle = "Convertir Guia en Compra"

            detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=detalle_inventario.item.categoria_item.plan_cuenta_compra.id)
            detalle_asiento_inventario.valor = costo
            detalle_asiento_inventario.fecha_creacion = now
            detalle_asiento_inventario.usuario_creacion = request.user.username
            detalle_asiento_inventario.save()

    if total_inventario > 0:
        cuenta = PlanCuenta.objects.get(tipo=TipoCuenta.objects.get(id=16))  # Cuenta transitoria
        detalle_asiento = Detalle_Comp_Contable()
        detalle_asiento.cabecera_contable = cabecera_asiento
        detalle_asiento.fecha_asiento = cabecera_asiento.fecha
        detalle_asiento.dbcr = 'H'
        detalle_asiento.detalle = "Transitoria en compras de inventario"
        detalle_asiento.valor = total_inventario
        detalle_asiento.plan_cuenta = cuenta

        detalle_asiento.fecha_creacion = now
        detalle_asiento.usuario_creacion = request.user.username
        detalle_asiento.save()
    else:
        contador += 1
        messages.error(request, u"Debe ingresar un valor válido para la factura, no se permite una factura "
                                u"con un total de $ 0.00")

    if validarTotalCompra(cabecera_form, total_inventario):
        messages.error(request, u"El total de la compra no coincide con el total del costo de los "
                                u"productos")
        contador += 1

    return contador


@csrf_exempt
def cargar_guia_compra(request):
    """
    Carga en una tabla los items cargados de un proveedor por una guía de
    remisión
    :param request:
    :return:
    """
    id_prov = request.POST.get("id")
    detalle_form_formset = formset_factory(GuiaCompraForm, extra=0)
    l_detalle = []
    proveedor = Proveedores.objects.get(id=id_prov)
    items_sum = Proveedor_Item.objects.filter(proveedor=proveedor, cantidad_provision__gt=0)

    if len(items_sum) == 0:
        bandera = False
    else:
        bandera = True

    for obj in items_sum:
        l_detalle.append({"id_item": obj.item.id, "item": obj.item.id, "cantidad_guia": obj.cantidad_provision, "cantidad_documento": "",
                          "costo": "", "p_unitario": ""})
    detalle_form = detalle_form_formset(initial=l_detalle)

    return render_to_response('inventario/tabla_detalle_guia_remision_compra.html',
                              {"forms": detalle_form,
                               "bandera": bandera}, context_instance=RequestContext(request))

########################################################################################################################
#                                  FUNCIONES PARA GUIA REQUISICIÓN DE CONTRATO                                         #
########################################################################################################################


def GuardarCabeceraInventarioRequisicionContrato(inventario_cab, empresa_parametro, detalle_inv_contrato, cabecera_inv, cabecera_asiento, contador,  now, request):
    """
        Función GuardarCabeceraInventarioRequisicionContrato(inventario_cab, cabecera_form, cabecera_asiento , now, request)
        Detalle: Guarda en inventario cabecera y crea el comprobante_contable
    """
    costo_total = 0.0
    inventario_cab.fecha_reg = cabecera_inv.getFechaReg()
    inventario_cab.detalle = cabecera_inv.getConcepto()
    id_cta_inventario = []
    lista_cta_inventario = []
    id_cta_costo_venta = []
    lista_cta_costo_venta = []
    cont_msn_item_cost_cero = 0  # Contador para el mensaje de item con costo cero

    if cabecera_inv.getTipo() == "1":  # Ingreso
        inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=14)        #Ingreso de inventario
    else:
        inventario_cab.tipo_comprobante = TipoComprobante.objects.get(id=15)        #Egreso de inventario


    inventario_cab.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=cabecera_inv.getMotivo())  # Tipo Requisición de Contrato

    if inventario_cab.fecha_reg < empresa_parametro.fecha_ultimo_inventario:
        messages.error(request, u"La fecha de registro es menor a la del último registro de inventario, "
                                u"por favor verifique la fecha")
        contador += 1
    else:
        inventario_cab.num_comp = get_num_comp(inventario_cab.tipo_comprobante.id, inventario_cab.fecha_reg, False, request)
        empresa_parametro.fecha_ultimo_inventario = cabecera_inv.getFechaReg()
        empresa_parametro.save()

    if cabecera_inv.getNumContrato() == "":
        errors = cabecera_inv._errors.setdefault("num_contrato", ErrorList())
        errors.append(u"Por favor seleccione un contrato")
        contador += 1
        messages.error(request, u"Por favor seleccione un contrato")
    else:
        try:
            contrato = ContratoCabecera.objects.get(id=cabecera_inv.getNumContrato(), status=1)     #Obtengo el objeto de cabecera contrato
            inventario_cab.num_doc = contrato.num_cont
            inventario_cab.contrato_id = contrato.id
            inventario_cab.cliente_proveedor_id = contrato.cliente.id#esto 18 agosto agrega en clienteproveedorid un valor, el del dueño del contrato
            inventario_cab.fecha_creacion = now
            inventario_cab.usuario_creacion = request.user.username
            inventario_cab.save()

            cabecera_asiento.tipo_comprobante = inventario_cab.tipo_comprobante
            cabecera_asiento.numero_comprobante = inventario_cab.num_comp
            cabecera_asiento.concepto_comprobante = inventario_cab.detalle
            cabecera_asiento.fecha = inventario_cab.fecha_reg

            cabecera_asiento.fecha_creacion = now
            cabecera_asiento.usuario_creacion = request.user.username
            cabecera_asiento.save()

            # Si no existe problemas con la cabecera del inventario
            if contador == 0:
                if detalle_inv_contrato.is_valid():
                    for forms in detalle_inv_contrato:
                        informacion = forms.cleaned_data
                        id_item = informacion.get("item")
                        cantidad = informacion.get("cantidad")
                        if cantidad == 0:
                            errors = forms._errors.setdefault("cantidad", ErrorList())
                            errors.append(u"Debe ingresar una cantidad mayor a cero")
                            contador += 1
                        detalle_inventario = Inventario_Detalle()
                        detalle_inventario.inventario_cabecera = inventario_cab

                        try:
                            item = Item.objects.get(id=id_item)
                            costo = item.costo
                            if costo is None:
                                costo = 0

                            if cabecera_inv.getTipo() == "1":  # Ingreso
                                item_bodega = Item_Bodega.objects.get(item=item, bodega=Bodega.objects.filter(status=1)[0])
                                item_bodega.cantidad_actual += cantidad
                                item_bodega.fecha_actualizacion = now
                                item_bodega.usuario_actualizacion = request.user.username
                                item_bodega.save()
                            else:  # Egreso
                                item_bodega = Item_Bodega.objects.get(item=item, bodega=Bodega.objects.filter(status=1)[0])
                                if (item_bodega.cantidad_actual - cantidad) < 0:
                                    messages.error(request, u"La cantidad de" + unicode(item.descripcion) + u" que ingresó es superior a la que se encuentra en bodega, "
                                                            u"actualmente  se encuentra: " + str(item_bodega.cantidad_actual))
                                    errors = forms._errors.setdefault("cantidad", ErrorList())
                                    errors.append(u"La cantidad de" + unicode(item.descripcion) + u" que ingresó es superior a la que se encuentra en bodega, "
                                                    u"actualmente  se encuentra: " + str(item_bodega.cantidad_actual))
                                    contador += 1
                                else:
                                    item_bodega.cantidad_actual -= cantidad
                                    item_bodega.fecha_actualizacion = now
                                    item_bodega.usuario_actualizacion = request.user.username
                                    item_bodega.save()
                                    """
                                    if item_bodega.cantidad_actual == 0:
                                        item.costo = 0
                                        item.fecha_actualizacion = now
                                        item.usuario_actualizacion = request.user.username
                                        item.save()
                                    """
                        except:
                            costo = 0
                            item = None
                            errors = forms._errors.setdefault("item", ErrorList())
                            errors.append(u"El item que quiere usar no existe en bodega o el costo del item es de $0.00. "
                                          u"por favor revise")
                            contador += 1

                        if costo == 0:
                            errors = forms._errors.setdefault("item", ErrorList())
                            errors.append(u"El item que quiere tiene costo de $0.00. Y no se lo puede usar en esta transacción, "
                                          u"por favor verifique")
                            if cont_msn_item_cost_cero == 0:
                                messages.error(request, u"El/Los item/s que quiere utilizar tiene costo de $0.00. Y no se lo puede usar en esta transacción, "
                                                        u"por favor verifique")
                            contador += 1
                            cont_msn_item_cost_cero += 1

                        costo_x_item = cantidad * costo
                        costo_total += costo_x_item
                        detalle_inventario.costo = costo
                        detalle_inventario.cantidad = cantidad
                        detalle_inventario.bodega = Bodega.objects.filter(status=1)[0]
                        detalle_inventario.item = item
                        detalle_inventario.fecha_creacion = now
                        detalle_inventario.usuario_creacion = request.user.username
                        detalle_inventario.save()

                        id_cta_compra = item.categoria_item.plan_cuenta_compra.id
                        if id_cta_compra not in id_cta_inventario:
                            lista_cta_inventario.append([id_cta_compra, costo_x_item])
                            id_cta_inventario.append(id_cta_compra)
                        else:
                            for obj in lista_cta_inventario:
                                if obj[0] == id_cta_compra:
                                    obj[1] += costo_x_item

                        id_cta_venta = item.categoria_item.plan_cuenta_costo_venta.id
                        if id_cta_venta not in id_cta_costo_venta:
                            lista_cta_costo_venta.append([id_cta_venta, costo_x_item])
                            id_cta_costo_venta.append(id_cta_venta)
                        else:
                            for obj in lista_cta_costo_venta:
                                if obj[0] == id_cta_venta:
                                    obj[1] += costo_x_item

                    for obj in lista_cta_inventario:
                        detalle_asiento = Detalle_Comp_Contable()
                        detalle_asiento.cabecera_contable = cabecera_asiento
                        detalle_asiento.plan_cuenta_id = obj[0]  # id plan cuenta inventario
                        detalle_asiento.detalle = u"Requisión Contrato #" + contrato.num_cont

                        if cabecera_inv.getTipo() == "1":  # Ingreso
                            detalle_asiento.dbcr = "D"
                        else:  # Egreso
                            detalle_asiento.dbcr = "H"

                        detalle_asiento.fecha_asiento = cabecera_asiento.fecha
                        detalle_asiento.valor = obj[1]  # Valor total de cuenta inventario
                        detalle_asiento.fecha_creacion = now
                        detalle_asiento.usuario_creacion = request.user.username
                        detalle_asiento.save()

                    for obj in lista_cta_costo_venta:
                        detalle_asiento2 = Detalle_Comp_Contable()
                        detalle_asiento2.cabecera_contable = cabecera_asiento
                        detalle_asiento2.plan_cuenta_id = obj[0]  # id plan cuenta costo venta

                        if cabecera_inv.getTipo() == "1":  # Ingreso
                            detalle_asiento2.dbcr = "H"
                        else:  # Egreso
                            detalle_asiento2.dbcr = "D"

                        detalle_asiento2.valor = obj[1]  # Valor total de cuenta costo venta
                        detalle_asiento2.detalle = u"Requisión Contrato #" + contrato.num_cont
                        detalle_asiento2.fecha_asiento = cabecera_asiento.fecha
                        detalle_asiento2.fecha_creacion = now
                        detalle_asiento2.usuario_creacion = request.user.username
                        detalle_asiento2.save()

                    detalle_contrato = ContratoDetalle()
                    detalle_contrato.contrato_cabecera = contrato
                    detalle_contrato.valor = costo_total
                    detalle_contrato.modulo_id = inventario_cab.id
                    detalle_contrato.tipo_comprobante = inventario_cab.tipo_comprobante
                    detalle_contrato.fecha_creacion = now
                    detalle_contrato.usuario_creacion = request.user.username
                    detalle_contrato.save()
                else:
                    contador += 1
                    messages.error(request, u"Error en el detalle de requisición de contratos, "
                                            u"revise si estan bien ingresados los valores.")

        except:
            errors = cabecera_inv._errors.setdefault("num_contrato", ErrorList())
            errors.append(u"El número de contrato no se encuentra registrado en el sistema o no es "
                          u"válido o no se encuentra activo")
            contador += 1
            messages.error(request, u"El número de contrato no se encuentra registrado en el sistema o no es "
                                    u"válido o no se encuentra activo")

    return contador


@login_required(login_url='/')
@csrf_exempt
def buscar_item_contrato(request):
    id_item = request.POST.get("id_item", "")
    respuesta = []

    try:
        items = Item.objects.filter(id=int(id_item), status=1)

        if len(items) == 0:
            respuesta.append({"id": 0, "descripcion": "No se encuentra definido el item"})

        else:
            for obj in items:
                respuesta.append({"id": obj.id, "nombre": obj.nombre,
                                  "iva": obj.iva, "costo": obj.costo,
                                  "ice": obj.porc_ice, "tipo": obj.categoria_item.tipo_item.id})
    except:
        pass
        respuesta.append({"id": 0, "descripcion": "No se encuentra definido el item"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def get_cantidades_bodega_det_inventario(request):
    '''
       Función para validar la cantidad ingresada exista
       en bodega
    '''
    respuesta = []
    id = request.POST.get("id_item", "")
    cantidad = request.POST.get("cantidad", "")

    try:
        bodega_tem = Item_Bodega.objects.get(item__id=id)
        cant_bodega = float(bodega_tem.cantidad_actual)

        if cantidad != "":
            if float(str(cantidad)) <= cant_bodega:
                respuesta.append({"status": 1,
                                  "cantidad": cantidad,
                                  "cantidad_bodega": cant_bodega,
                                  "descripcion": "ok"})

            else:
                respuesta.append({"status": 2,
                                  "cantidad": cantidad,
                                  "cantidad_bodega": cant_bodega,
                                  "descripcion": "Error cantidad no disponible"})
        else:
            respuesta.append({"status": 1,
                                  "cantidad": cantidad,
                                  "cantidad_bodega": cant_bodega,
                                  "descripcion": "ok"})

    except:
        pass
        respuesta.append({"status": 0, "cantidad_bodega": 0, "descripcion": "Error en el servidor"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def get_existe_item_bodega_contrato(request):
    respuesta = []
    id = request.POST.get("id_item", "")
    try:
        item = Item.objects.get(status=1, id=id)
        if item.categoria_item.tipo_item.id == 1:
            if Item_Bodega.objects.filter(status=1, item__id=item.id, item__categoria_item__tipo_item__id=1).exists():
                respuesta.append({"id": id, "status": 1, "mensaje": "Existe en Bodega."})
                resultado = json.dumps(respuesta)
                return HttpResponse(resultado, mimetype='application/json')
            else:
                respuesta.append({"id": id, "status": 0, "mensaje": "No está registrado en bodega el item."})
                resultado = json.dumps(respuesta)
                return HttpResponse(resultado, mimetype='application/json')
        else:
            respuesta.append({"id": id, "status": 2, "mensaje": "Item Servicio."})
            resultado = json.dumps(respuesta)
            return HttpResponse(resultado, mimetype='application/json')
    except:
        respuesta.append({"status": -1, "mensaje": "Error en el servidor."})
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def get_select_cliente_contrato(request):
    id_cliente = request.POST.get("id", "")
    respuesta = []

    try:
        cliente = Cliente.objects.get(id=id_cliente)
        contratos = ContratoCabecera.objects.filter(status=1).filter(cliente=cliente)

        if len(contratos) == 0:
            respuesta.append({"status": 0, "id": 0, "descripcion": "No se encuentra contrato alguno registrado al cliente"})

        else:
            for obj in contratos:
                respuesta.append({"status": 1, "id": obj.id, "referencia": obj.referencia, "descripcion": "Consulta exitosa"})
    except:
        pass
        respuesta.append({"status": -1, "id": -1, "descripcion": "Error en el servidor"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")

########################################################################################################################
class Item_Inventario():
    def __init__(self, lista, cantidad_inicial=0.00, costo_inicial=0.0, cantidad_actual=0.0, costo_actual=0.0):
        self.item = Item()
        self.cantidad_inicial = cantidad_inicial
        self.costo_inicial = costo_inicial
        self.cantidad_actual = cantidad_actual
        self.costo_actual = costo_actual
        self.lista = lista

class Lista_Item_Inventario():
    def __init__(self, det_inv, cantidad_act=0.00, costo_linea=0.0, costo_act=0.0, reprocesar=False):
        self.item = Item()
        self.det_inv = det_inv
        self.cantidad_act = cantidad_act
        self.costo_linea = costo_linea
        self.costo_act = costo_act
        self.reprocesar = reprocesar

########################################################################################################################

def reprocesar_inventario(anio_reversar, parametro, request):
    '''
    Función que se encarga de modificar los costos
    del item y su stock según los movimientos de inventario (Todos)
    :param anio_reversar:
    :param parametro:
    :param request:
    :return:
    '''
    lista = []
    try:
        lista_items = Item.objects.filter(status=1, categoria_item__tipo_item_id=1).order_by("codigo")
        for c in lista_items:
            lista_trans = []
            if Item_Bodega_Anual.objects.filter(status=1, anio=anio_reversar, item=c).exists() and Item_Anual.objects.filter(status=1, anio=anio_reversar, item=c).exists():
                item_anual = Item_Anual.objects.get(status=1, anio=anio_reversar, item=c)
                item_bodega = Item_Bodega_Anual.objects.get(status=1, anio=anio_reversar, item=c)
                # DATOS INICIALES
                costo_inicial = item_anual.costo_actual
                cantidad_inicial = item_bodega.cantidad_actual
                cantidad_act = cantidad_inicial
                costo_act = costo_inicial
            else:
                costo_inicial = 0.0
                cantidad_inicial = 0.0
                cantidad_act = 0.0
                costo_act = 0.0

            try:
                cantidad_actual_item = Item_Bodega.objects.get(status=1, item=c).cantidad_actual
            except:
                cantidad_actual_item = 0.0

            for det_inv in Inventario_Detalle.objects.filter(status=1, item=c, inventario_cabecera__fecha_reg__year=anio_reversar).\
                    order_by("inventario_cabecera__fecha_reg", "inventario_cabecera__tipo_comprobante__id",
                    "inventario_cabecera__num_comp"):

                costo_temp_linea = det_inv.costo

                # Ajuste Cantidad - Venta - Producción - C.Interno
                if det_inv.inventario_cabecera.tipo_mov_inventario.modifica_costo:
                    det_inv.costo = costo_act


                # Costo Línea
                if det_inv.cantidad == 0.0:
                    costo_linea = float(det_inv.costo)
                else:
                    costo_linea = float(det_inv.cantidad*det_inv.costo)

                if det_inv.inventario_cabecera.tipo_comprobante_id == 14:   # Ingreso

                    if cantidad_act + det_inv.cantidad == 0.0:
                        costo_act = 0.0
                        cantidad_act = 0.0
                    else:
                        costo_act = (float(cantidad_act*costo_act) + float(costo_linea)) / (float(cantidad_act) + float(det_inv.cantidad))
                        cantidad_act = (float(cantidad_act) + float(det_inv.cantidad))

                else:               # Egreso

                    if cantidad_act - det_inv.cantidad == 0.0:
                        costo_act = 0.0
                        cantidad_act = 0.0
                    else:
                        costo_act = (float(cantidad_act*costo_act) - float(costo_linea)) / (float(cantidad_act) - float(det_inv.cantidad))
                        cantidad_act = (float(cantidad_act) - float(det_inv.cantidad))


                trans_inv = Lista_Item_Inventario(det_inv,  cantidad_act, costo_temp_linea, costo_act)
                trans_inv.item = c

                # Pregunto si reprocesa ya que es motivo: Ventas, Ajuste_Cantidad, Produccion
                if det_inv.inventario_cabecera.tipo_mov_inventario.modifica_costo:

                    costo_act_temp = redondeo(float(costo_act), 9)
                    costo_temp_linea_c = redondeo(float(costo_temp_linea), 9)

                    if costo_act_temp != costo_temp_linea_c:
                        trans_inv.reprocesar = True         # Reproceso
                    else:
                        trans_inv.reprocesar = False        # No se reprocesa (iguales)

                lista_trans.append(trans_inv)

            lista_final = Item_Inventario(lista_trans)
            lista_final.item = c
            lista_final.cantidad_inicial = cantidad_inicial
            lista_final.costo_inicial = costo_inicial
            lista_final.cantidad_actual = cantidad_actual_item
            lista_final.costo_actual = c.costo
            lista.append(lista_final)

            if parametro == "1":
                try:
                    # Actualizo los items
                    c.costo = costo_act
                    c.fecha_actualizacion = datetime.datetime.now()
                    c.usuario_actualizacion = request.user.username
                    c.save()
                    # Cantidad Bodega Actual
                    item_bodega = Item_Bodega.objects.filter(status=1).get(item=c)
                    item_bodega.cantidad_actual = cantidad_act
                    item_bodega.fecha_actualizacion = datetime.datetime.now()
                    item_bodega.usuario_actualizacion = request.user.username
                    item_bodega.save()
                except:
                    pass
    except:
        pass

    return lista

def reprocesar_inventario_item(anio_reversar, parametro, item, request):
    '''
    Función para buscar y reprocesar el item seleccionado
    :param anio_reversar:
    :param parametro:
    :param item:
    :param request:
    :return lista:
    '''

    lista = []
    lista_trans = []
    if Item_Bodega_Anual.objects.filter(status=1, anio=anio_reversar, item=item).exists() and Item_Anual.objects.filter(status=1, anio=anio_reversar, item=item).exists():
        item_anual = Item_Anual.objects.get(status=1, anio=anio_reversar, item=item)
        item_bodega = Item_Bodega_Anual.objects.get(status=1, anio=anio_reversar, item=item)
        ################################ DATOS INICIALES ###############################################
        costo_inicial = item_anual.costo_actual
        cantidad_inicial = item_bodega.cantidad_actual
        ################################################################################################
        cantidad_act = cantidad_inicial
        costo_act = costo_inicial
    else:
        costo_inicial = 0.0
        cantidad_inicial = 0.0
        cantidad_act = 0.0
        costo_act = 0.0

    try:
        cantidad_actual_item = Item_Bodega.objects.get(status=1, item=item).cantidad_actual
    except:
        cantidad_actual_item = 0.0

    for det_inv in Inventario_Detalle.objects.filter(status=1, item=item).order_by("inventario_cabecera__fecha_reg", "inventario_cabecera__tipo_comprobante__id", "inventario_cabecera__num_comp" ):
        costo_temp_linea = det_inv.costo

        if det_inv.inventario_cabecera.tipo_mov_inventario.modifica_costo == True:  # Ajuste Cantidad - Venta - Producción - C.Interno
            det_inv.costo = costo_act
        # Costo Linea
        if det_inv.cantidad == 0.0:
            costo_linea = float(det_inv.costo)
        else:
            costo_linea = float(det_inv.cantidad*det_inv.costo)
        ###################################################################

        if det_inv.inventario_cabecera.tipo_comprobante.id == 14:   # Ingreso
            if cantidad_act + det_inv.cantidad == 0.0:
                costo_act = 0.0
                cantidad_act = 0.0
            else:
                costo_act = (float(cantidad_act*costo_act) + float(costo_linea)) / (float(cantidad_act) + float(det_inv.cantidad))
                cantidad_act = (float(cantidad_act) + float(det_inv.cantidad))

        else:

            if cantidad_act - det_inv.cantidad == 0.0:
                #costo_act = 0.0
                cantidad_act = 0.0
            else:
                costo_act = (float(cantidad_act*costo_act) - float(costo_linea)) / (float(cantidad_act) - float(det_inv.cantidad))
                cantidad_act = (float(cantidad_act) - float(det_inv.cantidad))

        trans_inv = Lista_Item_Inventario(det_inv,  cantidad_act, costo_temp_linea, costo_act)              # Objeto Lista_Item_Inventario
        trans_inv.item = item


        if det_inv.inventario_cabecera.tipo_mov_inventario.modifica_costo == True:  # Pregunto si reprocesa ya que es motivo: Ventas, Ajuste_Cantidad, Produccion
            if redondeo(float(costo_act), 9) != redondeo(float(costo_temp_linea), 9):
                trans_inv.reprocesar = True         # Reproceso
            else:
                trans_inv.reprocesar = False        # No se reprocesa (iguales)

        lista_trans.append(trans_inv)

    lista_final = Item_Inventario(lista_trans)
    lista_final.item = item
    lista_final.cantidad_inicial = cantidad_inicial
    lista_final.costo_inicial = costo_inicial
    lista_final.cantidad_actual = cantidad_actual_item
    lista_final.costo_actual = item.costo
    lista.append(lista_final)

    if parametro == "1":
        ########## ACTUALIZO LOS ITEMS #########################
        try:
            item.costo = costo_act
            item.fecha_actualizacion = datetime.datetime.now()
            item.usuario_actualizacion = request.user.username
            item.save()

            ################ Cantidad en bodega Actual ###################
            item_bodega = Item_Bodega.objects.filter(status=1).get(item=item)
            item_bodega.cantidad_actual = cantidad_act
            item_bodega.fecha_actualizacion = datetime.datetime.now()
            item_bodega.usuario_actualizacion = request.user.username
            item_bodega.save()
        except:
            pass
    return lista




def guardar_detalle_reproceso_tmp(anio_reversar, request):
    '''
    Función que realiza el procedimiento de reproceso y llena la tabla temporal
    cada vez que es llamada esta función realiza un delete
    :param anio_reversar:
    :param request:
    :return:
    '''
    detalles_tmp_reprocesop = Tmp_Detalle_Reproceso.objects.filter(status=1)

    if len(detalles_tmp_reprocesop) > 0:
        for d in detalles_tmp_reprocesop:
            d.delete()

    else:

        lista_items = Item.objects.filter(status=1, categoria_item__tipo_item_id=1).order_by("codigo")

        for c in lista_items:

            if c.cantidad_item_inicial_anual(anio_reversar) > 0:
                costo_inicial = c.costo_item_inicial_anual(anio_reversar)
                cantidad_inicial = c.cantidad_item_inicial_anual(anio_reversar)
                costo_act = costo_inicial
                cantidad_act = cantidad_inicial
            else:
                cantidad_act = 0.0
                costo_act = 0.0


            for det_inv in Inventario_Detalle.objects.filter(status=1, item=c, inventario_cabecera__fecha_reg__year=anio_reversar).order_by("inventario_cabecera__fecha_reg", "inventario_cabecera__tipo_comprobante__id", "inventario_cabecera__num_comp"):
                costo_temp_linea = det_inv.costo

                # Ajuste Cantidad - Venta - Producción - C.Interno
                if det_inv.inventario_cabecera.tipo_mov_inventario.modifica_costo:
                    det_inv.costo = costo_act

                # Costo Línea
                if det_inv.cantidad == 0.0:
                    costo_linea = float(det_inv.costo)
                else:
                    costo_linea = float(det_inv.cantidad*det_inv.costo)

                if det_inv.inventario_cabecera.tipo_comprobante_id == 14:   # Ingreso

                    if cantidad_act + det_inv.cantidad == 0.0:
                        costo_act = 0.0
                        cantidad_act = 0.0
                    else:
                        costo_act = (float(cantidad_act*costo_act) + float(costo_linea)) / (float(cantidad_act) + float(det_inv.cantidad))
                        cantidad_act = (float(cantidad_act) + float(det_inv.cantidad))

                else:               # Egreso

                    if cantidad_act - det_inv.cantidad == 0.0:
                        costo_act = 0.0
                        cantidad_act = 0.0
                    else:
                        costo_act = (float(cantidad_act*costo_act) - float(costo_linea)) / (float(cantidad_act) - float(det_inv.cantidad))
                        cantidad_act = (float(cantidad_act) - float(det_inv.cantidad))

                tmp_detalle_costo = Tmp_Detalle_Reproceso()
                tmp_detalle_costo.id_detalle_inv = det_inv.id
                tmp_detalle_costo.item = c
                tmp_detalle_costo.bodega = Bodega.objects.get(id=1)
                tmp_detalle_costo.inventario_cabecera = det_inv.inventario_cabecera
                tmp_detalle_costo.tipo_mov_inventario = det_inv.inventario_cabecera.tipo_mov_inventario
                tmp_detalle_costo.num_comp = det_inv.inventario_cabecera.num_comp
                tmp_detalle_costo.fecha_reg = det_inv.inventario_cabecera.fecha_reg
                tmp_detalle_costo.orden_comp = det_inv.inventario_cabecera.orden_comp

                tmp_detalle_costo.cantidad = det_inv.cantidad     # Cantidad Inventario
                tmp_detalle_costo.costo = costo_temp_linea        # Costo Inventario

                tmp_detalle_costo.costo_reproceso = costo_act
                tmp_detalle_costo.cantidad_actual = cantidad_act
                # Cantidad Inventario * Costo Proceso
                tmp_detalle_costo.saldo_actual = redondeo(det_inv.cantidad*costo_act, 9)
                tmp_detalle_costo.save()


def UpdateAsientoInvProdudccionI_E(inventario_cabecera, tipo_comp, now, request):
    '''
        Función para reprocesar la contabilidad movimiento inventario
    '''
    id_cta_inventario = []
    lista_cta_inventario = []
    id_cta_costo_venta = []
    lista_cta_costo_venta = []

    cabecera_asiento = Cabecera_Comp_Contable.objects.get(status=1, numero_comprobante=inventario_cabecera.num_comp)
    detalles = Detalle_Comp_Contable.objects.filter(status=1, cabecera_contable=cabecera_asiento)
    detalles_inventario = Inventario_Detalle.objects.filter(status=1, inventario_cabecera=inventario_cabecera)

    # Borro las cuentas del asiento
    for d in detalles:
        d.delete()

    for obj in detalles_inventario:
        costo_x_item = float(obj.cantidad)*float(obj.item.costo)
        id_cta_compra = obj.item.categoria_item.plan_cuenta_compra.id

        if id_cta_compra not in id_cta_inventario:
            lista_cta_inventario.append([id_cta_compra, costo_x_item])
            id_cta_inventario.append(id_cta_compra)

        else:

            for obj1 in lista_cta_inventario:
                if obj1[0] == id_cta_compra:
                    obj1[1] += costo_x_item

        id_cta_venta = obj.item.categoria_item.plan_cuenta_costo_venta.id

        if id_cta_venta not in id_cta_costo_venta:
            lista_cta_costo_venta.append([id_cta_venta, costo_x_item])
            id_cta_costo_venta.append(id_cta_venta)

        else:

            for obj2 in lista_cta_costo_venta:
                if obj2[0] == id_cta_venta:
                    obj2[1] += costo_x_item

    for obj in lista_cta_inventario:
        detalle_asiento = Detalle_Comp_Contable()
        detalle_asiento.cabecera_contable = cabecera_asiento
        detalle_asiento.plan_cuenta = PlanCuenta.objects.get(id=obj[0])   # id plan cuenta comp inventario
        detalle_asiento.detalle = u"Requisión Contrato"

        if tipo_comp == 14:   # Ingreso
            detalle_asiento.dbcr = "D"
        else:
            detalle_asiento.dbcr = "H"

        detalle_asiento.fecha_asiento = cabecera_asiento.fecha
        detalle_asiento.valor = obj[1]  # Valor total de cuenta inventario

        detalle_asiento.fecha_creacion = now
        detalle_asiento.fecha_actualizacion = now
        detalle_asiento.usuario_creacion = request.user.username
        detalle_asiento.usuario_actualizacion = request.user.username
        detalle_asiento.save()

    for obj in lista_cta_costo_venta:
        detalle_asiento2 = Detalle_Comp_Contable()
        detalle_asiento2.cabecera_contable = cabecera_asiento
        detalle_asiento2.plan_cuenta = PlanCuenta.objects.get(id=obj[0])  # id plan cuenta costo venta

        if tipo_comp == 14:   # Ingreso
            detalle_asiento2.dbcr = "H"
        else:
            detalle_asiento2.dbcr = "D"

        detalle_asiento2.valor = obj[1]  # Valor total de cuenta costo venta
        detalle_asiento2.detalle = u"Requisión Contrato"
        detalle_asiento2.fecha_asiento = cabecera_asiento.fecha
        detalle_asiento2.fecha_creacion = now
        detalle_asiento2.fecha_actualizacion = now
        detalle_asiento2.usuario_creacion = request.user.username
        detalle_asiento2.save()

def UpdateAsientoInvVentaE(inventario_cabecera, now, request):
    '''
    Función para actualizar el asiento contable de inventario de venta
    :param inventario_cabecera:
    :param now:
    :param request:
    :return:
    '''
    id_cta_inventario = []
    lista_cta_inventario = []
    id_cta_costo_venta = []
    lista_cta_costo_venta = []

    cabecera_asiento = Cabecera_Comp_Contable.objects.get(status=1, numero_comprobante=inventario_cabecera.num_comp)
    detalles_contabilidad = Detalle_Comp_Contable.objects.filter(status=1, cabecera_contable=cabecera_asiento)
    detalles_inventario = Inventario_Detalle.objects.filter(status=1, inventario_cabecera=inventario_cabecera)

    # Borro las cuentas del asiento
    for d in detalles_contabilidad:
        d.delete()


    for obj in detalles_inventario:
        costo_x_item = redondeo(float(obj.cantidad)*float(obj.item.costo), 4)
        id_cta_compra = obj.item.categoria_item.plan_cuenta_compra.id

        if id_cta_compra not in id_cta_inventario:
            lista_cta_inventario.append([id_cta_compra, costo_x_item])
            id_cta_inventario.append(id_cta_compra)

        else:

            for obj1 in lista_cta_inventario:
                if obj1[0] == id_cta_compra:
                    obj1[1] += costo_x_item

        id_cta_venta = obj.item.categoria_item.plan_cuenta_costo_venta.id

        if id_cta_venta not in id_cta_costo_venta:
            lista_cta_costo_venta.append([id_cta_venta, costo_x_item])
            id_cta_costo_venta.append(id_cta_venta)
        else:

            for obj2 in lista_cta_costo_venta:
                if obj2[0] == id_cta_venta:
                    obj2[1] += costo_x_item

    for obj in lista_cta_inventario:
        detalle_asiento = Detalle_Comp_Contable()
        detalle_asiento.cabecera_contable = cabecera_asiento
        detalle_asiento.plan_cuenta = PlanCuenta.objects.get(id=obj[0])                 # id plan cuenta inventario
        detalle_asiento.detalle = u"Egreso de Inventario por Venta con # Factura: "+str(inventario_cabecera.num_doc)
        detalle_asiento.dbcr = "D"
        detalle_asiento.fecha_asiento = cabecera_asiento.fecha
        detalle_asiento.valor = obj[1]  # Valor total de cuenta inventario
        detalle_asiento.fecha_creacion = now
        detalle_asiento.usuario_creacion = request.user.username
        detalle_asiento.save()

    for obj in lista_cta_costo_venta:
        detalle_asiento2 = Detalle_Comp_Contable()
        detalle_asiento2.cabecera_contable = cabecera_asiento
        detalle_asiento2.plan_cuenta = PlanCuenta.objects.get(id=obj[0])              # id plan cuenta costo venta
        detalle_asiento2.dbcr = "H"
        detalle_asiento2.valor = obj[1]  # Valor total de cuenta costo venta
        detalle_asiento2.detalle = u"Egreso de Inventario por Venta con # Factura: "+str(inventario_cabecera.num_doc)
        detalle_asiento2.fecha_asiento = cabecera_asiento.fecha

        detalle_asiento2.fecha_creacion = now
        detalle_asiento2.usuario_creacion = request.user.username
        detalle_asiento2.save()

def UpdateAsientoInv_ACosto_ACantidad_CInterno_I_E(inventario_cabecera, tipo_comp, now, request):
    '''
        Función que permite generar los asientos contables de Inventario
        motivos: Ajuste Costo - Ajuste Cantidad - Consumo Interno
        tipo_comprobante: Ingreso - Egreso
        :param inventario_cabecera:
        :param tipo_comp:
        :param now:
        :param request:
        :return:
    '''
    cabecera_contable = Cabecera_Comp_Contable.objects.filter(status=1).get(numero_comprobante=inventario_cabecera.num_comp)
    detalle_contable = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_contable)
    detalles_inventario = Inventario_Detalle.objects.filter(status=1, inventario_cabecera=inventario_cabecera)

    # Borró las cuentas del asiento contable
    for d in detalle_contable:
        d.delete()


    for det_inv in detalles_inventario:
        # CONTRA_CUENTA
        detalle_asiento_contable = Detalle_Comp_Contable()
        detalle_asiento_contable.cabecera_contable = cabecera_contable
        detalle_asiento_contable.fecha_asiento = inventario_cabecera.fecha_reg
        detalle_asiento_contable.detalle = "Ajuste de inventario Costo Ingreso"
        detalle_asiento_contable.plan_cuenta = det_inv.plan_cuenta

        if det_inv.inventario_cabecera.tipo_mov_inventario.id == 4:                                 # Ajuste a la Cantidad
            detalle_asiento_contable.valor = redondeo(float(det_inv.cantidad*det_inv.costo), 4)
        elif det_inv.inventario_cabecera.tipo_mov_inventario.id == 5:                               # Ajuste al Costo
            detalle_asiento_contable.valor = redondeo(float(det_inv.costo), 4)
        elif det_inv.inventario_cabecera.tipo_mov_inventario.id == 8:                               # Consumo Interno
            detalle_asiento_contable.valor = redondeo(float(det_inv.cantidad*det_inv.costo), 4)

        if tipo_comp == 14:     # Ingreso
            detalle_asiento_contable.dbcr = "H"
        else:                   # Egreso
            detalle_asiento_contable.dbcr = "D"

        detalle_asiento_contable.fecha_creacion = now
        detalle_asiento_contable.usuario_creacion = request.user.username
        detalle_asiento_contable.save()

        # CUENTA_ITEM
        detalle_asiento_contable_item = Detalle_Comp_Contable()
        detalle_asiento_contable_item.cabecera_contable = cabecera_contable
        detalle_asiento_contable_item.fecha_asiento = inventario_cabecera.fecha_reg
        detalle_asiento_contable_item.detalle = "Ajuste de inventario Costo Ingreso"
        detalle_asiento_contable_item.plan_cuenta = det_inv.item.categoria_item.plan_cuenta_compra

        if det_inv.inventario_cabecera.tipo_mov_inventario.id == 4:       # Ajuste a la Cantidad
            detalle_asiento_contable_item.valor = redondeo(float(det_inv.cantidad*det_inv.costo), 4)
        elif det_inv.inventario_cabecera.tipo_mov_inventario.id == 5:     # Ajuste al Costo
            detalle_asiento_contable_item.valor = redondeo(float(det_inv.costo), 4)
        elif det_inv.inventario_cabecera.tipo_mov_inventario.id == 8:     # Consumo Interno
            detalle_asiento_contable_item.valor = redondeo(float(det_inv.cantidad*det_inv.costo), 4)

        if tipo_comp == 14:     # Ingreso
            detalle_asiento_contable_item.dbcr = "D"
        else:                   # Egreso
            detalle_asiento_contable_item.dbcr = "H"

        detalle_asiento_contable_item.fecha_creacion = now
        detalle_asiento_contable_item.usuario_creacion = request.user.username
        detalle_asiento_contable_item.save()

def UpdateAsientoCompraInventario_I_E(inventario_cabecera, tipo_comp, now, request):
    '''
    Función que genera el asiento de una Compra de Inventario
    :param inventario_cabecera:
    :param tipo_comp:
    :param now:
    :param request:
    :return:
    '''
    cuentas_item_ids = []       # Variable que guardara el id de cada plan de cuenta de los items
    detalle_asiento_list = []   # Variable que guardara la cuenta y el valor total de pago
    detalles_inv = Inventario_Detalle.objects.filter(status=1).filter(inventario_cabecera=inventario_cabecera)
    cabecera_comp = Cabecera_Comp_Contable.objects.get(status=1, numero_comprobante=inventario_cabecera.num_comp)
    detalles_comp = Detalle_Comp_Contable.objects.filter(status=1, cabecera_contable=cabecera_comp)
    total_inventario = 0.0
    costo = 0.0

    # Borró las cuentas del asiento
    for det in detalles_comp:
        det.delete()

    for d in detalles_inv:

        total_inventario += redondeo(float(d.costo)*float(d.cantidad), 4)

        if d.item.categoria_item.plan_cuenta_compra.id in cuentas_item_ids:  # Si ya se encuentra un id nuevo de item
            for obj in detalle_asiento_list:
                if obj[0] == d.item.categoria_item.plan_cuenta_compra.id:
                    obj[1] += costo

        else:
            cuentas_item_ids.append(d.item.categoria_item.plan_cuenta_compra.id)
            costo = redondeo(float(d.costo)*float(d.cantidad), 4)

            detalle_asiento_list.append([(d.item.categoria_item.plan_cuenta_compra, costo)])

    for obj in detalle_asiento_list:
        detalle_asiento_inventario = Detalle_Comp_Contable()
        detalle_asiento_inventario.cabecera_contable = cabecera_comp
        detalle_asiento_inventario.fecha_asiento = cabecera_comp.fecha

        if tipo_comp == 14:         # Ingreso
            detalle_asiento_inventario.dbcr = 'D'
            detalle_asiento_inventario.detalle = "Compras de inventario"
        else:                       # Egreso
            detalle_asiento_inventario.dbcr = 'H'
            detalle_asiento_inventario.detalle = "Compras de inventario"

        detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj[0].id)
        detalle_asiento_inventario.valor = obj[1]   # Costo total de la cta_inv
        detalle_asiento_inventario.fecha_creacion = now
        detalle_asiento_inventario.usuario_creacion = request.user.username
        detalle_asiento_inventario.save()

    cuenta = PlanCuenta.objects.get(tipo=TipoCuenta.objects.get(id=16))

    detalle_asiento = Detalle_Comp_Contable()
    detalle_asiento.cabecera_contable = cabecera_comp
    detalle_asiento.fecha_asiento = cabecera_comp.fecha

    if tipo_comp == 14:         # Ingreso
        detalle_asiento.dbcr = 'H'
        detalle_asiento.detalle = "Transitoria en compras de inventario"
    else:                       # Egreso
        detalle_asiento.dbcr = 'D'
        detalle_asiento.detalle = "Transitoria en compras de inventario"

    detalle_asiento.valor = total_inventario
    detalle_asiento.plan_cuenta = cuenta
    detalle_asiento.fecha_creacion = now
    detalle_asiento.usuario_creacion = request.user.username
    detalle_asiento.save()

def UpdateCuentaDetalleInv(inventario_cabecera, motivo, now, request):
    '''
    Función que actualiza con la contra cuenta del asiento en la columna de cuenta del
    detalle inventario los movimientos de inventario donde el usuario ingresa una cuenta para realizar
    la transacción
    :param inventario_cabecera:
    :param motivo:
    :param now:
    :param request:
    :return:
    '''
    array_id_cuenta_h = []
    arrays_id_cuenta_d = []
    detalles_inv = Inventario_Detalle.objects.filter(status=1).filter(inventario_cabecera=inventario_cabecera)
    cabecera_comp = Cabecera_Comp_Contable.objects.get(status=1, numero_comprobante=inventario_cabecera.num_comp)
    detalles_comp = Detalle_Comp_Contable.objects.filter(status=1, cabecera_contable=cabecera_comp)

    if motivo == 4 or motivo == 5 or motivo == 8:    # Tipo Movimiento = 4 (Ajuste - Cantidad) y
                                                     # Tipo Movimiento = 5 (Ajuste - Costo) y
                                                     # Tipo Movimiento = 8 (Consumo Interno)

        for det_cont in detalles_comp:
            if det_cont.dbcr == "H":
                if det_cont.plan_cuenta.id not in array_id_cuenta_h:
                    array_id_cuenta_h.append([det_cont.cabecera_contable.numero_comprobante, det_cont.plan_cuenta, det_cont.valor])
            else:
                if det_cont.plan_cuenta.id not in arrays_id_cuenta_d:
                    arrays_id_cuenta_d.append([det_cont.cabecera_contable.numero_comprobante, det_cont.plan_cuenta, det_cont.valor])

        for det_inv in detalles_inv:

            if det_inv.inventario_cabecera.tipo_comprobante.id == 14:   # Ingreso
                for a in array_id_cuenta_h:
                    if a[0] == det_inv.inventario_cabecera.num_comp:
                        if motivo == 5:
                            if redondeo(det_inv.costo, 4) == redondeo(a[2], 4):
                                det_inv.plan_cuenta = a[1]
                        else:
                            if redondeo(det_inv.cantidad*det_inv.costo, 4) == redondeo(a[2], 4):
                                det_inv.plan_cuenta = a[1]

            else:                               # Egreso

                for i in arrays_id_cuenta_d:
                    if i[0] == det_inv.inventario_cabecera.num_comp:
                        if motivo == 5:
                            if redondeo(det_inv.costo, 4) == redondeo(i[2], 4):
                                det_inv.plan_cuenta = i[1]
                        else:
                            if redondeo(det_inv.cantidad*det_inv.costo, 4) == redondeo(i[2], 4):
                                det_inv.plan_cuenta = i[1]

            det_inv.fecha_actualizacion = now
            det_inv.usuario_actualizacion = request.user.username
            det_inv.save()

    elif motivo == 1:   # Compra Inv.

        for det_inv in detalles_inv:
            det_inv.plan_cuenta = PlanCuenta.objects.get(tipo=TipoCuenta.objects.get(id=16))    # Compra Transitoria
            det_inv.fecha_actualizacion = now
            det_inv.usuario_actualizacion = request.user.username
            det_inv.save()

    else:

        for det_inv in detalles_inv:
            det_inv.plan_cuenta = None
            det_inv.fecha_actualizacion = now
            det_inv.usuario_actualizacion = request.user.username
            det_inv.save()


def reprocesar_inventario_contabilidad(anio_reversar, request):
    '''
    Función que permite reprocesar la contabilidad del inventario
    :param anio_reversar:
    :param request:
    :return:
    '''
    now = datetime.datetime.now()
    for obj in Inventario_Cabecera.objects.filter(status=1, fecha_reg__year=anio_reversar).order_by("fecha_reg", "num_comp"):
        motivo = obj.tipo_mov_inventario.id
        tipo_comp = obj.tipo_comprobante.id
        UpdateCuentaDetalleInv(obj, motivo, now, request)   # Función que actualiza la contra cuenta en los escenarios de Ajuste Cantidad, Costo
                                                            # y Consumo interno donde el usuario ingresa una cuenta en el detalle de inventario

        if motivo == 2:                                   # Asiento Contable Inventario por Venta
            UpdateAsientoInvVentaE(obj, now, request)


        ##################### Funciones para realizar los Asientos Contables ###########################################
        if motivo == 1:                                     # Asiento Contable Inventario por Compra Inventario
            UpdateAsientoCompraInventario_I_E(obj, tipo_comp, now, request)


        elif motivo == 3:                                   # Asiento Contable Inventario por Producción
            UpdateAsientoInvProdudccionI_E(obj, tipo_comp, now, request)

        elif motivo == 4 or motivo == 5 or motivo == 8:     # Asiento Contable Inventario por Ajuste a Cantidad - Ajuste al Costo
                                                            # Consumo Interno

            UpdateAsientoInv_ACosto_ACantidad_CInterno_I_E(obj, tipo_comp, now, request)








def reprocesarCostoInventarioItem(item, num_comp, request):

    lista_items_vta = []

    venta_cabecera = Venta_Cabecera.objects.filter(status=1).get(num_comp=num_comp)
    num_doc_vta = venta_cabecera.vigencia_doc_empresa.serie+"-"+venta_cabecera.num_documento

    for detalle_vta in Venta_Detalle.objects.filter(status=1, venta_cabecera=venta_cabecera):
        item_vta = Item_Cant_Venta()
        item_vta.item = detalle_vta.item
        item_vta.cantidad = detalle_vta.cant_entregada
        lista_items_vta.append(item_vta)

    if Inventario_Cabecera.objects.filter(num_doc=num_doc_vta).exists():
        print "existe"
        lista_detalles = Inventario_Detalle.objects.filter(status=1).\
            filter(inventario_cabecera__tipo_comprobante__id=15, item=item, inventario_cabecera__tipo_mov_inventario__id=2).order_by("inventario_cabecera__fecha_reg")

        for d in lista_detalles:
            if d.cantidad == 0.0 and d.costo == 0.0:
                if len(lista_items_vta) > 0.0:
                    for obj in lista_items_vta:
                        if obj.item.id == d.item_id:
                            d.cantidad = obj.cantidad
                            d.costo = d.item.costo
                            d.usuario_actualizacion = request.user.username
                            d.fecha_actualizacion = datetime.datetime.now()
                            d.save()
