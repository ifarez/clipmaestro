#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from contabilidad.formularios.AsientoContableForm import *
from reportes.formularios.reportes_form import *
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
from django.db.models import Q, Count, Min, Sum, Avg
import ho.pisa as pisa
import cStringIO as StringIO
import cgi
from django.contrib.auth.models import User
from librerias.funciones.validacion_formularios import *
from django.template.loader import render_to_string
import datetime
import time
from django.http import HttpResponse
from django import template
from django.core.paginator import *
from librerias.funciones.paginacion import *
import operator
import calendar
from contabilidad.funciones.contabilidad_func import *
from librerias.funciones.funciones_vistas import *
from django.db import connection


register = template.Library()

@login_required(login_url='/')
@transaction.commit_on_success
def lista_asientos(request):
    buscador = FormBuscadoresAsientoContable(request.GET)
    bandera = 1
    # Función Utilizada para Contabilidad y Libro Diario
    cabeceras = busqueda_contabilidad(buscador, 1)
    contadorasientos = len(cabeceras)
    paginacion = Paginator(cabeceras, get_num_filas_template())
    numero_pagina = request.GET.get("page")
    try:
        lista_cabecera = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista_cabecera = paginacion.page(1)
    except EmptyPage:
        lista_cabecera = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = lista_cabecera.number
    lista = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('asiento_contable/lista_asiento_contable.html', {"buscador": buscador,
                                                    "cabecera": lista_cabecera,
                                                    "request": request,
                                                    "total_paginas": total_paginas,
                                                    "bandera": bandera,
                                                    "contadorasientos": contadorasientos,
                                                    "arreglo_paginado": lista,
                                                    "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                                                    "numero": numero}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def registrar_asiento_contable(request):
    dia_actual = datetime.datetime.now().strftime('%Y-%m-%d')
    now = datetime.datetime.now()
    debe = 0.0
    haber = 0.0
    tipo = 1
    detalleform = formset_factory(FormDetalleComprobanteAsiento, extra=2)
    formset = detalleform
    tipo_c = TipoComprobante.objects.filter(status=1).get(id=6)

    if Seq_Comp_Contable.objects.filter(anio=now.year, mes=now.strftime("%m"), tipo_comp=tipo_c).exists():
        secuencia_t_comp = Seq_Comp_Contable.objects.get(anio=now.year, mes=now.strftime("%m"), tipo_comp=tipo_c)
        secuencia = secuencia_t_comp.secuencia
        fecha_actual = secuencia_t_comp.fecha_actual
    else:
        secuencia = 0
        fecha_actual = now.date()

    formulario = CabeceraAsientoForm(initial={"utlimo_asiento": secuencia, "fecha": fecha_actual})

    try:
        if request.method == "POST":
            formulario = CabeceraAsientoForm(request.POST)
            formset = detalleform(request.POST)
            if formulario.is_valid():

                cabecera_cont = Cabecera_Comp_Contable()
                contador = GuardarCabeceraAsientoContable(formulario, cabecera_cont, tipo, dia_actual, request)
                if contador == 0:     # Contador Validador
                    if formset.is_valid():
                        contador += GuardarDetalleAsientoContable(formset, cabecera_cont, dia_actual, request, tipo)
                    else:
                        contador += 1
                        messages.error(request, u"Error llene correctamente la información del detalle del asiento")

                if contador > 0:
                    transaction.rollback()
                else:
                    messages.success(request, u"Se registró exitósamente el asiento contable"
                                          u" con número de comprobante: "+str(cabecera_cont.numero_comprobante))
                    return HttpResponseRedirect(redireccionar_contabilidad(request, cabecera_cont))

            else:
                transaction.rollback()
                messages.error(request, u"Error al registrar la información del asiento contable")

    except ErrorContabilidad, e:
        transaction.rollback()
        messages.error(request, unicode(e.valor))

    return render_to_response("asiento_contable/agregar_asiento_contable.html",
                              {"formulario": formulario, "formulario_detalle": formset,
                               "total_debe": debe, "total_haber": haber, "request": request,
                               "tipo": tipo, "dia": str(dia_actual)},  context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def detalle_asiento_contable(request, id):
    total_debe = 0.00
    total_haber = 0.00
    try:
        cabecera = Cabecera_Comp_Contable.objects.get(id=id)

        if cabecera.status == 1:
            detalles_debe = Detalle_Comp_Contable.objects.filter(status=1).filter(cabecera_contable=cabecera.id, dbcr="D").order_by("plan_cuenta__codigo")
            detalles_haber = Detalle_Comp_Contable.objects.filter(status=1).filter(cabecera_contable=cabecera.id, dbcr="H").order_by("plan_cuenta__codigo")
            for obj in detalles_debe:
                total_debe += float(obj.valor)
            for obj in detalles_haber:
                total_haber += float(obj.valor)
        else:

            detalles_debe = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera.id, dbcr="D").order_by("plan_cuenta__codigo")
            detalles_haber = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera.id, dbcr="H").order_by("plan_cuenta__codigo")
            for obj in detalles_debe:
                total_debe += float(obj.valor)
            for obj in detalles_haber:
                total_haber += float(obj.valor)
    except:
        messages.error(request, u"Error al cargar el comprobante")
        transaction.rollback()
        return HttpResponseRedirect(reverse("lista_asientos"))
    return render_to_response('asiento_contable/detalle_asiento_contable.html',
                              {"total_debe": total_debe, "total_haber": total_haber,
                               "objeto_cabecera": cabecera,
                               "objeto_detalle_debe": detalles_debe,
                               "objeto_detalle_haber": detalles_haber},
                              context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def anular_generar_asiento_contable(request, id):
    tipo = 2
    now = datetime.datetime.now()
    tipo_c = TipoComprobante.objects.filter(status=1).get(id=6)
    ###########################################
    #          Anula el comprobante           #
    ###########################################
    try:
        cabecera_comprobante = Cabecera_Comp_Contable.objects.get(id=id)
        detalle_comprobante = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_comprobante)
        cabecera_comprobante.status = 2
        cabecera_comprobante.save()

        for obj in detalle_comprobante:
            obj.status = 2
            obj.save()
    except:
        transaction.rollback()
        messages.error(request, u"Existió un problema al anular el comprobante")
        return HttpResponseRedirect(reverse("lista_asientos"))


    if Seq_Comp_Contable.objects.filter(anio=now.year, mes=now.strftime("%m"), tipo_comp=tipo_c).exists():
        secuencia_t_comp = Seq_Comp_Contable.objects.get(anio=now.year, mes=now.strftime("%m"), tipo_comp=tipo_c)
        fecha_actual = secuencia_t_comp.fecha_actual
    else:
        fecha_actual = now.date()

    formulario = CabeceraAsientoForm(initial={"fecha": fecha_actual,
                                              "concepto": cabecera_comprobante.concepto_comprobante})

    multiform = formset_factory(FormDetalleComprobanteAsiento, extra=0)
    formulario_detalle = multiform(initial=instancia_detalle_formset(cabecera_comprobante))

    if request.method == "POST":
        formulario = CabeceraAsientoForm(request.POST)
        formulario_detalle = multiform(request.POST, request.FILES)

    try:
        if formulario.is_valid():
            cabecera_cont = Cabecera_Comp_Contable()
            contador = GuardarCabeceraAsientoContable(formulario, cabecera_cont, tipo, now,  request)

            if contador == 0:
                if formulario_detalle.is_valid():
                    contador += GuardarDetalleAsientoContable(formulario_detalle, cabecera_cont, now, request, tipo)
                else:
                    contador += 1
                    messages.error(request, u"Error llene correctamente la información del detalle del "
                                            u"asiento contable")

            if contador > 0:
                transaction.rollback()
            else:
                messages.success(request, u"El Asiento Contable se ha anulado y generado con éxito")
                return HttpResponseRedirect(redireccionar_contabilidad(request, cabecera_cont))
    except:
        transaction.rollback()
        messages.error(request, u"Error al realizar la transacción!")

    return render_to_response("asiento_contable/agregar_asiento_contable.html",
                            {"formulario": formulario, "formulario_detalle": formulario_detalle,
                            "tipo": tipo, "id": id, "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def copia_asiento(request, id):
    tipo = 3
    now = datetime.datetime.now()
    tipo_c = TipoComprobante.objects.get(id=6)

    if Seq_Comp_Contable.objects.filter(anio=now.year, mes=now.strftime("%m"), tipo_comp=tipo_c).exists():
        secuencia_t_comp = Seq_Comp_Contable.objects.get(anio=now.year, mes=now.strftime("%m"), tipo_comp=tipo_c)
        fecha_actual = secuencia_t_comp.fecha_actual
    else:
        fecha_actual = now.date()

    try:
        cabecera_comprobante = Cabecera_Comp_Contable.objects.get(id=id)
        formulario = CabeceraAsientoForm(initial={"fecha": fecha_actual,
                                                  "concepto": cabecera_comprobante.concepto_comprobante})

        multiform = formset_factory(FormDetalleComprobanteAsiento, extra=0)
        formulario_detalle = multiform(initial=instancia_detalle_formset(cabecera_comprobante))
    except:
        messages.error(request, u"Existió un problema al cargar el comprobante contable")
        return HttpResponseRedirect(reverse("lista_asientos"))

    try:
        if request.method == "POST":
            formulario = CabeceraAsientoForm(request.POST)
            formulario_detalle = multiform(request.POST, request.FILES)

            if formulario.is_valid():
                cabecera_cont = Cabecera_Comp_Contable()

                contador = GuardarCabeceraAsientoContable(formulario, cabecera_cont, tipo, now, request)

                if contador == 0:
                    if formulario_detalle.is_valid():
                        contador += GuardarDetalleAsientoContable(formulario_detalle, cabecera_cont, now, request, tipo)
                    else:
                        contador += 1
                        messages.error(request, u"Error llene correctamente el detalle del asiento contable")

                if contador > 0:
                    transaction.rollback()
                else:
                    messages.success(request, u"Se registró exitósamente el asiento contable "
                                              u"con número de comprobante: "+str(cabecera_cont.numero_comprobante)+u"")
                    return HttpResponseRedirect(redireccionar_contabilidad(request, cabecera_cont))

            else:
                messages.error(request, u"Error llene correctamente el formulario")
                transaction.rollback()
    except:
        transaction.rollback()
        messages.error(request, u"Error al realizar la transaccion!")
    else:
        transaction.commit()

    return render_to_response("asiento_contable/agregar_asiento_contable.html",
                            {"formulario": formulario, "formulario_detalle": formulario_detalle,
                            "tipo": tipo, "id": id, "request": request}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def modificar_asiento(request, id):
    tipo = 4
    param = request.GET.get("param", "")
    contador = 0
    now = datetime.datetime.now()
    try:
        cabecera_comprobante = Cabecera_Comp_Contable.objects.filter(status=1).get(id=id)
        detalle_comprobante = Detalle_Comp_Contable.objects.filter(status=1).filter(cabecera_contable=cabecera_comprobante)
        formulario = CabeceraAsientoForm(initial={"fecha": cabecera_comprobante.fecha.strftime('%Y-%m-%d'),
                                                  "concepto": cabecera_comprobante.concepto_comprobante})

        if cabecera_comprobante.tipo_comprobante.id == 6:
            multiform = formset_factory(FormDetalleComprobanteAsiento, extra=0)
        else:
            multiform = formset_factory(FormDetalleComprobanteAsientoInicial, extra=0)

        formulario_detalle = multiform(initial=instancia_detalle_formset(cabecera_comprobante))
    except:
        messages.error(request, u"Error al obtener la información del comprobante contable.")
        return HttpResponseRedirect(reverse("lista_asientos"))

    try:

        if request.method == "POST":
            formulario = CabeceraAsientoForm(request.POST)
            formulario_detalle = multiform(request.POST, request.FILES)

            if formulario.is_valid():
                cabecera_comprobante.concepto_comprobante = formulario.getConceptoComprobante()
                cabecera_comprobante.fecha_actualizacion = now
                cabecera_comprobante.usuario_actualizacion = request.user.username
                cabecera_comprobante.save()

                for d in detalle_comprobante:
                    d.status = -1
                    d.usuario_actualizacion = request.user.username
                    d.fecha_actualizacion = now
                    d.save()

                    if cabecera_comprobante.id == d.cabecera_contable.id and d.status == -1:
                        d.delete()

                if formulario_detalle.is_valid():
                    contador += GuardarDetalleAsientoContable(formulario_detalle, cabecera_comprobante, now, request, tipo)

                    if contador == 0:
                        messages.success(request, u"Se editó exitosamente el asiento contable con # Comp.: "+
                                                  unicode(cabecera_comprobante.numero_comprobante))

                        if param == "1":
                            return HttpResponseRedirect(reverse("libro_diario"))
                        else:
                            return HttpResponseRedirect(redireccionar_contabilidad(request, cabecera_comprobante))

                else:
                    contador += 1
                    messages.error(request, u"Error en los valores del debe y el haber, verifique que sean "
                            u"iguales y mayores que 0.0 para continuar")

            else:
                contador += 1
                messages.error(request, u"Error al ingresar la información de la cabecera del asiento contable")

            if contador > 0:
                transaction.rollback()

    except ErrorContabilidad, e:
        transaction.rollback()
        messages.error(request, e)

    return render_to_response("asiento_contable/agregar_asiento_contable.html",
                              {"formulario": formulario, "formulario_detalle": formulario_detalle, "param": param,
                              "tipo": tipo, "id": id,  "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def anular_asiento_contable(request, id):
    param = request.GET.get("param", "")
    now = datetime.datetime.now()
    cabecera_comprobante = Cabecera_Comp_Contable.objects.get(id=id)
    contador = Anular_Asiento_Contable(cabecera_comprobante, now, request)

    if contador == 0:
        messages.success(request, u"El comprobante contable : "+str(cabecera_comprobante.numero_comprobante)+u" se ha anulado con éxito")
    else:
        transaction.rollback()
        messages.error(request, u"Existió un problema al anular el comprobante")

    if param == "1":
        return HttpResponseRedirect(reverse("libro_diario"))
    else:
        return HttpResponseRedirect(reverse("lista_asientos"))

@login_required(login_url='/')
def generar_asiento_pdf(request, id):
    total_debe = 0.00
    total_haber = 0.00
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    try:
        try:
            cabecera = Cabecera_Comp_Contable.objects.get(id=id)

            if cabecera.status == 1:
                detalles_debe = Detalle_Comp_Contable.objects.filter(status=1).filter(cabecera_contable=cabecera.id, dbcr="D").order_by("plan_cuenta__codigo")
                detalles_haber = Detalle_Comp_Contable.objects.filter(status=1).filter(cabecera_contable=cabecera.id, dbcr="H").order_by("plan_cuenta__codigo")
                for obj in detalles_debe:
                    total_debe += float(obj.valor)
                for obj in detalles_haber:
                    total_haber += float(obj.valor)
            else:
                detalles_debe = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera.id, dbcr="D").order_by("plan_cuenta__codigo")
                detalles_haber = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera.id, dbcr="H").order_by("plan_cuenta__codigo")
                for obj in detalles_debe:
                    total_debe += float(obj.valor)
                for obj in detalles_haber:
                    total_haber += float(obj.valor)

            html = render_to_string('asiento_contable/detalle_asiento_contable_pdf.html', {'pagesize': 'A4',
                                    "empresa": empresa,
                                    "usuario": usuario,
                                    "fecha_actual": datetime.datetime.now(),
                                    "request": request,
                                    "total_debe": total_debe,
                                    "total_haber": total_haber,
                                    "objeto_cabecera": cabecera,
                                    "objeto_detalle_debe": detalles_debe,
                                    "objeto_detalle_haber": detalles_haber}, context_instance=RequestContext(request))

            return generar_pdf_get(html)

        except Cabecera_Comp_Contable.DoesNotExist:
            return Http404

    except:
        messages.error(request, u"Error no encuentra el recurso")
        return HttpResponseRedirect("lista_asientos")

def agregar_asiento_inicial(request):
    '''
    Vista que realiza el asiento de carga inicial
    :param request:
    :return:
    '''
    dia_actual = datetime.datetime.now().strftime('%Y-%m-%d')
    debe = 0.0
    haber = 0.0
    tipo = 5                                    # Para reutilizar el mismo html de agregar asiento pero en asiento ini
    detalleform = formset_factory(FormDetalleComprobanteAsientoInicial, extra=2)
    formset = detalleform
    fecha_inicio_sistema = get_parametros_empresa().fecha_inicio_sistema - datetime.timedelta(days=1)
    formulario = CabeceraAsientoForm(initial={"fecha": fecha_inicio_sistema.strftime("%Y-%m-%d")})

    try:
        if request.method == "POST":
            formulario = CabeceraAsientoForm(request.POST)
            formset = detalleform(request.POST)
            if formulario.is_valid():
                cabecera_cont = Cabecera_Comp_Contable()
                contador = GuardarCabeceraAsientoContable(formulario, cabecera_cont, tipo, dia_actual, request)
                if contador == 0:       # Contador Validador
                    if formset.is_valid():
                        contador += GuardarDetalleAsientoContable(formset, cabecera_cont, dia_actual, request, tipo)
                    else:
                        contador += 1
                        messages.error(request, u"Error llene correctamente la información del detalle del asiento")
                if contador > 0:
                    transaction.rollback()
                else:
                    messages.success(request, u"Se registró exitósamente el asiento contable"
                                          u" con número de comprobante: "+str(cabecera_cont.numero_comprobante))
                    return HttpResponseRedirect(redireccionar_contabilidad(request, cabecera_cont))
            else:
                formulario = CabeceraAsientoForm(initial={"fecha": fecha_inicio_sistema.strftime("%Y-%m-%d")})
                messages.error(request, u"Error al registrar la información del asiento contable")

    except ErrorContabilidad, e:
        transaction.rollback()
        messages.error(request, unicode(e.valor))

    return render_to_response("asiento_contable/agregar_asiento_contable.html",
                              {"formulario": formulario, "formulario_detalle": formset,
                               "total_debe": debe, "total_haber": haber, "request": request,
                               "tipo": tipo, "dia": str(dia_actual)},  context_instance=RequestContext(request))



@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def cierre_anio_contable(request):
    '''
    Vista que realiza
    :param request:
    :return:
    '''
    formulario = BuscadorReportes(initial={"anio": get_parametros_empresa().anio_cierre,
                                            "mes": get_mes(get_parametros_empresa().mes_cierre)})

    if request.method == "POST":
        pass

    return render_to_response("asiento_contable/cierre_contable.html",
                              {"buscador": formulario},  context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def cierre_mes_contable(request):
    '''
    Vista que realiza el proceso de cierre contable mensual
    :param request:
    :return:
    '''
    parametro_empresa = get_parametros_empresa()
    contador = 0

    if parametro_empresa.anio_cierre is not None and parametro_empresa.mes_cierre is not None:
        formulario = BuscadorReportes(initial={"anio": parametro_empresa.anio_cierre,
                                                "mes": get_mes(parametro_empresa.mes_cierre + 1)})
    else:
        fecha_inicio_sistema = parametro_empresa.fecha_inicio_sistema
        anio_inicio = fecha_inicio_sistema.year
        mes_inicio = fecha_inicio_sistema.month
        formulario = BuscadorReportes(initial={"anio": anio_inicio, "mes": get_mes(mes_inicio)})

    if request.method == "POST":
        parametro_empresa.anio_cierre = datetime.datetime.now().year

        fecha_cierre_tmp = datetime.date(parametro_empresa.anio_cierre, parametro_empresa.mes_cierre,
                                                       calendar.mdays[parametro_empresa.mes_cierre])

        if fecha_cierre_tmp > datetime.datetime.now().date():
            contador += 1
            messages.error(request, u"Error al realizar el cierre mensual la fecha de cierre no puede ser mayor a "
                                    u"la fecha actual por favor verifique el proceso "
                                    u"ó comuníquese con el administrador del sistema")

        if contador == 0:
            if parametro_empresa.mes_cierre is not None:
                parametro_empresa.mes_cierre += 1
            else:
                parametro_empresa.mes_cierre = parametro_empresa.fecha_inicio_sistema.month

            parametro_empresa.fecha_cierre = datetime.date(parametro_empresa.anio_cierre, parametro_empresa.mes_cierre,
                                                           calendar.mdays[parametro_empresa.mes_cierre])

            parametro_empresa.usuario_actualizacion = request.user.username
            parametro_empresa.fecha_actualizacion = datetime.datetime.now()
            parametro_empresa.save()

            messages.success(request, u"El proceso de cierre mensual se ha realizado con éxito")
            return HttpResponseRedirect(reverse("lista_asientos"))

        else:
            transaction.rollback()

    return render_to_response("asiento_contable/cierre_contable_mensual.html",
                              {"buscador": formulario},  context_instance=RequestContext(request))