#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'

from django.contrib.auth.decorators import login_required
from django.core.paginator import *
from administracion.models import *
from librerias.funciones.permisos import *
from contabilidad.models import *
import json
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from contabilidad.formularios.ComprasForm import FormDetalleCompCompra, DetalleRetencionForm
from django.forms.formsets import formset_factory
from django.template.loader import render_to_string
from librerias.funciones.funciones_vistas import get_fila_t_detalle_compra, \
    es_reembolso_gasto, tiene_doc_modifica, es_nota_debito, es_nota_credito, get_fila_formulario_formset
from contabilidad.formularios.InventarioForm import *

@login_required(login_url='/')
def buscar_tipo_documento(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    p = q.replace(".", "")
    q = p.replace("_", "")
    if pkey_val is not None and pkey_val != "":
        try:
            p = Documento.objects.get(id=pkey_val)
            resultado = json.dumps({"name": p.codigo_documento + " - " + p.descripcion_documento, "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except Documento.DoesNotExist:
            respuesta = []
    else:
        lista = Documento.objects.filter(status=1).filter(Q(descripcion_documento__icontains=q)
                                                            |
                                                            Q(codigo_documento__icontains=q)).order_by("codigo_documento").only("codigo_documento",
                                                                                                   "descripcion_documento")
        count = len(lista)

        paginacion = Paginator(lista, per_page)
        try:
            lista = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            lista = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            lista = paginacion.page(paginacion.num_pages)

        for p in lista:
            respuesta.append({"name": p.codigo_documento + " - " + p.descripcion_documento, "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


def es_nota_credito_view(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    q = request.GET.get("id", "")

    if q not in (None, ""):
        status = es_nota_credito(q)
        resultado = json.dumps({"status": status})
    else:
        resultado = json.dumps({"status": False})

    return HttpResponse(resultado, mimetype='application/json')


def tiene_doc_modifica_view(request):
    """
    Funcion que retorna True/False si es que es nota de credito
    o nota de debito
    :param request:
    :return:
    """
    q = request.GET.get("id", "")

    if q not in (None, ""):
        status =  tiene_doc_modifica(q)
        resultado = json.dumps({"status": status})
    else:
        resultado = json.dumps({"status": False})

    return HttpResponse(resultado, mimetype='application/json')


def es_nota_debito_view(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    q = request.GET.get("id", "")

    if q not in (None, ""):
        status = es_nota_debito(q)
        resultado = json.dumps({"status": status})
    else:
        resultado = json.dumps({"status": False})

    return HttpResponse(resultado, mimetype='application/json')


def es_reembolso_gasto_view(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    q = request.GET.get("id", "")

    if q not in (None, ""):
        status = es_reembolso_gasto(q)
        resultado = json.dumps({"status": status})
    else:
        resultado = json.dumps({"status": False})

    return HttpResponse(resultado, mimetype='application/json')


def es_compra_exterior(request):
    """
    Funcion que retorna True/False si es que es compra Exterior
    :param request:
    :return:
    """
    q = request.GET.get("id", "")

    if q not in (None, ""):
        try:
            tipo_pago = Tipo_Pago_SRI.objects.get(id=q)
            if tipo_pago.codigo == '2':  # Compra Exterior
                resultado = json.dumps({"status": True})
            elif tipo_pago.codigo == '1':  # Compra local
                resultado = json.dumps({"status": False})
            else:
                resultado = json.dumps({"status": False})

        except (Tipo_Pago_SRI.DoesNotExist, ValueError):
            resultado = json.dumps({"status": False})

    else:
        resultado = json.dumps({"status": False})

    return HttpResponse(resultado, mimetype='application/json')


def es_pago_credito(request):
    """
    Funcion que retorna True/False si es que es compra Exterior
    :param request:
    :return:
    """
    q = request.GET.get("id", "")

    if q not in (None, ""):
        try:
            tipo_pago = TipoPago.objects.get(id=q)
            if tipo_pago.id == 2:  # Compra Contado
                resultado = json.dumps({"status": True})
            elif tipo_pago.id == 1:  # Compra Credito
                resultado = json.dumps({"status": False})
            else:
                resultado = json.dumps({"status": False})

        except (TipoPago.DoesNotExist, ValueError):
            resultado = json.dumps({"status": False})

    else:
        resultado = json.dumps({"status": False})

    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
def buscar_origen_pago_sri(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    if pkey_val is not None and pkey_val != "":
        try:
            p = Tipo_Pago_SRI.objects.exclude(status=0).get(id=pkey_val)
            resultado = json.dumps({"name": p.descripcion, "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except Documento.DoesNotExist:
            respuesta = []
    else:
        lista = Tipo_Pago_SRI.objects.filter(status=1).filter(
            descripcion__icontains=q).order_by("descripcion").only("descripcion")
        count = len(lista)

        paginacion = Paginator(lista, per_page)

        try:
            lista = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            lista = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            lista = paginacion.page(paginacion.num_pages)

        for p in lista:
            respuesta.append({"name": p.descripcion, "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
def buscar_forma_pago_sri(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    if pkey_val is not None and pkey_val != "":
        try:
            p = TipoPago.objects.exclude(status=0).get(id=pkey_val)
            resultado = json.dumps({"name": p.descripcion, "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except Documento.DoesNotExist:
            respuesta = []
    else:
        lista = TipoPago.objects.filter(status=1).filter(
            descripcion__icontains=q).order_by("descripcion").only("descripcion")
        count = len(lista)

        paginacion = Paginator(lista, per_page)

        try:
            lista = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            lista = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            lista = paginacion.page(paginacion.num_pages)

        for p in lista:
            respuesta.append({"name": p.descripcion, "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
def buscar_contratos(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    if pkey_val is not None and pkey_val != "":
        try:
            p = ContratoCabecera.objects.exclude(status=0).get(id=pkey_val)
            resultado = json.dumps({"name": str(p.num_cont) + " - " + p.cliente.razon_social[0:30] +
                                    "(" + p.cliente.ruc + ")", "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except (ContratoCabecera.DoesNotExist, ValueError):
            respuesta = []
    else:
        lista = ContratoCabecera.objects.filter(status=1).filter(
            Q(num_cont__icontains=q) |
            Q(cliente__ruc__icontains=q) |
            Q(cliente__razon_social__icontains=q)).order_by("num_cont")

        count = len(lista)

        paginacion = Paginator(lista, per_page)

        try:
            lista = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            lista = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            lista = paginacion.page(paginacion.num_pages)

        for p in lista:
            respuesta.append({"name": str(p.num_cont) + " - " + p.cliente.razon_social[0:30] +
                              "(" + p.cliente.ruc + ")", "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
def buscar_paises(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    if pkey_val is not None and pkey_val != "":
        try:
            p = Pais.objects.exclude(status=0).get(id=pkey_val)
            resultado = json.dumps({"name": p.descripcion, "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except (Pais.DoesNotExist, ValueError):
            respuesta = []
    else:
        lista = Pais.objects.filter(status=1).filter(descripcion__icontains=q).order_by("descripcion")

        count = len(lista)

        paginacion = Paginator(lista, per_page)

        try:
            lista = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            lista = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            lista = paginacion.page(paginacion.num_pages)

        for p in lista:
            respuesta.append({"name": p.descripcion, "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
def buscar_block_retenciones(request):
    """
    Funcion que me busca los blocks de retenciones
    de la empresa(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    fecha_asiento = request.GET.get("fecha_asiento", None)

    count = 0

    if pkey_val is not None and pkey_val != "":
        try:
            p = VigenciaDocEmpresa.objects.exclude(status=0).get(id=pkey_val)
            resultado = json.dumps({"name": p.serie, "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except (VigenciaDocEmpresa.DoesNotExist, ValueError):
            respuesta = []
    else:
        lista = VigenciaDocEmpresa.objects.filter(status=1).filter(serie__icontains=q,
                                                                   documento_sri__id=24,
                                                                   fecha_vencimiento__gte=fecha_asiento,
                                                                   fecha_emi__lte=fecha_asiento).order_by("serie")

        count = len(lista)

        paginacion = Paginator(lista, per_page)

        try:
            lista = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            lista = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            lista = paginacion.page(paginacion.num_pages)

        for p in lista:
            respuesta.append({"name": p.serie, "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
def buscar_cuentas_retencion(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    if pkey_val is not None and pkey_val != "":
        try:
            p = Cuenta_Retencion.objects.exclude(status=0).get(id=pkey_val)
            resultado = json.dumps({"name": p.plan_cuenta.descripcion, "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except (Cuenta_Retencion.DoesNotExist, ValueError):
            respuesta = []
    else:

        lista = Cuenta_Retencion.objects.filter(status=1).filter(Q(plan_cuenta__descripcion__icontains=q)).order_by("plan_cuenta__descripcion")

        count = len(lista)

        paginacion = Paginator(lista, per_page)

        try:
            lista = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            lista = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            lista = paginacion.page(paginacion.num_pages)

        for p in lista:
            respuesta.append({"name": p.plan_cuenta.descripcion, "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def calcular_totales_detalle_compra(request):
    formsetDetalle_Comp = formset_factory(FormDetalleCompCompra)
    detalleform = formsetDetalle_Comp(request.POST, request.FILES, prefix="detalle_compra")

    compra_0 = 0
    compra_iva = 0
    total_iva = 0
    total_ret = 0
    detalleform.is_valid()

    for obj in detalleform:
        fields = obj.cleaned_data
        valor = redondeo(fields.get("valor")) if fields.get("valor", None) is not None else 0
        cod_ret_fte_id = fields.get("fte")
        cod_ret_iva_id = fields.get("iva")
        cod_iva_id = fields.get("valor_iva")
        porc_ret_fte = fields.get("porc_ret_fte", None)
        porc_ret_iva = fields.get("porc_ret_iva", None)

        if valor > 0:
            try:
                cod_sri_ret_fte = Codigo_SRI.objects.get(id=cod_ret_fte_id)
                porcentaje = cod_sri_ret_fte.porcentaje if not cod_sri_ret_fte.porc_variable else porc_ret_fte
                if porcentaje is not None and porcentaje > 0:
                    total_ret += (porcentaje * valor / 100)
            except (Codigo_SRI.DoesNotExist, ValueError):
                pass

            try:
                cod_sri_ret_iva = Codigo_SRI.objects.get(id=cod_ret_iva_id)
                codigo_sri_iva = Codigo_SRI.objects.get(id=cod_iva_id)
                porcentaje = cod_sri_ret_iva.porcentaje if cod_sri_ret_iva.porc_variable else porc_ret_iva

                if porcentaje is not None and porcentaje > 0:
                    total_ret += (porcentaje * valor * codigo_sri_iva.porcentaje / 10000)
            except (Codigo_SRI.DoesNotExist, ValueError):
                pass
            try:
                codigo_sri_iva = Codigo_SRI.objects.get(id=cod_iva_id)
                if codigo_sri_iva.porcentaje is not None and codigo_sri_iva.porcentaje > 0:
                    compra_iva += valor
                    total_iva += (valor * codigo_sri_iva.porcentaje / 100)
                else:
                    compra_0 += valor
            except (Codigo_SRI.DoesNotExist, ValueError):
                pass
        else:
            pass
    total_pagar = compra_0 + compra_iva + total_iva - total_ret

    resultado = json.dumps({"compra_0": compra_0, "compra_iva": compra_iva,
                            "total_iva": total_iva, "total_retenciones": total_ret,
                            "total_pagar": total_pagar})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def calcular_totales_detalle_retencion(request):
    detalle_ret_formset = formset_factory(DetalleRetencionForm, extra=0)

    formsetDetalle_Comp = formset_factory(FormDetalleCompCompra)
    detalleform = formsetDetalle_Comp(request.POST, request.FILES, prefix="detalle_compra")

    lista_ret_iva = []
    ids_lista_ret_iva = []
    lista_ret_fte = []
    ids_lista_ret_fte = []
    detalleform.is_valid()

    for obj in detalleform:
        fields = obj.cleaned_data
        valor = redondeo(fields.get("valor")) if fields.get("valor", None) is not None else 0
        cod_ret_fte_id = fields.get("fte")
        cod_ret_iva_id = fields.get("iva")
        cod_iva_id = fields.get("valor_iva")
        porc_ret_iva = fields.get("porc_ret_iva")
        porc_ret_fte = fields.get("porc_ret_fte")

        if valor > 0:
            try:
                cod_sri_ret_fte = Codigo_SRI.objects.get(id=cod_ret_fte_id)

                porcentaje = cod_sri_ret_fte.porcentaje if not cod_sri_ret_fte.porc_variable else porc_ret_fte

                if porcentaje is not None and porcentaje > 0:
                    valor_ret = (porcentaje * valor / 100)

                    if cod_sri_ret_fte.id not in ids_lista_ret_fte:
                        ids_lista_ret_fte.append(cod_sri_ret_fte.id)
                        lista_ret_fte.append([cod_sri_ret_fte, valor_ret, valor, porcentaje])
                    else:
                        for obj_ret in lista_ret_fte:
                            if obj_ret[0].id == cod_sri_ret_fte.id:
                                obj_ret[1] += valor_ret
                                obj_ret[2] += valor
                                obj_ret[3] = porcentaje

            except (Codigo_SRI.DoesNotExist, ValueError):
                pass

            try:
                cod_sri_ret_iva = Codigo_SRI.objects.get(id=cod_ret_iva_id)
                codigo_sri_iva = Codigo_SRI.objects.get(id=cod_iva_id)
                porcentaje = cod_sri_ret_iva.porcentaje if not cod_sri_ret_iva.porc_variable else porc_ret_iva

                if porcentaje is not None and porcentaje > 0:
                    iva = (valor * codigo_sri_iva.porcentaje / 100)
                    valor_ret = (porcentaje * iva / 100)

                    if cod_sri_ret_iva.id not in ids_lista_ret_iva:
                        ids_lista_ret_iva.append(cod_sri_ret_iva.id)
                        lista_ret_iva.append([cod_sri_ret_iva, valor_ret, iva, porcentaje])
                    else:
                        for obj_ret in lista_ret_iva:
                            if obj_ret[0].id == cod_sri_ret_iva.id:
                                obj_ret[1] += valor_ret
                                obj_ret[2] += iva
                                obj_ret[3] = porcentaje

            except (Codigo_SRI.DoesNotExist, ValueError):
                pass
        else:
            pass

    instanciar_form_fte = []
    for obj in lista_ret_fte:
        porcentaje = obj[3]
        try:
            cta_retencion = Cuenta_Retencion.objects.filter(porcentaje=porcentaje)[0]
            cod_cta = cta_retencion.id
        except:
            cod_cta = ""

        instanciar_form_fte.append({
            "tipo": "1", "cod_sri": obj[0].id,
            "codigo_ret": "Ret. Fte. " + obj[0].codigo + " " + str(obj[3]) + "%",
            "cod_cta": cod_cta, "base": obj[2], "porcentaje": obj[3],
            "total": obj[1]
        })

    instanciar_form_iva = []
    for obj in lista_ret_iva:
        porcentaje = obj[3]
        try:
            cta_retencion = Cuenta_Retencion.objects.filter(porcentaje=porcentaje)[0]
            cod_cta = cta_retencion.id
        except:
            cod_cta = ""

        instanciar_form_iva.append({
            "tipo": "2", "cod_sri": obj[0].id,
            "codigo_ret": "Ret. IVA. " + obj[0].codigo + " " + str(obj[3]) + "%",
            "cod_cta": cod_cta, "base": obj[2], "porcentaje": obj[3],
            "total": obj[1]
        })

    detalle_ret = detalle_ret_formset(initial=instanciar_form_fte+instanciar_form_iva,
                                      prefix="detalle_ret")

    html = render_to_string('tags/compras/tabla_retenciones.html', {'forms': detalle_ret})
    return HttpResponse(html)


@login_required(login_url='/')
def get_datos_codigo_sri(request):
    """
    Funcion que me busca el porcentaje del codigo SRI y tambien
    si ese codigo SRI es de porcentaje variable
    :param request:
    :return:
    """

    id = request.GET.get("id", "")

    if id is not None and id != "":
        try:
            p = Codigo_SRI.objects.exclude(status=0).get(id=id)

            variable = True if p.porc_variable else False
            porc_max = 0 if p.porc_max is None else float(p.porc_max)
            porc_min = 0 if p.porc_min is None else float(p.porc_min)

            resultado = json.dumps({"codigo": p.codigo, "porcentaje": p.porcentaje,
                                    "status": 1, "variable": variable,
                                    "porc_max": porc_max, "porc_min": porc_min})

            return HttpResponse(resultado, mimetype='application/json')
        except Codigo_SRI.DoesNotExist:
            resultado = json.dumps({"codigo": "", "porcentaje": "",
                                    "status": 0, "variable": False,
                                    "porc_max": "", "porc_min": ""})
    else:
        resultado = json.dumps({"codigo": "", "porcentaje": "",
                                "status": 0, "variable": False,
                                "porc_max": "", "porc_min": ""})

    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def agregar_fila_det_compra(request):
    formsetDetalle_Comp = formset_factory(FormDetalleCompCompra, extra=1)
    detalleform = formsetDetalle_Comp(prefix="detalle_compra")

    html = get_fila_t_detalle_compra(detalleform[0])
    return HttpResponse(html)


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def buscar_items(request):
    """
    Funcion que me busca los items dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    if pkey_val is not None and pkey_val != "":
        try:
            p = Item.objects.exclude(status=0).get(id=pkey_val)
            resultado = json.dumps({"name": p.codigo + " - " + p.nombre[0:30], "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except (Item.DoesNotExist, ValueError):
            respuesta = []
    else:
        lista = Item.objects.filter(status=1,
                                    categoria_item__tipo_item__id=1
                                    ).filter(Q(codigo__icontains=q) | Q(nombre__icontains=q)).order_by("codigo", "nombre")

        count = len(lista)

        paginacion = Paginator(lista, per_page)

        try:
            lista = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            lista = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            lista = paginacion.page(paginacion.num_pages)

        for p in lista:
            respuesta.append({"name": p.codigo + " - " + p.nombre[0:30], "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def buscar_clientes(request):
    """
    Funcion que me busca los items dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    if pkey_val is not None and pkey_val != "":
        try:
            p = Cliente.objects.exclude(status=0).get(id=pkey_val)
            resultado = json.dumps({"name": p.razon_social[0:30] + "(" + p.ruc + ")", "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except (Cliente.DoesNotExist, ValueError):
            respuesta = []
    else:
        lista = Cliente.objects.filter(status=1
                                       ).filter(Q(razon_social__icontains=q) | Q(ruc__icontains=q)).order_by("razon_social", "ruc")

        count = len(lista)

        paginacion = Paginator(lista, per_page)

        try:
            lista = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            lista = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            lista = paginacion.page(paginacion.num_pages)

        for p in lista:
            respuesta.append({"name": p.razon_social[0:30] + "(" + p.ruc + ")", "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def buscar_ciudad(request):
    """
    Funcion que me busca los items dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    if pkey_val is not None and pkey_val != "":
        try:
            p = Ciudad.objects.exclude(status=0).get(id=pkey_val)
            resultado = json.dumps({"name": p.descripcion, "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except (Ciudad.DoesNotExist, ValueError):
            respuesta = []
    else:
        lista = Ciudad.objects.filter(status=1,
                                       ).filter(Q(descripcion__icontains=q)).order_by("descripcion")

        count = len(lista)

        paginacion = Paginator(lista, per_page)

        try:
            lista = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            lista = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            lista = paginacion.page(paginacion.num_pages)

        for p in lista:
            respuesta.append({"name": p.descripcion, "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


def get_fila_form(request, class_form):
    formset_init = formset_factory(class_form, extra=1)
    form = class_form()  # Para saber el sufijo de CompraInventario
    formset = formset_init(prefix=form.formset_prefix)
    html = get_fila_formulario_formset(request, formset[0])  # Solo necesito una fila del form
    return HttpResponse(html)


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def get_fila_form_compra_inventario(request):
    """
    Función que devuelve una fila de formulario compra_inventario
    :param request:
    :return:
    """
    return get_fila_form(request, CompraInventario)

@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def get_fila_form_ajuste_cantidad_inventario(request):
    """
    Funcion que me busca los items dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """

    return get_fila_form(request, AjusteCantidadForm)


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def get_fila_form_ajuste_costo_inventario(request):
    """
    Funcion que me busca los items dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """

    return get_fila_form(request, AjusteCostoForm)


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def get_fila_form_guia_rem_inventario(request):
    """
    Funcion que me busca los items dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """

    return get_fila_form(request, DetalleInventarioGRForm)


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def get_fila_form_compra_inicial_inventario(request):
    """
    Funcion que me busca los items dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """

    return get_fila_form(request, CompraInventarioInicial)


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def get_fila_form_produccion_inventario(request):
    """
    Funcion que me busca los items dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """

    return get_fila_form(request, FormDetalleInventarioContrato)


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def get_fila_form_consumo_interno_inventario(request):
    """
    Funcion que me busca los items dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """

    return get_fila_form(request, ConsumoInternoForm)


@login_required(login_url='/')
@csrf_exempt
def get_unidad_medida_item(request):
    """
    Funcion que me busca el porcentaje del codigo SRI y tambien
    si ese codigo SRI es de porcentaje variable
    :param request:
    :return:
    """

    id = request.POST.get("id", "")

    if id is not None and id != "":
        try:
            p = Item.objects.exclude(status=0).get(id=id)

            resultado = json.dumps({"status": 1, "unidad_medida": p.unidad.descripcion})

            return HttpResponse(resultado, mimetype='application/json')
        except Item.DoesNotExist:
            resultado = json.dumps({"status": 0, "unidad_medida": ""})
    else:
        resultado = json.dumps({"status": 0, "unidad_medida": ""})

    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def get_costo_item(request):
    """
    Funcion que me busca el porcentaje del codigo SRI y tambien
    si ese codigo SRI es de porcentaje variable
    :param request:
    :return:
    """

    id = request.POST.get("id", "")

    if id is not None and id != "":
        try:
            p = Item.objects.exclude(status=0).get(id=id)

            resultado = json.dumps({"status": 1, "costo": p.costo})

            return HttpResponse(resultado, mimetype='application/json')
        except Item.DoesNotExist:
            resultado = json.dumps({"status": 0, "costo": ""})
    else:
        resultado = json.dumps({"status": 0, "costo": ""})

    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_protect
@csrf_exempt
def calcular_totales_compra_inventario(request):
    formsetDetalle_Comp = formset_factory(CompraInventario)
    detalleform = formsetDetalle_Comp(request.POST, request.FILES, prefix=CompraInventario().formset_prefix)
    costo_total = 0.0
    subtotal_iva = 0.0
    for forms in detalleform:
        forms.is_valid()
        informacion = forms.cleaned_data
        try:
            id_item = informacion.get("item")
            costo = redondeo((informacion.get("costo", 0)))
            cantidad = redondeo((informacion.get("cantidad", 0)))

            item = Item.objects.get(id=id_item)
            if item.iva > 0:
                subtotal_iva += (costo * item.iva)/100
            costo_total += costo

        except (Item.DoesNotExist, ValueError):
            pass

    resultado = json.dumps({"status": 1, "costo_total": costo_total,
                            "subtotal_iva": subtotal_iva, "total": costo_total+subtotal_iva})

    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def esta_reversado(request):
    """
    Funcion que me busca el porcentaje del codigo SRI y tambien
    si ese codigo SRI es de porcentaje variable
    :param request:
    :return:
    """

    id = request.POST.get("id", "")

    if id is not None and id != "":
        try:
            p = Cobro.objects.exclude(status=0).get(id=id)

            resultado = json.dumps({"status": 1, "esta_reversado": p.esta_reversado()})

            return HttpResponse(resultado, mimetype='application/json')
        except Cobro.DoesNotExist:
            resultado = json.dumps({"status": 0, "esta_reversado": False})
    else:
        resultado = json.dumps({"status": 0, "esta_reversado": False})

    return HttpResponse(resultado, mimetype='application/json')


from librerias.funciones.funciones_vistas import get_parametros_empresa
@login_required(login_url='/')
@csrf_exempt
def get_fecha_reversar(request):
    """
    Funcion que me busca el porcentaje del codigo SRI y tambien
    si ese codigo SRI es de porcentaje variable
    :param request:
    :return:
    """
    try:
        secuencia_tipo_comp = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=27)
        p = secuencia_tipo_comp.fecha_actual.strftime("%Y-%m-%d")
        resultado = json.dumps({"status": 1, "fecha": p})

        return HttpResponse(resultado, mimetype='application/json')
    except SecuenciaTipoComprobante.DoesNotExist:
        empresa_param = get_parametros_empresa()
        resultado = json.dumps({"status": 1, "fecha": empresa_param.fecha_inicio_sistema.strftime("%Y-%m-%d")})

    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def get_fecha_reversar_pago(request):
    """
    Funcion que me busca el porcentaje del codigo SRI y tambien
    si ese codigo SRI es de porcentaje variable
    :param request:
    :return:
    """
    try:
        secuencia_tipo_comp = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=28)
        p = secuencia_tipo_comp.fecha_actual.strftime("%Y-%m-%d")
        resultado = json.dumps({"status": 1, "fecha": p})

        return HttpResponse(resultado, mimetype='application/json')
    except SecuenciaTipoComprobante.DoesNotExist:
        empresa_param = get_parametros_empresa()
        resultado = json.dumps({"status": 1, "fecha": empresa_param.fecha_inicio_sistema.strftime("%Y-%m-%d")})

    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def esta_reversado_pago(request):
    """
    Funcion que me busca el porcentaje del codigo SRI y tambien
    si ese codigo SRI es de porcentaje variable
    :param request:
    :return:
    """

    id = request.POST.get("id", "")

    if id is not None and id != "":
        try:
            p = Pago.objects.exclude(status=0).get(id=id)

            resultado = json.dumps({"status": 1, "esta_reversado": p.esta_reversado()})

            return HttpResponse(resultado, mimetype='application/json')
        except Pago.DoesNotExist:
            resultado = json.dumps({"status": 0, "esta_reversado": False})
    else:
        resultado = json.dumps({"status": 0, "esta_reversado": False})

    return HttpResponse(resultado, mimetype='application/json')
