#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
import datetime
from django.db.models import Q
from django.db import connection
from contabilidad.formularios.CajaChicaForms import *
from django.forms.util import ErrorList
from django.core.paginator import *
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django import template
from librerias.funciones.validacion_rucs import *
from librerias.funciones.permisos import *
from administracion.models import *
from librerias.funciones.permisos import *
register = template.Library()

IVA = 0.12
class ErrorComprasCajaChica(Exception):
    def __init__(self, valor):
        self.valor = valor
    def __str__(self):
        return "Error " + str(self.valor)
@login_required(login_url='/')
@csrf_exempt
def lista_compras_caja_chica(request):
    fecha = request.GET.get("fecha", "")
    compra_caja_chica = Cabecera_Caja_Chica.objects.exclude(status=0).order_by("fecha")

    if fecha:
        compra_caja_chica.objects.exclude(status=0).filter(fecha=fecha).order_by("fecha_emision")

    paginacion = Paginator(compra_caja_chica, 7)
    numero_pagina = request.GET.get("page")
    try:
        lista_cabecera = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista_cabecera = paginacion.page(1)
    except EmptyPage:
        lista_cabecera = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = lista_cabecera.number

    return render_to_response('caja_chica/lista_caja_chica.html',
                              {"objetos_cabecera": compra_caja_chica,
                               "total_paginas": total_paginas, "numero":numero,
                              "request": request}, context_instance=RequestContext(request))
@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def registrar_caja_chica(request):
    dia_actual = datetime.datetime.now().strftime('%Y-%m-%d')
    formulario = FormCabeceraCaja()### Formulario de Cabecera Caja Chica
    multiform = formset_factory(FormDetalleCajaChica, extra=1)### Formulario para llenar los detalles de Caja Chica
    formulario_detalle_compra = multiform(prefix="detalle_compra_caja")
    cursor = connection.cursor()
    tipo_c = TipoComprobante.objects.get(id=8)
    now = datetime.datetime.now()
    contador = 0
    tipo = 1 # variable para manejar el titulo del template
    documentos = []
    flag = 0

    try:
        if request.method == "POST":
            formulario = FormCabeceraCaja(request.POST)
            formulario_detalle_compra = multiform(request.POST, request.FILES, prefix="detalle_compra_caja")
            tmonto0 = 0.0
            tmontoi = 0.0
            total_iva = 0.0
            totales_base0 = 0.0
            totales_base12 = 0.0
            resultado_iva = 0.0
            # Calcula valores totales
            if formulario_detalle_compra.is_valid():
                for form in formulario_detalle_compra:
                    informacion = form.cleaned_data
                    iva = 0.0
                    valor = 0.0
                    iva = float(str(informacion.get("porc_iva")).split("-")[1])
                    costgto = int(informacion.get("cto_gto"))
                    if informacion.get("valor") != "":
                        valor = float(informacion.get("valor"))
                    ### Si es base0 ó baseIva y solo costo ###
                #####################################################
                    if costgto == 1:
                        if iva == 0.0:
                            tmonto0 = tmonto0 + valor
                        else:
                            tmontoi = tmontoi + valor
                ######################################################
                    if iva == 0.0:
                        totales_base0 = totales_base0 + valor
                    else:
                        totales_base12 = totales_base12 + valor
                resultado_iva = totales_base12 * IVA
                total_iva = tmontoi * IVA

            if formulario.is_valid():
                cabecera_caja = Cabecera_Caja_Chica()
                cabecera_comprobante = Cabecera_Comp_Contable()
                fecha = datetime.datetime.strptime(formulario.getFecha(), "%Y-%m-%d").date()

                ##################### Cabecera Comprobante Contable #####################
                cabecera_comprobante.fecha = fecha
                cabecera_comprobante.concepto_comprobante = formulario.getConcepto()
                cursor.execute("select get_next_seq(%s,8);", [fecha])
                total_rows = cursor.fetchone()
                cabecera_comprobante.numero_comprobante = total_rows[0]
                cabecera_comprobante.tipo_comprobante = tipo_c
                cabecera_comprobante.fecha_creacion = now
                cabecera_comprobante.usuario_creacion = request.user.username
                cabecera_comprobante.save()

                ###################### Cabecera de Caja Chica ###############################################
                cabecera_caja.fecha = fecha
                cabecera_caja.concepto = formulario.getConcepto()
                cabecera_caja.plan_cuenta = PlanCuenta.objects.filter(status=1).get(id=formulario.getResponsable())
                cabecera_caja.numero_comprobante = cabecera_comprobante.numero_comprobante
                cabecera_caja.total_pagar = (totales_base12+totales_base0+resultado_iva)

                cabecera_caja.fecha_creacion = now
                cabecera_caja.usuario_creacion = request.user.username
                cabecera_caja.save()

                ####################################### DetalleCajaChica #####################################
                if formulario_detalle_compra.is_valid() and formulario.is_valid():
                    for form in formulario_detalle_compra:
                        informacion = form.cleaned_data
                        detalle_compra_chica = DetalleCajaChica()
                        detalle_compra_chica.cabecera_caja_chica = cabecera_caja
                        codigo = unicode(informacion.get("cuenta"))
                        codigo2 = codigo.split("-")[0]
                        detalle_compra_chica.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(codigo=codigo2)

                        if informacion.get("cto_gto") == "1":
                            detalle_compra_chica.iva_cg = 1 # COSTO
                            flag = 1
                        else:
                            detalle_compra_chica.iva_cg = 2 # GASTO

                        detalle_compra_chica.codigo_sri = \
                            Codigo_SRI.objects.filter(status=1).filter(vigencia_retencion=VigenciaRetencionSRI.objects.filter(tipo_codigo_sri=Tipos_Retencion.objects.filter(status=1).get(id=2)).get(fecha_final__year=9999).id).get(codigo=str(informacion.get("porc_iva")).split("-")[0])

                        ############################################# ATS #####################################
                        ats = ATS()
                        ats.tipo_comprobante = cabecera_comprobante.tipo_comprobante
                        ats.compra_pk = cabecera_caja.id
                        ats.sustento_tributario_sri = Sustento_Tributario.objects.filter(status=1).get(id=informacion.get("sustento_tributario"))
                        ats.identificacion = Identificacion.objects.filter(status=1).get(codigo=informacion.get("tipo_identificacion"))
                        ats.num_id = informacion.get("proveedor")
                        ats.documento = Documento.objects.filter(status=1).get(id=informacion.get("tipo_documento"))
                        ats.num_doc = informacion.get("serie")
                        ats.fecha_reg = cabecera_caja.fecha
                        ats.fecha_emi = informacion.get("fecha_emision")
                        ats.autorizacion = informacion.get("num_autorizacion")
                        ats.vence = informacion.get("fecha_vencimiento")

                        if detalle_compra_chica.codigo_sri.porcentaje == 12.0:
                            ats.baseiva = float(informacion.get("valor"))
                            ats.base0 = 0.0
                            ats.monto_iva = float(informacion.get("valor"))*0.12

                        if detalle_compra_chica.codigo_sri.porcentaje == 0.0:
                            ats.baseiva = 0.0
                            ats.base0 = float(informacion.get("valor"))
                            ats.monto_iva = 0.0

                        ats.base_ice = 0.0
                        ats.monto_ice = 0.0
                        ats.fecha_creacion = now
                        ats.usuario_creacion = request.user.username
                        ats.save()

                        ##################################################
                        #  validación de ruc ó cedula ingresada          #
                        ##################################################
                        if len(informacion.get("proveedor")) != 10 and len(informacion.get("proveedor")) != 13:
                            errors = formulario._errors.setdefault("ruc", ErrorList())
                            messages.error(request, u"Número de Identificación es incorrecto.")
                            contador += 1
                        else:
                            contador = 0

                        if ats.identificacion.codigo == "R" and len(informacion.get("proveedor")) == 13:

                            if isPersonaJuridica(ats.num_id) or isEmpresaPublica(ats.num_id) or isPersonaNatural(ats.num_id):
                                contador = 0

                            else:

                                if contador == 0:
                                    errors = formulario._errors.setdefault("ruc", ErrorList())
                                    messages.error(request, u"Número de Identificación incorrecto.")
                                    contador += 1
                        else:

                            if ats.identificacion.codigo == "C" and len(informacion.get("proveedor")) == 10:
                                if isCedula(ats.num_id):
                                    contador = 0

                                else:

                                    if contador == 0:
                                        errors = formulario._errors.setdefault("ruc", ErrorList())
                                        messages.error(request, u"Número de Identificación incorrecto.")
                                        contador += 1
                            else:

                                if contador == 0:
                                    errors = formulario._errors.setdefault("ruc", ErrorList())
                                    messages.error(request, u"Número de Identificación incorrecto.")
                                    contador += 1

                        if ats.identificacion.codigo == "P":
                            contador = 0


                        ##################################################
                        #  validación de posibles documentos repetidos.  #
                        ##################################################
                        print ats.documento.codigo_documento
                        print ats.autorizacion
                        print ats.num_id
                        print ats.num_doc

                        if ATS.objects.filter(status=1).filter(num_doc=ats.num_doc, num_id=ats.num_id, documento=ats.documento, autorizacion=ats.autorizacion).exists():
                            print "ingresó"
                        else:
                            print "no entro"

                        if contador > 0:
                            raise ErrorComprasCajaChica("Algunos campos no son válidos")

                        else:
                            detalle_compra_chica.ats = ats
                            detalle_compra_chica.fecha_creacion = now
                            detalle_compra_chica.usuario_creacion = request.user.username
                            detalle_compra_chica.save()

                        ###############################################
                        #         Detalle de comprobante Contable     #
                        ###############################################
                        detalle_comp_cont = Detalle_Comp_Contable()
                        detalle_comp_cont.cabecera_contable = cabecera_comprobante
                        detalle_comp_cont.fecha_asiento = cabecera_comprobante.fecha
                        detalle_comp_cont.centro_costo = Centro_Costo.objects.get(id=1)
                        detalle_comp_cont.plan_cuenta = PlanCuenta.objects.filter(status=1).get(codigo=codigo2)

                        # ES GASTO Y BASE_IVA
                        if detalle_compra_chica.iva_cg != 1 and detalle_compra_chica.codigo_sri.porcentaje != 0.0:
                            detalle_comp_cont.valor = informacion.get("valor") + float(informacion.get("valor"))*0.12
                        else:
                            detalle_comp_cont.valor = informacion.get("valor")

                        detalle_comp_cont.dbcr = "D"
                        detalle_comp_cont.detalle = PlanCuenta.objects.filter(status=1).get(codigo=codigo2).descripcion

                        detalle_comp_cont.fecha_creacion = now
                        detalle_comp_cont.usuario_creacion = request.user.username
                        detalle_comp_cont.save()

                    if flag == 1:
                        #########################################
                        #         Cuenta de Iva Pagado          #
                        #########################################
                        detalle_comp_iva = Detalle_Comp_Contable()
                        detalle_comp_iva.cabecera_contable = cabecera_comprobante
                        detalle_comp_iva.fecha_asiento = cabecera_comprobante.fecha
                        detalle_comp_iva.plan_cuenta = PlanCuenta.objects.filter(status=1).get(tipo__alias=11)
                        detalle_comp_iva.dbcr = "D"
                        detalle_comp_iva.detalle = PlanCuenta.objects.filter(status=1).get(tipo__alias=11).descripcion
                        detalle_comp_iva.valor = total_iva

                        detalle_comp_iva.fecha_creacion = now
                        detalle_comp_iva.usuario_creacion = request.user.username
                        detalle_comp_iva.save()

                    ########################################## Detalle Comprobante Contable ###################################################
                    ###########################)============== Cuenta Proveedores ====================################
                    detalle_comprobante = Detalle_Comp_Contable()
                    detalle_comprobante.cabecera_contable = cabecera_comprobante
                    detalle_comprobante.fecha_asiento = cabecera_comprobante.fecha
                    detalle_comprobante.plan_cuenta = PlanCuenta.objects.filter(status=1).get(tipo__alias=3, nivel=5)
                    detalle_comprobante.dbcr = "H"
                    detalle_comprobante.valor = (totales_base12+totales_base0+resultado_iva)
                    detalle_comprobante.detalle = PlanCuenta.objects.filter(status=1).get(tipo__alias=3, nivel=5).descripcion
                    detalle_comprobante.fecha_creacion = now
                    detalle_comprobante.usuario_creacion = request.user.username
                    detalle_comprobante.save()
                else:
                    contador += 1
            else:
                contador += 1

            if contador > 0:
                raise ErrorComprasCajaChica("Algunos campos no son válidos")

            messages.success(request, u"La compra se ha grabado con éxito")
            return HttpResponseRedirect(reverse("lista_compras_caja_chica"))

    except ErrorComprasCajaChica, e:
        transaction.rollback()
        messages.error(request, str(e))
    return render_to_response('caja_chica/caja_chica.html', {"formulario": formulario,
                                                             "multiform": formulario_detalle_compra, "fecha_actual": dia_actual,"documentos": documentos,
                                                             "request": request, "tipo": tipo}, context_instance=RequestContext(request))






@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def copiar_caja_chica(request, id):
    try:
        compra_caja_nueva = Cabecera_Caja_Chica.objects.exclude(status=0).get(id=id)
        detalle_compra_caja_nueva = DetalleCajaChica.objects.filter(cabecera_caja_chica=compra_caja_nueva)

        ats = ATS.objects.filter(status=1).filter(compra_pk=compra_caja_nueva.id)

        now = datetime.datetime.now()
        dia_actual = datetime.datetime.now().strftime('%Y-%m-%d')
        contador = 0
        cursor = connection.cursor()
        tipo = 3
        tipo_c = TipoComprobante.objects.get(codigo="cch")

        formulario = FormCabeceraCaja(initial={
            "fecha": compra_caja_nueva.fecha.strftime('%Y-%m-%d'),
            "concepto": compra_caja_nueva.concepto,
            "responsable": compra_caja_nueva.plan_cuenta.id
        })

        ###################################### Formulario de Detalle de Caja Chica #########################################
        det_form = []
        multiform = formset_factory(FormDetalleCajaChica, extra=0)

        if (ats):
            for obj in ats:
                if obj.getDatos().codigo_sri.porcentaje == 12:
                    det_form.append({"cuenta": obj.getDatos().plan_cuenta.descripcion+"-"+obj.getDatos().plan_cuenta.codigo,
                                     "tipo_identificacion": obj.identificacion.codigo, "proveedor": "0"+obj.num_id,
                                     "sustento_tributario": obj.sustento_tributario_sri.id, "tipo_documento": obj.documento.id, "porc_iva": obj.getDatos().codigo_sri.codigo+"-"+str(obj.getDatos().codigo_sri.porcentaje),
                                     "serie": obj.num_doc, "fecha_emision": obj.fecha_emi.strftime("%Y-%m-%d"), "num_autorizacion": obj.autorizacion,
                                     "fecha_vencimiento": obj.vence.strftime("%Y-%m-%d"),
                                     "cto_gto": obj.getDatos().iva_cg, "valor": obj.baseiva})
                else:

                    det_form.append({"cuenta": obj.getDatos().plan_cuenta.descripcion+"-"+obj.getDatos().plan_cuenta.codigo,
                                     "tipo_identificacion": obj.identificacion.codigo, "proveedor": "0"+obj.num_id,
                                     "sustento_tributario": obj.sustento_tributario_sri.id, "tipo_documento": obj.documento.id, "porc_iva": obj.getDatos().codigo_sri.codigo+"-"+str(obj.getDatos().codigo_sri.porcentaje),
                                     "serie": obj.num_doc, "fecha_emision": obj.fecha_emi.strftime("%Y-%m-%d"), "num_autorizacion": obj.autorizacion,
                                     "fecha_vencimiento": obj.vence.strftime("%Y-%m-%d"),
                                     "cto_gto": obj.getDatos().iva_cg, "valor": obj.base0})

        formulario_detalle_compra = multiform(initial=det_form, prefix="detalle_compra_caja")
    except:
        messages.error(request, u"Existió un problema al cargar la compra sin retención")
        return HttpResponseRedirect(reverse("lista_compras_caja_chica"))


    try:
        if(request.method=="POST"):
            cabecera_compra_caja = Cabecera_Caja_Chica()
            formulario = FormCabeceraCaja(request.POST)
            formulario_detalle_compra = multiform(request.POST, request.FILES, prefix="detalle_compra_caja")
            tmonto0 = 0.0
            tmontoi = 0.0
            total_iva = 0.0

            ####### Calcula valores totales ##########
            if formulario_detalle_compra.is_valid():
                for form in formulario_detalle_compra:
                    informacion = form.cleaned_data
                    iva = 0.0
                    valor = 0.0
                    iva = float(str(informacion.get("porc_iva")).split("-")[1])
                    print iva

                    if informacion.get("valor") != "":
                        valor = float(informacion.get("valor"))
                    if iva == 0.0:
                        tmonto0 = tmonto0 + valor
                    else:
                        tmontoi = tmontoi + valor
                total_iva = tmontoi * IVA
                print total_iva

            if formulario.is_valid():
                cabecera_caja = Cabecera_Caja_Chica()
                cabecera_comprobante = Cabecera_Comp_Contable()
                fecha = datetime.datetime.strptime(formulario.getFecha(), "%Y-%m-%d").date()
                cabecera_caja.concepto = formulario.getConcepto()
                ##################### Cabecera Comprobante Contable #####################
                cabecera_comprobante.fecha = fecha
                cabecera_comprobante.concepto_comprobante = cabecera_caja.concepto
                cursor.execute("select get_next_seq(%s,8);", [fecha])
                total_rows = cursor.fetchone()
                cabecera_comprobante.numero_comprobante = total_rows[0]
                cabecera_comprobante.tipo_comprobante = tipo_c
                cabecera_comprobante.fecha_creacion = now
                cabecera_comprobante.usuario_creacion = request.user.username
                cabecera_comprobante.save()

                ###################### Cabecera de Caja Chica #############################
                cabecera_caja.fecha = cabecera_comprobante.fecha
                cabecera_caja.numero_comprobante = cabecera_comprobante.numero_comprobante
                cabecera_caja.base_ice = 0.0
                cabecera_caja.monto_ice = 0.0
                cabecera_caja.cabecera_comprobante_contable = cabecera_comprobante
                cabecera_caja.save()

                ######################## DetalleCajaChica ##################################
                print formulario_detalle_compra.errors
                if formulario_detalle_compra.is_valid() and formulario.is_valid():
                    for form in formulario_detalle_compra:
                        informacion = form.cleaned_data
                        detalle_compra_chica = DetalleCajaChica()
                        detalle_compra_chica.cabecera_caja_chica = cabecera_caja

                        codigo = unicode(informacion.get("cuenta"))
                        codigo2 = codigo.split("-")[1]
                        detalle_compra_chica.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(codigo=codigo2)
                        detalle_compra_chica.tipo_identificacion = Identificacion.objects.exclude(status=0).get(codigo=informacion.get("tipo_identificacion"))
                        detalle_compra_chica.proveedorcchica = informacion.get("proveedor")
                        detalle_compra_chica.sust_trib = Sustento_Tributario.objects.exclude(status=0).get(id=informacion.get("sustento_tributario"))
                        detalle_compra_chica.tipo_doc = Documento.objects.exclude(status=0).get(id=informacion.get("tipo_documento"))
                        detalle_compra_chica.fecha_emision = informacion.get("fecha_emision")
                        detalle_compra_chica.fecha_vencimiento = informacion.get("fecha_vencimiento")
                        detalle_compra_chica.codigo_iva = str(informacion.get("porc_iva")).split("-")[0]
                        detalle_compra_chica.porcentaje_iva = float(str(informacion.get("porc_iva")).split("-")[1])
                        b_s = informacion.get("bs")
                        detalle_compra_chica.b_s = b_s
                        detalle_compra_chica.serie = informacion.get("serie")
                        detalle_compra_chica.numero_autorizacion = informacion.get("num_autorizacion")
                        detalle_compra_chica.valor = informacion.get("valor")
                        total = (tmonto0 + tmontoi + total_iva)

                        if detalle_compra_chica.valor + total_iva >= 1000:
                            detalle_compra_chica.forma_pago = Forma_Pago_SRI.objects.exclude(status=0).get(codigo =informacion.get("id_info"))

                        ##################################################
                        #  validación de ruc ó cedula ingresada          #
                        ##################################################
                        if len(detalle_compra_chica.proveedorcchica) != 10 and len(detalle_compra_chica.proveedorcchica) != 13:
                            errors = formulario._errors.setdefault("ruc", ErrorList())
                            messages.error(request, u"Número de Identificación incorrecto.")
                            contador += 1
                        else:
                            contador = 0

                        if detalle_compra_chica.tipo_identificacion.codigo == "R" and len(detalle_compra_chica.proveedorcchica) == 13:
                            if isPersonaJuridica(detalle_compra_chica.proveedorcchica) or isEmpresaPublica(detalle_compra_chica.proveedorcchica) or isPersonaNatural(detalle_compra_chica.proveedorcchica):
                                contador = 0
                            else:
                                if contador == 0:
                                    errors = formulario._errors.setdefault("ruc", ErrorList())
                                    messages.error(request, u"Número de Identificación incorrecto.")
                                    contador += 1
                        else:
                            if detalle_compra_chica.tipo_identificacion.codigo == "C" and len(detalle_compra_chica.proveedorcchica) == 10:
                                if isCedula(detalle_compra_chica.proveedorcchica):
                                    contador = 0
                                else:
                                    if contador == 0:
                                        errors = formulario._errors.setdefault("cedula", ErrorList())
                                        messages.error(request, u"Número de Identificación incorrecto.")
                                        contador += 1
                            else:
                                if contador == 0:
                                    errors = formulario._errors.setdefault("cedula", ErrorList())
                                    messages.error(request, u"Número de Identificación incorrecto.")
                                    contador += 1

                        if detalle_compra_chica.tipo_identificacion.codigo == "P":
                            contador = 0

                        ##################################################
                        #  validación de posibles documentos repetidos.  #
                        ##################################################
                        lista_compras_caja = DetalleCajaChica.objects.all()
                        for c in lista_compras_caja:
                            if c.proveedorcchica == detalle_compra_chica.proveedorcchica and c.tipo_doc.codigo_documento == detalle_compra_chica.tipo_doc.codigo_documento \
                                and c.serie == detalle_compra_chica.serie and c.numero_autorizacion == detalle_compra_chica.numero_autorizacion:
                                errors = formulario._errors.setdefault("serie", ErrorList())
                                messages.error(request, u"Número de Documento ya ingresado, por favor verifique el número.")
                                contador += 1
                        detalle_compra_chica.pago = Tipo_Pago_SRI.objects.get(codigo=informacion.get("pago"))
                        detalle_compra_chica.fecha_actualizacion = now
                        detalle_compra_chica.fecha_creacion = now
                        detalle_compra_chica.usuario_actualizacion = request.user.username
                        detalle_compra_chica.usuario_creacion = request.user.username

                        if contador > 0:
                            raise ErrorComprasCajaChica("Algunos campos no son válidos")
                        else:
                            detalle_compra_chica.save()
                            cabecera_caja.total_pagar = total
                            cabecera_caja.tlbase0 = tmonto0
                            cabecera_caja.tlbaseiva = tmontoi
                            cabecera_caja.save()

                        ###############################################
                        #         Detalle de comprobante Contable     #
                        ###############################################
                        detalle_comp_cont = Detalle_Comp_Contable()
                        detalle_comp_cont.cabecera_contable = cabecera_comprobante
                        detalle_comp_cont.centro_costo = Centro_Costo.objects.get(id=1)
                        detalle_comp_cont.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(codigo=codigo2)
                        detalle_comp_cont.valor = informacion.get("valor")
                        detalle_comp_cont.dbcr = "D"
                        detalle_comp_cont.detalle = PlanCuenta.objects.exclude(status=0).get(codigo=codigo2).descripcion

                        detalle_comp_cont.fecha_actualizacion = now
                        detalle_comp_cont.fecha_creacion = now
                        detalle_comp_cont.usuario_actualizacion = request.user.username
                        detalle_comp_cont.usuario_creacion = request.user.username
                        detalle_comp_cont.save()

                    #########################################
                    #         Cuenta de Iva Pagado          #
                    #########################################
                    detalle_comp_iva = Detalle_Comp_Contable()
                    detalle_comp_iva.cabecera_contable = cabecera_comprobante
                    detalle_comp_iva.centro_costo = Centro_Costo.objects.get(id=1)
                    detalle_comp_iva.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(codigo="110502001")
                    detalle_comp_iva.valor = total_iva
                    detalle_comp_iva.dbcr = "D"
                    detalle_comp_iva.detalle = PlanCuenta.objects.exclude(status=0).get(codigo="110502001").descripcion

                    detalle_comp_iva.fecha_actualizacion = now
                    detalle_comp_iva.fecha_creacion = now
                    detalle_comp_iva.usuario_actualizacion = request.user.username
                    detalle_comp_iva.usuario_creacion = request.user.username
                    detalle_comp_iva.save()

                    ########################################## Detalle Comprobante Contable ###################################################
                    ###########################)============== Cuenta Proveedores ====================################
                    detalle_comprobante = Detalle_Comp_Contable()
                    detalle_comprobante.cabecera_contable = cabecera_comprobante
                    detalle_comprobante.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(codigo=codigo2)
                    detalle_comprobante.centro_costo = Centro_Costo.objects.get(id=1)
                    detalle_comprobante.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(codigo="210101001")
                    detalle_comprobante.dbcr = "H"
                    detalle_comprobante.valor = (tmontoi+total_iva+tmonto0)
                    detalle_comprobante.detalle = PlanCuenta.objects.exclude(status=0).get(codigo="210101001").descripcion

                    detalle_comprobante.fecha_actualizacion = now
                    detalle_comprobante.fecha_creacion = now
                    detalle_comprobante.usuario_actualizacion = request.user.username
                    detalle_comprobante.usuario_creacion = request.user.username
                    detalle_comprobante.save()
                else:
                    contador += 1
            else:
                contador += 1

            if contador > 0:
                raise ErrorComprasCajaChica("Algunos campos no son válidos")
            else:
                messages.success(request, u"La compra se ha grabado con éxito")
                return HttpResponseRedirect(reverse("lista_compras_caja_chica"))

    except ErrorComprasCajaChica, e:
        transaction.rollback()
        messages.error(request, str(e))

    return render_to_response('caja_chica/caja_chica.html',
                               {"formulario":formulario, "multiform": formulario_detalle_compra, "tipo": tipo,
                                   "id": id, "request": request, "dia": str(dia_actual)}, context_instance=RequestContext(request))
@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def anular_generar_compra_caja_chica(request, id):
    tipo = 2
    contador = 0
    now = datetime.datetime.now()
    cursor = connection.cursor()
    tipo_c = TipoComprobante.objects.exclude(status=0).get(codigo="cch")
    dia_actual = datetime.datetime.now().strftime('%Y-%m-%d')
    ###########################################
    #          Anula el comprobante           #
    ###########################################
    try:
        compra_caja_chica = Cabecera_Caja_Chica.objects.get(id=id)
        detalle_comp_caja_chica = DetalleCajaChica.objects.filter(cabecera_caja_chica=compra_caja_chica)
        compra_caja_chica.status = 2
        compra_caja_chica.save()

        for obj in detalle_comp_caja_chica:
            obj.status = 2
            obj.save()

        messages.success(request, u"La compra se ha anulado con éxito")
    except:
        transaction.rollback()
        messages.error(request, u"Existió un problema al anular la compra")
        return HttpResponseRedirect(reverse("caja_chica"))



    compra_caja_nueva = Cabecera_Caja_Chica.objects.exclude(status=0).get(id=id)
    ats = ATS.objects.filter(status=1).filter(compra_pk=compra_caja_nueva.id)

    now = datetime.datetime.now()
    dia_actual = datetime.datetime.now().strftime('%Y-%m-%d')
    contador = 0
    cursor = connection.cursor()
    tipo = 3
    tipo_c = TipoComprobante.objects.get(codigo="cch")

    formulario = FormCabeceraCaja(initial={
        "fecha": compra_caja_nueva.fecha.strftime('%Y-%m-%d'),
        "concepto": compra_caja_nueva.concepto,
        "responsable": compra_caja_nueva.plan_cuenta.id
    })

    ###################################### Formulario de Detalle de Caja Chica #########################################
    det_form = []
    multiform = formset_factory(FormDetalleCajaChica, extra=0)

    if (ats):
        for obj in ats:
            if obj.getDatos().codigo_sri.porcentaje == 12:
                det_form.append({"cuenta": obj.getDatos().plan_cuenta.descripcion+"-"+obj.getDatos().plan_cuenta.codigo,
                                 "tipo_identificacion": obj.identificacion.codigo, "proveedor": "0"+obj.num_id,
                                 "sustento_tributario": obj.sustento_tributario_sri.id, "tipo_documento": obj.documento.id, "porc_iva": obj.getDatos().codigo_sri.codigo+"-"+str(obj.getDatos().codigo_sri.porcentaje),
                                 "serie": obj.num_doc, "fecha_emision": obj.fecha_emi.strftime("%Y-%m-%d"), "num_autorizacion": obj.autorizacion,
                                 "fecha_vencimiento": obj.vence.strftime("%Y-%m-%d"),
                                 "cto_gto": obj.getDatos().iva_cg, "valor": obj.baseiva})
            else:

                det_form.append({"cuenta": obj.getDatos().plan_cuenta.descripcion+"-"+obj.getDatos().plan_cuenta.codigo,
                                 "tipo_identificacion": obj.identificacion.codigo, "proveedor": "0"+obj.num_id,
                                 "sustento_tributario": obj.sustento_tributario_sri.id, "tipo_documento": obj.documento.id, "porc_iva": obj.getDatos().codigo_sri.codigo+"-"+str(obj.getDatos().codigo_sri.porcentaje),
                                 "serie": obj.num_doc, "fecha_emision": obj.fecha_emi.strftime("%Y-%m-%d"), "num_autorizacion": obj.autorizacion,
                                 "fecha_vencimiento": obj.vence.strftime("%Y-%m-%d"),
                                 "cto_gto": obj.getDatos().iva_cg, "valor": obj.base0})

    formulario_detalle_compra = multiform(initial=det_form, prefix="detalle_compra_caja")


    ##############################################################################################################

    try:
        if(request.method=="POST"):
            cabecera_compra_caja = Cabecera_Caja_Chica()
            formulario = FormCabeceraCaja(request.POST)
            formulario_detalle_compra = multiform(request.POST, request.FILES, prefix="detalle_compra_caja")
            tmonto0 = 0.0
            tmontoi = 0.0
            total_iva = 0.0

            ####### Calcula valores totales ##########
            if formulario_detalle_compra.is_valid():
                for form in formulario_detalle_compra:
                    informacion = form.cleaned_data
                    iva = 0.0
                    valor = 0.0
                    iva = float(str(informacion.get("porc_iva")).split("-")[1])
                    print iva

                    if informacion.get("valor") != "":
                        valor = float(informacion.get("valor"))
                    if iva == 0.0:
                        tmonto0 = tmonto0 + valor
                    else:
                        tmontoi = tmontoi + valor
                total_iva = tmontoi * IVA
                print total_iva

            if formulario.is_valid():
                cabecera_caja = Cabecera_Caja_Chica()
                cabecera_comprobante = Cabecera_Comp_Contable()
                fecha = datetime.datetime.strptime(formulario.getFecha(), "%Y-%m-%d").date()
                cabecera_caja.concepto = formulario.getConcepto()
                ##################### Cabecera Comprobante Contable #####################
                cabecera_comprobante.fecha = fecha
                cabecera_comprobante.concepto_comprobante = cabecera_caja.concepto
                cursor.execute("select get_next_seq(%s,8);", [fecha])
                total_rows = cursor.fetchone()
                cabecera_comprobante.numero_comprobante = total_rows[0]
                cabecera_comprobante.tipo_comprobante = tipo_c
                cabecera_comprobante.fecha_creacion = now
                cabecera_comprobante.usuario_creacion = request.user.username
                cabecera_comprobante.save()

                ###################### Cabecera de Caja Chica #############################
                cabecera_caja.fecha = cabecera_comprobante.fecha
                cabecera_caja.numero_comprobante = cabecera_comprobante.numero_comprobante
                cabecera_caja.base_ice = 0.0
                cabecera_caja.monto_ice = 0.0
                cabecera_caja.cabecera_comprobante_contable = cabecera_comprobante
                cabecera_caja.save()

                ######################## DetalleCajaChica ##################################
                print formulario_detalle_compra.errors
                if formulario_detalle_compra.is_valid() and formulario.is_valid():
                    for form in formulario_detalle_compra:
                        informacion = form.cleaned_data
                        detalle_compra_chica = DetalleCajaChica()
                        detalle_compra_chica.cabecera_caja_chica = cabecera_caja

                        codigo = unicode(informacion.get("cuenta"))
                        codigo2 = codigo.split("-")[1]
                        detalle_compra_chica.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(codigo=codigo2)
                        detalle_compra_chica.tipo_identificacion = Identificacion.objects.exclude(status=0).get(codigo=informacion.get("tipo_identificacion"))
                        detalle_compra_chica.proveedorcchica = informacion.get("proveedor")
                        detalle_compra_chica.sust_trib = Sustento_Tributario.objects.exclude(status=0).get(id=informacion.get("sustento_tributario"))
                        detalle_compra_chica.tipo_doc = Documento.objects.exclude(status=0).get(id=informacion.get("tipo_documento"))
                        detalle_compra_chica.fecha_emision = informacion.get("fecha_emision")
                        detalle_compra_chica.fecha_vencimiento = informacion.get("fecha_vencimiento")
                        detalle_compra_chica.codigo_iva = str(informacion.get("porc_iva")).split("-")[0]
                        detalle_compra_chica.porcentaje_iva = float(str(informacion.get("porc_iva")).split("-")[1])
                        b_s = informacion.get("bs")
                        detalle_compra_chica.b_s = b_s
                        detalle_compra_chica.serie = informacion.get("serie")
                        detalle_compra_chica.numero_autorizacion = informacion.get("num_autorizacion")
                        detalle_compra_chica.valor = informacion.get("valor")
                        total = (tmonto0 + tmontoi + total_iva)

                        if detalle_compra_chica.valor + total_iva >= 1000:
                            detalle_compra_chica.forma_pago = Forma_Pago_SRI.objects.exclude(status=0).get(codigo =informacion.get("id_info"))

                        ##################################################
                        #  validación de ruc ó cedula ingresada          #
                        ##################################################
                        if len(detalle_compra_chica.proveedorcchica) != 10 and len(detalle_compra_chica.proveedorcchica) != 13:
                            errors = formulario._errors.setdefault("ruc", ErrorList())
                            messages.error(request, u"Número de Identificación incorrecto.")
                            contador += 1
                        else:
                            contador = 0

                        if detalle_compra_chica.tipo_identificacion.codigo == "R" and len(detalle_compra_chica.proveedorcchica) == 13:
                            if isPersonaJuridica(detalle_compra_chica.proveedorcchica) or isEmpresaPublica(detalle_compra_chica.proveedorcchica) or isPersonaNatural(detalle_compra_chica.proveedorcchica):
                                contador = 0
                            else:
                                if contador == 0:
                                    errors = formulario._errors.setdefault("ruc", ErrorList())
                                    messages.error(request, u"Número de Identificación incorrecto.")
                                    contador += 1
                        else:
                            if detalle_compra_chica.tipo_identificacion.codigo == "C" and len(detalle_compra_chica.proveedorcchica) == 10:
                                if isCedula(detalle_compra_chica.proveedorcchica):
                                    contador = 0
                                else:
                                    if contador == 0:
                                        errors = formulario._errors.setdefault("cedula", ErrorList())
                                        messages.error(request, u"Número de Identificación incorrecto.")
                                        contador += 1
                            else:
                                if contador == 0:
                                    errors = formulario._errors.setdefault("cedula", ErrorList())
                                    messages.error(request, u"Número de Identificación incorrecto.")
                                    contador += 1

                        if detalle_compra_chica.tipo_identificacion.codigo == "P":
                            contador = 0

                        ##################################################
                        #  validación de posibles documentos repetidos.  #
                        ##################################################
                        lista_compras_caja = DetalleCajaChica.objects.exclude(status=2)
                        for c in lista_compras_caja:
                            if c.proveedorcchica == detalle_compra_chica.proveedorcchica and c.tipo_doc.codigo_documento == detalle_compra_chica.tipo_doc.codigo_documento \
                                and c.serie == detalle_compra_chica.serie and c.numero_autorizacion == detalle_compra_chica.numero_autorizacion and c.status == detalle_compra_chica.status:
                                errors = formulario._errors.setdefault("serie", ErrorList())
                                messages.error(request, u"Número de Documento ya ingresado, por favor verifique el número.")
                                contador += 1

                        ############################################
                        #    Validación de Pago (local-exterior)   #
                        ############################################
                        '''
                        if(informacion.get("pago")=="2"):
                            if(informacion.get("pais") == ""):
                                errors = formulario._errors.setdefault("pais", ErrorList())
                                errors.append(u"Usted debe de ingresar el país desde donde vino la compra")
                                contador += 1
                            else:
                                detalle_compra_chica.pais = Pais.objects.get(codigo=informacion.get("pais"))
                                print detalle_compra_chica.pais.descripcion

                                try:
                                    detalle_compra_chica.sujeto_reten = informacion.get("sujeto_reten")
                                except:
                                    pass
                                try:
                                    detalle_compra_chica.conv_doble_trib = informacion.get("conv_doble_tributacion")
                                except:
                                    pass
                        '''

                        detalle_compra_chica.pago = Tipo_Pago_SRI.objects.get(codigo=informacion.get("pago"))
                        detalle_compra_chica.fecha_actualizacion = now
                        detalle_compra_chica.fecha_creacion = now
                        detalle_compra_chica.usuario_actualizacion = request.user.username
                        detalle_compra_chica.usuario_creacion = request.user.username

                        if contador > 0:
                            raise ErrorComprasCajaChica("Algunos campos no son válidos")
                        else:
                            detalle_compra_chica.save()
                            cabecera_caja.total_pagar = total
                            cabecera_caja.tlbase0 = tmonto0
                            cabecera_caja.tlbaseiva = tmontoi
                            cabecera_caja.save()

                        ###############################################
                        #         Detalle de comprobante Contable     #
                        ###############################################
                        detalle_comp_cont = Detalle_Comp_Contable()
                        detalle_comp_cont.cabecera_contable = cabecera_comprobante
                        detalle_comp_cont.centro_costo = Centro_Costo.objects.get(id=1)
                        detalle_comp_cont.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(codigo=codigo2)
                        detalle_comp_cont.valor = informacion.get("valor")
                        detalle_comp_cont.dbcr = "D"
                        detalle_comp_cont.detalle = PlanCuenta.objects.exclude(status=0).get(codigo=codigo2).descripcion

                        detalle_comp_cont.fecha_actualizacion = now
                        detalle_comp_cont.fecha_creacion = now
                        detalle_comp_cont.usuario_actualizacion = request.user.username
                        detalle_comp_cont.usuario_creacion = request.user.username
                        detalle_comp_cont.save()

                    #########################################
                    #         Cuenta de Iva Pagado          #
                    #########################################
                    detalle_comp_iva = Detalle_Comp_Contable()
                    detalle_comp_iva.cabecera_contable = cabecera_comprobante
                    detalle_comp_iva.centro_costo = Centro_Costo.objects.get(id=1)
                    detalle_comp_iva.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(codigo="110502001")
                    detalle_comp_iva.valor = total_iva
                    detalle_comp_iva.dbcr = "D"
                    detalle_comp_iva.detalle = PlanCuenta.objects.exclude(status=0).get(codigo="110502001").descripcion

                    detalle_comp_iva.fecha_actualizacion = now
                    detalle_comp_iva.fecha_creacion = now
                    detalle_comp_iva.usuario_actualizacion = request.user.username
                    detalle_comp_iva.usuario_creacion = request.user.username
                    detalle_comp_iva.save()

                    ########################################## Detalle Comprobante Contable ###################################################
                    ###########################)============== Cuenta Proveedores ====================################
                    detalle_comprobante = Detalle_Comp_Contable()
                    detalle_comprobante.cabecera_contable = cabecera_comprobante
                    detalle_comprobante.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(codigo=codigo2)
                    detalle_comprobante.centro_costo = Centro_Costo.objects.get(id=1)
                    detalle_comprobante.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(codigo="210101001")
                    detalle_comprobante.dbcr = "H"
                    detalle_comprobante.valor = (tmontoi+total_iva+tmonto0)
                    detalle_comprobante.detalle = PlanCuenta.objects.exclude(status=0).get(codigo="210101001").descripcion

                    detalle_comprobante.fecha_actualizacion = now
                    detalle_comprobante.fecha_creacion = now
                    detalle_comprobante.usuario_actualizacion = request.user.username
                    detalle_comprobante.usuario_creacion = request.user.username
                    detalle_comprobante.save()
                else:
                    contador += 1
            else:
                contador += 1

            if contador > 0:
                raise ErrorComprasCajaChica("Algunos campos no son válidos")
            else:
                messages.success(request, u"La compra se ha grabado con éxito")
                return HttpResponseRedirect(reverse("lista_compras_caja_chica"))

    except ErrorComprasCajaChica, e:
        transaction.rollback()
        messages.error(request, str(e))

    return render_to_response('caja_chica/caja_chica.html',
                               {"formulario":formulario, "multiform": formulario_detalle_compra, "tipo": tipo,
                                   "id": id, "request": request, "dia": str(dia_actual), "documentos": documentos}, context_instance=RequestContext(request))





def generar_pdf(template_src, context_dict):
    template = get_template(template_src)
    context = Context(context_dict)
    html = template.render(context)
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), mimetype='application/pdf')
    return HttpResponse('Existen algunos errores<pre>%s</pre>' % escape(html))
@login_required(login_url='/')
@csrf_exempt
def detalle_compra_caja_chica_pdf(request, id):
    compra_cabecera_caja_chica = Cabecera_Caja_Chica.objects.get(id=id)
    detalle_compra = DetalleCajaChica.objects.exclude(status=0).filter(cabecera_caja_chica=compra_cabecera_caja_chica)
    mayor = Cabecera_Comp_Contable.objects.get(numero_comprobante=compra_cabecera_caja_chica.numero_comprobante)
    detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=mayor)
    ats = ATS.objects.filter(status=1).filter(compra_pk=compra_cabecera_caja_chica.id)

    return generar_pdf('caja_chica/detalle_caja_chica_pdf.html',
                              {'pagesize': 'A4',
                               "compra": compra_cabecera_caja_chica,
                               "detalles":detalle_compra,
                               "ats": ats,
                               "mayor_cab": mayor,
                               "detalle_may": detalle_mayor,
                               "fecha_actual": datetime.datetime.now()})
@login_required(login_url='/')
@csrf_exempt
def detalle_compra_caja_chica(request, id):
    compra_caja_chica = Cabecera_Caja_Chica.objects.get(id=id)
    detalle_compra_caja = DetalleCajaChica.objects.exclude(status=0).filter(cabecera_caja_chica=compra_caja_chica)
    ats = ATS.objects.filter(status=1).filter(compra_pk=compra_caja_chica.id)

    cabecera_mayor = Cabecera_Comp_Contable.objects.get(numero_comprobante=compra_caja_chica.numero_comprobante)
    detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_mayor)

    return render_to_response('caja_chica/detalle_caja_chica.html',
                              {"compra": compra_caja_chica, "request":request,
                               "detalles":detalle_compra_caja,
                               "ats": ats,
                               "mayor_cab": cabecera_mayor,
                               "detalle_may": detalle_mayor}, context_instance=RequestContext(request))
@login_required(login_url='/')
@transaction.commit_on_success
def anular_compra_caja_chica(request, id):
    try:
        compra_caja_chica = Cabecera_Caja_Chica.objects.get(id=id)
        detalle_comp_caja_chica = DetalleCajaChica.objects.filter(cabecera_caja_chica=compra_caja_chica)
        cabecera_asiento = Cabecera_Comp_Contable.objects.get(numero_comprobante=compra_caja_chica.numero_comprobante)
        detalle_asiento = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera_asiento)

        compra_caja_chica.status = 2
        compra_caja_chica.save()

        for obj in detalle_comp_caja_chica:
            obj.status = 2
            obj.save()

        if compra_caja_chica.status == 2:
            cabecera_asiento.status = 2
            cabecera_asiento.save()

            for d in detalle_asiento:
                d.status = 2
                d.save()

        messages.success(request, u"La compra se ha anulado con éxito")
    except:
        transaction.rollback()
        messages.error(request, u"Existió un problema al anular la compra")
    return HttpResponseRedirect(reverse("lista_compras_caja_chica"))
@login_required(login_url='/')
@csrf_exempt
def buscar_cuenta(request):
    q = request.GET.get("term", "")
    try:
        cuentas = PlanCuenta.objects.exclude(status=0).exclude(tipo__alias=2).filter(status=1, nivel=5).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q))[0:5]
        respuesta = []
        for p in cuentas:
            respuesta.append({"value": p.codigo+"-"+p.descripcion, "key": p.codigo+"-"+p.descripcion})
    except:
        respuesta = []
        respuesta.append({"value": "error base", "key": "error"})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')


