#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
import datetime
from django.db.models import Q
from contabilidad.formularios.CategoriaItemForm import *
from librerias.funciones.paginacion import *
from django.forms.util import ErrorList
from django.core.paginator import *
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django import template
from django.core.paginator import *
from librerias.funciones.permisos import *
import operator
from librerias.funciones.funciones_vistas import *
register = template.Library()

def busqueda_categoria_item(buscador):
    """
    Funcion para buscar categoria_item(buscador)
    """
    predicates = []
    predicates.append(('descripcion__icontains', buscador.getDescripcion()))
    if buscador.getTipoItem() is not None:
        predicates.append(('tipo_item', buscador.getTipoItem()))
    if buscador.getCuentaCostoVenta() is not None:
        predicates.append(('plan_cuenta_costo_venta', buscador.getCuentaCostoVenta()))
    if buscador.getCuentaInventario() is not None:
        predicates.append(('plan_cuenta_compra', buscador.getCuentaInventario()))
    if buscador.getCuentaVenta() is not None:
        predicates.append(('plan_cuenta_venta', buscador.getCuentaVenta()))

    q_list = [Q(x) for x in predicates]
    entries = Categoria_Item.objects.filter(reduce(operator.and_, q_list)).distinct().distinct()
    return entries

@login_required(login_url='/')
def lista_categoria(request):
    buscador = BusquedaCategoriaItemForm(request.GET)
    categorias = busqueda_categoria_item(buscador)
    paginacion = Paginator(categorias, get_num_filas_template())
    numero_pagina = request.GET.get("page")
    cantidad_elementos = len(categorias)
    try:
        categorias = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        categorias = paginacion.page(1)
    except EmptyPage:
        categorias = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = categorias.number
    lista = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('Categoria_Item/lista_categorias.html',
                              {"objetos": categorias,
                               "total_paginas": total_paginas,
                               "arreglo_paginado": lista,
                               "numero": numero,
                               "buscador": buscador,
                               "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                               "cantidad_elementos": cantidad_elementos}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def registrar_categoria_item(request):
    '''
    Vista que realiza el proceso de guardar la categoría del item
    :param request:
    :return:
    '''
    today = datetime.datetime.now()
    contador = 0
    formulario = categoriaItemForm()

    if request.method == "POST":
        formulario = categoriaItemForm(request.POST)

        if formulario.is_valid():
            categoria = Categoria_Item()
            categoria.descripcion = formulario.getNombre()
            categoria.tipo_item = Tipo_Item.objects.filter(status=1).get(id=formulario.geTipo())

            if categoria.tipo_item_id == 1:  # Productos
                if formulario.getCuentaCompra() != "":
                    categoria.plan_cuenta_compra = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaCompra())
                else:
                    contador += 1

                '''
                if formulario.getCuentaCostoVenta() != "":
                    categoria.plan_cuenta_costo_venta = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaCostoVenta())
                else:
                    contador += 1
                if formulario.getCuentaInventarioProceso() != "":
                    categoria.plan_cuenta_inventario_proceso = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaInventarioProceso()).id
                if formulario.getCuentaPRoductoTerminado() != "":
                    categoria.plan_cuenta_producto_terminado = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaPRoductoTerminado()).id
                '''

            categoria.plan_cuenta_venta = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaVenta())
            categoria.plan_cuenta_descuento_venta = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaDescuentoVenta())
            categoria.plan_cuenta_devolucion_venta = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaDevolucionVenta())

            if contador == 0:
                categoria.fecha_creacion = today
                categoria.usuario_creacion = request.user.username
                categoria.save()
                messages.success(request, u"La Categoría  se  registró exitósamente")
                return HttpResponseRedirect(reverse("lista_categoria"))
            else:
                transaction.rollback()
                messages.error(request, u"Existió un error al ingresar las cuentas requeridas para "
                                        u"generar la categoría  de item")

        else:

            lista_errores = u"Por favor verifique los siguientes campos: "
            for i in formulario.errors:
                lista_errores = lista_errores +(unicode(i)) + ", "
            if formulario.errors:
                messages.error(request, unicode(lista_errores[0:-2]))
            for i in formulario:
                if i.errors:
                    i.field.widget.attrs['class'] = 'selectpicker campo_requerido'

    return render_to_response('Categoria_Item/registrar_categoria.html', {"formulario": formulario, "tipo": 1,
                                                                          "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def detalle_categoria_item(request, id):
    tipo = 3
    id_cuenta_compra = 0
    id_cuenta_costo_venta = 0
    id_cuenta_inv_proceso = 0
    id_cuenta_producto_terminado = 0
    id_cuenta_descuento_venta = 0
    id_cuenta_devolucion_venta = 0

    try:
        categoria = Categoria_Item.objects.get(id=id)

        if categoria.tipo_item_id == 1:   # Productos
            id_cuenta_compra = categoria.plan_cuenta_compra.id
            '''
            try:
                if categoria.plan_cuenta_costo_venta is not None:
                    id_cuenta_costo_venta = categoria.plan_cuenta_costo_venta.id
            except PlanCuenta.DoesNotExist:
                messages.info(request, u"La categoría de item: "+unicode(categoria.descripcion)+u" no posee las cuentas "
                                                                u"de COSTO DE VENTA")
            try:
                if categoria.plan_cuenta_inventario_proceso is not None:
                    id_cuenta_inv_proceso = categoria.plan_cuenta_inventario_proceso.id
            except PlanCuenta.DoesNotExist:
                messages.info(request, u"La categoría de item: "+unicode(categoria.descripcion)+u" no posee las cuentas "
                                                                u"de INVENTARIO PROCESO")
            try:
                if categoria.plan_cuenta_producto_terminado is not None:
                    id_cuenta_producto_terminado = categoria.plan_cuenta_producto_terminado.id
            except PlanCuenta.DoesNotExist:
                messages.info(request, u"La categoría de item: "+unicode(categoria.descripcion)+u" no posee las cuentas "
                                                                u"de PRODUCTO TERMINADO")
            '''
        try:
            if categoria.plan_cuenta_descuento_venta is not None:
                id_cuenta_descuento_venta = categoria.plan_cuenta_descuento_venta.id
        except PlanCuenta.DoesNotExist:
            messages.info(request, u"La categoría de item: "+unicode(categoria.descripcion)+u" no posee las cuentas "
                                                                u"de DESCUENTO EN VENTA")
        try:
            if categoria.plan_cuenta_devolucion_venta is not None:
                id_cuenta_devolucion_venta = categoria.plan_cuenta_devolucion_venta.id
        except PlanCuenta.DoesNotExist:
            messages.info(request, u"La categoría de item: "+unicode(categoria.descripcion)+u" no posee las cuentas "
                                                                u"DEVOLUCIÓN EN VENTA")

        formulario = CategoriaItemFormReadOnly(initial={"nombre": categoria.descripcion,
                                                        "tipo": categoria.tipo_item.id,
                                                        "cuenta_compra": id_cuenta_compra,
                                                        "cuenta_venta": categoria.plan_cuenta_venta.id,
                                                        "cuenta_costo_venta": id_cuenta_costo_venta,
                                                        "cuenta_inv_proceso": id_cuenta_inv_proceso,
                                                        "cuenta_producto_terminado": id_cuenta_producto_terminado,
                                                        "cuenta_devolucion_venta": id_cuenta_devolucion_venta,
                                                        "cuenta_descuento_venta": id_cuenta_descuento_venta})
    except:
        messages.error(request, u"Existen errores al obtener la información "
                               u"de la categoría de item")

        return HttpResponseRedirect(reverse("lista_categoria"))

    return render_to_response('Categoria_Item/registrar_categoria.html',
                              {"objetos": categoria,
                               "formulario": formulario,
                               "tipo": tipo,
                               "id": id}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def editar_categoria_item(request, id):
    now_ac = datetime.datetime.now()
    tipo = 2
    contador = 0
    id_cuenta_compra = 0
    id_cuenta_costo_venta = 0
    id_cuenta_inv_proceso = 0
    id_cuenta_producto_terminado = 0
    id_cuenta_devolucion_venta = 0
    id_cuenta_descuento_venta = 0

    try:
        categoria_nueva = Categoria_Item.objects.get(id=id)

        if categoria_nueva.isCategoriaTransaccion():
            messages.error(request, u"Error la categoría no puede ser editara debido a que ya se han "
                                    u"realizado transacciones con la misma")
            return HttpResponseRedirect(reverse("lista_categoria"))


        try:
            if categoria_nueva.plan_cuenta_descuento_venta is not None:
                id_cuenta_descuento_venta = categoria_nueva.plan_cuenta_descuento_venta
        except PlanCuenta.DoesNotExist:
            messages.info(request, u"La categoría de item: "+unicode(categoria_nueva.descripcion)+u" no posee las cuentas "
                                                                u"de DESCUENTO EN VENTA")

        try:
            if categoria_nueva.plan_cuenta_devolucion_venta is not None:
                id_cuenta_devolucion_venta = categoria_nueva.plan_cuenta_devolucion_venta
        except PlanCuenta.DoesNotExist:
            messages.info(request, u"La categoría de item: "+unicode(categoria_nueva.descripcion)+u" no posee las cuentas "
                                                                u"DEVOLUCIÓN EN VENTA")

        if categoria_nueva.tipo_item.id == 1:   # Productos
            id_cuenta_compra = categoria_nueva.plan_cuenta_compra
            '''
            id_cuenta_costo_venta = categoria_nueva.plan_cuenta_costo_venta.id

            try:
                if categoria_nueva.plan_cuenta_inventario_proceso is not None:
                    id_cuenta_inv_proceso = categoria_nueva.plan_cuenta_inventario_proceso.id
            except PlanCuenta.DoesNotExist:
                messages.info(request, u"La categoría de item: "+unicode(categoria_nueva.descripcion)+u" no posee las cuentas "
                                                                u"de INVENTARIO PROCESO")
            try:
                if categoria_nueva.plan_cuenta_producto_terminado is not None:
                    id_cuenta_producto_terminado = categoria_nueva.plan_cuenta_producto_terminado.id
            except PlanCuenta.DoesNotExist:
                messages.info(request, u"La categoría de item: "+unicode(categoria_nueva.descripcion)+u" no posee las cuentas "
                                                                u"de PRODUCTO TERMINADO")
            '''
        formulario = categoriaItemForm(initial={
            "nombre": categoria_nueva.descripcion,
            "tipo": categoria_nueva.tipo_item.id,
            "cuenta_compra": id_cuenta_compra,
            "cuenta_venta": categoria_nueva.plan_cuenta_venta.id,
            "cuenta_costo_venta": id_cuenta_costo_venta,
            "cuenta_inv_proceso": id_cuenta_inv_proceso,
            "cuenta_producto_terminado": id_cuenta_producto_terminado,
            "cuenta_cuenta_devolucion_venta": id_cuenta_devolucion_venta,
            "cuenta_cuenta_descuento_venta": id_cuenta_descuento_venta,
        })

    except:
        messages.error(request, u"Error no pudo obtener la información de "
                                u"la Categoría de Item")
        return HttpResponseRedirect(reverse("lista_categoria"))

    if request.method == "POST":
        formulario = categoriaItemForm(request.POST)
        if formulario.is_valid():
            categoria_nueva.descripcion = formulario.getNombre()
            categoria_nueva.tipo_item = Tipo_Item.objects.filter(status=1).get(id=formulario.geTipo())

            categoria_nueva.plan_cuenta_venta = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaVenta())
            categoria_nueva.plan_cuenta_descuento_venta = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaDescuentoVenta())
            categoria_nueva.plan_cuenta_devolucion_venta = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaDevolucionVenta())

            if categoria_nueva.tipo_item.id == 1:  # Productos
                if formulario.getCuentaCompra() != "":
                    categoria_nueva.plan_cuenta_compra = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaCompra())
                else:
                    contador += 1

                '''
                if formulario.getCuentaCostoVenta() != "":
                    categoria_nueva.plan_cuenta_costo_venta = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaCostoVenta())
                else:
                    contador += 1
                if formulario.getCuentaInventarioProceso() != "":
                    categoria_nueva.plan_cuenta_inventario_proceso = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaInventarioProceso())
                if formulario.getCuentaPRoductoTerminado() != "":
                    categoria_nueva.plan_cuenta_producto_terminado = PlanCuenta.objects.filter(status=1).get(id=formulario.getCuentaPRoductoTerminado())
                '''

            if contador == 0:
                categoria_nueva.usuario_actualizacion = request.user.username
                categoria_nueva.fecha_actualizacion = now_ac
                categoria_nueva.save()
                messages.success(request, u"La Categoría de Item: "+unicode(categoria_nueva.descripcion)+
                                          u" ha sido modificada exitósamente")
                return HttpResponseRedirect(reverse("lista_categoria"))

            else:
                 transaction.rollback()
                 messages.error(request, u"Existió un error al ingresar las cuentas requeridas para "
                                        u"generar la categoría item")

        else:

            lista_errores = "Por favor verifique los siguientes campos: "
            transaction.rollback()

            for i in formulario.errors:
                lista_errores = lista_errores +(unicode(i)) + ", "
            if formulario.errors:
                messages.error(request, unicode(lista_errores[0:-2]))
            for i in formulario:
                if i.errors:
                    i.field.widget.attrs['class'] = 'selectpicker campo_requerido'

    return render_to_response('Categoria_Item/registrar_categoria.html',
                              {"formulario": formulario,
                               "request": request,
                               "tipo": tipo,
                               "id": id,
                                "objeto": categoria_nueva}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def eliminar_categoria_item(request, id):
    contador = 0
    try:
        categoria_eliminar = Categoria_Item.objects.get(id=id)
        now_ac = datetime.datetime.now()

        if Item.objects.filter(categoria_item=categoria_eliminar).exists():
            contador += 1
            messages.error(request, u'Error, La categoría  : "'+unicode(categoria_eliminar.descripcion) + u'" no puede ser eliminada, '
                                                                                                          u'dado que posee items asociados.')


        elif Detalle_Comp_Contable.objects.filter(plan_cuenta=categoria_eliminar.plan_cuenta_venta).exists():
            contador += 1
            messages.error(request, u"Error la categoría no puede ser editara debido a que ya se han "
                                    u"realizado transacciones con la misma")


        if contador == 0:
            categoria_eliminar.status = 0
            categoria_eliminar.usuario_actualizacion = request.user.username
            categoria_eliminar.fecha_actualizacion = now_ac
            categoria_eliminar.save()
            messages.success(request, u'La categoría  : "'+unicode(categoria_eliminar.descripcion)+ u'" ha sido eliminada exitosamente')
            return HttpResponseRedirect(reverse("lista_categoria"))

        else:
            transaction.rollback()

    except:
        transaction.rollback()
        messages.error(request, u"Existe un error al eliminar la Categoría de Item")

    return HttpResponseRedirect(reverse("lista_categoria"))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def cambiar_estado_categoria_item(request, id):
    try:
        categoria = Categoria_Item.objects.get(id=id)
        now_ac = datetime.datetime.now()

        if categoria.status == 1:
            categoria.status = 2
            categoria.usuario_actualizacion = request.user.username
            categoria.fecha_actualizacion = now_ac

            messages.success(request, u'Se inactivó la categoría : "'+categoria.descripcion + u'" exitosamente')

        elif categoria.status == 2:
            categoria.status = 1
            categoria.usuario_actualizacion = request.user.username
            categoria.fecha_actualizacion = now_ac

            messages.success(request, u'Se activó la categoría : "'+categoria.descripcion + u'" exitosamente')

        categoria.save()
    except:
        messages.error(request(), u"Existieron problemas al cambiar estado de la categoría")
    return HttpResponseRedirect(reverse("lista_categoria"))


# AJAX
@login_required(login_url='/')
@csrf_exempt
def buscar_cuenta(request):
    q = request.GET.get("term", "")
    try:
        cuentas = PlanCuenta.objects.exclude(status=0).exclude(tipo__alias=2).filter(status=1, nivel=5).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q))[0:5]
        respuesta = []
        for p in cuentas:
            respuesta.append({"value": p.descripcion+"-"+p.codigo, "key": p.descripcion+"-"+p.codigo})
    except:
        respuesta = []
        respuesta.append({"value": "error base", "key": "error"})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def buscar_cuenta_inventario_compra(request):
    q = request.GET.get("term", "")
    try:
        cuentas = PlanCuenta.objects.exclude(status=0).exclude(tipo__alias=2).filter(status=1, nivel=5, tipo=4).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q))[0:5]
        respuesta = []
        for p in cuentas:
            respuesta.append({"value": p.descripcion+"-"+p.codigo, "key": p.descripcion+"-"+p.codigo})
    except:
        respuesta = []
        respuesta.append({"value": "error base", "key": "error"})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def buscar_cuenta_costo_venta(request):
    q = request.GET.get("term", "")
    try:
        cuentas = PlanCuenta.objects.exclude(status=0).exclude(tipo__alias=2).filter(status=1, nivel=5, tipo=6).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q))[0:8]
        respuesta = []
        for p in cuentas:
            respuesta.append({"value": p.descripcion+"-"+p.codigo, "key": p.descripcion+"-"+p.codigo})
    except:
        respuesta = []
        respuesta.append({"value": "error base", "key": "error"})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def buscar_cuenta_venta(request):
    q = request.GET.get("term", "")
    try:
        cuentas = PlanCuenta.objects.exclude(status=0).exclude(tipo__alias=2).filter(status=1, nivel=5, tipo=5).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q))[0:5]
        respuesta = []
        for p in cuentas:
            respuesta.append({"value": p.descripcion+"-"+p.codigo, "key": p.descripcion+"-"+p.codigo})
    except:
        respuesta = []
        respuesta.append({"value": "error base", "key": "error"})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')