#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.core.context_processors import request
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import *
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.core.paginator import *
from contabilidad.models import *
from administracion.models import *
from contabilidad.formularios.ColaboradoresForm import *
from clipmaestro.templatetags.formulario_base import *
from sys import modules
from sys import path
import operator
import datetime
import time
from django.forms.util import ErrorList
from django.core.paginator import *
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from librerias.funciones.paginacion import *
from django.forms.formsets import formset_factory
from django.core.context_processors import request
from django.shortcuts import render_to_response
from django.http import *
from django.forms.util import ErrorList
from django.db import connection
from django.db import IntegrityError, transaction
from librerias.funciones.funciones_vistas import *
from librerias.funciones.permisos import *
import json

__author__ = 'Roberto'

def busqueda_colaboradores(buscador):
    '''
    Función que realiza las búsquedas
    :param buscador:
    :return:
    '''
    predicates = []
    predicates.append(('nombres_completos__icontains', buscador.getNombres()))
    predicates.append(('apellidos_completos__icontains', buscador.getApellidos()))
    if buscador.getFechaNacimiento() is not None:
        predicates.append(('fecha_nacimiento', buscador.getFechaNacimiento()))
    if buscador.getNumID() != "":
        predicates.append(('num_id__icontains', buscador.getNumID()))
    if buscador.getEmail() != "":
        predicates.append(('email__icontains', buscador.getEmail()))
    q_list = [Q(x) for x in predicates]
    entries = Colaboradores.objects.exclude(status=0).filter(reduce(operator.and_, q_list)).distinct()
    return entries


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def lista_vendedores(request):
    '''
    Vista que enlista todos los vendedores - colaboradores
    registrados en el sistema
    :param request:
    :return:
    '''
    buscador = FormBuscadorColaboradores(request.GET)
    colaboradores = busqueda_colaboradores(buscador)
    paginacion = Paginator(colaboradores, 10)
    numero_pagina = request.GET.get("page")

    try:
        colaboradores = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        colaboradores = paginacion.page(1)
    except EmptyPage:
        colaboradores = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = colaboradores.number
    lista = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('Colaboradores/lista_colaboradores.html',
                              {"total_paginas": total_paginas, "objetos": colaboradores,
                               "buscador": buscador,
                               "arreglo_paginado": lista,
                               "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                               "numero": numero}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def agregar_vendedores(request):
    '''
    Vista que permite agregar a los vendedores - colaboradores
    :param request:
    :return:
    '''
    tipo = 1                            # Parámetro que me indica que se va a registrar para utilizar el mismo html
    formulario = FormColaboradores()
    contador = 0.0

    if request.method == "POST":
        formulario = FormColaboradores(request.POST)

        print "formulario errors: ", formulario.errors
        if formulario.is_valid():
            colaborador = Colaboradores()
            colaborador.nombres_completos = formulario.getNombres()
            colaborador.apellidos_completos = formulario.getApellidos()
            colaborador.num_id = formulario.getNum_ID()
            colaborador.direccion = formulario.getDireccion()
            colaborador.email = formulario.getCorreo()
            if formulario.getTelefono() != "":
                colaborador.telefono = formulario.getTelefono()
            colaborador.fecha_nacimiento = formulario.getFecha_Nac()
            colaborador.sexo = formulario.getSexo()
            colaborador.estado_civil = formulario.getEstado_Civil()

            if not isCedula(colaborador.num_id):
                contador += 1
                errors = formulario._errors.setdefault("num_id", ErrorList())
                errors.append(u"")
                messages.error(request, u"El N° de Identificación es incorrecto.")

            elif Colaboradores.objects.exclude(status=0).filter(num_id=colaborador.num_id).exists():
                contador += 1
                errors = formulario._errors.setdefault("num_id", ErrorList())
                errors.append(u"")
                messages.error(request, u"El N° de Identificación ya ha sido ingresado.")

            if contador == 0.0:
                colaborador.fecha_creacion = datetime.datetime.now()
                colaborador.usuario_creacion = request.user.username
                colaborador.save()
                messages.success(request, u"Se agregó satisfactoriamente al vendedor: "+
                                 unicode(colaborador.nombres_completos)+" "+unicode(colaborador.apellidos_completos)+u"")
                return HttpResponseRedirect(reverse("lista_vendedores"))

            else:
                transaction.rollback()
        else:
            transaction.rollback()
            messages.error(request, u"Error, llene correctamente la información del formulario")

    return render_to_response('Colaboradores/agregar_colaboradores.html',
                              {"formulario": formulario, "tipo": tipo}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def editar_vendedores(request, id):
    '''
    Vista que permite editar la información de los
    vendedores - colaboradores
    :param request:
    :return:
    '''
    tipo = 2                    # Parámetro que me indica que se va a editar para utilizar el mismo html
    contador = 0.0              # Validador
    try:
        nuevo = Colaboradores.objects.filter(status=1).get(id=id)
        formulario = FormColaboradores(initial={"num_id": nuevo.num_id, "nombres": nuevo.nombres_completos,
                                                "apellidos": nuevo.apellidos_completos,
                                                "fecha_Nac": nuevo.fecha_nacimiento, "telefono": nuevo.telefono,
                                                "direccion": nuevo.direccion, "email": nuevo.email,
                                                "estado_civil": nuevo.estado_civil, "sexo": nuevo.sexo})
    except:
        messages.error(request, u"Exisitó un error al obtener la información del vendedor")
        return HttpResponseRedirect(reverse("lista_vendedores"))

    if request.method == "POST":
        formulario = FormColaboradores(request.POST)

        if formulario.is_valid():
            nuevo.nombres_completos = formulario.getNombres()
            nuevo.apellidos_completos = formulario.getApellidos()
            nuevo.num_id = formulario.getNum_ID()
            nuevo.direccion = formulario.getDireccion()
            nuevo.email = formulario.getCorreo()
            if formulario.getTelefono() != "":
                nuevo.telefono = formulario.getTelefono()
            nuevo.fecha_nacimiento = formulario.getFecha_Nac()
            nuevo.sexo = formulario.getSexo()
            nuevo.estado_civil = formulario.getEstado_Civil()

            if Colaboradores.objects.exclude(status=0).filter(Q(num_id=nuevo.num_id), ~Q(id=nuevo.id)).exists():
                contador += 1
                errors = formulario._errors.setdefault("num_id", ErrorList())
                errors.append(u"")
                messages.error(request, u"El N° de Identificación ya ha sido ingresado.")

            if not isCedula(nuevo.num_id):
                contador += 1
                errors = formulario._errors.setdefault("num_id", ErrorList())
                errors.append(u"")
                messages.error(request, u"El N° de Identificación es incorrecto.")

            if contador == 0.0:
                nuevo.fecha_actualizacion = datetime.datetime.now()
                nuevo.usuario_actualizacion = request.user.username
                nuevo.save()
                messages.success(request, u"La información del vendedor ha sido editada exitosamente")
                return HttpResponseRedirect(reverse("lista_vendedores"))
            else:
                transaction.rollback()
        else:
            transaction.rollback()
            messages.error(request, u"Error al llenar la información del formulario")

    return render_to_response('Colaboradores/agregar_colaboradores.html',
                              {"formulario": formulario, "tipo": tipo,
                               "id": id}, context_instance=RequestContext(request))


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def ver_vendedores(request, id):
    '''
    Vista que permite mostrar la información de los vendedores
    :param request:
    :param id:
    :return:
    '''
    tipo = 3

    try:
        # VENDEDOR
        vendedor = Colaboradores.objects.get(id=id)
        formulario = FormColaboradoresOnly(initial={"num_id": vendedor.num_id, "nombres": vendedor.nombres_completos,
                                                    "apellidos": vendedor.apellidos_completos,
                                                    "telefono": vendedor.telefono, "direccion": vendedor.direccion,
                                                    "fecha_Nac": vendedor.fecha_nacimiento, "email": vendedor.email,
                                                    "estado_civil": vendedor.estado_civil, "sexo": vendedor.sexo})
    except:
        messages.error(request, u"Existen errores al cargar los datos del vendedor")
        return HttpResponseRedirect(reverse("lista_vendedores"))

    return render_to_response('Colaboradores/agregar_colaboradores.html',
                              {"objeto": vendedor,
                               "id": id,
                               "formulario": formulario,
                               "tipo": tipo,
                               "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def eliminar_vendedores(request, id):
    '''
    :param request:
    :param id:
    :return:
    '''
    now_ac = datetime.datetime.now()
    try:
        vendedor = Colaboradores.objects.get(id=id)

        if vendedor.status != 0:
            vendedor.status = 0
            vendedor.usuario_actualizacion = request.user.username
            vendedor.fecha_actualizacion = now_ac
            vendedor.save()

            messages.success(request, u"El vendedor ha sido eliminado exitósamente")
            return HttpResponseRedirect(reverse("lista_vendedores"))

    except:
        messages.error(request, u"Error al eliminar al vendedor")

    return HttpResponseRedirect(reverse("lista_vendedores"))


@login_required(login_url='/')
@csrf_exempt
def cambiar_estado_vendedores(request, id):
    try:
        persona_select = Colaboradores.objects.get(id=id)
        now_ac = datetime.datetime.now()
        if persona_select.status == 1:
            persona_select.status = 2
            persona_select.usuario_actualizacion = request.user.username
            persona_select.fecha_actualizacion = now_ac
            messages.success(request, u'Se inactivó al vendedor : "'+unicode(persona_select.nombres_completos)+" "+
                                                                    unicode(persona_select.apellidos_completos) +
                                                                    u'" exitosamente')

        elif persona_select.status == 2:
            persona_select.status = 1
            persona_select.usuario_actualizacion = request.user.username
            persona_select.fecha_actualizacion = now_ac
            messages.success(request, u'Se activó al vendedor : "'+unicode(persona_select.nombres_completos)+" "+
                                                                   unicode(persona_select.apellidos_completos) +
                                                                   u'" exitosamente')
        persona_select.save()
    except:
        messages.error(request, u"Error al cambiar de estado.")
    return HttpResponseRedirect(reverse("lista_vendedores"))