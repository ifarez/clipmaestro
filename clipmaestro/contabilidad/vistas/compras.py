#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'

from django.template import RequestContext
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt, csrf_protect
import json
from contabilidad.funciones.compras_func import *
from django.forms.formsets import formset_factory
from django.contrib.auth.decorators import login_required
from contabilidad.formularios.RetencionesCuentaForm import *
from contabilidad.formularios.ComprasForm import *

from django.db import transaction
from django.db import connection
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context

from django.core.paginator import *
from django import template
from administracion.models import *
from librerias.funciones.permisos import *
from librerias.funciones.paginacion import *
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from excel_response import ExcelResponse
import xlwt
from django.db.models import Sum
import operator
from reportes.formularios.reportes_form import * # le puse esto para el reporte y los formularios
from librerias.funciones.comprobantes_electronicos import *
import requests
from smtplib import SMTPSenderRefused

register = template.Library()




#########################################
# Variables para manejar el titulo      #
# del template de compras               #
# 1 compras normales                    #
# 2 anular y generar                    #
# 3 copiar compra
# 4 Uso de provisión                    #
# nombre de la variable = "tipo"        #
#########################################


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def lista_compras(request):
    """
    function lista_compras(request)
    @ Return html file
    """
    buscador = FormBuscador(request.GET)
    compra = busqueda_compras(buscador)  # Query para la búsqueda
    paginacion = Paginator(compra, get_num_filas_template())
    numero_pagina = request.GET.get("page")
    formulario_retencion = FormCambiarRetencion()
    form_anular_ret = FormAnularRetencion()
    form_correo = FormEnvioCorreo()

    try:
        lista_cabecera = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista_cabecera = paginacion.page(1)
    except EmptyPage:
        lista_cabecera = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = lista_cabecera.number
    arreglo_paginado = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('Compras/lista_compras.html',
                              {"objetos": lista_cabecera, "total_paginas": total_paginas, "numero": numero,
                              "formulario_retencion": formulario_retencion,
                              'buscador': buscador,
                              'form_correo': form_correo,
                              "arreglo_paginado": arreglo_paginado,
                              "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                              'form_anular_ret': form_anular_ret}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def detalle_compra(request, id):
    """
    function detalle_compra(request, id)
    @ Return html file
    """
    try:
        compra = CompraCabecera.objects.exclude(status=0).get(id=id)
        detalle_compra = CompraDetalle.objects.exclude(status=0).filter(compra_cabecera=compra)
        try:
            mayor = Cabecera_Comp_Contable.objects.exclude(status=0).get(numero_comprobante=compra.num_comp)
            detalle_mayor = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=mayor).order_by("dbcr", "plan_cuenta__codigo")
        except:
            mayor = None
            detalle_mayor = None
        try:
            detalle_contrato = ContratoDetalle.objects.exclude(status=0).get(modulo_id=id, tipo_comprobante_id=1, status=1)
            contrato = ContratoCabecera.objects.exclude(status=0).get(id=detalle_contrato.contrato_cabecera.id)
        except ContratoDetalle.DoesNotExist, ContratoCabecera.DoesNotExist:
            contrato = None

        if compra.plazo is not None:
            fecha_vencimiento = (compra.fecha_emi + datetime.timedelta(days=compra.plazo)).strftime("%Y-%m-%d")
        else:
            fecha_vencimiento = ""
        try:
            retencion_cabecera = RetencionCompraCabecera.objects.exclude(status=0).filter(compra_cabecera=compra).order_by("id").reverse() [0]
            #  El exclude es cuando se haya anulado y generado una compra, el detalle de la retención se pone status = 0
            retencion_detalle = RetencionCompraDetalle.objects.exclude(status=0).filter(retencion_cabecera_compra=retencion_cabecera)
        except:
            retencion_cabecera = None
            retencion_detalle = None

        try:
            if es_reembolso_gasto(compra.documento.id):
                try:
                    reembolso = Reembolso_gasto_compra.objects.exclude(status=0).filter(compra_cabecera=compra)
                except:
                    reembolso = None
            else:
                reembolso = None
        except:
            reembolso = None
        return render_to_response('Compras/Detalle_Compra.html',
                                  {"compra": compra, "request": request,
                                   "detalles": detalle_compra,
                                   "ret_cab": retencion_cabecera,
                                   "ret_det": retencion_detalle,
                                   "reembolso": reembolso,
                                   "mayor_cab": mayor,
                                   "contrato": contrato,
                                   "fecha_vencimiento": fecha_vencimiento,
                                   "detalle_may": detalle_mayor}, context_instance=RequestContext(request))
    except CompraCabecera.DoesNotExist:
        raise Http404


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def detalle_compra_pdf(request, id):
    """
    function detalle_compra_pdf(request, id)
    @ Return pdf file
    """
    try:
        empresa = Empresa.objects.filter(status=1)[0]
        usuario = request.user.first_name+' '+request.user.last_name
        compra = CompraCabecera.objects.get(id=id)
        detalle_compra = CompraDetalle.objects.filter(compra_cabecera=compra)
        try:
            mayor = Cabecera_Comp_Contable.objects.get(numero_comprobante=compra.num_comp)
            detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=mayor).order_by("dbcr", "plan_cuenta__codigo")
        except Cabecera_Comp_Contable.DoesNotExist:
            mayor = None
            detalle_mayor = None

        if compra.plazo is not None:
            fecha_vencimiento = (compra.fecha_emi + datetime.timedelta(days=compra.plazo)).strftime("%Y-%m-%d")
        else:
            fecha_vencimiento = ""

        try:
            detalle_contrato = ContratoDetalle.objects.get(modulo_id=id, tipo_comprobante_id=1, status=1)
            contrato = ContratoCabecera.objects.get(id=detalle_contrato.contrato_cabecera.id)
        except ContratoDetalle.DoesNotExist, ContratoCabecera.DoesNotExist:
            contrato = None

        try:
            retencion_cabecera = RetencionCompraCabecera.objects.get(compra_cabecera=compra)
            retencion_detalle = RetencionCompraDetalle.objects.exclude(status=0).filter(retencion_cabecera_compra=retencion_cabecera)
        except:
            retencion_cabecera = None
            retencion_detalle = None
        try:
            if es_reembolso_gasto(compra.documento.id):
                try:
                    reembolso = Reembolso_gasto_compra.objects.filter(compra_cabecera=compra)
                except Reembolso_gasto_compra.DoesNotExist:
                    reembolso = None
            else:
                reembolso = None
        except:
            reembolso = None

        html = render_to_string('Compras/compra_detalle_pdf.html',{'pagesize': 'A4',
                                   "empresa": empresa,
                                   "usuario": usuario,
                                   "compra": compra,
                                   "request": request,
                                   "detalles": detalle_compra,
                                   "reembolso": reembolso,
                                   "ret_cab": retencion_cabecera,
                                   "ret_det": retencion_detalle,
                                   "mayor_cab": mayor,
                                   "detalle_may": detalle_mayor,
                                   "contrato": contrato,
                                   "fecha_vencimiento": fecha_vencimiento,
                                   "fecha_actual": datetime.datetime.now()}, context_instance=RequestContext(request))

        return generar_pdf_get(html)
    except CompraCabecera.DoesNotExist:
        raise Http404


@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
def tiene_retencion_compra(request):
    """
    Funcion que me retorna si la compra tiene o no retención
    return status
    """
    try:
        id_compra = request.POST.get("id", "")
        compra = CompraCabecera.objects.get(id=id_compra, status=1)
        if compra.getRetencion() is not None:
            respuesta = {"status": 1}
        else:
            respuesta = {"status": 0}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')
    except CompraCabecera.DoesNotExist:
        respuesta = {"status": 2}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
def cambiar_retencion(request):
    """
    function cambiar_retencion(request)
    @ Return html file
    """
    try:
        form = FormCambiarRetencion(request.POST)
        compra = CompraCabecera.objects.get(id=form.getId(), status=1)
        retencion = RetencionCompraCabecera.objects.filter(status=1).get(compra_cabecera=compra)
        serie = VigenciaDocEmpresa.objects.get(id=form.getSerie())
        now = datetime.datetime.now()
        respuesta = []

        if form.getNuevoNumeroRetencion() == "":
            respuesta.append({"status": "2", "respuesta": 'success', "mensaje": u'Número de retención requerido'})
            return HttpResponse(json.dumps(respuesta), content_type="application/json; charset=utf-8")
        # validar la fecha de emision este en el rango del block de retenciones
        elif form.getFechaEmi() is None:
            respuesta.append({"status": "2", "respuesta": 'success', "mensaje": u"La fecha de emisión ingresada es inválida, por favor"
                                                                                u" ingrese una fecha con formato año-mes-dia"})
            return HttpResponse(json.dumps(respuesta), content_type="application/json; charset=utf-8")
        elif form.getFechaEmi().date() > serie.fecha_vencimiento.date() or form.getFechaEmi().date() < serie.fecha_emi.date():
            respuesta.append({"status": "2", "respuesta": 'success', "mensaje": u"La fecha de emisión ingresada no esta en el rango"
                                    u" válido del block de retención, por favor verifique"})
            return HttpResponse(json.dumps(respuesta), content_type="application/json; charset=utf-8")
        elif serie.serie == retencion.vigencia_doc_empresa.serie and retencion.is_DocRep(form.getNuevoNumeroRetencion(), serie):
            respuesta.append({"status": "2", "respuesta": 'success', "mensaje": u'Esa Retención ya '
                                                                                u'se encuentra registrada en el sistema'})
            return HttpResponse(json.dumps(respuesta), content_type="application/json; charset=utf-8")
        else:
            new_retencion = RetencionCompraCabecera()
            new_retencion.numero_ret = form.getNuevoNumeroRetencion()
            new_retencion.vigencia_doc_empresa = serie
            new_retencion.fecha_emi = form.getFechaEmi()
            new_retencion.compra_cabecera = compra
            new_retencion.direccion = retencion.direccion
            new_retencion.totalretfte = retencion.totalretfte
            new_retencion.totalretiva = retencion.totalretiva

            new_retencion.fecha_creacion = now
            new_retencion.usuario_creacion = request.user.username
            new_retencion.save()

            for obj in RetencionCompraDetalle.objects.filter(status=1, retencion_cabecera_compra=retencion):
                obj.retencion_cabecera_compra = new_retencion
                obj.fecha_actualizacion = now
                obj.usuario_actualizacion = request.user.username
                obj.save()

            retencion.status = 2
            retencion.fecha_actualizacion = now
            retencion.usuario_actualizacion = request.user.username
            retencion.save()
            messages.success(request, u"El cambio de comprobante se retención se ha realizado con éxito")
            respuesta.append({"status": "1", "respuesta": 'success', "mensaje": u'Se cambió la retención con éxito'})
            return HttpResponse(json.dumps(respuesta),content_type="application/json; charset=utf-8")
    except:
        transaction.rollback()
        respuesta = []
        respuesta.append({"status": "1", "respuesta": 'error', "mensaje": u'Existió un problema al cambiar la retención'})
        return HttpResponse(json.dumps(respuesta),content_type="application/json; charset=utf-8")


@login_required(login_url='/')
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def anular_compra(request, id):
    """
    function anular_compra(request, id)
    @ Return html file
    """
    try:
        compra = CompraCabecera.objects.get(id=id)
        detalle_comp = CompraDetalle.objects.filter(compra_cabecera=compra)
        now = datetime.datetime.now()
        status = compra.status

        if not esta_periodo_contable(compra.fecha_reg):
            messages.error(request, u"La compra que desea anular se encuentra dentro del periodo del cierre contable, "
                                    u"por favor verifique")
            return HttpResponseRedirect(reverse("lista_compras"))
        else:
            try:
                cuentas_pagar = Cuentas_por_Pagar.objects.filter(status=1).get(compra_cabecera=compra)
                # Cuentas por pagar
                if cuentas_pagar.pagado > 0:
                    raise ErrorCompras(u"No se puede eliminar la compra, ya que se han hecho pagos a "
                                       u"ella valor pagado: " + str(cuentas_pagar.pagado) + u".Primero anule el pago")
                else:
                    cuentas_pagar.status = 2
                    cuentas_pagar.fecha_actualizacion = now
                    cuentas_pagar.usuario_actualizacion = request.user.username
                    cuentas_pagar.save()
            except Cuentas_por_Pagar.DoesNotExist:
                if status != 3:
                    messages.error(request, u'La compra que anuló no tenía asociado una cuenta por pagar, '
                                            u'si no esta seguro de eso, por favor comuníquese con el '
                                            u'administrador del sistema')

            # Compra
            compra.status = 2

            # Anular ATS
            for ats in ATS.objects.filter(compra_cabecera=compra):
                ats.status = 2
                ats.fecha_actualizacion = now
                ats.usuario_actualizacion = now
                ats.save()

            compra.fecha_actualizacion = datetime.datetime.now()
            compra.usuario_actualizacion = request.user.username
            compra.save()

            for obj in detalle_comp:
                obj.status = 2
                obj.fecha_actualizacion = now
                obj.usuario_actualizacion = request.user.username
                obj.save()

            # Anular retenciones si las hubiera
            try:
                retencion_cab = RetencionCompraCabecera.objects.filter(compra_cabecera=compra).order_by("id").reverse()[0]
                retencion_cab.status = 2
                retencion_cab.fecha_actualizacion = now
                retencion_cab.usuario_actualizacion = request.user.username
                retenciones_det = RetencionCompraDetalle.objects.filter(retencion_cabecera_compra=retencion_cab)
                for obj in retenciones_det:
                    obj.status = 2
                    obj.fecha_actualizacion = now
                    obj.usuario_actualizacion = request.user.username
                    obj.save()
                retencion_cab.save()
            except:
                pass

            # Anular documento que modifica si los hubiera
            try:
                documento_modifica = Doc_Modifica.objects.filter(status=1).get(compra_cabecera=compra)
                documento_modifica.status = 2
                documento_modifica.fecha_actualizacion = now
                documento_modifica.usuario_actualizacion = request.user.username
                documento_modifica.save()
            except Doc_Modifica.DoesNotExist:
                pass

            # Anula los reembolsos de gastos si los hubiera
            try:
                reembolso = Reembolso_gasto_compra.objects.filter(status=1).filter(compra_cabecera=compra)
                for obj in reembolso:
                    obj.status = 2
                    obj.fecha_actualizacion = now
                    obj.usuario_actualizacion = request.user.username
                    obj.save()
            except:
                pass

            # Comprobante contable
            try:
                cabecera_comprobante = Cabecera_Comp_Contable.objects.get(numero_comprobante=compra.num_comp)
                detalle_comp_contable = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_comprobante)

                cabecera_comprobante.status = 2
                cabecera_comprobante.fecha_actualizacion  = now
                cabecera_comprobante.usuario_actualizacion = request.user.username
                cabecera_comprobante.save()

                for obj in detalle_comp_contable:
                    obj.status = 2
                    obj.fecha_actualizacion = now
                    obj.usuario_actualizacion = request.user.username
                    obj.save()
            except Cabecera_Comp_Contable.DoesNotExist:
                pass
            # Contrato
            """
            Revisar la anulacion del contrato, falta la cabecera de contratos costo
            """
            try:
                contratos = ContratoDetalle.objects.filter(tipo_comprobante_id=1, modulo_id=id)
                for obj in contratos:
                    obj.status = 2
                    obj.fecha_actualizacion = now
                    obj.usuario_actualizacion = request.user.username
                    obj.save()
                # Costo Distribución Compra
                try:
                    costo_dist_compra = CostoDistribuirCompras.objects.get(compra_cabecera=compra)
                    costo_dist_compra.status = 2
                    costo_dist_compra.fecha_actualizacion = now
                    costo_dist_compra.usuario_actualizacion = request.user.username
                    costo_dist_compra.save()
                except CostoDistribuirCompras.DoesNotExist:
                    pass

            except:
                pass

        messages.success(request, u"La compra se ha anulado con éxito")
    except ErrorCompras, e:
        transaction.rollback()
        messages.error(request, e)
    return HttpResponseRedirect(reverse("lista_compras"))


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
@csrf_exempt
@transaction.commit_on_success
@csrf_protect
def agregarcompras(request):
    """
    Función para agregar una nueva compra al sistema
    """
    now = datetime.datetime.now()
    try:
        secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=1)  # Secuencia de Compras
        fecha_comp = secuencia.fecha_actual
    except SecuenciaTipoComprobante.DoesNotExist:
        fecha_comp = now.strftime("%Y-%m-%d")

    formulario = CabeceraCompForm(initial={"fecha_asiento": fecha_comp, "fecha": fecha_comp})
    formsetDetalle_Comp = formset_factory(FormDetalleCompCompra, extra=1)
    detalleform = formsetDetalle_Comp(prefix="detalle_compra")

    dia_actual = datetime.datetime.now().strftime('%Y-%m-%d')

    retencion = FormRetencionCompra()
    detalle_ret_formset = formset_factory(DetalleRetencionForm, extra=1)
    detalle_ret = detalle_ret_formset(prefix="detalle_ret")
    reembolso_gasto_formset = formset_factory(FormReembolsoGasto, extra=1)
    reembolso_gasto = reembolso_gasto_formset(prefix="reembolso_gasto")
    contador = 0  # contador para los errores de los formularios
    tipo = 1  # variable para manejar el titulo del template
    form_proveedor = ProveedorForm()

    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

    try:
        if request.method == "POST":
            cabecera_compra = CompraCabecera()
            formulario = CabeceraCompForm(request.POST)
            retencion = FormRetencionCompra(request.POST)
            detalleform = formsetDetalle_Comp(request.POST, request.FILES, prefix="detalle_compra")
            detalle_ret = detalle_ret_formset(request.POST, request.FILES, prefix="detalle_ret")
            reembolso_gasto = reembolso_gasto_formset(request.POST, prefix="reembolso_gasto")

            # Calcula valores totales
            total_iva, tmonto0, tmontoi, tmontoi_cta_iva_pagado, tmontoi_sin_cred_trib, total_ret_fte, total_ret_iva = CalcularTotales(detalleform)

            ####################################################
            #   Manejador del formulario cabecera de compra    #
            ####################################################

            if formulario.is_valid():
                if not esta_periodo_contable(formulario.getFecha_asiento()):
                    contador += 1
                    messages.error(request, u"La fecha del registro es menor a la fecha de cierre contable, "
                                            u"por favor verifique")
                    errors = formulario._errors.setdefault("fecha_asiento", ErrorList())
                    errors.append(u"La fecha del registro es menor a la fecha de cierre contable, por favor verifique")
                else:
                    if tmonto0 == 0 and tmontoi == 0 and tmontoi_sin_cred_trib == 0:
                        messages.error(request, u"El valor de la compra no puede ser de 0 dólares")
                        contador += 1

                    #############################################
                    #  Se valida la fecha de cierre de la       #
                    #               empresa                     #
                    #############################################

                    # Guardar Cabecera Compra
                    mayor_cabecera = Cabecera_Comp_Contable()
                    contador += GuardarCabecera(cabecera_compra, mayor_cabecera, detalleform, formulario, tmonto0,
                                                total_iva + tmontoi_sin_cred_trib, tmontoi, contador, now, 0, reembolso_gasto, tipo, request)

                    # Guardar Cuentas Por Pagar
                    cuentas_pagar = Cuentas_por_Pagar()
                    total = tmonto0 + tmontoi + total_iva + tmontoi_sin_cred_trib - total_ret_fte - total_ret_iva

                    GuardarCuentasPagar(cuentas_pagar, cabecera_compra, total, now, request)

                    if contador == 0:
                        if detalleform.is_valid():
                            # Guardar el detalle de la compra y su asiento respectivo
                            contador += GuardarDetalleCompraAsiento(detalleform, formulario, mayor_cabecera,
                                                                    cabecera_compra, now, contador, request)

                            # Guardar Asiento de comprobante Contable "Proveedores"
                            detalle_comp_prov = Detalle_Comp_Contable()
                            GuardarAsientoProveedor(detalle_comp_prov, mayor_cabecera, formulario, cabecera_compra,
                                                    tmonto0, tmontoi, total_iva + tmontoi_sin_cred_trib, total_ret_fte,
                                                    total_ret_iva, now, request)

                            # Cuenta de Iva Pagado
                            if tmontoi_cta_iva_pagado > 0:
                                # Guardar Cuenta IVA PAGADO
                                detalle_comp_iva = Detalle_Comp_Contable()
                                GuardarAsientoIvaPagado(detalle_comp_iva, mayor_cabecera, formulario,
                                                        tmontoi_cta_iva_pagado, now, request)
                        else:
                            contador += 1
                            messages.error(request, u"Debe llenar todos los datos del detalle de la compra, "
                                                    u"revise los valores del detalle y si están correctas las cuentas.")

                        if total_ret_fte + total_ret_iva > 0:
                            if retencion.is_valid():
                                retencion_cabecera = RetencionCompraCabecera()

                                # Guardar Retención Cabecera
                                contador += GuardarRetencionCabecera(retencion_cabecera, retencion, cabecera_compra,
                                                                      empresa, total_ret_fte, total_ret_iva, contador,
                                                                      now, tipo, request)
                                if detalle_ret.is_valid():
                                    # Guardar Detalle Retención con su respectivo Asiento
                                    GuardarDetalleRetencion(detalle_ret, retencion_cabecera, mayor_cabecera, now, request)
                                else:
                                    contador += 1
                                    messages.error(request, u"Por favor llene todos los campos obligatorios del detalle "
                                                            u"de retención.")
                            else:
                                contador += 1
                                messages.error(request, u"Por favor llene todos los campos obligatorios de la retención")

            else:
                contador += 1
                messages.error(request, u"Por favor llene los campos obligatorios de la compra")

            if contador > 0:
                transaction.rollback()
            else:
                if cabecera_compra.getRetencion():
                    messages.success(request, u"La compra se ha grabado con éxito, con numero de comprobante: "
                                     + cabecera_compra.num_comp + u" y numero de retención: " + cabecera_compra.getRetencion().numero_ret)
                else:
                    messages.success(request, u"La compra se ha grabado con éxito, con numero de comprobante: "
                                     + cabecera_compra.num_comp)
                # Redireccionando la compra
                return HttpResponseRedirect(redireccionar_compra(request, cabecera_compra))

    except ErrorCompras, e:
        transaction.rollback()
        messages.error(request, unicode(e.valor))

    return render_to_response('Compras/comprobante_contable.html',
                              {"formulario": formulario, "detalleform": detalleform, "form_ret": retencion,
                               "dia": dia_actual, "detalle_ret": detalle_ret, "reembolso": reembolso_gasto,
                               "tipo": tipo, "form_proveedor": form_proveedor},
                              context_instance=RequestContext(request))


def AnularCompra(id, now, request, contador):
    """
    Función para anular la compra, esta se utiliza dentro de la vista
    no responde a ningun request, esta función se utiliza en "Anular y generar compra"
    """

    compra = CompraCabecera.objects.get(id=id)
    detalle_comp = CompraDetalle.objects.filter(compra_cabecera=compra)
    cuenta_por_pagar = Cuentas_por_Pagar.objects.get(compra_cabecera=compra, status=1)
    cabecera_comprobante = Cabecera_Comp_Contable.objects.get(numero_comprobante=compra.num_comp)

    try:
        retencion = RetencionCompraCabecera.objects.get(compra_cabecera=compra)
        retencion.estado_firma = 0
        retencion.save()
        for obj in RetencionCompraDetalle.objects.filter(retencion_cabecera_compra=retencion):
            obj.status = 0
            obj.fecha_actualizacion = now
            obj.usuario_actualizacion = request.user.username
            obj.save()
    except RetencionCompraCabecera.DoesNotExist:
        pass

    #ATS
    for ats in ATS.objects.filter(compra_cabecera=compra):
        ats.fecha_actualizacion = now
        ats.usuario_actualizacion = request.user.username
        ats.status = 2
        ats.save()

    #Reembolso de gasto
    reembolso_gasto = Reembolso_gasto_compra.objects.filter(compra_cabecera=compra)
    for obj in reembolso_gasto:
        obj.fecha_actualizacion = now
        obj.usuario_actualizacion = request.user.username
        obj.status = 2
        obj.save()

    #N/D o N/C
    try:
        notadc = Doc_Modifica.objects.get(compra_cabecera=compra)
        notadc.fecha_actualizacion = now
        notadc.usuario_actualizacion = request.user.username
        notadc.status = 2
        notadc.save()
    except:
        pass

    if cuenta_por_pagar.pagado > 0:
        contador += 1
        messages.error(request, u"No se puede anular, ya que existen pagos realizados a esa compra")
    else:
        cuenta_por_pagar.fecha_actualizacion = now
        cuenta_por_pagar.usuario_actualizacion = request.user.username
        cuenta_por_pagar.status = 2
        cuenta_por_pagar.save()
    compra.status = 2
    compra.fecha_actualizacion = datetime.datetime.now()
    compra.usuario_actualizacion = request.user.username
    compra.save()

    for obj in detalle_comp:
        obj.status = 2
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.usuario_actualizacion = request.user.username
        obj.save()

    cabecera_comprobante.status = 2
    cabecera_comprobante.fecha_actualizacion = now
    cabecera_comprobante.usuario_actualizacion = request.user.username
    cabecera_comprobante.save()
    for obj in cabecera_comprobante.getDetalleComp():
        obj.status = 2
        obj.fecha_actualizacion = now
        obj.usuario_actualizacion = request.user.username
        obj.save()


    # Contrato
    contratos = ContratoDetalle.objects.filter(tipo_comprobante_id=1, modulo_id=compra.id)
    for obj in contratos:
        obj.status = 2
        obj.fecha_actualizacion = now
        obj.usuario_actualizacion = request.user.username
        obj.save()

    # Costo Distribución Compra
    try:
        costo_dist_compra = CostoDistribuirCompras.objects.get(compra_cabecera=compra)
        costo_dist_compra.status = 2
        costo_dist_compra.fecha_actualizacion = now
        costo_dist_compra.usuario_actualizacion = request.user.username
        costo_dist_compra.save()
    except CostoDistribuirCompras.DoesNotExist:
        pass

    return contador


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@csrf_protect
@permiso_accion(mensaje=mensaje_permiso)
def anular_generar_compra(request, id):
    """
    function anular_generar_compras(request)
    @Return html file
    Details: anula y genera una compra con los datos de la compra anulada
    """
    tipo = 2  # variable para manejar el titulo del template
    contador = 0
    now = datetime.datetime.now()
    form_proveedor = ProveedorForm()
    dia_actual = datetime.datetime.now().strftime('%Y-%m-%d')
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

    compra = CompraCabecera.objects.get(id=id)
    compra_detalle = CompraDetalle.objects.filter(compra_cabecera=compra)
    ats_anulada = ATS.objects.filter(compra_cabecera=compra)[0]

    ##############################################
    #   Llenar el formulario de Cab. Compra      #
    ##############################################

    formulario = CabeceraCompForm(initial=inst_form_cab_compra(compra, ats_anulada, now, request))

    ################################################
    #    Llenar el formulario de detalle Compra    #
    ################################################
    det_form = []
    inst_comp_detalle(compra_detalle, det_form, compra)
    formsetDetalle_Comp = formset_factory(FormDetalleCompCompra, extra=0)

    detalleform = formsetDetalle_Comp(initial=det_form, prefix="detalle_compra")
    retencion = FormRetencionCompra()
    detalle_ret_formset = formset_factory(DetalleRetencionForm, extra=1)
    detalle_ret = detalle_ret_formset(prefix="detalle_ret")
    reembolso_gasto_formset = formset_factory(FormReembolsoGasto, extra=1)
    reembolso_gasto = reembolso_gasto_formset(prefix="reembolso_gasto")

    ################################################
    #        Si es un Reembolso de Gastos          #
    ################################################
    if es_reembolso_gasto(compra.documento.id):
        reembolsos = Reembolso_gasto_compra.objects.exclude(status=0).filter(compra_cabecera=compra)
        ####################################################
        #    Llenar el formulario de Reembolso de Gasto    #
        ####################################################
        det_reem_form = []
        inst_reembolso_compra(reembolsos, det_reem_form)
        reembolso_gasto = reembolso_gasto_formset(prefix="reembolso_gasto", initial=det_reem_form)

    else:
        try:
            ###########################################################
            #  Se llenan los formularios de retención si las hubiera  #
            ###########################################################
            retencion_cabecera = RetencionCompraCabecera.objects.get(compra_cabecera=compra)
            detalle_retencion = RetencionCompraDetalle.objects.filter(retencion_cabecera_compra=retencion_cabecera)
            retencion = FormRetencionCompra(initial=inst_cab_retencion_compra(retencion_cabecera))

            det_ret_form = []
            inst_det_retencion_compra(detalle_retencion, det_ret_form)
            detalle_ret = detalle_ret_formset(prefix="detalle_ret", initial=det_ret_form)

            if retencion_cabecera.estado_firma >= 3:
                messages.error(request, u"La compra que seleccionó no se puede anular y generar ya que la retención "
                                        u"asociada a esa compra ya ha sido enviada al SRI, en el caso que desee anular "
                                        u"la retención, tiene que hacerlo desde la página del SRI, y en el sistema "
                                        u"usar la opción de anular la compra y una ves anulada luego copiarla")

                return HttpResponseRedirect(reverse("lista_compras"))

        except RetencionCompraCabecera.DoesNotExist:
            pass

    #try:
    if request.method == "POST":
        cabecera_compra = CompraCabecera()
        formulario = CabeceraCompForm(request.POST)
        retencion = FormRetencionCompra(request.POST)
        detalleform = formsetDetalle_Comp(request.POST, request.FILES, prefix="detalle_compra")
        detalle_ret = detalle_ret_formset(request.POST, request.FILES, prefix="detalle_ret")
        reembolso_gasto = reembolso_gasto_formset(request.POST, prefix="reembolso_gasto")

        # Calcula valores totales
        total_iva, tmonto0, tmontoi, tmontoi_cta_iva_pagado, tmontoi_sin_cred_trib, total_ret_fte, total_ret_iva = CalcularTotales(detalleform)

        if formulario.is_valid():
            if not esta_periodo_contable(formulario.getFecha_asiento()):
                    contador += 1
                    messages.error(request, u"La fecha del registro es menor a la fecha de cierre contable, "
                                            u"por favor verifique")
                    errors = formulario._errors.setdefault("fecha_asiento", ErrorList())
                    errors.append(u"La fecha del registro es menor a la fecha de cierre contable, por favor verifique")
            else:
                if tmonto0 == 0 and tmontoi == 0 and tmontoi_sin_cred_trib == 0:
                    messages.error(request, u"El valor de la compra no puede ser de 0 dólares")
                    contador += 1

                # Guardar Cabecera Compra
                mayor_cabecera = Cabecera_Comp_Contable()
                contador += GuardarCabecera(cabecera_compra, mayor_cabecera, detalleform, formulario,
                                            tmonto0, total_iva + tmontoi_sin_cred_trib, tmontoi, contador, now, id, reembolso_gasto,
                                            tipo, request)

                # Guardar Cuentas Por Pagar
                cuentas_pagar = Cuentas_por_Pagar()
                total = tmonto0 + tmontoi + total_iva + tmontoi_sin_cred_trib - total_ret_fte - total_ret_iva
                GuardarCuentasPagar(cuentas_pagar, cabecera_compra, total, now, request)

                if contador == 0:

                    if detalleform.is_valid():
                        # Guardar el detalle de la compra y su asiento respectivo
                        contador += GuardarDetalleCompraAsiento(detalleform, formulario, mayor_cabecera,
                                                                cabecera_compra, now, contador, request)

                        # Guardar Asiento de comprobante Contable "Proveedores"
                        detalle_comp_prov = Detalle_Comp_Contable()
                        GuardarAsientoProveedor(detalle_comp_prov, mayor_cabecera, formulario, cabecera_compra,
                                                tmonto0, tmontoi, total_iva + tmontoi_sin_cred_trib, total_ret_fte,
                                                total_ret_iva, now, request)

                        #########################################
                        #         Cuenta de Iva Pagado          #
                        #########################################
                        if tmontoi_cta_iva_pagado > 0:
                            # Guardar Cuenta IVA PAGADO
                            detalle_comp_iva = Detalle_Comp_Contable()
                            GuardarAsientoIvaPagado(detalle_comp_iva, mayor_cabecera, formulario,
                                                    tmontoi_cta_iva_pagado, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Debe llenar todos los datos del detalle de la compra, revise los valores del detalle y si están correctas las cuentas.")
                    try:
                        id_retencion = CompraCabecera.objects.get(id=id).getRetencion().id
                        retencion_cabecera = RetencionCompraCabecera.objects.get(id=id_retencion)
                    except:
                        retencion_cabecera = None

                    if total_ret_fte > 0 or total_ret_iva > 0:
                        if retencion.is_valid():
                            # Guardar Retención Cabecera
                            try:
                                id_retencion = CompraCabecera.objects.get(id=id).getRetencion().id
                                retencion_cabecera = RetencionCompraCabecera.objects.get(id=id_retencion)
                            except:
                                retencion_cabecera = RetencionCompraCabecera()

                            contador += GuardarRetencionCabecera(retencion_cabecera, retencion, cabecera_compra,
                                                                      empresa, total_ret_fte, total_ret_iva, contador,
                                                                      now, tipo, request)


                            if detalle_ret.is_valid():
                                # Guardar Detalle Retención con su respectivo Asiento
                                GuardarDetalleRetencion(detalle_ret, retencion_cabecera, mayor_cabecera, now, request)
                            else:
                                messages.error(request, u"Por favor llene todos los campos obligatorios del detalle de retención.")
                                if retencion.getFechaEmisionRetencion():
                                    errors = retencion._errors.setdefault("fecha_emision_retencion", ErrorList())
                                    errors.append(u"La feha de emisión de la retención es requerida")
                                contador += 1
                        else:
                            contador += 1
                            messages.error(request, u"Por favor llene todos los campos obligatorios de la retención")
                    else:
                        if retencion_cabecera is not None:
                            messages.error(request, u"Esta compra tiene asignada la retención: " +
                                           str(retencion_cabecera.numero_ret) +
                                           u", para crear una copia de esta compra sin retención"
                                           u" el proceso es anularla y luego copiarla")
                            contador += 1
        else:
            contador += 1
            messages.error(request, u"Por favor llene los campos obligatorios de la compra")

        if contador == 0:
            contador = AnularCompra(id, now, request, contador)  # Anula la compra
            if contador == 0:
                if cabecera_compra.getRetencion():
                    messages.success(request, u"La compra se ha anulado y se generado exitosamente, con numero de comprobante: "
                                     + cabecera_compra.num_comp + u" y numero de retención: " + cabecera_compra.getRetencion().numero_ret)
                else:
                    messages.success(request, u"La compra se ha anulado y generado exitosamente, con numero de comprobante: "
                                     + cabecera_compra.num_comp)

                # Redireccioando la compra
                return HttpResponseRedirect(redireccionar_compra(request, cabecera_compra))
            else:
                transaction.rollback()
        else:
            transaction.rollback()
    #except ErrorCompras, e:
    transaction.rollback()
    #    messages.error(request, unicode(e.valor))
    return render_to_response('Compras/comprobante_contable.html',
                              {"formulario": formulario, "detalleform": detalleform, "form_ret": retencion,
                               "dia": str(dia_actual), "detalle_ret": detalle_ret, "reembolso": reembolso_gasto,
                               "tipo": tipo, "id": id, "form_proveedor": form_proveedor,
                               "compra": compra
                               },
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@csrf_protect
@permiso_accion(mensaje=mensaje_permiso)
def copiar_compra(request, id):
    """
    function copiar_compras(request)
    @Return html file
    Details: Hace una copia de una compra ya realizada.
    """
    now = datetime.datetime.now()
    cursor = connection.cursor()
    contador = 0
    tipo = 3  # variable para manejar el titulo del template
    form_proveedor = ProveedorForm()
    dia_actual = datetime.datetime.now().strftime('%Y-%m-%d')

    compra = CompraCabecera.objects.get(id=id)
    compra_detalle = CompraDetalle.objects.filter(compra_cabecera=compra)
    ats_copiado = ATS.objects.filter(compra_cabecera=compra)[0]

    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

    ##############################################
    #   Llenar el formulario de Cab. Compra      #
    ##############################################

    formulario = CabeceraCompForm(initial=inst_form_cab_compra(compra, ats_copiado, now, request))

    ################################################
    #    Llenar el formulario de detalle Compra    #
    ################################################
    det_form = []
    formsetDetalle_Comp = formset_factory(FormDetalleCompCompra, extra=0)
    inst_comp_detalle(compra_detalle, det_form, compra)
    detalleform = formsetDetalle_Comp(initial=det_form, prefix="detalle_compra")
    ################################################
    #        Si es un Reembolso de Gastos          #
    ################################################
    retencion = FormRetencionCompra()
    detalle_ret_formset = formset_factory(DetalleRetencionForm, extra=1)
    detalle_ret = detalle_ret_formset(prefix="detalle_ret")
    reembolso_gasto_formset = formset_factory(FormReembolsoGasto, extra=1)
    reembolso_gasto = reembolso_gasto_formset(prefix="reembolso_gasto")

    if es_reembolso_gasto(compra.documento.id):
        reembolsos = Reembolso_gasto_compra.objects.exclude(status=0).filter(compra_cabecera=compra)
        ####################################################
        #    Llenar el formulario de Reembolso de Gasto    #
        ####################################################
        det_reem_form = []
        inst_reembolso_compra(reembolsos, det_reem_form)
        reembolso_gasto = reembolso_gasto_formset(prefix="reembolso_gasto", initial=det_reem_form)

    try:
        if request.method == "POST":
            cabecera_compra = CompraCabecera()
            formulario = CabeceraCompForm(request.POST)
            retencion = FormRetencionCompra(request.POST)
            detalleform = formsetDetalle_Comp(request.POST, request.FILES, prefix="detalle_compra")
            detalle_ret = detalle_ret_formset(request.POST, request.FILES, prefix="detalle_ret")
            reembolso_gasto = reembolso_gasto_formset(request.POST, prefix="reembolso_gasto")
            tmonto0 = 0.0
            tmontoi = 0.0
            tmontoi_sin_cred_trib = 0.0
            tmontoi_cta_iva_pagado = 0.0
            total_iva = 0.0
            total_ret_fte = 0.0
            total_ret_iva = 0.0

            # Calcula valores totales
            if detalleform.is_valid():
                total_iva, tmonto0, tmontoi, tmontoi_cta_iva_pagado, tmontoi_sin_cred_trib, total_ret_fte, total_ret_iva = CalcularTotales(detalleform)

            ####################################################
            #   Manejador del formulario cabecera de compra    #
            ####################################################

            if formulario.is_valid():

                if not esta_periodo_contable(formulario.getFecha_asiento()):
                    contador += 1
                    messages.error(request, u"La fecha del registro es menor a la fecha de cierre contable, "
                                            u"por favor verifique")
                    errors = formulario._errors.setdefault("fecha_asiento", ErrorList())
                    errors.append(u"La fecha del registro es menor a la fecha de cierre contable, por favor verifique")
                else:
                    if tmonto0 == 0 and tmontoi == 0 and tmontoi_sin_cred_trib == 0:
                        messages.error(request, u"El valor de la compra no puede ser de 0 dólares")
                        contador += 1

                    # Guardar Cabecera Compra
                    mayor_cabecera = Cabecera_Comp_Contable()
                    contador += GuardarCabecera(cabecera_compra, mayor_cabecera, detalleform, formulario, tmonto0,
                                                total_iva + tmontoi_sin_cred_trib, tmontoi, contador, now, 0, reembolso_gasto, tipo, request)

                    # Guardar Cuentas Por Pagar
                    cuentas_pagar = Cuentas_por_Pagar()
                    total = tmonto0 + tmontoi + total_iva + tmontoi_sin_cred_trib - total_ret_fte - total_ret_iva
                    GuardarCuentasPagar(cuentas_pagar, cabecera_compra, total, now, request)

                    if contador == 0:
                        if detalleform.is_valid() and formulario.is_valid():
                            # Guardar el detalle de la compra y su asiento respectivo
                            contador += GuardarDetalleCompraAsiento(detalleform, formulario, mayor_cabecera,
                                                                    cabecera_compra, now, contador, request)

                            # Guardar Asiento de comprobante Contable "Proveedores"
                            detalle_comp_prov = Detalle_Comp_Contable()
                            GuardarAsientoProveedor(detalle_comp_prov, mayor_cabecera, formulario, cabecera_compra,
                                                    tmonto0, tmontoi, total_iva + tmontoi_sin_cred_trib, total_ret_fte, total_ret_iva,
                                                    now, request)

                            # Cuenta de Iva Pagado
                            if tmontoi_cta_iva_pagado > 0:
                                # Guardar Cuenta IVA PAGADO
                                detalle_comp_iva = Detalle_Comp_Contable()
                                GuardarAsientoIvaPagado(detalle_comp_iva, mayor_cabecera, formulario,
                                                        tmontoi_cta_iva_pagado, now, request)
                        else:
                            contador += 1
                            messages.error(request, u"Debe llenar todos los datos del detalle de la compra, revise los valores del detalle y si están correctas las cuentas.")

                        if total_ret_fte > 0 or total_ret_iva > 0:
                            if retencion.is_valid():
                                retencion_cabecera = RetencionCompraCabecera()

                                # Guardar Retención Cabecera
                                contador += GuardarRetencionCabecera(retencion_cabecera, retencion, cabecera_compra,
                                                                      empresa, total_ret_fte, total_ret_iva, contador,
                                                                      now, tipo, request)
                                if detalle_ret.is_valid():
                                    # Guardar Detalle Retención con su respectivo Asiento
                                    GuardarDetalleRetencion(detalle_ret, retencion_cabecera, mayor_cabecera, now, request)
                                else:
                                    messages.error(request, u"Por favor llene todos los campos obligatorios del detalle de retención.")
                                    contador += 1
                            else:
                                contador += 1
                                messages.error(request, u"Por favor llene todos los campos obligatorios de la retención")
            else:
                contador += 1
                messages.error(request, u"Por favor llene los campos obligatorios de la compra")

            if contador == 0:
                if cabecera_compra.getRetencion():
                    messages.success(request, u"La compra se ha grabado con éxito, con numero de comprobante: "
                                     + cabecera_compra.num_comp + u" y numero de retención: " + cabecera_compra.getRetencion().numero_ret)
                else:
                    messages.success(request, u"La compra se ha grabado con éxito, con numero de comprobante: "
                                     + cabecera_compra.num_comp)

                # Redireccioando la compra
                return HttpResponseRedirect(redireccionar_compra(request, cabecera_compra))

            else:
                transaction.rollback()

    except ErrorCompras, e:
        transaction.rollback()
        messages.error(request, unicode(e.valor))
        return HttpResponseRedirect(reverse("lista_compras"))

    return render_to_response('Compras/comprobante_contable.html',
                              {"formulario": formulario,"detalleform":detalleform, "form_ret":retencion, "dia": str(dia_actual),
                               "detalle_ret":detalle_ret, "reembolso":reembolso_gasto, "tipo": tipo,
                               "id": id, "form_proveedor": form_proveedor},
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@csrf_protect
@permiso_accion(mensaje=mensaje_permiso)
def uso_provision_compra(request, id):
    """
    Función para agregar una nueva compra provisionada al sistema
    """

    now = datetime.datetime.now()
    formsetDetalle_Comp = formset_factory(FormDetalleCompCompra, extra=1)
    detalleform = formsetDetalle_Comp(prefix="detalle_compra")
    dia_actual = datetime.datetime.now().strftime('%Y-%m-%d')
    retencion = FormRetencionCompra()
    detalle_ret_formset = formset_factory(DetalleRetencionForm, extra=1)
    detalle_ret = detalle_ret_formset(prefix="detalle_ret")
    reembolso_gasto_formset = formset_factory(FormReembolsoGasto, extra=1)
    reembolso_gasto = reembolso_gasto_formset(prefix="reembolso_gasto")
    cursor = connection.cursor()

    cabecera_compra = CompraCabecera.objects.get(id=id)
    formulario = CabeceraCompForm(initial={"fecha_asiento": cabecera_compra.fecha_reg.strftime('%Y-%m-%d'),
                                           "fecha": cabecera_compra.fecha_reg.strftime('%Y-%m-%d')})

    contador = 0  # contador para los errores de los formularios
    tipo = 4  # variable para manejar el titulo del template
    form_proveedor = ProveedorForm()
    try:
        retencion_cabecera = RetencionCompraCabecera.objects.get(compra_cabecera=cabecera_compra)
    except RetencionCompraCabecera.DoesNotExist:  # Cuando es una provisión sin retención
        retencion_cabecera = None

    if cabecera_compra.status == 3:
        id_empresa = Empresa.objects.all()[0].empresa_general_id
        empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

        try:
            if request.method == "POST":
                formulario = CabeceraCompForm(request.POST)
                retencion = FormRetencionCompra(request.POST)
                detalleform = formsetDetalle_Comp(request.POST, request.FILES, prefix="detalle_compra")
                detalle_ret = detalle_ret_formset(request.POST, request.FILES, prefix="detalle_ret")
                reembolso_gasto = reembolso_gasto_formset(request.POST, prefix="reembolso_gasto")

                # Calcula valores totales
                total_iva, tmonto0, tmontoi, tmontoi_cta_iva_pagado, tmontoi_sin_cred_trib, total_ret_fte, total_ret_iva = CalcularTotales(detalleform)

                if total_ret_fte + total_ret_iva > 0:
                    ###########################################################
                    #   Validar que tenga registrados bloc de retenciones     #
                    ###########################################################
                    tipo_doc_ret = Documento.objects.get(id=24)  # Tipo retención

                    if not VigenciaDocEmpresa.objects.filter(documento_sri=tipo_doc_ret, status=1).exists():
                        messages.error(request, u"Debe de tener registrado el/los bloc de retencion de la empresa, "
                                                u"por favor regístrelos "
                                                u"en el menú de Configuración -> Documentos")
                        transaction.rollback()
                        return render_to_response('Compras/comprobante_contable.html',
                                                  {"formulario": formulario, "detalleform": detalleform, "tipo": tipo,
                                                   "form_ret": retencion, "dia": str(dia_actual),
                                                   "detalle_ret": detalle_ret, "reembolso": reembolso_gasto,
                                                   "form_proveedor": form_proveedor, "compra": cabecera_compra
                                                   }, context_instance=RequestContext(request))

                else:  # No posee retenciones
                    if retencion_cabecera is not None:
                        messages.error(request, u"La compra que esta ingresando debe de estar asociada a una retención,"
                                                u" por favor revise")
                        transaction.rollback()
                        return render_to_response('Compras/comprobante_contable.html',
                                                  {"formulario": formulario, "detalleform": detalleform,
                                                   "form_ret": retencion, "dia": str(dia_actual),
                                                   "detalle_ret": detalle_ret, "reembolso": reembolso_gasto,
                                                   "tipo": tipo, "form_proveedor": form_proveedor,
                                                   "compra": cabecera_compra
                                                   }, context_instance=RequestContext(request))
                    else:
                        pass

                ####################################################
                #   Manejador del formulario cabecera de compra    #
                ####################################################
                if formulario.is_valid():
                    if not esta_periodo_contable(formulario.getFecha_asiento()):
                        contador += 1
                        messages.error(request, u"La fecha del registro es menor a la fecha de cierre contable, "
                                                u"por favor verifique")
                        errors = formulario._errors.setdefault("fecha_asiento", ErrorList())
                        errors.append(u"La fecha del registro es menor a la fecha de cierre contable, por favor verifique")
                    else:
                        if tmonto0 == 0 and tmontoi == 0 and tmontoi_sin_cred_trib == 0:
                            messages.error(request, u"El valor de la compra no puede ser de 0 dólares")
                            contador += 1

                        # Guardar Cabecera Compra
                        mayor_cabecera = Cabecera_Comp_Contable()
                        contador += GuardarCabecera(cabecera_compra, mayor_cabecera, detalleform, formulario, tmonto0,
                                                    total_iva + tmontoi_sin_cred_trib, tmontoi, contador, now, 0,
                                                    reembolso_gasto, tipo, request)

                        # Guardar Cuentas Por Pagar
                        cuentas_pagar = Cuentas_por_Pagar()
                        total = tmonto0 + tmontoi + total_iva + tmontoi_sin_cred_trib - total_ret_fte - total_ret_iva

                        GuardarCuentasPagar(cuentas_pagar, cabecera_compra, total, now, request)

                        if contador == 0:

                            ##############################################
                            #     Todos los campos del formulario        #
                            #     detalle_compra ya estan validados      #
                            #          desde la clase form               #
                            ##############################################
                            if detalleform.is_valid():
                                # Guardar el detalle de la compra y su asiento respectivo
                                contador += GuardarDetalleCompraAsiento(detalleform, formulario, mayor_cabecera,
                                                                        cabecera_compra, now, contador, request)

                                # Guardar Asiento de comprobante Contable "Proveedores"
                                detalle_comp_prov = Detalle_Comp_Contable()
                                GuardarAsientoProveedor(detalle_comp_prov, mayor_cabecera, formulario, cabecera_compra,
                                                        tmonto0, tmontoi, total_iva + tmontoi_sin_cred_trib,
                                                        total_ret_fte, total_ret_iva, now, request)

                                #########################################
                                #         Cuenta de Iva Pagado          #
                                #########################################
                                if tmontoi_cta_iva_pagado > 0:
                                    # Guardar Cuenta IVA PAGADO
                                    detalle_comp_iva = Detalle_Comp_Contable()
                                    GuardarAsientoIvaPagado(detalle_comp_iva, mayor_cabecera, formulario,
                                                            tmontoi_cta_iva_pagado, now, request)
                            else:
                                contador += 1
                                messages.error(request, u"Debe llenar todos los datos del detalle de la compra, revise los valores del detalle y si están correctas las cuentas.")

                            if total_ret_fte > 0 or total_ret_iva > 0:
                                if retencion_cabecera is not None:
                                    if retencion.is_valid() or retencion.getFechaEmisionRetencion():  # Solo se valida la fecha ya que es una retención previamente creada
                                        # Guardar Retención Cabecera
                                        contador += GuardarRetencionCabecera(retencion_cabecera, retencion,
                                                                             cabecera_compra,empresa, total_ret_fte,
                                                                             total_ret_iva, contador,
                                                                             now, tipo, request)
                                        if detalle_ret.is_valid():
                                            # Guardar Detalle Retención con su respectivo Asiento
                                            GuardarDetalleRetencion(detalle_ret, retencion_cabecera, mayor_cabecera,
                                                                    now, request)
                                        else:
                                            contador += 1
                                            messages.error(request, u"Por favor llene todos los campos obligatorios del detalle de retención.")
                                    else:
                                        contador += 1
                                        errors = retencion._errors.setdefault("fecha_emision_retencion", ErrorList())
                                        errors.append(u"La feha de emisión de la retención es requerida")
                                        messages.error(request, u"Por favor llene todos los campos obligatorios de la retención")
                                else:
                                    messages.error(request, u"La compra provisionada es sin retención "
                                                            u"favor verifíque")
                                    contador += 1
                            else:
                                pass
                else:
                    contador += 1
                    contador += ValidacionFormsCompras(formulario, detalleform, contador)
                    lista_errores = "Por favor verifique los siguientes campos: "
                    for i in formulario.errors:
                        if i == "proveedor":
                            messages.error(request, u'El proveedor que ingresó se encuentra inactivo o no existe')
                        lista_errores = lista_errores +(unicode(i)) + ", "
                    if formulario.errors:
                        messages.error(request, unicode(lista_errores[0:-2]))

                if contador > 0:
                    transaction.rollback()
                else:
                    if cabecera_compra.getRetencion():
                        messages.success(request, u"La compra se ha grabado con éxito, con numero de comprobante: "
                                         + cabecera_compra.num_comp + u" y numero de retención: " + cabecera_compra.getRetencion().numero_ret)
                    else:
                        messages.success(request, u"La compra se ha grabado con éxito, con numero de comprobante: "
                                         + cabecera_compra.num_comp)

                    # Redireccioando la compra
                    return HttpResponseRedirect(redireccionar_compra(request, cabecera_compra))

        except ErrorCompras, e:
            transaction.rollback()
            messages.error(request, unicode(e.valor))
    else:
        messages.error(request, u"La compra seleccionada no esta provisionada, por favor revise")
        return HttpResponseRedirect(reverse("lista_compras"))
    return render_to_response('Compras/comprobante_contable.html',
                              {"formulario": formulario,
                               "detalleform": detalleform, "form_ret": retencion, "dia": str(dia_actual),
                               "detalle_ret": detalle_ret, "reembolso": reembolso_gasto, "tipo": tipo,
                               "form_proveedor": form_proveedor, "compra": cabecera_compra
                               }, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@csrf_protect
@permiso_accion(mensaje=mensaje_permiso)
def cuentas_retenciones(request):
    """
    Función que muestra las cuentas de retenciones y permite cambiarle
    el porcentaje de interes que tenga cada cuenta
    :param request:
    :return:
    """
    cuentas_grupo_retenciones = PlanCuenta.objects.exclude(status=0).filter(nivel=5, tipo=2)
    l_form = []
    formsets = formset_factory(RetencionCuentaFormulario, extra=0)
    formset = formsets()
    if (cuentas_grupo_retenciones):
        for cuenta in cuentas_grupo_retenciones:
            try:
                cuenta_retencion = Cuenta_Retencion.objects.get(plan_cuenta=cuenta)
                l_form.append({"id": cuenta.id, "nombre_cta": cuenta.descripcion, "tipo": cuenta_retencion.tipo_codigo_sri.id, "porcentaje": cuenta_retencion.porcentaje})
            except:
                l_form.append({"id": cuenta.id, "nombre_cta": cuenta.descripcion, "tipo": "", "porcentaje": ""})
        if l_form:
            formset = formsets(initial=l_form)
        else:
            formset = formsets()
        if request.method == "POST":
            formset = formsets(request.POST)
            if formset.is_valid():
                for form in formset:
                    informacion = form.cleaned_data
                    try:
                        plan_cuenta = PlanCuenta.objects.get(id=informacion.get("id"))
                        cuenta_ret = Cuenta_Retencion.objects.get(plan_cuenta=plan_cuenta)
                        tipo_reten = Tipos_Retencion.objects.get(id=int(informacion.get("tipo")))
                        cuenta_ret.tipo = tipo_reten
                        if informacion.get("porcentaje") == None:
                            cuenta_ret.porcentaje = 0.0
                        else:
                            cuenta_ret.porcentaje = informacion.get("porcentaje")
                        cuenta_ret.fecha_actualizacion = datetime.datetime.now()
                        cuenta_ret.usuario_actualizacion = request.user.username
                        cuenta_ret.save()
                    except:
                        cuenta_ret = Cuenta_Retencion()
                        plan_cuenta = PlanCuenta.objects.get(id=informacion.get("id"))
                        tipo_reten = Tipos_Retencion.objects.get(id=int(informacion.get("tipo")))
                        cuenta_ret.tipo_codigo_sri = tipo_reten

                        if informacion.get("porcentaje") == None:
                            cuenta_ret.porcentaje = 0
                        else:
                            cuenta_ret.porcentaje = informacion.get("porcentaje")

                        cuenta_ret.plan_cuenta = plan_cuenta
                        cuenta_ret.fecha_actualizacion = datetime.datetime.now()
                        cuenta_ret.fecha_creacion = datetime.datetime.now()
                        cuenta_ret.usuario_actualizacion = request.user.username
                        cuenta_ret.usuario_creacion = request.user.username
                        cuenta_ret.status = 1
                        cuenta_ret.save()

                messages.success(request, u"Se grabó satisfactoriamente")
                return HttpResponseRedirect(reverse('lista_compras'))
            else:
                pass
    return render_to_response('VigenciaDocumentoEmpresa/cuentas_retenciones.html',{"cuentas":cuentas_grupo_retenciones,"formulario":formset, "request":request}, context_instance=RequestContext(request))


@login_required()
def enviar_retencion_electronica_sri(request, id):
    """
    Vista que sirve para enviar y autorizar las retenciones al SRI con ayuda
    del API de datilmedia, se maneja por estados de firma que es una columna el
    la tabla de retencion_compra_cabecera
    :param request:
    :param id:
    :return:
    """
    compra = CompraCabecera.objects.get(id=id)
    if get_apikey() == "" or get_passwordapi() == "":
        messages.error(request, u"No posee las credenciales para enviar el documento electrónico al SRI, por favor "
                                u"consulte al administrador de sistemas")
    if compra.status == 1:
        if compra.getRetencion():
            if compra.getRetencion().estado_firma == 4:
                messages.success(request, u"La retención ya esta autorizada por el SRI")
                return HttpResponseRedirect(reverse("lista_compras"))
            else:
                status_envio = envio_retencion(compra)
                if status_envio[0] == 1:  # Se envió con éxito la retención al SRI
                    messages.success(request, u"Se ha realizado con éxito la firma y envio del documento")
                    return HttpResponseRedirect(reverse("lista_compras"))
                else:
                    error = status_envio[1]
                    messages.error(request, unicode(error))
                    return HttpResponseRedirect(reverse("lista_compras"))
        else:
            messages.error(request, u"La compra que hizo referencia no posee retención alguna, por favor verifíque")
            return HttpResponseRedirect(reverse("lista_compras"))
    else:
        messages.error(request, u"La compra que seleccionó se encuentra anulada")
        return HttpResponseRedirect(reverse("lista_compras"))


@csrf_exempt
@login_required()
def enviar_correo_retencion_electronica_view(request):
    """
    Envía un correo electrónico al proveedor con los datos de la
    retención electrónica mediante al API de datilmedia
    :param request:
    :param id:
    :return:
    """
    form_correo = FormEnvioCorreo(request.POST)
    form_correo.is_valid()
    id = form_correo.get_id()
    compra = CompraCabecera.objects.get(id=id)

    if request.method == "POST":
        if compra.status == 1:
            if compra.getRetencion():
                retencion = compra.getRetencion()
                if retencion.estado_firma >= 4:
                    try:
                        if enviar_correo_doc_electronico_retencion(request, compra):
                            respuesta = {"status": 1, "mensaje": ""}
                            resultado = json.dumps(respuesta)
                            return HttpResponse(resultado, mimetype='application/json')
                    except SMTPSenderRefused:
                        respuesta = {"status": 2, "mensaje": u"El correo que envió ha sido rechasado por favor verifique "
                                                             u"si el correo es válido."}
                        resultado = json.dumps(respuesta)
                        return HttpResponse(resultado, mimetype='application/json')
                    except:
                        respuesta = {"status": 2, "mensaje": u"Existió un problema en el envio del correo electrónico, "
                                                             u"por favor intente nuevamente"}
                        resultado = json.dumps(respuesta)
                        return HttpResponse(resultado, mimetype='application/json')
                else:
                    messages.error(request, u"La retención no se encuentra todavía autorizada por el SRI")

            else:
                messages.error(request, u"La compra que hizo referencia no posee retención alguna, por favor verifique")

        else:
            messages.error(request, u"La compra que seleccionó se encuentra anulada")

        respuesta = {"status": 2, "mensaje": ""}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url="/")
@transaction.commit_on_success
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso, es_ajax=True)
def anular_retencion(request):
    """
    Función que anula la retención actual o la que ingrese el usuario,
    dependiendo si la generación es manual o automática
    :param request:
    :return:
    """
    now = datetime.datetime.now()
    form_anular = FormAnularRetencion(request.POST)
    try:

        if form_anular.is_valid():
            id_vig = form_anular.getDocumento()
            vigencia = VigenciaDocEmpresa.objects.get(id=id_vig)
            fecha = form_anular.get_fecha()

            num_ret = get_num_doc(vigencia, fecha, request)
            comp_contable = Cabecera_Comp_Contable()
            comp_contable.fecha = fecha
            comp_contable.tipo_comprobante = TipoComprobante.objects.get(id=1)
            comp_contable.numero_comprobante = get_num_comp(comp_contable.tipo_comprobante_id,
                                                            fecha, True, request)
            comp_contable.concepto_comprobante = u"Anulación de retención"
            comp_contable.fecha_creacion = now
            comp_contable.usuario_creacion = request.user.username
            comp_contable.status = 2
            comp_contable.save()

            compra = CompraCabecera()
            compra.fecha_emi = fecha
            compra.fecha_reg = fecha
            compra.num_doc = ''
            compra.concepto = u"Anulación de Retención #" + \
                              str(vigencia.serie) + "-" + str(num_ret)
            compra.num_comp = comp_contable.numero_comprobante
            compra.fecha_creacion = now
            compra.usuario_creacion = request.user.username
            compra.status = 2
            compra.save()

            retencion = RetencionCompraCabecera()
            retencion.fecha_emi = fecha
            retencion.compra_cabecera = compra
            retencion.numero_ret = num_ret
            retencion.status = 2
            retencion.vigencia_doc_empresa = vigencia

            retencion.usuario_creacion = request.user.username
            retencion.fecha_creacion = now
            retencion.status = 2
            retencion.save()
            if comp_contable.numero_comprobante == -1:
                respuesta = {"status": 2, "mensaje": 'La fecha ingresada es menor a la última fecha de compra ingresada, por favor verifique'}
                transaction.rollback()
            elif num_ret == -1:
                respuesta = {"status": 2, "mensaje": 'El block ingresado no tiene mas documentos disponibles.'}
                transaction.rollback()
            else:
                respuesta = {"status": 1, "num_ret": num_ret, "num_comp": comp_contable.numero_comprobante}
                messages.success(request, u"Se anuló satisfactoriamente la retención #: " +
                             str(vigencia.serie) + "-" + num_ret)
        else:
            respuesta = {'status': 2, 'mensaje': 'Por favor ingrese los datos requeridos'}
            transaction.rollback()
        resultado = json.dumps(respuesta)
    except:
        respuesta = {"status": 2, "mensaje": "error interno en el servidor"}
        resultado = json.dumps(respuesta)
        transaction.rollback()

    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def buscar_cuenta(request):
    respuesta = []
    q = request.GET.get("term", "")
    cuentas = PlanCuenta.objects.filter(status=1).exclude(tipo__alias=2).filter(nivel=5, descripcion__istartswith=q).only("codigo", "descripcion")[0:5]
    for p in cuentas:
        respuesta.append({"value": p.codigo + "-" + p.descripcion, "key": p.codigo})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def buscar_autorizacion(request):
    """
    ########################################
    # Para devolver la autorización de la  #
    #        retención en la compra        #
    ########################################
    :param request:
    :return:
    """
    try:
        id_vigencia = request.POST.get("serie", "")
        vigencia_ret = VigenciaDocEmpresa.objects.get(id=id_vigencia)
        respuesta = {"status": 1, "autorizacion": str(vigencia_ret.autorizacion),
                     "num_ret": vigencia_ret.serie + "-" + str(vigencia_ret.sec_actual).zfill(9)}
    except:
        respuesta = {"status": 0, "value": "error base", "key": "error"}
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def buscar_cuenta_codigo(request):
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0
    nivel_padre = 3  # nivel que se va a presentar a lado de la cuenta de gasto

    p = q.replace(".", "")
    q = p.replace("_", "")
    if pkey_val is not None and pkey_val != "":
        try:
            p = PlanCuenta.objects.get(id=pkey_val)
            descrp_padre = p.get_descripcion_cta_n_padre(nivel_padre)
            name = p.codigo + ' - ' +\
                   p.descripcion if descrp_padre == "" else p.codigo + ' - ' + p.descripcion[:40] + " - " + descrp_padre[:40]

            resultado = json.dumps({"name": name, "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except PlanCuenta.DoesNotExist:
            respuesta = []
    else:
        tipo_cuenta = TipoCuenta.objects.filter(modulo_compra=True)
        cuentas = PlanCuenta.objects.filter(status=1, nivel=5).filter(Q(tipo__in=tipo_cuenta) | Q(tipo=None)).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q)).only("codigo", "descripcion")
        count = len(cuentas)
        paginacion = Paginator(cuentas, per_page)
        try:
            cuentas = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            cuentas = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            cuentas = paginacion.page(paginacion.num_pages)

        for p in cuentas:
            descrp_padre = p.get_descripcion_cta_n_padre(nivel_padre)
            name = p.codigo + ' - ' +\
                   p.descripcion if descrp_padre == "" else p.codigo + ' - ' + p.descripcion[:40] + " - " + descrp_padre[:40]
            respuesta.append({"name": name, "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def buscar_cuenta_codigo_exacto(request):
    """
    Funcion que me busca la cuenta dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("term", "")
    p = q.replace(".", "")
    q = p.replace("_", "")
    try:
        cuenta = PlanCuenta.objects.filter(status=1).exclude(tipo__alias=2).get(nivel=5, codigo=q)
        respuesta.append({"value": cuenta.codigo, "key": cuenta.descripcion, "ret": False})
    except:
        respuesta.append({"value": "cuenta no encontrada", "key": "", "ret": False})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def buscar_cuenta_retenciones(request):
    """
    Función que retorna una lista de las retenciones dependiendo
    del tipo y el nombre
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("term", "")
    tipo = request.GET.get("tipo", "")
    if tipo == "r":   # 103 codigo de la retención fuente
        cuentas = Cuenta_Retencion.objects.filter(status=1).filter(tipo__codigo=103).filter(Q(cuenta__codigo__icontains=q) | Q(cuenta__descripcion__icontains=q))[0:7]
    else:   # 104 codigo de la retención IVA
        cuentas = Cuenta_Retencion.objects.filter(status=1).filter(tipo__codigo=104).filter(Q(cuenta__codigo__icontains=q) | Q(cuenta__descripcion__icontains=q))[0:7]
    for p in cuentas:
        respuesta.append({"value": p.cuenta.codigo +"-"+p.cuenta.descripcion, "key": p.cuenta.descripcion, "ret": True})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
def buscar_proveedor(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    p = q.replace(".", "")
    q = p.replace("_", "")
    if pkey_val is not None and pkey_val != "":
        try:
            p = Proveedores.objects.get(id=pkey_val)
            resultado = json.dumps({"name": p.ruc + ' - ' + p.razon_social, "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except Proveedores.DoesNotExist:
            respuesta = []
    else:
        cuentas = Proveedores.objects.filter(status=1).filter(Q(ruc__icontains=q) | Q(razon_social__icontains=q)).only("razon_social", "ruc")
        paginacion = Paginator(cuentas, per_page)
        try:
            cuentas = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            cuentas = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            cuentas = paginacion.page(paginacion.num_pages)

        count = len(Proveedores.objects.filter(status=1).filter(Q(ruc__icontains=q) | Q(razon_social__icontains=q)).only("razon_social", "ruc"))
        for p in cuentas:
            respuesta.append({"name": p.ruc + ' - ' + p.razon_social, "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
def buscar_centro_costo(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    p = q.replace(".", "")
    q = p.replace("_", "")
    if pkey_val is not None and pkey_val != "":
        try:
            p = Centro_Costo.objects.get(id=pkey_val)
            resultado = json.dumps({"name": p.descripcion, "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except Centro_Costo.DoesNotExist:
            respuesta = []
    else:
        cuentas = Centro_Costo.objects.filter(status=1).filter(descripcion__icontains=q).only("descripcion")
        paginacion = Paginator(cuentas, per_page)
        try:
            cuentas = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            cuentas = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            cuentas = paginacion.page(paginacion.num_pages)

        count = len(Centro_Costo.objects.filter(status=1).filter(descripcion__icontains=q).only("descripcion"))
        for p in cuentas:
            respuesta.append({"name": p.descripcion, "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
def buscar_sustento_tributario(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    p = q.replace(".", "")
    q = p.replace("_", "")
    if pkey_val is not None and pkey_val != "":
        try:
            p = Sustento_Tributario.objects.get(id=pkey_val)
            resultado = json.dumps({"name": p.codigo + ' - ' + p.descripcion, "id": p.codigo})
            return HttpResponse(resultado, mimetype='application/json')
        except Sustento_Tributario.DoesNotExist:
            respuesta = []
    else:
        cuentas = Sustento_Tributario.objects.filter(status=1).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q)).only("codigo", "descripcion")
        paginacion = Paginator(cuentas, per_page)
        try:
            cuentas = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            cuentas = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            cuentas = paginacion.page(paginacion.num_pages)

        count = len(Sustento_Tributario.objects.filter(status=1).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q)).only("codigo", "descripcion"))
        for p in cuentas:
            respuesta.append({"name": p.codigo+ ' - ' + p.descripcion, "id": p.codigo})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
def buscar_codigo_sri_fte(request):#carga el combo dinamico de las cosas de retencion fuente en una crear compra
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    print('codigos de la fuenteeee')
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    fecha_asiento = request.GET.get("fecha_asiento", None)
    tipo_documento_id = request.GET.get("tipo_documento", None)
    count = 0

    p = q.replace(".", "")
    q = p.replace("_", "")

    if pkey_val is not None and pkey_val != "":
        try:
            p = Codigo_SRI.objects.get(id=pkey_val)
            porcentaje = "" if p.porc_variable else p.porcentaje
            resultado = json.dumps({"name": p.codigo, "id": p.id,
                                    "porcentaje": porcentaje, "descripcion": p.descripcion[:40]})
            return HttpResponse(resultado, mimetype='application/json')
        except Codigo_SRI.DoesNotExist:
            respuesta = []
    else:
        try:
            if tiene_doc_modifica(tipo_documento_id):
                porc_ret_cero = True
            else:
                porc_ret_cero = False
        except:
            porc_ret_cero = False

        q1 = VigenciaRetencionSRI.objects.get(tipo_codigo_sri=Tipos_Retencion.objects.get(id=1),
                                              fecha_inicio__lte=fecha_asiento,
                                              fecha_final__gte=fecha_asiento)
        if porc_ret_cero:
            cuentas = Codigo_SRI.objects.filter(status=1).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q))\
                .filter(vigencia_retencion=q1, porcentaje=0).filter(Q(porc_variable=False) | Q(porc_variable=None)).order_by("orden", "codigo")
        else:
            cuentas = Codigo_SRI.objects.filter(status=1).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q))\
                .filter(vigencia_retencion=q1).order_by("orden", "codigo")

        count = len(cuentas)

        paginacion = Paginator(cuentas, per_page)
        try:
            cuentas = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            cuentas = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            cuentas = paginacion.page(paginacion.num_pages)

        for p in cuentas:
            porcentaje = str(p.porc_min) + " - " + str(p.porc_max) if p.porc_variable else p.porcentaje
            respuesta.append({"name": p.codigo, "id": p.id,
                              "porcentaje": porcentaje, "descripcion": p.descripcion[:40]})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
def buscar_codigo_sri_iva(request):#carga el combo dinamico de las cosas de iva en una crear compra
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    print('cosas del ivaaaa')
    respuesta = []

    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    fecha_asiento = request.GET.get("fecha_asiento", None)  #31 agosto esto agregado
    count = 0
    print 'la fecha de agregar compra es'+str(fecha_asiento)
    p = q.replace(".", "")
    q = p.replace("_", "")
    if pkey_val is not None and pkey_val != "":
        try:
            p = Codigo_SRI.objects.get(id=pkey_val)
            porcentaje = str(p.porc_min) + " - " + str(p.porc_max) if p.porc_variable else p.porcentaje
            resultado = json.dumps({"name": p.codigo, "id": p.id,
                                    "porcentaje": porcentaje, "descripcion": p.descripcion[:40]})
            return HttpResponse(resultado, mimetype='application/json')
        except Codigo_SRI.DoesNotExist:
            respuesta = []
    else:

        q1 = VigenciaRetencionSRI.objects.get(tipo_codigo_sri=Tipos_Retencion.objects.get(id=4),
                                              fecha_inicio__lte=fecha_asiento,  #datetime.date.today(),
                                              fecha_final__gte=fecha_asiento)  #datetime.date.today())
        cuentas = Codigo_SRI.objects.filter(status=1).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q)).filter(vigencia_retencion=q1).order_by("codigo", "descripcion")

        if q1:
            print str(q1.fecha_inicio)+'fecha de la cosa'
            print str(q1.fecha_final)+'fecha de fin de la cosa'
        else:
            print('no hay lksitta de datos con la consulta :o')

        if cuentas:
            print str(len(cuentas))+'talla de la lista de las cuentas de esta cosa'
        else:
            print('no hay lksitta de datos con la consulta :o')
        paginacion = Paginator(cuentas, per_page)
        try:
            cuentas = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            cuentas = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            cuentas = paginacion.page(paginacion.num_pages)

        count = len(Codigo_SRI.objects.filter(status=1).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q)).filter(vigencia_retencion=q1).order_by("codigo", "descripcion"))

        respuesta.append({"name": u"Sin retención", "id": "",
                          "porcentaje": "", "descripcion": ""})
        for p in cuentas:
            porcentaje = str(p.porc_min) + " - " + str(p.porc_max) if p.porc_variable else p.porcentaje
            respuesta.append({"name": p.codigo, "id": p.id,
                              "porcentaje": porcentaje, "descripcion": p.descripcion[:40]})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')



@login_required(login_url='/')
def buscar_codigo_iva(request):
    """
    Funcion que me busca el proveedor dependiendo de lo que escriba
    el usuario(autocompletado)
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("q_word[]", "")
    pkey_val = request.GET.get("pkey_val", None)
    per_page = int(request.GET.get("per_page", 10))
    page_num = int(request.GET.get("page_num", 1))
    count = 0

    p = q.replace(".", "")
    q = p.replace("_", "")
    if pkey_val is not None and pkey_val != "":
        try:
            p = Codigo_SRI.objects.get(id=pkey_val)
            resultado = json.dumps({"name": p.codigo + ' - ' + p.descripcion[:40], "id": p.id})
            return HttpResponse(resultado, mimetype='application/json')
        except Codigo_SRI.DoesNotExist:
            respuesta = []
    else:

        q1 = VigenciaRetencionSRI.objects.get(tipo_codigo_sri=Tipos_Retencion.objects.get(id=2),
                                              fecha_inicio__lte=datetime.date.today(),
                                              fecha_final__gte=datetime.date.today())

        cuentas = Codigo_SRI.objects.filter(status=1).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q)).filter(vigencia_retencion=q1).order_by("codigo", "descripcion")
        count = len(cuentas)

        paginacion = Paginator(cuentas, per_page)
        try:
            cuentas = paginacion.page(page_num)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            cuentas = paginacion.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            cuentas = paginacion.page(paginacion.num_pages)

        for p in cuentas:
            respuesta.append({"name": p.codigo+ ' - ' + p.descripcion[:40], "id": p.id})

    resultado = json.dumps({"result": respuesta, "cnt_whole": count})
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def obtener_datos_retencion(request):
    """
    Función que obtiene los datos de la retención en la compra
    :param request:
    :return:
    """
    id = request.POST.get("id", "")
    try:
        compra = CompraCabecera.objects.get(id=id, status=1)
        comp_retencion = RetencionCompraCabecera.objects.filter(status=1).get(compra_cabecera=compra)
        respuesta = {"numero": comp_retencion.numero_ret, "fecha": comp_retencion.fecha_emi.strftime("%Y-%m-%d"), "serie": comp_retencion.vigencia_doc_empresa.id}
    except:
        respuesta = {"value": "error base", "key": "error"}
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url="/")
@csrf_exempt
def get_ultima_ret_usada(request):
    """
    Retorna el número de la última retención usada
    :param request: el cual tiene el id del bloque de retenciones
    :return:
    """
    try:
        id_vigencia = request.POST.get("id_vigencia")
        respuesta = {"status": 1, "num_ret": VigenciaDocEmpresa.objects.get(id=id_vigencia).sec_actual}
        resultado = json.dumps(respuesta)
        transaction.rollback()
    except:
        respuesta = {"status": 2, "mensaje": "error interno en el servidor"}
        resultado = json.dumps(respuesta)
        transaction.rollback()

    return HttpResponse(resultado, mimetype='application/json')


def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [dict(zip([col[0] for col in desc], row)) for row in cursor.fetchall()]


@csrf_exempt
def buscar_retencion(request):
    """
    Función ajax que retorna una lista de cuentas de retenciones
    dependiendo del tipo y el porcentaje de la retención
    :param request:
    :return:
    """
    porcentaje = float(request.POST.get("porc_ret", ""))
    respuesta = []
    tipo = request.POST.get("tipo", "")
    if tipo == "r":
        tipo_retencion = Tipos_Retencion.objects.get(id=1)
    else:
        tipo_retencion = Tipos_Retencion.objects.get(id=4)

    cuentas_ret = Cuenta_Retencion.objects.filter(status=1, porcentaje=porcentaje, tipo_codigo_sri=tipo_retencion)
    if len(cuentas_ret) == 0:
        respuesta.append({"id": "0", "descripcion": "No existe cuenta retencion "+str(porcentaje)+"%"})
    else:
        for p in cuentas_ret:
            respuesta.append({"id": p.id, "descripcion": p.plan_cuenta.descripcion})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def get_combo_iva(request):
    """
    Función que me retorna una lista de codigos SRI pero solo IVA
    dependiendo de la fecha del asiento, es decir la fecha
    en la cual esta vigente ese codigo SRI
    :param request:
    :return:
    """
    respuesta = []

    q = request.POST.get("fecha_asiento", "")  # Fecha del asiento
    p = request.POST.get("fecha_anterior", "")  # Fecha anterior para validación

    fecha_split = str(q).split("-")
    fecha = datetime.datetime(int(fecha_split[0]), int(fecha_split[1]), int(fecha_split[2]))

    fecha_split_ant = str(p).split("-")
    fecha_ant = datetime.datetime(int(fecha_split_ant[0]), int(fecha_split_ant[1]), int(fecha_split_ant[2]))

    q1 = VigenciaRetencionSRI.objects.filter(
        tipo_codigo_sri=Tipos_Retencion.objects.get(id=2)).get(fecha_final__gte=fecha, fecha_inicio__lte=fecha)

    q2 = VigenciaRetencionSRI.objects.filter(
        tipo_codigo_sri=Tipos_Retencion.objects.get(id=2)).get(fecha_final__gte=fecha_ant, fecha_inicio__lte=fecha_ant)

    if q1.id != q2.id:  # Se van a cambiar los valores del combo de retencion fte en detalle de compra
        combo_ret_fte = Codigo_SRI.objects.filter(status=1).filter(vigencia_retencion=q1).order_by("porcentaje")
        for p in combo_ret_fte:
            respuesta.append({"id": p.id, "codigo": p.codigo, "porcentaje": p.porcentaje, "descripcion": str(p.porcentaje)+"%" + " - "+ str(p.codigo) + " - " + p.descripcion})
        datos = {'status': 1, 'lista': respuesta}
    else:
        datos = {'status': 0}

    resultado = json.dumps(datos)
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def get_combo_ret_fte(request):
    """
    Función que me retorna una lista de codigos SRI pero solo RET_FTE
    dependiendo de la fecha del asiento, es decir la fecha
    en la cual esta vigente ese codigo SRI
    :param request:
    :return:
    """

    q = request.POST.get("fecha_asiento", "")  # Fecha del asiento
    p = request.POST.get("fecha_anterior", "")  # Fecha anterior para validación

    fecha_split = str(q).split("-")
    fecha = datetime.datetime(int(fecha_split[0]), int(fecha_split[1]), int(fecha_split[2]))

    fecha_split_ant = str(p).split("-")
    fecha_ant = datetime.datetime(int(fecha_split_ant[0]), int(fecha_split_ant[1]), int(fecha_split_ant[2]))

    q1 = VigenciaRetencionSRI.objects.filter(
        tipo_codigo_sri=Tipos_Retencion.objects.get(id=1)).get(fecha_final__gte=fecha, fecha_inicio__lte=fecha)

    q2 = VigenciaRetencionSRI.objects.filter(
        tipo_codigo_sri=Tipos_Retencion.objects.get(id=1)).get(fecha_final__gte=fecha_ant, fecha_inicio__lte=fecha_ant)

    if q1.id != q2.id:  # Se van a cambiar los valores del combo de retencion fte en detalle de compra
        p = Codigo_SRI.objects.filter(status=1).filter(vigencia_retencion=q1).order_by("orden")[0]  # Obtiene el primer valor por default
        respuesta = {"name": p.codigo, "id": p.id,
                     "porcentaje": p.porcentaje, "descripcion": p.descripcion[:40]}

        datos = {'status': 1, 'respuesta': respuesta}
    else:
        datos = {'status': 0}

    resultado = json.dumps(datos)
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def get_combo_ret_iva(request):
    """
    Función que me retorna una lista de codigos SRI pero solo RET_IVA
    dependiendo de la fecha del asiento, es decir la fecha
    en la cual esta vigente ese codigo SRI
    :param request:
    :return:
    """
    respuesta = []

    q = request.POST.get("fecha_asiento", "")
    p = request.POST.get("fecha_anterior", "")  # Fecha anterior para validación

    fecha_split = str(q).split("-")
    fecha = datetime.datetime(int(fecha_split[0]), int(fecha_split[1]), int(fecha_split[2]))

    fecha_split_ant = str(p).split("-")
    fecha_ant = datetime.datetime(int(fecha_split_ant[0]), int(fecha_split_ant[1]), int(fecha_split_ant[2]))

    q1 = VigenciaRetencionSRI.objects.filter(
        tipo_codigo_sri=Tipos_Retencion.objects.get(id=4)).get(fecha_final__gte=fecha, fecha_inicio__lte=fecha)

    q2 = VigenciaRetencionSRI.objects.filter(
        tipo_codigo_sri=Tipos_Retencion.objects.get(id=4)).get(fecha_final__gte=fecha_ant, fecha_inicio__lte=fecha_ant)

    if q1.id != q2.id:  # Se van a cambiar los valores del combo de retencion fte en detalle de compra
        combo_ret_fte = Codigo_SRI.objects.filter(status=1).filter(vigencia_retencion=q1).order_by("porcentaje")

        for p in combo_ret_fte:
            respuesta.append({"id": p.id, "codigo": p.codigo, "porcentaje": p.porcentaje,
                              "descripcion": str(p.porcentaje)+"%" + " - " + str(p.codigo) + " - " + p.descripcion})
        datos = {'status': 1, 'lista': respuesta}
    else:
        datos = {'status': 0}

    resultado = json.dumps(datos)
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def get_combo_ret_serie(request):
    """
    Función que me retorna la serie de la vigencia DOC empresa,
    es decir la serie de los blocks de documentos registrados en la empresa y
    que sean válidos
    :param request:
    :return:
    """
    respuesta = []
    q = request.POST.get("fecha_emi_ret", "")
    fecha_split = str(q).split("-")
    fecha = datetime.date(int(fecha_split[0]), int(fecha_split[1]), int(fecha_split[2]))

    combo_serie_ret = VigenciaDocEmpresa.objects.filter(documento_sri=Documento.objects.get(id=24),
                                                        fecha_emi__lte=fecha,
                                                        fecha_vencimiento__gte=fecha, status=1)
    if emite_docs_electronicos():
        for p in combo_serie_ret:
            respuesta.append({"id": p.id, "num_doc": p.sec_actual, "descripcion": p.serie})
            #respuesta.append({"id": 1, "descripcion": u"Retención Electrónica"})
    else:
        if len(combo_serie_ret) == 0:
            respuesta.append({"id": 0, "descripcion": "Error"})
        else:
            for p in combo_serie_ret:
                respuesta.append({"id": p.id, "num_doc": p.sec_actual, "descripcion": p.serie})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def get_datos_proveedor(request):
    """
    Función que me retorna los datos de la última factura en compras
    a ese proveedor, me ayuda cuando se le hace muchas compras a ese proveedor para no escribir
    muchos datos la autorizacion y la fecha de vencimiento del documento
    :param request:
    :return:
    """
    respuesta = []
    id_prov = request.POST.get("id_prov", "")
    id_tipo_doc = request.POST.get("id_tipo_doc", "")

    try:
        proveedor = Proveedores.objects.get(id=id_prov)
        tipo_doc = Documento.objects.get(codigo_documento=id_tipo_doc)
        tipo_comp = TipoComprobante.objects.get(id=1)
        vigencia_doc_per = VigenciaDocPersona.objects.get(documento_sri=tipo_doc,
                                                          tipo_comprobante=tipo_comp,
                                                          cliente_proveedor_id=proveedor.id)
        respuesta = {"status": 1, "autorizacion": vigencia_doc_per.autorizacion,
                     "vencimiento": vigencia_doc_per.fecha_vencimiento.strftime("%Y-%m-%d")}
    except:
        respuesta = {"status": 0}

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def get_proveedores(request):
    respuesta = []
    for obj in Proveedores.objects.filter(status=1).only("id", "ruc", "razon_social"):
        respuesta.append({"id": obj.id, "ruc": obj.ruc, "razon_social": obj.razon_social})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def get_correo_proveedor(request):
    id_compra = request.POST.get("id", "")
    try:
        compra = CompraCabecera.objects.get(id=id_compra)
        if compra.proveedor.email:
            respuesta = {"status": 1, "correo": compra.proveedor.email}
        else:
            respuesta = {"status": 1, "correo": "El proveedor no tiene asignado un correo electrónico por favor verifique"}
    except CompraCabecera.DoesNotExist:
        respuesta = {"status": 0, "mensaje": "El proveedor no existe"}

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@csrf_exempt
def verificar_fecha_vencimiento_retencion(request):
    """
    Función ajax que valida que la fecha de emisión de la retención
    no sea mayor a su fecha de vencimiento
    :param request:
    :return:
    """
    try:
        fecha_emision = request.POST.get("fecha_emi", "")
        id_vigencia = request.POST.get("id_vigencia", "")
        vigencia = VigenciaDocEmpresa.objects.get(id=id_vigencia)
        fecha_emision_date = datetime.date(int(fecha_emision.split("-")[0]),int(fecha_emision.split("-")[1]), int(fecha_emision.split("-")[2]))

        if fecha_emision_date > vigencia.fecha_vencimiento:
            respuesta = {"status": "2", "mensaje": u"La fecha de emisión de la retención es mayor a "
                                                   u"su fecha de vencimiento"}
        else:
            respuesta = {"status": "1", "mensaje": u"Ok"}

    except:
        respuesta = {"status": "0", "mensaje": u"error"}

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')


class Detalle_Compra(models.Model):
    num_comp = models.CharField(max_length=20, db_column='ncomp')
    cuenta = models.CharField(max_length=20)
    codigo_iva = models.IntegerField()  # id de codigo de porcentaje del iva
    bs = models.IntegerField(db_column='bien_servicio')  # 1 bien / 2 servicios
    ret_fte = models.IntegerField(db_column='cod_ret_fte')  # id de codigo de porcentaje de ret fuente
    ret_iva = models.IntegerField(db_column='cod_ret_iva')  # id de codigo de porcentaje de ret IVA
    valor = models.FloatField()

    class Meta:
        db_table = "dco2015"

class Compra(models.Model):
    num_comp = models.CharField(max_length=20, db_column='ncomp')
    fecha_reg = models.DateField(db_column='fecha_asiento')
    fecha_emi = models.DateField(db_column='fecha_emision')
    proveedor = models.CharField(max_length=30)
    sustento_tributario_sri = models.ForeignKey(Sustento_Tributario, db_column='sust_tributario')
    documento = models.CharField(db_column='tipo_documento')
    num_doc = models.CharField(max_length=20, db_column="num_documento")
    autorizacion = models.CharField(max_length=20)
    vence = models.DateField(db_column='vencimiento')
    forma_pago = models.IntegerField()
    plazo = models.IntegerField(null=True)
    pais = models.IntegerField()

    doble_tributacion = models.BooleanField(db_column='convenio_doble_tributacion')
    sujeto_retencion = models.BooleanField()

    concepto = models.CharField(max_length=250)
    nretencion = models.CharField(max_length=20, db_column='num_retencion')
    vigencia_doc_empresa = models.ForeignKey(VigenciaDocEmpresa, db_column='vigencia_doc_empresa')
    fecha_retencion = models.DateField(db_column='fecha_retencion')
    autorizacion_ret = models.CharField(max_length=10, db_column='autoriza_retencion')
    status = models.IntegerField()

    class Meta:
        db_table = "cco2015"


@transaction.commit_on_success
def migrar_compras(request):
    """
    Función para migrar las compras desde una tabla
    :return:
    """
    now = datetime.datetime.now()
    t_cont = len(Compra.objects.all())
    cont = 1.0

    for obj_compra in Compra.objects.all():
        print "%.2f" % ((float(cont*100)/float(t_cont))), "% ..."
        cont += 1

        if obj_compra.status == 1:
            #Variables para totales
            total_base0 = 0
            total_base_iva = 0
            total_iva = 0
            total_ret_fte = 0
            total_ret_iva = 0
            retenciones_fte = []
            retenciones_iva = []
            id_ret_iva = []
            id_ret_fte = []

            # Calculando los totales de la compra
            for obj in Detalle_Compra.objects.filter(num_comp=obj_compra.num_comp):
                cod_iva = Codigo_SRI.objects.get(id=obj.codigo_iva)
                cod_ret_fte = Codigo_SRI.objects.get(id=obj.ret_fte)

                try:
                    cod_ret_iva = Codigo_SRI.objects.get(id=obj.ret_iva)
                except Codigo_SRI.DoesNotExist:
                    cod_ret_iva = None

                valor = obj.valor

                if int(cod_iva.porcentaje) == 0:
                    base_0 = valor
                    base_iva = 0
                    iva = 0
                else:
                    base_iva = valor
                    iva = (valor * cod_iva.porcentaje) / 100
                    base_0 = 0

                if int(cod_ret_fte.porcentaje) != 0:
                    total_ret_fte += ((valor * cod_ret_fte.porcentaje) / 100)
                    #################################################
                    # Te totaliza los valores de ret_fte por codigo #
                    #################################################
                    if cod_ret_fte.id not in id_ret_fte:
                        retenciones_fte.append([cod_ret_fte, valor])
                        id_ret_fte.append(cod_ret_fte.id)
                    else:
                        for obj in retenciones_fte:
                            if obj[0].id == cod_ret_fte.id:
                                obj[1] += valor

                if cod_ret_iva is not None:
                    if int(cod_ret_iva.porcentaje) != 0:
                        total_ret_iva += (iva * cod_ret_iva.porcentaje) / 100
                        #################################################
                        # Te totaliza los valores de ret_iva por codigo #
                        #################################################
                        if cod_ret_iva.id not in id_ret_iva:
                            retenciones_iva.append([cod_ret_iva, iva])
                            id_ret_iva.append(cod_ret_iva.id)
                        else:
                            for obj in retenciones_iva:
                                if obj[0].id == cod_ret_iva.id:
                                    obj[1] += iva

                total_base0 += base_0
                total_base_iva += base_iva
                total_iva += iva

            total_compra = (total_base0 + total_base_iva + redondeo(total_iva)) - \
                           (redondeo(total_ret_fte) + redondeo(total_ret_iva))

            # COMPRA
            compra_cabecera = CompraCabecera()
            compra_cabecera.num_comp = obj_compra.num_comp
            compra_cabecera.fecha_emi = obj_compra.fecha_emi
            compra_cabecera.fecha_reg = obj_compra.fecha_reg
            if obj_compra.concepto is not None:
                compra_cabecera.concepto = obj_compra.concepto
            else:
                compra_cabecera.concepto = ""
            compra_cabecera.documento = Documento.objects.get(codigo_documento=obj_compra.documento)
            compra_cabecera.proveedor = Proveedores.objects.get(ruc=obj_compra.proveedor)
            compra_cabecera.num_doc = obj_compra.num_doc
            compra_cabecera.plazo = obj_compra.plazo
            compra_cabecera.base0 = total_base0
            compra_cabecera.baseiva = total_base_iva
            compra_cabecera.monto_iva = total_iva
            compra_cabecera.tipo_pago_id = 1
            compra_cabecera.base_ice = 0
            compra_cabecera.fecha_creacion = now
            compra_cabecera.usuario_creacion = 'migracion'
            compra_cabecera.save()

            # ATS
            ats = ATS()
            ats.num_doc = obj_compra.num_doc
            ats.compra_cabecera = compra_cabecera
            ats.documento = Documento.objects.get(codigo_documento=obj_compra.documento)
            ats.autorizacion = obj_compra.autorizacion
            ats.doble_tributacion = obj_compra.doble_tributacion
            ats.fecha_emi = obj_compra.fecha_emi
            ats.fecha_reg = obj_compra.fecha_reg
            ats.pais = obj_compra.pais
            ats.sujeto_retencion = obj_compra.sujeto_retencion
            ats.sustento_tributario_sri = obj_compra.sustento_tributario_sri
            ats.vence = obj_compra.vence
            ats.tipo_pago_sri_id = obj_compra.forma_pago
            ats.base0 = total_base0
            ats.base_ice = 0.0
            ats.monto_ice = 0.0
            ats.num_id = compra_cabecera.proveedor.ruc
            ats.monto_iva = redondeo(total_iva)
            ats.baseiva = total_base_iva
            ats.tipo_comprobante_id = 1  # Tipo de comprobante de compra
            proveedor = Proveedores.objects.get(ruc=obj_compra.proveedor)
            ats.identificacion_id = proveedor.tipo_identificacion_id

            ats.fecha_creacion = now
            ats.usuario_creacion = 'migracion'
            ats.save()

            ###########################
            #    Asiento del Mayor    #
            ###########################
            mayor_cabecera = Cabecera_Comp_Contable()
            mayor_cabecera.concepto_comprobante = compra_cabecera.concepto
            mayor_cabecera.fecha = compra_cabecera.fecha_reg
            mayor_cabecera.numero_comprobante = compra_cabecera.num_comp
            mayor_cabecera.tipo_comprobante = TipoComprobante.objects.get(id=1)

            mayor_cabecera.fecha_creacion = now
            mayor_cabecera.usuario_creacion = 'migracion'
            mayor_cabecera.save()

            ##################################
            #       Cuentas por Pagar        #
            ##################################
            cuentas_pagar = Cuentas_por_Pagar()
            cuentas_pagar.proveedor = compra_cabecera.proveedor

            if es_nota_credito(compra_cabecera.documento.id):
                cuentas_pagar.naturaleza = 1
            else:
                cuentas_pagar.naturaleza = 2

            cuentas_pagar.documento = compra_cabecera.documento
            cuentas_pagar.num_doc = compra_cabecera.num_doc
            cuentas_pagar.compra_cabecera = compra_cabecera

            cuentas_pagar.fecha_reg = compra_cabecera.fecha_reg
            cuentas_pagar.fecha_emi = compra_cabecera.fecha_emi
            cuentas_pagar.plazo = compra_cabecera.plazo
            cuentas_pagar.monto = total_compra
            cuentas_pagar.pagado = 0.0

            cuentas_pagar.fecha_creacion = now
            cuentas_pagar.usuario_creacion = 'migracion'
            cuentas_pagar.save()

            ################################
            #    Asiento del Proveedor     #
            ################################
            detalle_comp_prov = Detalle_Comp_Contable()
            detalle_comp_prov.cabecera_contable = mayor_cabecera
            detalle_comp_prov.plan_cuenta = compra_cabecera.proveedor.plan_cuenta
            detalle_comp_prov.valor = total_compra

            if es_nota_credito(compra_cabecera.documento.id):
                detalle_comp_prov.dbcr = "H"
            else:
                detalle_comp_prov.dbcr = "D"

            detalle_comp_prov.detalle = compra_cabecera.proveedor.plan_cuenta.descripcion
            detalle_comp_prov.fecha_asiento = mayor_cabecera.fecha

            detalle_comp_prov.fecha_creacion = now
            detalle_comp_prov.usuario_creacion = 'migracion'
            detalle_comp_prov.save()

            #############################
            #   Asiento del Iva pagado  #
            #############################
            if total_iva > 0:
                detalle_comp_iva = Detalle_Comp_Contable()
                detalle_comp_iva.cabecera_contable = mayor_cabecera
                detalle_comp_iva.plan_cuenta = PlanCuenta.objects.exclude(status=0).get(tipo=11)  # Tipo Iva PAgado
                detalle_comp_iva.valor = redondeo(total_iva)

                # Cuando es una nota de crédito el iva va al haber
                if es_nota_credito(compra_cabecera.documento.id):
                    detalle_comp_iva.dbcr = "D"
                else:
                    detalle_comp_iva.dbcr = "H"

                detalle_comp_iva.detalle = detalle_comp_iva.plan_cuenta.descripcion
                detalle_comp_iva.fecha_asiento = mayor_cabecera.fecha

                detalle_comp_iva.fecha_creacion = now
                detalle_comp_iva.usuario_creacion = 'migracion'
                detalle_comp_iva.save()


            ####################
            #     Retención    #
            ####################
            if obj_compra.nretencion is not None:
                retencion_cabecera = RetencionCompraCabecera()
                retencion_cabecera.compra_cabecera = compra_cabecera
                retencion_cabecera.numero_ret = obj_compra.nretencion
                retencion_cabecera.fecha_emi = obj_compra.fecha_retencion
                retencion_cabecera.vigencia_doc_empresa_id = obj_compra.vigencia_doc_empresa_id
                retencion_cabecera.totalretfte = total_ret_fte
                retencion_cabecera.totalretiva = total_ret_iva

                retencion_cabecera.fecha_creacion = now
                retencion_cabecera.usuario_creacion = 'migracion'
                retencion_cabecera.save()

                ##############################
                #    Detalle de Retención    #
                ##############################

                # Retención Fuente
                for obj in retenciones_fte:
                    plan_cuenta_id = Cuenta_Retencion.objects.get(porcentaje=obj[0].porcentaje).plan_cuenta_id
                    monto = redondeo((obj[1] * obj[0].porcentaje / 100), 2)
                    ret_detalle = RetencionCompraDetalle()
                    ret_detalle.retencion_cabecera_compra = retencion_cabecera
                    ret_detalle.codigo_sri = obj[0]
                    ret_detalle.valor_retenido = monto
                    ret_detalle.base = obj[1]
                    ret_detalle.plan_cuenta_id = plan_cuenta_id

                    ret_detalle.fecha_creacion = now
                    ret_detalle.usuario_creacion = 'migracion'
                    ret_detalle.save()

                    ############################################################
                    #         Detalle de comprobante Contable retenciones      #
                    ############################################################
                    detalle_comp_cont = Detalle_Comp_Contable()
                    detalle_comp_cont.cabecera_contable = mayor_cabecera
                    detalle_comp_cont.plan_cuenta = ret_detalle.plan_cuenta
                    detalle_comp_cont.valor = ret_detalle.valor_retenido
                    detalle_comp_cont.dbcr = "H"
                    detalle_comp_cont.detalle = ret_detalle.plan_cuenta.descripcion
                    detalle_comp_cont.fecha_asiento = mayor_cabecera.fecha

                    detalle_comp_cont.fecha_creacion = now
                    detalle_comp_cont.usuario_creacion = 'migracion'
                    detalle_comp_cont.save()

                # Retención IVA
                for obj in retenciones_iva:
                    plan_cuenta_id = Cuenta_Retencion.objects.get(porcentaje=obj[0].porcentaje).plan_cuenta_id
                    monto = redondeo((obj[1] * obj[0].porcentaje / 100), 2)
                    ret_detalle = RetencionCompraDetalle()
                    ret_detalle.retencion_cabecera_compra = retencion_cabecera
                    ret_detalle.codigo_sri = obj[0]
                    ret_detalle.valor_retenido = monto
                    ret_detalle.base = obj[1]

                    if obj[0].porcentaje == 30:
                        ret_detalle.tipo_iva = 1  # Bienes
                    else:
                        ret_detalle.tipo_iva = 2  # Servicios

                    ret_detalle.plan_cuenta_id = plan_cuenta_id

                    ret_detalle.fecha_creacion = now
                    ret_detalle.usuario_creacion = 'migracion'
                    ret_detalle.save()

                    ############################################################
                    #         Detalle de comprobante Contable retenciones      #
                    ############################################################
                    detalle_comp_cont = Detalle_Comp_Contable()
                    detalle_comp_cont.cabecera_contable = mayor_cabecera
                    detalle_comp_cont.plan_cuenta = ret_detalle.plan_cuenta
                    detalle_comp_cont.valor = ret_detalle.valor_retenido
                    detalle_comp_cont.dbcr = "H"
                    detalle_comp_cont.detalle = ret_detalle.plan_cuenta.descripcion
                    detalle_comp_cont.fecha_asiento = mayor_cabecera.fecha

                    detalle_comp_cont.fecha_creacion = now
                    detalle_comp_cont.usuario_creacion = 'migracion'
                    detalle_comp_cont.save()


            ########################################
            #        Detalle de la Compra          #
            ########################################

            for obj in Detalle_Compra.objects.filter(num_comp=obj_compra.num_comp):
                cuenta = PlanCuenta.objects.get(codigo=obj.cuenta)
                codigo_iva = Codigo_SRI.objects.get(id=obj.codigo_iva)
                codigo_ret_fte = Codigo_SRI.objects.get(id=obj.ret_fte)
                try:
                    codigo_ret_iva = Codigo_SRI.objects.get(id=obj.ret_iva)
                except Codigo_SRI.DoesNotExist:
                    codigo_ret_iva = None

                if codigo_ret_fte.porcentaje != 0:
                    ret_fte = redondeo((obj.valor * codigo_ret_fte.porcentaje) / 100, 2)
                else:
                    ret_fte = 0

                if codigo_ret_iva is not None:
                    if codigo_ret_iva.porcentaje != 0:
                        ret_iva = redondeo((redondeo(obj.valor * codigo_iva.porcentaje) * codigo_ret_iva.porcentaje) / 10000)
                    else:
                        ret_iva = 0
                else:
                    ret_iva = 0

                detalle_compra = CompraDetalle()
                detalle_compra.valor = obj.valor
                detalle_compra.codigo_iva_id = obj.codigo_iva
                detalle_compra.codigo_ret_fte_id = round(obj.ret_fte, 2)
                detalle_compra.codigo_ret_iva_id = obj.ret_iva
                detalle_compra.compra_cabecera = compra_cabecera
                detalle_compra.sustento_tributario_id = ats.sustento_tributario_sri_id

                detalle_compra.monto_iva = redondeo(obj.valor * codigo_iva.porcentaje/100)
                detalle_compra.monto_ret_fte = ret_fte
                detalle_compra.monto_ret_iva = ret_iva
                detalle_compra.plan_cuenta = cuenta
                detalle_compra.tipo_iva = obj.bs

                detalle_compra.fecha_creacion = now
                detalle_compra.usuario_creacion = 'migracion'
                detalle_compra.save()

                ###############################################
                #         Detalle de comprobante Contable     #
                ###############################################

                detalle_comp_cont = Detalle_Comp_Contable()
                detalle_comp_cont.cabecera_contable = mayor_cabecera

                detalle_comp_cont.plan_cuenta = detalle_compra.plan_cuenta
                detalle_comp_cont.valor = detalle_compra.valor

                # Cuando es una nota de crédito se valora los datos en el haber
                if es_nota_credito(compra_cabecera.documento.id):
                    detalle_comp_cont.dbcr = "D"
                else:
                    detalle_comp_cont.dbcr = "H"

                detalle_comp_cont.detalle = detalle_comp_cont.plan_cuenta.descripcion
                detalle_comp_cont.fecha_asiento = mayor_cabecera.fecha

                detalle_comp_cont.fecha_creacion = now
                detalle_comp_cont.usuario_creacion = 'migracion'
                detalle_comp_cont.save()

        elif obj_compra.status == 2:  # Compras ANULADAS
            ################
            #     COMPRA   #
            ################
            compra_cabecera = CompraCabecera()
            compra_cabecera.num_comp = obj_compra.num_comp
            compra_cabecera.fecha_emi = obj_compra.fecha_emi
            compra_cabecera.fecha_reg = obj_compra.fecha_reg
            compra_cabecera.concepto = obj_compra.concepto
            compra_cabecera.num_doc = ""
            compra_cabecera.concepto = ""
            compra_cabecera.fecha_creacion = now
            compra_cabecera.usuario_creacion = 'migracion'
            compra_cabecera.status = 2
            compra_cabecera.save()

            ###########################
            #    Asiento del Mayor    #
            ###########################
            mayor_cabecera = Cabecera_Comp_Contable()
            mayor_cabecera.concepto_comprobante = compra_cabecera.concepto
            mayor_cabecera.fecha = compra_cabecera.fecha_reg
            mayor_cabecera.numero_comprobante = compra_cabecera.num_comp
            mayor_cabecera.tipo_comprobante = TipoComprobante.objects.get(id=1)  #Comprobante de compra

            mayor_cabecera.fecha_creacion = now
            mayor_cabecera.usuario_creacion = 'migracion'
            mayor_cabecera.status = 2
            mayor_cabecera.save()

            ####################
            #     Retención    #
            ####################
            retencion_cabecera = RetencionCompraCabecera()
            retencion_cabecera.compra_cabecera = compra_cabecera
            retencion_cabecera.numero_ret = obj_compra.nretencion
            retencion_cabecera.fecha_emi = obj_compra.fecha_retencion
            retencion_cabecera.vigencia_doc_empresa_id = obj_compra.vigencia_doc_empresa_id
            retencion_cabecera.totalretfte = 0
            retencion_cabecera.totalretiva = 0

            retencion_cabecera.fecha_creacion = now
            retencion_cabecera.usuario_creacion = 'migracion'
            retencion_cabecera.status = 2
            retencion_cabecera.save()

        elif obj_compra.status == 3:
            ################
            #     COMPRA   #
            ################
            compra_cabecera = CompraCabecera()
            compra_cabecera.num_comp = obj_compra.num_comp
            compra_cabecera.fecha_emi = obj_compra.fecha_emi
            compra_cabecera.fecha_reg = obj_compra.fecha_reg
            compra_cabecera.concepto = obj_compra.concepto
            compra_cabecera.num_doc = ""
            compra_cabecera.concepto = ""
            compra_cabecera.fecha_creacion = now
            compra_cabecera.usuario_creacion = 'migracion'
            compra_cabecera.status = 3
            compra_cabecera.save()

            ####################
            #     Retención    #
            ####################
            retencion_cabecera = RetencionCompraCabecera()
            retencion_cabecera.compra_cabecera = compra_cabecera
            retencion_cabecera.numero_ret = obj_compra.nretencion
            retencion_cabecera.fecha_emi = obj_compra.fecha_retencion
            retencion_cabecera.vigencia_doc_empresa_id = obj_compra.vigencia_doc_empresa_id
            retencion_cabecera.totalretfte = 0
            retencion_cabecera.totalretiva = 0

            retencion_cabecera.fecha_creacion = now
            retencion_cabecera.usuario_creacion = 'migracion'
            retencion_cabecera.status = 3
            retencion_cabecera.save()
        else:
            pass

    return HttpResponse('<body> Sucess </body>')