#! /usr/bin/python
# -*- coding: UTF-8 -*-

from administracion.models import *

LIMITES = 3

def arreglo_paginas_template(numero_maximo, pagina_actual):
    arreglo_numeros = []

    if LIMITES >= numero_maximo:
        for i in range(1, int(numero_maximo)+1):
            arreglo_numeros.append(i)
    else:
        if pagina_actual >= LIMITES:
            ##########################################
            #  Caso en que esta al limite superior   #
            ##########################################
            if pagina_actual > numero_maximo - LIMITES + 1:
                arreglo_numeros.append(1)
                arreglo_numeros.append("...")
                for i in range(numero_maximo - LIMITES + 1, numero_maximo + 1):
                    arreglo_numeros.append(i)
            #######################################
            #  Caso en que esta en el intermedio  #
            #######################################
            elif pagina_actual <= numero_maximo - LIMITES + 1:
                arreglo_numeros.append(1)
                arreglo_numeros.append("...")
                for i in range(pagina_actual - 1, pagina_actual + LIMITES - 1):
                    arreglo_numeros.append(i)
                if pagina_actual < numero_maximo:
                    arreglo_numeros.append("...")
                    arreglo_numeros.append(numero_maximo)
        else:
            ###################################
            # Caso en que paguina actual sea  #
            #        mayor a LIMITE           #
            ###################################
            n = pagina_actual + LIMITES
            if pagina_actual > 1:
                arreglo_numeros.append(1)
                arreglo_numeros.append("...")
            for i in range(int(pagina_actual), n):
                arreglo_numeros.append(i)
            arreglo_numeros.append("...")
            arreglo_numeros.append(numero_maximo)

    return arreglo_numeros


def get_num_filas_template():
    """
    funcion que retorna el número de filas que se muestran en la lista de los templates,
    ej: lista de compras, lista de ventas
    :return:
    """
    empresa = Empresa.objects.all()[0]
    empresa_general = empresa.get_empresa_general()
    empresa_parametro = Empresa_Parametro.objects.using("base_central").get(id=empresa_general.id)
    try:
        if empresa_parametro.num_filas_template is not None:
            return empresa_parametro.num_filas_template
        else:
            return 50
    except:
        return 50

def muestra_paginacion_template(total_paginas):
    '''
    Función que indica si el total de los registros es mayor al #
    de filas que la empresa requiere mostrar (Dato parametrizado en empresa_parametro)
    ej: lista_ventas, lista_compras, lista_centros_costo
    :param total_paginas:
    :return:
    '''
    if total_paginas > get_num_filas_template():
        return True
    else:
        return False