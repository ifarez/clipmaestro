from contabilidad.models import *


class SQLSumCase(models.sql.aggregates.Aggregate):
    """
    Clase que me ayuda a hacer un SUM CASE en SQL
    is_ordinal  = variable para que retorne un valor entero o flotante
    """
    is_ordinal = False
    sql_function = 'SUM'
    sql_template = "%(function)s(CASE %(case)s WHEN %(when)s THEN %(field)s ELSE 0 END)"

    def __init__(self, col, **extra):
        if isinstance(extra['when'], basestring):
            extra['when'] = "'%s'" % extra['when']

        if not extra.get('case', None):
            extra['case'] = '"%s"."%s"' % (extra['source'].model._meta.db_table, extra['source'].name)

        if extra['when'] is None:
            extra['when'] = True
            extra['case'] += ' IS NULL '

        super(SQLSumCase, self).__init__(col, **extra)


class SumCase(models.Aggregate):
    name = 'SUM'

    def add_to_query(self, query, alias, col, source, is_summary):
        aggregate = SQLSumCase(col, source=source, is_summary=is_summary, **self.extra)
        query.aggregates[alias] = aggregate


def get_monto_cobrado_fecha_func(id_cta_x_cobrar, fecha_final):
    cta_cobrar = Cuentas_por_Cobrar.objects.get(id=id_cta_x_cobrar)

    cobrado = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar=cta_cobrar, status=1,
                                                         cobro__fecha_reg__lte=fecha_final).
        filter(~Q(cuentas_x_cobrar__num_doc=F('cobro__num_comp')))
        .aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"])

    return cobrado


def get_monto_saldo_cxc_fecha_func(id_cta_x_cobrar, fecha_final):
    cta_cobrar = Cuentas_por_Cobrar.objects.get(id=id_cta_x_cobrar)
    cobrado = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar=cta_cobrar, status=1,
                                                         cobro__fecha_reg__lte=fecha_final
                                                         ).filter(~Q(cuentas_x_cobrar__num_doc=F('cobro__num_comp')))
        .aggregate(Sum('monto',
                       field="cxc_cobro.monto"))["monto__sum"])

    if cta_cobrar.naturaleza == 2:
        return cobrado - cta_cobrar.monto
    else:
        return cta_cobrar.monto - cobrado


def get_monto_pagado_fecha_func(id_cta_x_pagar, fecha_final):
    cta_pagar = Cuentas_por_Pagar.objects.get(id=id_cta_x_pagar)

    pagado = convert_cero_none(CXP_pago.objects.filter(cuentas_x_pagar=cta_pagar,
                                                       status=1,
                                                       pago__fecha_reg__lte=fecha_final
                                                       ).filter(~Q(cuentas_x_pagar__num_doc=F('pago__num_comp'))).
        aggregate(Sum('monto', field="cxp_pago.monto"))["monto__sum"])

    return pagado


def get_monto_saldo_cxp_fecha_func(id_cta_x_pagar, fecha_final):
    cta_pagar = Cuentas_por_Pagar.objects.get(id=id_cta_x_pagar)
    pagado = convert_cero_none(CXP_pago.objects.filter(cuentas_x_pagar=cta_pagar,
                                                       status=1,
                                                       pago__fecha_reg__lte=fecha_final,
                                                       ).filter(~Q(cuentas_x_pagar__num_doc=F('pago__num_comp'))).
        aggregate(Sum('monto',
                      field="cxp_pago.monto"))["monto__sum"])

    if cta_pagar.naturaleza == 2:
        return pagado - cta_pagar.monto
    else:
        return cta_pagar.monto - pagado