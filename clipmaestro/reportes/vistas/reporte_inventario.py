#! /usr/bin/python
# -*- coding: UTF-8-*-
from librerias.funciones.funciones_vistas import redondeo

__author__ = 'Clip Maestro'
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from contabilidad.models import *
import json
import datetime
from contabilidad.formularios.ComprasForm import *
from contabilidad.formularios.ProveedorForm import *
from django.forms.formsets import formset_factory
from django.contrib.auth.decorators import login_required
from contabilidad.formularios.RetencionesCuentaForm import *
from reportes.formularios.reportes_form import *
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import transaction
from django.forms.util import ErrorList
from django.db import connection
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django.core.paginator import *
from django import template, forms
from administracion.models import *
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
import datetime
import time
import requests
from librerias.funciones.permisos import *
from librerias.funciones.funciones_vistas import *
from reportes.formularios.reportes_form import *
import xlwt
from django.db.models import Sum
from librerias.funciones.excel_response_mod import *
import requests
from librerias.funciones.permisos import *
from librerias.funciones.excel_response_mod import *
from librerias.funciones.funciones_vistas import *
from librerias.funciones.permisos import *
import xlwt

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldo_por_item(request):
    cursor = connection.cursor()
    lista = []
    cabecera_tabla = []
    buscador = BuscadorReportes(initial={"fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    if request. method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaFinal()
        if fecha_ini is not None:
            cursor.execute("select rpt_saldos_por_item(%s);", [fecha_ini])
            total_rows = cursor.fetchall()
            if len(total_rows) > 0:
                for tupla in total_rows[0]:
                    objetos_menu = []
                    for n in str(tupla).split(","):
                        objetos_menu.append(n.replace('(', "").replace(')', "").replace('"', ""))
                    cabecera_tabla.append(objetos_menu)

            for obj in total_rows[1:]:
                objetos_lista = []
                for tupla in obj:
                    for n in str(tupla).split(","):
                        objetos_lista.append(n.replace('(', "").replace(')', "").replace('"', ""))
                lista.append(objetos_lista)

    return render_to_response('reporte_inventario/reporte_saldo_item.html',
                              {"buscador": buscador, "objetos": lista,
                               "cabecera_tabla": cabecera_tabla}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldo_por_item_pdf(request):
    cursor = connection.cursor()
    lista = []
    cabecera_tabla = []
    buscador = BuscadorReportes()

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaFinal()
        if fecha_ini is not None:
            cursor.execute("select rpt_saldos_por_item(%s);", [fecha_ini])
            total_rows = cursor.fetchall()
            if len(total_rows) > 0:
                for tupla in total_rows[0]:
                    objetos_menu = []
                    for n in str(tupla).split(","):
                        objetos_menu.append(n.replace('(', "").replace(')', "").replace('"', ""))
                    cabecera_tabla.append(objetos_menu)

            for obj in total_rows[1:]:
                objetos_lista = []
                for tupla in obj:
                    cont = 0
                    for n in str(tupla).split(","):
                        col = n.replace('(', "").replace(')', "").replace('"', "")
                        if cont == 0:
                            objetos_lista.append(format_html(u"<p>{0}</p>", col))
                        elif cont == 1:
                            objetos_lista.append(format_html(u"<p>{0}</p>", col))
                        elif cont == 2:
                            objetos_lista.append(format_html(u'<p style="text-align:right;">{0}</p>', col))
                        elif cont == 3:
                            objetos_lista.append(format_html(u'<p style="text-align:right;">{0}</p>', col))
                        else:
                            objetos_lista.append(format_html(u'<p style="text-align:right;">{0}</p>', col))
                        cont += 1
                lista.append(objetos_lista)

            empresa = Empresa.objects.filter(status=1)[0]
            usuario = request.user.first_name+' '+request.user.last_name
            fecha = buscador.getFechaFinal().strftime("%d-%m-%Y")
            datos = {"buscador": buscador,
                     "objetos": lista,
                     "cabecera_tabla": cabecera_tabla,
                     'empresa': empresa,
                     'usuario': usuario,
                     'fecha': fecha}

            html = render_to_string('reporte_inventario/reporte_saldo_item_pdf.html', datos,
                                    context_instance=RequestContext(request))
            nombre = 'Rpt_Saldos_Item_hasta_'+ fecha_ini.strftime("%Y-%m-%d")
            return generar_pdf_nombre(html, nombre)


    datos = {"buscador": buscador, "objetos": lista, "cabecera_tabla": cabecera_tabla}
    return render_to_response('reporte_inventario/reporte_saldo_item.html', datos,
                              context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldo_por_item_excel(request):
    cursor = connection.cursor()
    lista = []
    cabecera_tabla = []
    buscador = BuscadorReportes()

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaFinal()
        if fecha_ini is not None:
            cursor.execute("select rpt_saldos_por_item(%s);", [fecha_ini])
            total_rows = cursor.fetchall()
            lista.append(['','',u'Reporte de Saldos por Item'+TITULO])
            lista.append([''])
            lista.append(['',u'Fecha de Saldo'+SUBTITULO,fecha_ini.strftime("%Y-%m-%d")+SUBTITULO])
            lista.append([''])
            cabecera = [u'Código'+COLUMNA, u'Item'+COLUMNA, 'Cantidad'+COLUMNA,'Costo Unitario'+COLUMNA,'Total'+COLUMNA]
            lista.append(cabecera)

            for obj in total_rows[1:]:
                objetos_lista = []
                for tupla in obj:
                    for n in str(tupla).split(","):
                        objetos_lista.append(n.replace('(',"").replace(')',"").replace('"',"").replace('0000000000_Código','Código'))
                lista.append(objetos_lista)

            return ExcelResponse(lista, 'Rpt_Saldos_Item_hasta_'+ fecha_ini.strftime("%Y-%m-%d"))

    datos = {"buscador": buscador, "objetos": lista, "cabecera_tabla": cabecera_tabla}
    return render_to_response('reporte_inventario/reporte_saldo_item.html', datos
                          , context_instance=RequestContext(request))

class BuscadorReportesKardexItems(forms.Form):
    fecha_ini = forms.DateField(label=u"Fecha inicial", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    fecha_final = forms.DateField(label=u"Fecha final", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    item = forms.ChoiceField(choices=[], label=u"Cliente", required=False)
    todoslositems = forms.BooleanField(label=u"Todos los Items", required=False)

    def getFechaIni(self):
        try:
            value = self.cleaned_data["fecha_ini"]
            return value
        except:
            return datetime.datetime.now()

    def getFechaFinal(self):
        try:
            value = self.cleaned_data["fecha_final"]
            return value
        except:
            return datetime.datetime.now()

    def gettodoslositems(self):
        try:
            value = self.cleaned_data["todoslositems"]
            return value
        except:
            return False

    def getItem(self):
        try:
            value = self.data["item"]
            return Item.objects.get(id=value)
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(BuscadorReportesKardexItems, self).__init__(*args, **kwargs)
        self.fields['fecha_ini'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['item'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['title'] = "Seleccione un Item"
        self.fields['item'].choices = [("", "")] + [(x.id, x.nombre) for x in Item.objects.filter(status=1).order_by("nombre")]
        self.fields['item'].widget.attrs['data-style'] = "slc-primary"
        self.fields['item'].widget.attrs['data-width'] = "100%"
        self.fields['item'].widget.attrs['data-size'] = "auto"


def listar_kardex(buscador, clientes, lista_items, cabecera_tabla):
    """
    Lista las cuentas por pagar pendientes por fecha en un arreglo
    :param buscador:
    :param proveed_formset:
    :param lista_saldo_client:
    :param lista_id_client:
    :param request:
    :return:
    """

    class Item_seleccion():
        def __init__(self):
            self.item = Item()
            self.lista = []
            self.listakardexitem = []

    lista_id_item = []
    cursor = connection.cursor()
    cursor2 = connection.cursor()
    fecha_ini = buscador.getFechaIni()
    bandera = 1
    milistadekardexitem = []

    fecha_final = buscador.getFechaFinal()
    if None not in (fecha_ini, fecha_final):
        buscador.is_valid()
        if buscador.getAllItems():
            cursor.execute("select rpt_kardex_por_item(%s,%s);", [fecha_ini, fecha_final])
            total_rows = cursor.fetchall()

            for tupla in total_rows[0]:
                    objetos_menu = []
                    for n in str(tupla).split(","):
                        objetos_menu.append(n.replace('(', "").replace(')', "").replace('"', ""))
                    encabezado = ['Tipo Movimiento', u'Código Item', 'Nombre Item',
                                  'Tipo Comprob', 'Num Comprob', 'Fecha Reg', 'Num Doc', u'Descripción', 'Cantidad',
                                  'Costo Total']
                    cabecera_tabla.append(encabezado)

            for obj in total_rows[1:]:
                objetos_lista = []
                for tupla in obj:
                    for n in str(tupla).split(","):
                        objetos_lista.append(n.replace('(', "").replace(')', "").replace('"', ""))
                lista_items.append(objetos_lista)

        else:
            bandera = 2
            if clientes.is_valid():
                for obj in clientes:
                    informacion = obj.cleaned_data
                    id_item = informacion.get("item")
                    lista = []
                    #try:
                    if id_item not in lista_id_item:
                        lista_id_item.append(id_item)
                        item = Item.objects.get(id=id_item)

                        cursor2.execute('select rpt_kardex_por_item_filtro_yobjeto(\'%s\',\'%s\',%s);' % (fecha_ini.strftime("%Y-%m-%d"),
                                                                                        fecha_final.strftime("%Y-%m-%d"),
                                                                                        id_item))
                        #Cosa con 2 columnas y que me ha cargado un poco de data de una misma consulta,
                        #debo mejor insertar estas cosas en la lista y luego que se borren solitosa
                        #los registros de la base
                        milistadekardexitem.append(Tmp_rpt_kardex_por_item.objects.all())
                        cursor.execute('select rpt_kardex_por_item_filtro(\'%s\',\'%s\',%s);' % (fecha_ini.strftime("%Y-%m-%d"),
                                                                                        fecha_final.strftime("%Y-%m-%d"),
                                                                                        id_item))
                        total_rows = cursor.fetchall()
                        for obj_list in total_rows:
                            for tupla in obj_list:
                                objetos_menu = []
                                for n in str(tupla).split(","):
                                    objetos_menu.append(n.replace('(',"").replace(')',"").replace('"',""))
                                lista.append(objetos_menu)

                        item_selec = Item_seleccion()
                        item_selec.item = item
                        item_selec.lista = lista
                        item_selec.listakardexitem = milistadekardexitem
                        lista_items.append(item_selec)
    return bandera

class Kardex_Item():
    def __init__(self, saldo_inicial=0.0, cantidad_inicial=0.0):
        self.item = Item()
        self.saldo_inicial = saldo_inicial
        self.cantidad_inicial = cantidad_inicial
        self.lista = []

class Kardex_Saldo():
    def __init__(self, saldo=0.0, cantidad=0.0):
        self.inv_detalle = Inventario_Detalle()
        self.saldo = saldo
        self.cantidad = cantidad

@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_kardex_por_item(request):
    """
    Reporte de kardex por items
    :param request:
    :return:
    """
    cabecera_tabla = []
    lista_id_item = []
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year) + "-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    items_formset = formset_factory(ReporteSaldosForm, extra=1)
    mibandera = 0
    items = items_formset(prefix='items')
    objetos = []
    cursor_saldo_inicial = connection.cursor()
    cursor_cantidad_inicial = connection.cursor()

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        items = items_formset(request.POST, prefix='items')
        buscador.is_valid()  # Se llama a la función para usar cleaned_data
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_final):
            if buscador.getAllItems():
                for obj in Item.objects.filter(status=1):
                    cursor_cantidad_inicial.execute('select rpt_saldos_por_item_especifico(%s, %s)', [fecha_ini.strftime("%Y-%m-%d"), obj.id])
                    cantidad_inicial = cursor_cantidad_inicial.fetchone()
                    try:
                        cantidad = float(convert_cero_none(cantidad_inicial[0]))
                    except:
                        cantidad = 0.0

                    cursor_saldo_inicial.execute('select  rpt_saldos_por_item_especifico_saldo(%s, %s)', [fecha_ini.strftime("%Y-%m-%d"), obj.id])
                    saldo_inicial = cursor_saldo_inicial.fetchone()
                    try:
                        saldo = float(convert_cero_none(saldo_inicial[0]))
                    except:
                        saldo = 0.0


                    kardex = Kardex_Item(saldo, cantidad)
                    kardex.item = obj
                    lista = []
                    query = Inventario_Detalle.objects.filter(status=1,
                                                              inventario_cabecera__fecha_reg__range=(fecha_ini.strftime("%Y-%m-%d"), fecha_final.strftime("%Y-%m-%d")), item=obj).order_by("inventario_cabecera__fecha_reg", "inventario_cabecera__tipo_comprobante", "inventario_cabecera__num_comp")

                    saldo_actual = saldo
                    cantidad_actual = cantidad

                    for obj_item in query:

                        if obj_item.cantidad == 0:
                            obj_item.cantidad = 1

                        if obj_item.inventario_cabecera.tipo_comprobante_id == 14:  # ingreso
                            saldo_actual += redondeo(obj_item.cantidad*obj_item.costo, 2)
                            cantidad_actual += redondeo(obj_item.cantidad)
                        elif obj_item.inventario_cabecera.tipo_comprobante_id == 15:  # Egreso
                            saldo_actual -= redondeo(obj_item.cantidad*obj_item.costo, 2)
                            cantidad_actual -= redondeo(obj_item.cantidad)

                        kardex_saldo = Kardex_Saldo(saldo_actual, cantidad_actual)
                        kardex_saldo.inv_detalle = obj_item
                        lista.append(kardex_saldo)
                    kardex.lista = lista

                    if len(lista) > 0:
                        objetos.append(kardex)

            else:

                if items.is_valid():
                    for obj in items:
                        informacion = obj.cleaned_data
                        id_item = informacion.get("item")

                        if id_item is not None and id_item not in lista_id_item:
                            lista_id_item.append(id_item)
                            item = Item.objects.get(id=id_item)
                            cursor_cantidad_inicial.execute('select rpt_saldos_por_item_especifico(%s, %s)', [fecha_ini.strftime("%Y-%m-%d"), item.id])
                            cantidad_inicial = cursor_cantidad_inicial.fetchone()

                            try:
                                cantidad = float(convert_cero_none(cantidad_inicial[0]))
                            except:
                                cantidad = 0.0

                            cursor_saldo_inicial.execute('select  rpt_saldos_por_item_especifico_saldo(%s, %s)', [fecha_ini.strftime("%Y-%m-%d"), item.id])
                            saldo_inicial = cursor_saldo_inicial.fetchone()
                            try:
                                saldo = float(convert_cero_none(saldo_inicial[0]))
                            except:
                                saldo = 0.0

                            kardex = Kardex_Item(saldo, cantidad)
                            kardex.item = item
                            lista = []
                            query = Inventario_Detalle.objects.filter(status=1, inventario_cabecera__fecha_reg__range=(fecha_ini.strftime("%Y-%m-%d"), fecha_final.strftime("%Y-%m-%d")), item=item).order_by("inventario_cabecera__fecha_creacion",
                                                                                 "inventario_cabecera__tipo_comprobante", "inventario_cabecera__num_comp")#inventario_cabecera__fecha_reg estaba en el order_by de arriba

                            saldo_actual = saldo
                            cantidad_actual = cantidad

                            for obj_item in query:
                                if obj_item.cantidad == 0.0:
                                    obj_item.cantidad = 1

                                if obj_item.inventario_cabecera.tipo_comprobante_id == 14:  # ingreso
                                    saldo_actual += redondeo(obj_item.cantidad*obj_item.costo, 2)
                                    cantidad_actual += redondeo(obj_item.cantidad)
                                elif obj_item.inventario_cabecera.tipo_comprobante_id == 15:  # Egreso
                                    saldo_actual -= redondeo(obj_item.cantidad*obj_item.costo, 2)
                                    cantidad_actual -= redondeo(obj_item.cantidad)

                                kardex_saldo = Kardex_Saldo(saldo_actual, cantidad_actual)
                                kardex_saldo.inv_detalle = obj_item
                                lista.append(kardex_saldo)
                            kardex.lista = lista

                            if len(lista) > 0:
                                objetos.append(kardex)

    return render_to_response('reporte_inventario/reporte_kardex_item.html',
                              {"buscador": buscador,
                               "formset_item": items,
                               "objetos": objetos,
                               "cabecera_tabla": cabecera_tabla,
                               "total_paginas": 0, "numero": 0, "mibandera": mibandera, "acumulador": 0},
                              context_instance=RequestContext(request))


'''
def llenar_lista_rep_kardex_item(buscador, motivos, lista_id_motivos, cursor, lista_cont):
    fecha_ini = buscador.getFechaIni().strftime("%Y-%m-%d")
    fecha_final = buscador.getFechaFinal().strftime("%Y-%m-%d")
    if fecha_final > fecha_ini:
        if fecha_final > fecha_ini:
            if motivos.is_valid():
                for obj in motivos:
                    lista = []
                    informacion = obj.cleaned_data
                    id_motivo = informacion.get("tipo_mov_inv")
                    if id_motivo not in lista_id_motivos:
                        lista_id_motivos.append(id_motivo)
                        cursor.execute("select rpt_mov_invent_por_motivo(%s,%s,%s);", [fecha_ini, fecha_final, id_motivo])
                        total_rows = cursor.fetchall()
                        if len(total_rows) > 0:
                            for obj in total_rows:
                                objetos_lista = []
                                for tupla in obj:
                                    for n in str(tupla).split(","):
                                        objetos_lista.append(n.replace('(',"").replace(')',"").replace('"', ""))
                                lista.append(objetos_lista)
                            lista_cont.append(lista)
'''

@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_kardex_item_pdf(request):
    """
    Reporte de kardex por items
    :param request:
    :return:
    """
    cabecera_tabla = []
    lista_id_item = []
    buscador = BuscadorReportes(initial={"fecha_ini":
                                             str(datetime.datetime.now().year) + "-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    items_formset = formset_factory(ReporteSaldosForm, extra=1)
    mibandera = 0
    items = items_formset(prefix='items')
    objetos = []
    cursor_saldo_inicial = connection.cursor()
    cursor_cantidad_inicial = connection.cursor()

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        items = items_formset(request.POST, prefix='items')
        buscador.is_valid()  # Se llama a la función para usar cleaned_data
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_final):
            if buscador.getAllItems():
                for obj in Item.objects.filter(status=1):
                    cursor_cantidad_inicial.execute('select rpt_saldos_por_item_especifico(%s, %s)', [fecha_ini.strftime("%Y-%m-%d"), obj.id])
                    cantidad_inicial = cursor_cantidad_inicial.fetchone()
                    try:
                        cantidad = float(convert_cero_none(cantidad_inicial[0]))
                    except:
                        cantidad = 0.0

                    cursor_saldo_inicial.execute('select  rpt_saldos_por_item_especifico_saldo(%s, %s)', [fecha_ini.strftime("%Y-%m-%d"), obj.id])
                    saldo_inicial = cursor_saldo_inicial.fetchone()
                    try:
                        saldo = float(convert_cero_none(saldo_inicial[0]))
                    except:
                        saldo = 0.0

                    kardex = Kardex_Item(saldo, cantidad)
                    kardex.item = obj
                    lista = []
                    query = Inventario_Detalle.objects.filter(status=1, inventario_cabecera__status=1,
                                                              inventario_cabecera__fecha_reg__range=(fecha_ini.strftime("%Y-%m-%d"),
                                                              fecha_final.strftime("%Y-%m-%d")), item=obj).order_by("inventario_cabecera__fecha_reg", "inventario_cabecera__tipo_comprobante", "inventario_cabecera__num_comp")

                    saldo_actual = saldo
                    cantidad_actual = cantidad

                    for obj_item in query:
                        if obj_item.cantidad == 0:
                            obj_item.cantidad = 1

                        if obj_item.inventario_cabecera.tipo_comprobante_id == 14:  # ingreso
                            saldo_actual += redondeo(obj_item.cantidad*obj_item.costo, 2)
                            cantidad_actual += redondeo(obj_item.cantidad)

                        elif obj_item.inventario_cabecera.tipo_comprobante_id == 15:  # Egreso
                            saldo_actual -= redondeo(obj_item.cantidad*obj_item.costo, 2)
                            cantidad_actual -= redondeo(obj_item.cantidad)

                        kardex_saldo = Kardex_Saldo(saldo_actual, cantidad_actual)
                        kardex_saldo.inv_detalle = obj_item
                        lista.append(kardex_saldo)
                    kardex.lista = lista

                    if len(lista) > 0:
                        objetos.append(kardex)
                empresa = Empresa.objects.filter(status=1)[0]
                usuario = request.user.first_name+' '+request.user.last_name
                datos = {'objetos': objetos, 'empresa': empresa, 'usuario': usuario}
                html = render_to_string('reporte_inventario/reporte_kardex_item_pdf.html', datos,
                                        context_instance=RequestContext(request))

                nombre = "Rpt_Kardex_item"+str(fecha_ini)+"-"+str(fecha_final)
                return generar_pdf_nombre(html, nombre)

            else:
                if items.is_valid():
                    for obj in items:
                        informacion = obj.cleaned_data
                        id_item = informacion.get("item")

                        if id_item not in (None, "") and id_item not in lista_id_item:
                            lista_id_item.append(id_item)
                            item = Item.objects.get(id=id_item)
                            cursor_cantidad_inicial.execute('select rpt_saldos_por_item_especifico(%s, %s)', [fecha_ini.strftime("%Y-%m-%d"), item.id])
                            cantidad_inicial = cursor_cantidad_inicial.fetchone()
                            try:
                                cantidad = float(convert_cero_none(cantidad_inicial[0]))
                            except:
                                cantidad = 0.0

                            cursor_saldo_inicial.execute('select  rpt_saldos_por_item_especifico_saldo(%s, %s)', [fecha_ini.strftime("%Y-%m-%d"), item.id])
                            saldo_inicial = cursor_saldo_inicial.fetchone()
                            try:
                                saldo = float(convert_cero_none(saldo_inicial[0]))
                            except:
                                saldo = 0.0

                            kardex = Kardex_Item(saldo, cantidad)
                            kardex.item = item
                            lista = []
                            query = Inventario_Detalle.objects.filter(status=1, inventario_cabecera__status=1,
                                                                      inventario_cabecera__fecha_reg__range=(fecha_ini.strftime("%Y-%m-%d"),
                                                                     fecha_final.strftime("%Y-%m-%d")), item=item).order_by("inventario_cabecera__fecha_reg", "inventario_cabecera__tipo_comprobante", "inventario_cabecera__num_comp" )

                            saldo_actual = saldo
                            cantidad_actual = cantidad
                            for obj_item in query:
                                if obj_item.cantidad == 0:
                                    obj_item.cantidad = 1

                                if obj_item.inventario_cabecera.tipo_comprobante_id == 14:  # ingreso
                                    saldo_actual += redondeo(obj_item.cantidad*obj_item.costo, 2)
                                    cantidad_actual += redondeo(obj_item.cantidad)
                                elif obj_item.inventario_cabecera.tipo_comprobante_id == 15:  # Egreso
                                    saldo_actual -= redondeo(obj_item.cantidad*obj_item.costo, 2)
                                    cantidad_actual -= redondeo(obj_item.cantidad)

                                kardex_saldo = Kardex_Saldo(saldo_actual,cantidad_actual)
                                kardex_saldo.inv_detalle = obj_item
                                lista.append(kardex_saldo)
                            kardex.lista = lista

                            if len(lista) > 0:
                                objetos.append(kardex)

                empresa = Empresa.objects.filter(status=1)[0]
                usuario = request.user.first_name+' '+request.user.last_name
                datos = {'objetos': objetos, 'empresa': empresa, 'usuario': usuario}

                html = render_to_string('reporte_inventario/reporte_kardex_item_pdf.html', datos, context_instance=RequestContext(request))
                nombre = "Rpt_Kardex_item"+str(fecha_ini)+"-"+str(fecha_final)

                if len(objetos) > 0:
                    return generar_pdf_nombre(html, nombre)

    return render_to_response('reporte_inventario/reporte_kardex_item.html',
                              {"buscador": buscador,
                               "formset_item": items,
                               "objetos": objetos,
                               "cabecera_tabla": cabecera_tabla
                               }, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_kardex_por_item_excel(request):
    """
    Reporte de cuentas por cobrar
    :param request:
    :return:
    """
    lista_final = []
    lista_final.append(['', '', u'Reporte de Kardex por Item'+TITULO])
    lista_final.append([''])
    textodescriptivo = ''
    cabecera_tabla = []
    lista_id_item = []
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year) + "-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    items_formset = formset_factory(ReporteSaldosForm, extra=1)
    items = items_formset(prefix='items')
    objetos = []
    cursor_saldo_inicial = connection.cursor()
    cursor_cantidad_inicial = connection.cursor()
    costo_invnetario = 0.0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        items = items_formset(request.POST, prefix='items')
        buscador.is_valid()  # Se llama a la función para usar cleaned_data
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_final):
            lista_final.append(['', u'Fecha Inicio: '+SUBTITULO, fecha_ini.strftime("%Y-%m-%d")+SUBTITULO, '',
                                u'Fecha Hasta: '+SUBTITULO, fecha_final.strftime("%Y-%m-%d")+SUBTITULO])

            lista_final.append([''])
            lista_final.append([''])
            if buscador.getAllItems():
                for obj in Item.objects.filter(status=1):
                    cursor_cantidad_inicial.execute('select rpt_saldos_por_item_especifico(%s, %s)',
                                                    [fecha_ini.strftime("%Y-%m-%d"), obj.id])

                    cantidad_inicial = cursor_cantidad_inicial.fetchone()

                    try:
                        cantidad = float(convert_cero_none(cantidad_inicial[0]))
                    except:
                        cantidad = 0.0

                    cursor_saldo_inicial.execute('select  rpt_saldos_por_item_especifico_saldo(%s, %s)', [fecha_ini.strftime("%Y-%m-%d"), obj.id])
                    saldo_inicial = cursor_saldo_inicial.fetchone()

                    try:
                        saldo = float(convert_cero_none(saldo_inicial[0]))
                    except:
                        saldo = 0.0

                    try:
                        costo_invnetario = redondeo(saldo/cantidad, 2)
                    except ZeroDivisionError:
                        costo_invnetario = 0.0

                    kardex = Kardex_Item(saldo)
                    kardex.item = obj
                    lista = []
                    query = Inventario_Detalle.objects.filter(inventario_cabecera__status=1, status=1,
                                                              inventario_cabecera__fecha_reg__range=(fecha_ini.strftime("%Y-%m-%d"), fecha_final.strftime("%Y-%m-%d")),
                                                              item=obj).order_by("inventario_cabecera__fecha_reg", "inventario_cabecera__tipo_comprobante", "inventario_cabecera__num_comp")


                    if len(query) > 0:
                        lista_final.append([u'Item: '+SUBTITULO, unicode(obj.nombre+SUBTITULO), '',
                                            u'Código: '+SUBTITULO, obj.codigo+SUBTITULO])

                        lista_final.append([u'Fecha Reg'+COLUMNA,
                                            u'Tipo Comp'+COLUMNA,
                                            u'Num Comp'+COLUMNA,
                                            u'Tipo Mov'+COLUMNA,
                                            u'Num Doc'+COLUMNA,
                                            u'Descripción'+COLUMNA,
                                            u'Cantidad'+COLUMNA,
                                            u'Costo Inventario'+COLUMNA,
                                            u'Debito'+COLUMNA,
                                            u'Crédito'+COLUMNA,
                                            u'Saldo'+COLUMNA,
                                            u'Stock'+COLUMNA])

                        lista_final.append([u'Saldo Inicial:'+SUBTITULO, '', '', '', '', '', '', str(costo_invnetario),
                                            '', '', saldo, cantidad])

                        saldo_actual = saldo
                        cantidad_actual = cantidad

                        for obj_item in query:

                            if obj_item.inventario_cabecera.tipo_comprobante_id == 14:  # Ingreso
                                saldo_actual += redondeo(obj_item.cantidad*obj_item.costo, 2)
                                cantidad_actual += redondeo(obj_item.cantidad)

                            elif obj_item.inventario_cabecera.tipo_comprobante_id == 15:  # Egreso
                                saldo_actual -= redondeo(obj_item.cantidad*obj_item.costo, 2)
                                cantidad_actual -= redondeo(obj_item.cantidad)

                            kardex_saldo = Kardex_Saldo(saldo_actual)
                            kardex_saldo.inv_detalle = obj_item

                            debito = 0.00
                            credito= 0.00

                            if obj_item.inventario_cabecera.tipo_comprobante_id == 14:
                                debito = redondeo(obj_item.cantidad * obj_item.costo, 2)
                            else:
                                credito = redondeo(obj_item.cantidad * obj_item.costo, 2)

                            try:
                                if obj_item.inventario_cabecera.tipo_comprobante.descripcion is not None:
                                    textodescriptivo = obj_item.inventario_cabecera.tipo_comprobante.descripcion
                            except:
                                textodescriptivo = 'No disponible'

                            mihilera = [str(obj_item.inventario_cabecera.fecha_reg),
                                            unicode(textodescriptivo),
                                            obj_item.inventario_cabecera.num_comp,
                                            unicode(obj_item.inventario_cabecera.tipo_mov_inventario.descripcion),
                                            obj_item.inventario_cabecera.num_doc,
                                            unicode(obj_item.inventario_cabecera.detalle),
                                            obj_item.cantidad,
                                            obj_item.costo,
                                            debito,
                                            credito,
                                            saldo_actual,
                                            cantidad_actual]

                            #lista_final.append(fila)
                            lista_final.append(mihilera)
                            lista.append(kardex_saldo)
                        kardex.lista = lista

                        if len(lista) > 0:

                            objetos.append(kardex.lista)
                        lista_final.append([''])
                        lista_final.append([''])

                return ExcelResponse(lista_final, 'Rpt_kardex_item'+str(fecha_ini)+'-'+str(fecha_final))

            else:
                if items.is_valid():
                    for obj in items:
                        informacion = obj.cleaned_data
                        id_item = informacion.get("item")

                        if id_item not in (None, "") and id_item not in lista_id_item:
                            #esto es de cada iteracion
                            lista_id_item.append(id_item)
                            item = Item.objects.get(id=id_item)
                            cursor_cantidad_inicial.execute('select rpt_saldos_por_item_especifico(%s, %s)', [fecha_ini.strftime("%Y-%m-%d"), item.id])
                            cantidad_inicial = cursor_cantidad_inicial.fetchone()
                            try:
                                cantidad = float(convert_cero_none(cantidad_inicial[0]))
                            except:
                                cantidad = 0.0

                            cursor_saldo_inicial.execute('select  rpt_saldos_por_item_especifico_saldo(%s, %s)', [fecha_ini.strftime("%Y-%m-%d"), item.id])
                            saldo_inicial = cursor_saldo_inicial.fetchone()
                            try:
                                saldo = float(convert_cero_none(saldo_inicial[0]))
                            except:
                                saldo = 0.0

                            cantidad_actual = cantidad

                            try:
                                costo_invnetario = redondeo(saldo/cantidad, 2)
                            except ZeroDivisionError:
                                costo_invnetario = 0.0


                            kardex = Kardex_Item(saldo)
                            kardex.item = item
                            lista = []
                            query = Inventario_Detalle.objects.filter(inventario_cabecera__status=1, status=1,
                                                                      inventario_cabecera__fecha_reg__range=(fecha_ini.strftime("%Y-%m-%d"), fecha_final.strftime("%Y-%m-%d")),
                                                                      item=item).order_by("inventario_cabecera__fecha_reg",
                                                                                          "inventario_cabecera__tipo_comprobante", "inventario_cabecera__num_comp")

                            if len(query) > 0:
                                saldo_actual = saldo
                                lista_final.append([u'Item: '+SUBTITULO,unicode(item.nombre+SUBTITULO),
                                                    '', u'Código: '+SUBTITULO, item.codigo+SUBTITULO])

                                lista_final.append([u'Fecha Reg'+COLUMNA,
                                                    u'Tipo Comp'+COLUMNA,
                                                    u'Num Comp'+COLUMNA,
                                                    u'Tipo Mov'+COLUMNA,
                                                    u'Num Doc'+COLUMNA,
                                                    u'Descripción'+COLUMNA,
                                                    u'Cantidad'+COLUMNA,
                                                    u'Costo Inventario'+COLUMNA,
                                                    u'Debito'+COLUMNA,
                                                    u'Crédito'+COLUMNA,  u'Saldo'+COLUMNA, u'Stock'+COLUMNA])


                                lista_final.append([u'Saldo Inicial:'+SUBTITULO, '', '', '', '', '', '',
                                                    str(costo_invnetario), '', '', saldo, cantidad])


                                for obj_item in query:
                                    if obj_item.inventario_cabecera.tipo_comprobante_id == 14:  # ingreso
                                        saldo_actual += redondeo(obj_item.cantidad*obj_item.costo, 2)
                                        cantidad_actual += redondeo(obj_item.cantidad)

                                    elif obj_item.inventario_cabecera.tipo_comprobante_id == 15:  # Egreso
                                        saldo_actual -= redondeo(obj_item.cantidad*obj_item.costo, 2)
                                        cantidad_actual -= redondeo(obj_item.cantidad)

                                    kardex_saldo = Kardex_Saldo(saldo_actual)
                                    kardex_saldo.inv_detalle = obj_item

                                    debito = 0.00
                                    credito= 0.00

                                    if obj_item.inventario_cabecera.tipo_comprobante_id == 14:
                                        debito = redondeo(obj_item.cantidad * obj_item.costo,2)
                                    else:
                                        credito = redondeo(obj_item.cantidad * obj_item.costo,2)

                                    fila = []
                                    mihilera = [unicode(str(obj_item.inventario_cabecera.fecha_reg)),
                                            unicode(obj_item.inventario_cabecera.tipo_comprobante.descripcion),
                                            obj_item.inventario_cabecera.num_comp,
                                            unicode(obj_item.inventario_cabecera.tipo_mov_inventario.descripcion),
                                            obj_item.inventario_cabecera.num_doc,
                                            unicode(obj_item.inventario_cabecera.detalle),
                                            obj_item.cantidad,
                                            str(obj_item.costo),
                                            debito,
                                            credito,
                                            saldo_actual,
                                            cantidad_actual]

                                    lista_final.append(mihilera)
                                    lista.append(kardex_saldo)
                                kardex.lista = lista

                                if len(lista) > 0:
                                    objetos.append(kardex)
                                lista_final.append([''])
                                lista_final.append([''])
                if len(lista_id_item) > 0:
                    return ExcelResponse(lista_final, 'Rpt_kardex_item'+str(fecha_ini)+'-'+str(fecha_final))

    return render_to_response('reporte_inventario/reporte_kardex_item.html',
                              {"buscador": buscador,
                               "formset_item": items,
                               "objetos": objetos,
                               "cabecera_tabla": cabecera_tabla
                               }, context_instance=RequestContext(request))


def llenar_lista_rep_inv_motivo(buscador, motivos, lista_id_motivos, cursor, lista_cont):
    try:
        fecha_ini = buscador.getFechaIni().strftime("%Y-%m-%d")
        fecha_final = buscador.getFechaFinal().strftime("%Y-%m-%d")
        if fecha_final > fecha_ini:
            if fecha_final > fecha_ini:
                if motivos.is_valid():
                    for obj in motivos:
                        lista = []
                        informacion = obj.cleaned_data
                        id_motivo = informacion.get("tipo_mov_inv")

                        if id_motivo not in (None, "") and id_motivo not in lista_id_motivos:
                            lista_id_motivos.append(id_motivo)
                            cursor.execute("select rpt_mov_invent_por_motivo(%s,%s,%s);", [fecha_ini, fecha_final, id_motivo])
                            total_rows = cursor.fetchall()
                            if len(total_rows) > 1:
                                for obj in total_rows:
                                    objetos_lista = []
                                    for tupla in obj:
                                        for n in str(tupla).split(","):
                                            objetos_lista.append(n.replace('(',  "").replace(')',"").replace('"', ""))
                                    lista.append(objetos_lista)
                                lista_cont.append(lista)
    except:
        print 'error en el llenado de los motivos'

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_movim_inventario_motivo(request):
    cursor = connection.cursor()
    lista_cont = []  # Lista que contendra a los detalles de cada uno de los motivos

    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year) + "-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    formset_motivo = formset_factory(ReporteSaldosForm, extra=1)
    motivos = formset_motivo(prefix='motivo')
    lista_id_motivos = []

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        motivos = formset_motivo(request.POST, prefix='motivo')

        buscador.is_valid()
        llenar_lista_rep_inv_motivo(buscador, motivos, lista_id_motivos, cursor, lista_cont)
    cursor.close()

    return render_to_response('reporte_inventario/reporte_movim_inventario_motivo.html',
                              {"buscador": buscador,
                               "objetos": lista_cont,
                               "motivos": motivos}, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_movim_inventario_motivo_excel(request):
    cursor = connection.cursor()
    lista_cont = []  # Lista que contendra a los detalles de cada uno de los motivos
    formset_motivo = formset_factory(ReporteSaldosForm, extra=1)
    lista_id_motivos = []
    lista = []
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year) + "-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    motivos = formset_motivo(prefix='motivo')

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        motivos = formset_motivo(request.POST, prefix='motivo')

        buscador.is_valid()
        if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
            llenar_lista_rep_inv_motivo(buscador, motivos, lista_id_motivos, cursor, lista_cont)
            lista.append([u"", u"", u"", u"", u"Reporte Inventario por Motivo"+TITULO])
            lista.append([u""])
            lista.append([u"", u"", u"Fecha Inicial: "+SUBTITULO, buscador.getFechaIni().strftime("%Y-%m-%d")])
            lista.append([u"", u"", u"Fecha Final: "+SUBTITULO, buscador.getFechaFinal().strftime("%Y-%m-%d")])
            for obj in lista_cont:
                cont = 0
                for obj_lista in obj:
                    cab = []
                    for obj_cab in obj_lista:
                        if cont == 0:
                            cab.append(obj_cab.decode('utf-8')+COLUMNA)
                        else:
                            cab.append(obj_cab.decode('utf-8'))
                    lista.append(cab)
                    cont += 1
                lista.append([u""])

        cursor.close()
        if len(lista_cont) > 0:
            return ExcelResponse(lista, 'Rpt_Inv_motivo_'+str(buscador.getFechaIni().strftime("%Y-%m-%d"))+'_'+str(buscador.getFechaFinal().strftime("%Y-%m-%d")))

    return render_to_response('reporte_inventario/reporte_movim_inventario_motivo.html',
                              {"buscador": buscador,
                               "objetos": lista_cont,
                               "motivos": motivos}, context_instance=RequestContext(request))


@login_required(login_url='/')
def reporte_inicio(request):
    return render_to_response('reportes_inicio/reporte_inicio.html', context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_items(request):
    items = Item.objects.filter(status=1).order_by('nombre')
    datos = {'items': items}
    return render_to_response('reporte_inventario/reporte_items.html', datos, context_instance=RequestContext(request))

'''
@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_items_jasperdescomentar(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    items = Item.objects.filter(status=1).order_by('nombre')
    match = resolve(request.path)
    print 'el path es', str(request.path)
    url = 'http://162.242.232.70:8080/jasperserver/rest_v2/reports' + empresa.path_jasper + '/'
    print str(url),"url del sistema zzzzzzzzzzzzz"
    permiso = Permisos.objects.using("base_central").get(etiqueta=str(match.url_name).replace(" ", ""))
    if permiso:
        print('permiso existe')
    else:
        print('permiso se cayo')
    print str(permiso.nombre_reporte),"los permsos del sistema zzzzzzzzzzzzz"
    #report = permiso.nombre_reporte + '.html'
    report = 'item' + '.html'
    # Authorisation credentials:
    auth = (empresa.user_jasper, empresa.pass_jasper)
    s = requests.Session()
    r = s.get(url=url+report, auth=auth)
    datos = {'items': items}
    return render_to_response('reporte_inventario/reporte_items.html',
                              {"objetos": r
                               }, context_instance=RequestContext(request))
'''

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_items_pdf(request):
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    items = Item.objects.filter(status=1).order_by('nombre')
    datos = {'items': items, 'empresa': empresa, 'usuario': usuario}
    html = render_to_string('reporte_inventario/reporte_items_pdf.html', datos,
                            context_instance=RequestContext(request))
    nombre = "Rpt_items_a_"+str(time.strftime("%d/%m/%y"))
    #return generar_pdf(html)
    return generar_pdf_nombre(html, nombre)

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_items_excel(request):
    items = Item.objects.filter(status=1).order_by('nombre')
    lista = []
    lista.append([u'', u'', u'Reportes de Items'+TITULO])
    lista.append([u'Fecha de Generación: '+SUBTITULO, str(datetime.datetime.now().strftime('%Y-%m-%d'))+SUBTITULO])
    lista.append([''])
    cabecera = [u'Código'+COLUMNA, u'Item'+COLUMNA, u'Unidad'+COLUMNA, u'Precio'+COLUMNA, u'Iva'+COLUMNA, u'Total'+COLUMNA, u'Stock'+COLUMNA]
    lista.append(cabecera)

    if items:
        for item in items:
            precio_iva = 0.00

            if item.TipoItem() == 1:
                if item.cantidadBodega():
                    stock = item.cantidadBodega()
                else:
                    stock = 0.0
            else:
                    stock = "              NO APLICA"

            if item.iva and item.iva == 12:
                precio_iva = redondeo(convert_cero_none(item.precio1)*convert_cero_none(0.12), 2)

            row = [item.codigo, item.nombre, item.unidad.descripcion, redondeo(convert_cero_none(item.precio1), 2),
                   redondeo(precio_iva, 2),
                   redondeo(convert_cero_none(item.precio1) * (1 + (convert_cero_none(item.iva)/100)), 2), stock]

            lista.append(row)

    return ExcelResponse(lista, 'Rpt_Items_a_'+str(datetime.datetime.now().strftime('%Y-%m-%d')))
