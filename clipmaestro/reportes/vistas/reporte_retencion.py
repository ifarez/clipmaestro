#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from contabilidad.formularios.ComprasForm import *
from django.forms.formsets import formset_factory
from django.contrib.auth.decorators import login_required
from contabilidad.formularios.RetencionesCuentaForm import *
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import transaction
from django.forms.util import ErrorList
from django.db import connection
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django.core.paginator import *
from django import template
from administracion.models import *
from librerias.funciones.permisos import *

@csrf_exempt
@login_required(login_url='/')
def buscar_proveedor_retencion(request):
    respuesta = []
    q = request.GET.get("term", "")
    try:

        cuentas = RetencionDetalle.objects.filter(status=1).filter(Q(retencion_cabecera__compra_cabecera__proveedor__ruc__icontains = q)|Q(retencion_cabecera__compra_cabecera__proveedor__nombre__icontains = q)).distinct('retencion_cabecera__compra_cabecera__proveedor__ruc')[0:20]

        for p in cuentas:

            respuesta.append({"value": p.retencion_cabecera.compra_cabecera.proveedor.nombre+" - "+p.retencion_cabecera.compra_cabecera.proveedor.ruc, "key": p.retencion_cabecera.compra_cabecera.proveedor.nombre+" - "+p.retencion_cabecera.compra_cabecera.proveedor.ruc})
    except:
        respuesta.append({"value": "error base", "key": "error"})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

from datetime import date, timedelta
def reporte_retencion_fuente(request):

    total_compras = 0.0
    fecha_inicio = request.GET.get("fecha_inicio","0001-01-01")
    fecha_fin = request.GET.get("fecha_fin",datetime.datetime.now())
    q = request.GET.get("proveedor_ruc","")

    provedor_tmp = q

    if "-" in q and q != "":

        provedor_nombre = q.split("-")[0].replace(" ","")
        provedor_ruc = q.split("-")[1].replace(" ","")
    else:

        provedor_nombre = q
        provedor_ruc = q


    if fecha_inicio == "0001-01-01" or fecha_inicio == "":
        fecha_tempo = str(datetime.datetime.now())
        anio = fecha_tempo.split("-")[0]
        fecha_inicio = anio+"-01-01"
        year = int(str(fecha_inicio).split("-")[0])
        month = int(str(fecha_inicio).split("-")[1])
        day = int(str(fecha_inicio).split("-")[2])
        fecha_inicio = datetime.date(year,month,day)
    else:
        year = int(str(fecha_inicio).split("-")[0])
        month = int(str(fecha_inicio).split("-")[1])
        day = int(str(fecha_inicio).split("-")[2])
        fecha_inicio = datetime.date(year,month,day)


    if fecha_fin == "" or  fecha_fin == datetime.datetime.now():
        fecha_fin = datetime.datetime.now()

    else:
        year = int(str(fecha_fin).split("-")[0])
        month = int(str(fecha_fin).split("-")[1])
        day = int(str(fecha_fin).split("-")[2])
        fecha_fin = datetime.date(year,month,day)


    retenciones = RetencionDetalle.objects.filter(status=1,tipo_retencion=1).filter(Q(retencion_cabecera__compra_cabecera__proveedor__ruc__icontains = provedor_ruc)|Q(retencion_cabecera__compra_cabecera__proveedor__nombre__icontains = provedor_nombre)).filter(retencion_cabecera__fecha_emision__range=(fecha_inicio, fecha_fin)).order_by('codigo_retencion_fte__codigo','retencion_cabecera__fecha_emision')


    total_registros= len(retenciones)


    paginacion = Paginator(retenciones, 10)
    numero_pagina = request.GET.get("page")

    try:
        lista_cabecera = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista_cabecera = paginacion.page(1)
    except EmptyPage:
        lista_cabecera = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = lista_cabecera.number


    return render_to_response("reportes_retenciones/reporte_retenciones.html",{"objetos":lista_cabecera,"total_registros":total_registros, "total_paginas": total_paginas, "numero":numero,"proveedor_ruc":provedor_tmp, "fecha_inicio":fecha_inicio, "fecha_fin":fecha_fin, "request": request}, context_instance=RequestContext(request))




from excel_response import ExcelResponse
import xlwt
def reporte_retencion_fuente_proveedor_fechas_excel(request):

    fecha_inicio = request.GET.get("fecha_inicio","0001-01-01")
    fecha_fin = request.GET.get("fecha_fin",datetime.datetime.now())
    q = request.GET.get("proveedor_ruc","")

    provedor_tmp = q

    if "-" in q and q != "":

        provedor_nombre = q.split("-")[0].replace(" ","")
        provedor_ruc = q.split("-")[1].replace(" ","")
    else:

        provedor_nombre = q
        provedor_ruc = q



    if fecha_inicio == "0001-01-01" or fecha_inicio == "":
        fecha_tempo = str(datetime.datetime.now())
        anio = fecha_tempo.split("-")[0]
        fecha_inicio = anio+"-01-01"
        year = int(str(fecha_inicio).split("-")[0])
        month = int(str(fecha_inicio).split("-")[1])
        day = int(str(fecha_inicio).split("-")[2])
        fecha_inicio = datetime.date(year,month,day)
    else:
        year = int(str(fecha_inicio).split("-")[0])
        month = int(str(fecha_inicio).split("-")[1])
        day = int(str(fecha_inicio).split("-")[2])
        fecha_inicio = datetime.date(year,month,day)


    if fecha_fin == "" or  fecha_fin == datetime.datetime.now():
        fecha_fin = datetime.datetime.now()

    else:
        year = int(str(fecha_fin).split("-")[0])
        month = int(str(fecha_fin).split("-")[1])
        day = int(str(fecha_fin).split("-")[2])
        fecha_fin = datetime.date(year,month,day)


    retenciones = RetencionDetalle.objects.filter(status=1,tipo_retencion=1).filter(Q(retencion_cabecera__compra_cabecera__proveedor__ruc__icontains = provedor_ruc)|Q(retencion_cabecera__compra_cabecera__proveedor__nombre__icontains = provedor_nombre)).filter(retencion_cabecera__fecha_emision__range=(fecha_inicio, fecha_fin)).order_by('codigo_retencion_fte__codigo','retencion_cabecera__fecha_emision')



    fila_vacia = []
    fila_vacia.append("")
    fila_vacia.append("")
    fila_vacia.append("")
    fila_vacia.append("")
    fila_vacia.append("")
    fila_vacia.append("")
    fila_vacia.append("")
    fila_vacia.append("")
    fila_vacia.append("")
    fila_vacia.append("")
    fila_vacia.append("")
    fila_vacia.append("")


    datos_proveedor = []
    excel = []
    excel.append(fila_vacia)
    if provedor_nombre != "":
        datos_proveedor.append("Provedor: ")
        datos_proveedor.append(provedor_nombre)
        datos_proveedor.append("Ruc: ")
        datos_proveedor.append(provedor_ruc)

    else:
        datos_proveedor.append("")
        datos_proveedor.append("")
        datos_proveedor.append("")
        datos_proveedor.append("")

    datos_proveedor.append("Fecha Inicio: ")
    datos_proveedor.append(fecha_inicio.strftime("%d-%m-%Y"))
    datos_proveedor.append("Fecha Fin: ")
    datos_proveedor.append(fecha_fin.strftime("%d-%m-%Y"))
    datos_proveedor.append("")
    datos_proveedor.append("")
    datos_proveedor.append("")
    datos_proveedor.append("")
    excel.append(datos_proveedor)



    cabecera = []
    cabecera.append("Fecha Emisión")
    cabecera.append("Código")
    cabecera.append("# Comprobante")
    cabecera.append("Glosa")

    if provedor_nombre == "":
         cabecera.append("Proveedor")
         cabecera.append("RUC")

    cabecera.append("# Documento")
    cabecera.append("Base")
    cabecera.append("Retención")
    cabecera.append("Verificación")

    cabecera.append("Diferencia")
    excel.append(fila_vacia)
    excel.append(cabecera)

    cod_ret_ant = ""


    for obj in retenciones:

        if cod_ret_ant != obj.codigo_retencion_fte.codigo:


            body = []

            body.append(obj.retencion_cabecera.fecha_emision.strftime('%d-%m-%Y'))
            body.append(obj.codigo_retencion_fte.codigo)
            body.append(obj.retencion_cabecera.numero_comprobante)
            body.append(obj.retencion_cabecera.compra_cabecera.concepto)
            if provedor_nombre == "":
                body.append(obj.retencion_cabecera.compra_cabecera.proveedor.nombre)
                body.append(obj.retencion_cabecera.compra_cabecera.proveedor.ruc)

            body.append(obj.retencion_cabecera.numero_retencion)
            body.append(("%.2f"%float(obj.valor)))
            body.append(("%.2f"%float(obj.total)))
            body.append(("%.2f"%float(obj.valor * (obj.porcentaje/100))))
            body.append(("%.2f"%float(obj.total - (obj.valor * (obj.porcentaje/100)))))


            excel.append(body)
        else:

            body = []

            body.append(obj.retencion_cabecera.fecha_emision.strftime('%d-%m-%Y'))
            body.append(obj.codigo_retencion_fte.codigo)
            body.append(obj.retencion_cabecera.numero_comprobante)
            body.append(obj.retencion_cabecera.compra_cabecera.concepto)
            if provedor_nombre == "":
                body.append(obj.retencion_cabecera.compra_cabecera.proveedor.nombre)
                body.append(obj.retencion_cabecera.compra_cabecera.proveedor.ruc)

            body.append(obj.retencion_cabecera.numero_retencion)
            body.append(("%.2f"%float(obj.valor)))
            body.append(("%.2f"%float(obj.total)))
            body.append(("%.2f"%float(obj.valor * (obj.porcentaje/100))))
            body.append(("%.2f"%float(obj.total - (obj.valor * (obj.porcentaje/100)))))

            excel.append(body)
        cod_ret_ant = obj.codigo_retencion_fte.codigo


    return ExcelResponse(excel,'Reporte de Retención Fuente')


def generar_retencion_fuente_pdf(template_src, context_dict):
    template = get_template(template_src)
    context = Context(context_dict)
    html = template.render(context)
    result = StringIO.StringIO()

    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), mimetype='application/pdf')
    return HttpResponse('Tenemos algunos errores<pre>%s</pre>' % escape(html))


@login_required(login_url='/')
def reporte_retencion_fuente_pdf(request):

    fecha_inicio = request.GET.get("fecha_inicio")
    fecha_fin = request.GET.get("fecha_fin",datetime.datetime.now())
    q = request.GET.get("proveedor_ruc","")

    provedor_tmp = q

    if "-" in q and q != "":

        provedor_nombre = q.split("-")[0].replace(" ","")
        provedor_ruc = q.split("-")[1].replace(" ","")
    else:

        provedor_nombre = q
        provedor_ruc = q



    if fecha_inicio == "":
        fecha_tempo = str(datetime.datetime.now())
        anio = fecha_tempo.split("-")[0]
        fecha_inicio = anio+"-01-01"
        year = int(str(fecha_inicio).split("-")[0])
        month = int(str(fecha_inicio).split("-")[1])
        day = int(str(fecha_inicio).split("-")[2])
        fecha_inicio = datetime.date(year,month,day)

    else:
        year = int(str(fecha_inicio).split("-")[0])
        month = int(str(fecha_inicio).split("-")[1])
        day = int(str(fecha_inicio).split("-")[2])
        fecha_inicio = datetime.date(year,month,day)

    if fecha_fin == "":
        fecha_fin = datetime.datetime.now()
    else:
        year = int(str(fecha_fin).split("-")[0])
        month = int(str(fecha_fin).split("-")[1])
        day = int(str(fecha_fin).split("-")[2])
        fecha_fin = datetime.date(year,month,day)


    retenciones = RetencionDetalle.objects.filter(status=1,tipo_retencion=1).filter(Q(retencion_cabecera__compra_cabecera__proveedor__ruc__icontains = provedor_ruc)|Q(retencion_cabecera__compra_cabecera__proveedor__nombre__icontains = provedor_nombre)).filter(retencion_cabecera__fecha_emision__range=(fecha_inicio, fecha_fin)).order_by('codigo_retencion_fte__codigo','retencion_cabecera__fecha_emision')

    reten_tmp = RetencionDetalle.objects.filter(status=1,tipo_retencion=1).filter(Q(retencion_cabecera__compra_cabecera__proveedor__ruc__icontains = provedor_ruc)|Q(retencion_cabecera__compra_cabecera__proveedor__nombre__icontains = provedor_nombre)).filter(retencion_cabecera__fecha_emision__range=(fecha_inicio, fecha_fin)).order_by('codigo_retencion_fte__codigo').distinct('codigo_retencion_fte__codigo')

    codigos = {}
    finales = {}
    lista = []
    base_total = 0.0
    retencion_total = 0.0
    verificacion_total = 0.0
    diferencia_total = 0.0

    total_final_base = 0.0
    total_final_retencion= 0.0
    total_final_verificacion =0.0
    total_final_diferencia =0.0

    for r in reten_tmp:
        codigos = {"tipo":3,"codigo":r.codigo_retencion_fte.codigo,"descripcion":r.codigo_retencion_fte.descripcion}
        lista.append(codigos)


        for ret in retenciones:

            if r.codigo_retencion_fte.codigo == ret.codigo_retencion_fte.codigo:
                verificacion = 0.0
                verificacion = (ret.valor * ret.porcentaje / 100)
                diferencia = 0.0
                diferencia = (ret.total - verificacion)

                finales = {"tipo":1,
                           "fecha_emision":ret.retencion_cabecera.fecha_emision,
                           "codigo_retencion":ret.codigo_retencion_fte.codigo,
                           "descripcion_retencion":ret.codigo_retencion_fte.descripcion,
                           "numero_comprobante":ret.retencion_cabecera.numero_comprobante,
                           "concepto":ret.retencion_cabecera.compra_cabecera.concepto,
                           "nombre":ret.retencion_cabecera.compra_cabecera.proveedor.nombre,
                           "numero_retencion":ret.retencion_cabecera.numero_retencion,
                           "valor":ret.valor,
                           "total":ret.total,
                           "verificacion": verificacion,
                           "diferencia": diferencia
                           }
                lista.append(finales)
                base_total+=ret.valor
                retencion_total+= ret.total
                verificacion_total+=verificacion
                diferencia_total+=diferencia
                total_final_base+=ret.valor
                total_final_retencion+=ret.total
                total_final_verificacion+=verificacion
                total_final_diferencia+=diferencia



        totales = {"tipo":2,
                   "fecha_emision":"",
                   "codigo_retencion":"",
                   "descripcion_retencion":"",
                   "numero_comprobante":"",
                   "concepto":"",
                   "nombre":"",
                   "numero_retencion":"Totales: ",
                   "valor":base_total,
                   "total":retencion_total,
                   "verificacion": verificacion_total,
                   "diferencia": diferencia_total
                   }

        lista.append(totales)
        base_total = 0.0
        retencion_total = 0.0


    total_registros= len(retenciones)


    fecha_actual = datetime.datetime.now()

    return generar_retencion_fuente_pdf('reportes_retenciones/reporte_retencion_fuente_pdf.html',
                              {'pagesize': 'A4',
                               "retenciones": lista,
                               "request": request,
                               "fecha_actual":fecha_actual,
                               "fecha_inicio":fecha_inicio,
                               "fecha_fin":fecha_fin,
                               "proveedor_ruc":provedor_tmp,
                               "total_registros":total_registros,
                               "total_final_base":total_final_base,
                               "total_final_retencion":total_final_retencion,
                               "total_final_verificacion":total_final_verificacion,
                               "total_final_diferencia":total_final_diferencia
                               })





